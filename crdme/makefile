include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

CRDME_LIB_DIR=${CRDME_DIR}/crdme/${PETSC_ARCH}
CRDME_INCL   =-I${CRDME_DIR}/crdme/include
CPP_INCL     =-I${CRDME_DIR}/crdme/include/cpp ${CRDME_INCL}
CFLAGS       +=${CRDME_INCL} -Wl,-rpath,${CRDME_LIB_DIR}
CXXFLAGS     +=${CPP_INCL} -fvisibility=default -Wl,-rpath,${CRDME_LIB_DIR} -Wl,-rpath,${CRDME_LIB_DIR}/lib -O3 -g -Wall -ffast-math -funsafe-math-optimizations -msse4.2
PCC_FLAGS+=-fvisibility=default
OS = $(shell uname)
ifeq (${OS},Darwin)
	SHARED_LIBRARY_EXTENSION = dylib
	SHARED_LIBRARY_FLAG      = -dynamiclib
else ifeq (${OS},Linux)
	SHARED_LIBRARY_EXTENSION = so
	SHARED_LIBRARY_FLAG      = -shared
endif


SOURCES = src/reactionsystem/reactionsystem.c $(wildcard src/geometry/*.c) \
																							$(wildcard src/geometry/impls/*.c) \
																							$(wildcard src/ssa/*.c) \
																							$(wildcard src/ssa/impls/*.c) \
																							$(wildcard src/util/*.c) \
																							$(wildcard src/util/heap/*.c) \
																							$(wildcard src/statistics/*.c)
CPP_SOURCES = src/cpp/overlap.cpp

OBJS   = $(SOURCES:.c=.o)
CPP_OBJS   = $(CPP_SOURCES:.cpp=.o)

.PHONY: crdme

check:	
				-@echo ${SHARED_LIBRARY_FLAG}
				-@echo ${PETSC_LIB}
				-@echo ${PCC_FLAGS}

crdme: ${PETSC_ARCH}/lib/liboverlap.${SHARED_LIBRARY_EXTENSION} ${PETSC_ARCH}/libCRDME.${SHARED_LIBRARY_EXTENSION}

${PETSC_ARCH}/lib/liboverlap.${SHARED_LIBRARY_EXTENSION}: ${CPP_OBJS} | ${PETSC_ARCH}
				 ${CXX} ${SHARED_LIBRARY_FLAG} ${PETSC_LIB} -o $@ $^

${PETSC_ARCH}/libCRDME.${SHARED_LIBRARY_EXTENSION}: ${OBJS} | ${PETSC_ARCH}
				 ${CC} ${SHARED_LIBRARY_FLAG} ${PETSC_LIB} -L${CRDME_LIB_DIR}/lib -loverlap -o $@ $^

${PETSC_ARCH}:
			mkdir $@
			mkdir $@/lib

crdmecleanall:
						${RM} ${PETSC_ARCH}/libCRDME.${SHARED_LIBRARY_EXTENSION} ${PETSC_ARCH}/lib/liboverlap.${SHARED_LIBRARY_EXTENSION} ${OBJS} ${CPP_OBJS} 

crdmecleanobj:
						${RM} ${OBJS} ${CPP_OBJS}

crdmecleanlib:
						${RM} ${PETSC_ARCH}/libCRDME.${SHARED_LIBRARY_EXTENSION}
