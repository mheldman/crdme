#include <petsc.h>

PetscErrorCode BinarySearchReal(PetscReal *arr, PetscInt l, PetscInt h, PetscReal r, PetscInt *out) {
  PetscErrorCode ierr;
  PetscInt       mid;

  PetscFunctionBegin;

  if(r < arr[l]) {
    *out=l;
    PetscFunctionReturn(0);
  }
  if(h - l >= 1) {
    mid = l + (h - l) / 2;
    if(arr[mid] < r && arr[mid+1] > r) {
      *out=mid+1;
      PetscFunctionReturn(0);
    }
    
    if(r > arr[mid]) {
      ierr = BinarySearchReal(arr,mid+1,h,r,out);CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }
    
    ierr = BinarySearchReal(arr,l,mid,r,out);CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }
  *out=-1;
  PetscFunctionReturn(0);
}

PetscErrorCode WriteBinaryDataToFile(const char *filename,PetscInt suffix,void *data,PetscInt n,PetscDataType type) {
  PetscErrorCode ierr;
  int            fd;
  char           f[PETSC_MAX_PATH_LEN];

  PetscFunctionBegin;

  if(suffix >= 0) {
    ierr = PetscSNPrintf(f,sizeof(f),"%s%d.dat",filename,suffix);CHKERRQ(ierr); 
  } else {
    ierr = PetscSNPrintf(f,sizeof(f),"%s.dat",filename,suffix);CHKERRQ(ierr); 
  }

  ierr = PetscBinaryOpen(f,FILE_MODE_WRITE,&fd);CHKERRQ(ierr);
  ierr = PetscBinaryWrite(fd,&n,1,PETSC_INT);CHKERRQ(ierr);
  ierr = PetscBinaryWrite(fd,data,n,type);CHKERRQ(ierr);
  ierr = PetscBinaryClose(fd);

  PetscFunctionReturn(0);

}

PetscErrorCode PlotDataArray(const char *filename,const char *title,const char* xname,const char *yname,PetscInt arrLen,PetscReal *xdata,PetscReal xlim[2],PetscReal *ydata,PetscReal ylim[2]) {
  PetscErrorCode ierr;
  PetscDraw      draw;
  PetscDrawLG    lg;
  PetscDrawAxis  axis;

  PetscFunctionBegin;

  ierr = PetscDrawCreate(PETSC_COMM_SELF,NULL,title,PETSC_DECIDE,PETSC_DECIDE,PETSC_DECIDE,PETSC_DECIDE,&draw);CHKERRQ(ierr);
  ierr = PetscDrawSetSave(draw,filename);CHKERRQ(ierr);
  ierr = PetscDrawSetType(draw,PETSC_DRAW_IMAGE);CHKERRQ(ierr);
  ierr = PetscDrawSetFromOptions(draw);CHKERRQ(ierr);
  ierr = PetscDrawLGCreate(draw,1,&lg);CHKERRQ(ierr);
  ierr = PetscDrawLGGetAxis(lg,&axis);CHKERRQ(ierr);
  ierr = PetscDrawAxisSetLabels(axis,PETSC_NULL,xname,yname);CHKERRQ(ierr);
  ierr = PetscDrawAxisSetLimits(axis,xlim[0],xlim[1],ylim[0],ylim[1]);CHKERRQ(ierr);
  ierr = PetscDrawAxisSetHoldLimits(axis,PETSC_TRUE);CHKERRQ(ierr);
  ierr = PetscDrawLGAddPoints(lg,arrLen,&xdata,&ydata);CHKERRQ(ierr);
  ierr = PetscDrawLGDraw(lg);CHKERRQ(ierr);
  ierr = PetscDrawLGSave(lg);CHKERRQ(ierr);
  ierr = PetscDrawLGDestroy(&lg);CHKERRQ(ierr);
  ierr = PetscDrawDestroy(&draw);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
