#include <petsc.h>
#include <heap.h>

PETSC_STATIC_INLINE void MoveNode(PriorityQueue queue, PetscInt loc, PetscInt target) {
  queue->index[queue->nextSubvolume[loc]]=target;
  queue->voxelNRT[target]                =queue->voxelNRT[loc];
  queue->nextSubvolume[target]           =queue->nextSubvolume[loc];
}

PETSC_STATIC_INLINE void _p_PriorityQueuePercolateDown(PriorityQueue queue, PetscInt i) {
  PetscInt  N      =queue->N;
  PetscInt  heapLoc=queue->index[i];
  PetscReal key    =queue->voxelNRT[heapLoc];  
  PetscInt  child;

  while((child = (heapLoc<<HEAP_ARITY)+1)<N) {
    if(child!=N-1 && queue->voxelNRT[child+1]<queue->voxelNRT[child]) child++;

    if(queue->voxelNRT[child]<key) {
      MoveNode(queue,child,heapLoc);
    } else {
      break;
    }
    heapLoc=child;
  }

  queue->voxelNRT[heapLoc]     =key;
  queue->nextSubvolume[heapLoc]=i;
  queue->index[i]              =heapLoc;
}

PETSC_STATIC_INLINE void _p_PriorityQueuePercolateUp(PriorityQueue queue, PetscInt i) {
  PetscInt  heapLoc=queue->index[i];
  PetscReal key    =queue->voxelNRT[heapLoc];  
  PetscInt  parent=1;

  while(parent > 0) {
    parent = ((heapLoc-1)>>HEAP_ARITY);
    if(queue->voxelNRT[parent]>key) {
      MoveNode(queue,parent,heapLoc);
    } else {
      break;
    }
    heapLoc=parent;
  }

  queue->voxelNRT[heapLoc]     =key;
  queue->nextSubvolume[heapLoc]=i;
  queue->index[i]              =heapLoc;
}

PetscErrorCode PriorityQueueUpdate(PriorityQueue queue,PetscInt i,PetscReal val) {
  PetscInt       idx=queue->index[i];
  PetscInt       parent=((idx-1)>>HEAP_ARITY);

  PetscFunctionBegin;

  queue->voxelNRT[idx]=val;
  if(idx>0 && val<queue->voxelNRT[parent]) {
    _p_PriorityQueuePercolateUp(queue,i);
  } else {
    _p_PriorityQueuePercolateDown(queue,i);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode PriorityQueueInitialize(PriorityQueue queue) {
  PetscInt       N = queue->N;

  PetscFunctionBegin;

  for (PetscInt i=(N-1)>>HEAP_ARITY; i>=0; i--) {
    _p_PriorityQueuePercolateDown(queue,i);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode PriorityQueueTestParity(PriorityQueue queue) {
  PetscInt i,N=queue->N;
  for(i=0; i<(N-1)/2; i++) {
    if(queue->voxelNRT[i]>queue->voxelNRT[2*i+1] || queue->voxelNRT[i]>queue->voxelNRT[2*i+2]) {
      return 1; 
    }
  }
  return 0;
}

PetscErrorCode PriorityQueueView(PriorityQueue queue) {	
  PetscInt i,N=queue->N;
  printf("\n");
  for(i=0; i<(N-1)/2; i++) {
    printf("%.10f %.10f %.10f\n",queue->voxelNRT[i],queue->voxelNRT[2*i+1],queue->voxelNRT[2*i+2]);
  }
  return 0;
}