#include <petsc.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <rng.h>
#define N_MC_SAMPLES 10000000

PetscErrorCode NbhdIteratorTestFn(CRDMEGeometry geom,CRDMEGeomGloIdx i,CRDMEGeomGloIdx j,PetscScalar radius,void* ctx,PetscBool *cap, PetscBool *exit) {
  
  PetscErrorCode ierr;
  CRDMEGeomPoint x={0,0,0},xb={0,0,0};
  CRDMEGeomVoxel *v=&geom->work_voxel[0];
  PetscReal      dist,area,varea;
  PetscBool      capped=PETSC_FALSE;

  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetCoordinates(geom,j,xb);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Node %d, coordinates (%1.5f, %1.5f, %1.5f)\n",j,xb[0],xb[1],xb[2]);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Distance %1.14f from base\n",PetscSqrtReal(dist));CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,xb,&dist);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Distance %1.14f from base\n",PetscSqrtReal(dist));CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,v);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel coords:\n");
  for(PetscInt l=0;l<v->length;l++) {
    if(geom->dim == 2) {
      v->nodes[l][2]=0.0;
    }
    ierr = PetscPrintf(PETSC_COMM_WORLD,"    (%1.7f, %1.7f, %1.7f)\n",v->nodes[l][0],v->nodes[l][1],v->nodes[l][2]);
  }
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*v,&dist);CHKERRQ(ierr);
  ierr=PetscPrintf(PETSC_COMM_WORLD,"  Minimum distance             %1.16f\n",PetscSqrtReal(dist));CHKERRQ(ierr);

  if(dist >= PetscSqr(radius) - 1e-8) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  Capped, intersection area should be 0, dimension %d\n",geom->dim);CHKERRQ(ierr);
    *cap=PETSC_TRUE;
    capped=PETSC_TRUE;
  }
  ierr = CRDMEGeometryVoxelGetArea(geom,*v,&varea);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel area (from voxel)           %1.16f\n",varea);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&varea);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel area (from index)           %1.16f\n",varea);CHKERRQ(ierr);

  ierr = CRDMEGeometryVoxelSphereIntersectionArea(geom,x,radius,*v,&area);CHKERRQ(ierr);
  area = (area < 0.0 ? 0.0 : area);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Intersection area            %1.16f\n",area);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  As fraction of total         %1.16f\n",area / varea);CHKERRQ(ierr);

  PetscReal n_in = 0;
  for(PetscInt jj=0; jj<N_MC_SAMPLES; jj++) {
    ierr = CRDMEGeometrySampleFromVoxel(geom,j,xb);CHKERRQ(ierr);
    ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,xb,&dist);CHKERRQ(ierr);
    if(dist < PetscSqr(radius)) {
      n_in+=1.0;
    }        
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Empirical intersection area: %1.16f\n",n_in / N_MC_SAMPLES);CHKERRQ(ierr);
  if(PetscAbs(n_in) > 1e-12) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  Relative difference: %1.16f\n",(varea / area)*PetscAbs(n_in / N_MC_SAMPLES - area / varea));CHKERRQ(ierr);
  }
  if(n_in > 1e-12 && capped) {
    PetscPrintf(PETSC_COMM_WORLD,"number in %d, but capped dist %f\n",n_in,dist);
    return 1;
  } else if(n_in < 1e-12 && !capped) {
    PetscPrintf(PETSC_COMM_WORLD,"number in %d, but not capped\n",n_in);
    PetscPrintf(PETSC_COMM_WORLD,"geometry dimesion %d\n",geom->dim);
    return 1;
  }
  if( (n_in > 1e-12 && PetscAbs(n_in / N_MC_SAMPLES - area / varea) > 1e-2) 
   || (n_in < 1e-12 && area / varea > 1e-12)) {
    PetscPrintf(PETSC_COMM_WORLD,"number in %d, percentage %f, area fraction %f\n",n_in,n_in / N_MC_SAMPLES,area / varea);
    return 1;
  }
  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  CRDMEGeometry   geom;
  PetscInt        m0 = PetscPowInt(2,4)-1;
  PetscInt        dim=3,m[3]={m0,m0,m0};
  PetscBool       periodicity[3]={0,0,0};
  PetscReal       limits[6]={0.0,1.0,0.0,1.0,0.0,1.0};
  CRDMEGeomGloIdx i=2;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Create a geometry systenm and do some operations");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create geometry system, dimension %d.\n",dim);CHKERRQ(ierr);
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create geometry system successful.\n\n");CHKERRQ(ierr);

  // ierr = PetscPrintf(PETSC_COMM_WORLD,"Set box geometry type.\n");CHKERRQ(ierr);
  // ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"Set geometry type successful.\n\n");CHKERRQ(ierr);

  // ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);

  // ierr = PetscPrintf(PETSC_COMM_WORLD,"Set box domain.\n");CHKERRQ(ierr);
  // ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"Set box domain successful.\n\n");CHKERRQ(ierr);
  DM dm;
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
  PetscInt faces[3]={m0,m0,m0};
  PetscReal upper[3]={20,20,20};
  ierr = DMPlexCreateBoxMesh(PETSC_COMM_SELF,2,PETSC_TRUE,faces,PETSC_NULL,upper,PETSC_NULL,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONCreateFromDMPlex(geom,dm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Set box geometry type.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Set geometry type successful.\n\n");CHKERRQ(ierr);

  //ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"GCON set up.\n");CHKERRQ(ierr);
  //ierr = CRDMEGeomGCONSetFromFolder(geom,"/projectnb2/fpkmc3d/mesh-crdme-test/mesh_data_binary/circle/mesh1");CHKERRQ(ierr);
  // ierr = CRDMEGeomGCONGmshCreateFromFile(geom,"/home/heldmanm/gmsh/2d/polygon/polymesh4.msh");CHKERRQ(ierr);

  i=0;
  while(1) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Node neighborhood test, node %d\n",i);CHKERRQ(ierr);
    //i = 2;
    //i = KISS % geom->N;
    ierr = CRDMEGeometryIterateVoxelNbhd(geom,i%geom->N,2,NbhdIteratorTestFn,NULL);CHKERRQ(ierr);
    i+=1;
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Destroy geometry system.\n");CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Destroy geometry system successful.\n\n");CHKERRQ(ierr);

  PetscFunctionReturn(0);
  
}

