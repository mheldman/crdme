#include <petsc.h>
#include <crdmegeometry.h>
#include <crdmegeomgcon.h>

#define N_MC_SAMPLES 1000000

PetscErrorCode NbhdIteratorTestFn(CRDMEGeometry geom,CRDMEGeomGloIdx i,CRDMEGeomGloIdx j,PetscScalar radius,void* ctx,PetscBool* cap,PetscBool* exit) {
  
  PetscErrorCode ierr;
  CRDMEGeomPoint x={0,0,0},xb={0,0,0};
  CRDMEGeomVoxel *v=&geom->work_voxel[0];
  PetscReal      dist,area,varea;
    
  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetCoordinates(geom,j,xb);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Node %d, coordinates (%1.7f, %1.7f)\n",j,xb[0],xb[1]);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Distance %1.14f from base\n",PetscSqrtReal(dist));CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,xb,&dist);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Distance %1.14f from base\n",PetscSqrtReal(dist));CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,v);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel coords %d %d\n",v->length,geom->maxNodeDegree);CHKERRQ(ierr);

  for(PetscInt o=0;o<v->length;o++) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"    (%1.7f, %1.7f)\n",v->nodes[o][0],v->nodes[o][1]);CHKERRQ(ierr);
  }
  

  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*v,&dist);CHKERRQ(ierr);
  if(i==j) {
    dist=0.0;
  }
  ierr=PetscPrintf(PETSC_COMM_WORLD,"  Minimum distance             %1.16f\n",PetscSqrtReal(dist));CHKERRQ(ierr);
  if(dist >= PetscSqr(radius)) {
    ierr=PetscPrintf(PETSC_COMM_WORLD,"  Capped, intersection area should be 0\n");CHKERRQ(ierr);
    *cap=PETSC_TRUE;
  }

  ierr = CRDMEGeometryVoxelGetArea(geom,*v,&varea);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel area (from voxel)           %1.16f\n",varea);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&varea);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Voxel area (from index)           %1.16f\n",varea);CHKERRQ(ierr);

  ierr = CRDMEGeometryVoxelSphereIntersectionArea(geom,x,radius,*v,&area);CHKERRQ(ierr);
  area = (area < 0.0 ? 0.0 : area);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Intersection area            %1.16f\n",area);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  As fraction of total         %1.16f\n",area / varea);CHKERRQ(ierr);

  PetscReal n_in = 0;
  for(PetscInt jj=0; jj<N_MC_SAMPLES; jj++) {
    ierr = CRDMEGeometrySampleFromVoxel(geom,j,xb);CHKERRQ(ierr);
    ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,xb,&dist);CHKERRQ(ierr);
    if(dist < PetscSqr(radius)) {
      n_in+=1.0;
    }        
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Empirical intersection area: %1.16f\n",n_in / N_MC_SAMPLES);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  CRDMEGeometry   geom;
  PetscInt        dim=2;
  CRDMEGeomGloIdx i=250;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Create a geometry systenm and do some operations");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create geometry system, dimension %d.\n",dim);CHKERRQ(ierr);
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create geometry system successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Set box geometry type.\n");CHKERRQ(ierr);
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Set geometry type successful.\n\n");CHKERRQ(ierr);

  ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"GCON set up.\n");CHKERRQ(ierr);
  //ierr = CRDMEGeomGCONSetFromFolder(geom,"/projectnb2/fpkmc3d/mesh-crdme-test/mesh_data_binary/circle/mesh1");CHKERRQ(ierr);
  ierr = CRDMEGeomGCONGmshCreateFromFile(geom,"/home/heldmanm/gmsh/2d/square5.msh");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"GCON set up successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Node neighborhood test, node %d\n",i);CHKERRQ(ierr);
  ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,.25,NbhdIteratorTestFn,NULL);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Destroy geometry system.\n");CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Destroy geometry system successful.\n\n");CHKERRQ(ierr);

  PetscFunctionReturn(0);
  
}

