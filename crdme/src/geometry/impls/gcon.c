/*
 * gcon.c
 *
 *  Created on: Jul 14, 2021
 *      Author: maxheldman
 */

#include <petsc.h>
#include <crdmegeometry.h>
#include <crdmegeomgcon.h>
#include <rng.h>

PETSC_STATIC_INLINE PetscReal TetBarycenter(PetscReal v1, PetscReal v2, PetscReal v3, PetscReal v4) {
  return .25*(v1 + v2 + v3 + v4);
}

PETSC_STATIC_INLINE PetscReal TriangleBarycenter(PetscReal v1, PetscReal v2, PetscReal v3) {
  return .333333333333*(v1 + v2 + v3);
}

PETSC_STATIC_INLINE PetscReal EdgeMidpoint(PetscReal v1, PetscReal v2) {
  return .5*(v1 + v2);
}
PetscErrorCode CRDMEGeometryNodeGetCoordinates_GCON(CRDMEGeometry geom,CRDMEGeomGloIdx idx,CRDMEGeomPoint x) {
  CRDMEGeometry_GCON gcon = geom->data;

  PetscFunctionBegin;
  
  for(PetscInt i=0;i<geom->dim;i++) {
    x[i] = gcon->nodes[geom->dim*idx + i];
  }

  PetscFunctionReturn(0);
}

PETSC_INLINE PetscInt CRDMEGeomGCONVoxelNumVertices(CRDMEGeometry geom,CRDMEGeomGloIdx i) {
  return geom->dim == 2 ? 2*(((CRDMEGeometry_GCON)geom->data)->Ap[i+1]-((CRDMEGeometry_GCON)geom->data)->Ap[i])
                        : (((CRDMEGeometry_GCON)geom->data)->Ap[i+1]-((CRDMEGeometry_GCON)geom->data)->Ap[i])
                        + (((CRDMEGeometry_GCON)geom->data)->cellPtr[i+1]-((CRDMEGeometry_GCON)geom->data)->cellPtr[i]);
}

PETSC_INLINE PetscInt CRDMEGeomGCONNodeNumNeighbors(CRDMEGeometry geom,CRDMEGeomGloIdx i) {
  return (((CRDMEGeometry_GCON)geom->data)->Ap[i+1]-((CRDMEGeometry_GCON)geom->data)->Ap[i]);
}

PetscErrorCode CRDMEGeometryNodeGetVoxel_GCON(CRDMEGeometry geom,CRDMEGeomGloIdx idx,CRDMEGeomVoxel *voxel) { /* only 2D */
  CRDMEGeometry_GCON gcon = geom->data;
  PetscInt           cnt = 0;
  PetscInt           jj, j, k;
  PetscInt           *Ap = gcon->Ap;
  PetscInt           *Aj = gcon->Aj;
  PetscReal          *nodes = gcon->nodes;

  PetscFunctionBegin;

  switch(geom->dim) {
    case 2:
    if(gcon->bndry[idx]) {
      voxel->nodes[cnt][0] = nodes[2*idx];
      voxel->nodes[cnt][1] = nodes[2*idx+1];
      cnt++;
      for(jj = Ap[idx]; jj < Ap[idx+1]-1; jj++) {
        j = Aj[jj];
        k = Aj[jj+1];
        voxel->nodes[cnt][0] = EdgeMidpoint(nodes[2*idx],nodes[2*j]);
        voxel->nodes[cnt][1] = EdgeMidpoint(nodes[2*idx+1],nodes[2*j+1]);
        cnt++;
        voxel->nodes[cnt][0] = TriangleBarycenter(nodes[2*idx],nodes[2*j],nodes[2*k]);
        voxel->nodes[cnt][1] = TriangleBarycenter(nodes[2*idx+1],nodes[2*j+1],nodes[2*k+1]);
        cnt++;
      }
      jj = Ap[idx+1]-1;
      j =  Aj[jj];
      voxel->nodes[cnt][0] = EdgeMidpoint(nodes[2*idx],nodes[2*j]);
      voxel->nodes[cnt][1] = EdgeMidpoint(nodes[2*idx+1],nodes[2*j+1]);
      cnt++;
      voxel->nodes[cnt][0] = nodes[2*idx];
      voxel->nodes[cnt][1] = nodes[2*idx+1];
    } else {
      for(jj = Ap[idx]; jj < Ap[idx+1]-1; jj++) {
        j = Aj[jj];
        k = Aj[jj+1];
        voxel->nodes[cnt][0] = EdgeMidpoint(nodes[2*idx],nodes[2*j]);
        voxel->nodes[cnt][1] = EdgeMidpoint(nodes[2*idx+1],nodes[2*j+1]);
        cnt++;
        voxel->nodes[cnt][0] = TriangleBarycenter(nodes[2*idx],nodes[2*j],nodes[2*k]);
        voxel->nodes[cnt][1] = TriangleBarycenter(nodes[2*idx+1],nodes[2*j+1],nodes[2*k+1]);
        cnt++;
      }
      jj = Ap[idx+1]-1;
      j  = Aj[jj];
      k  = Aj[Ap[idx]];
      voxel->nodes[cnt][0]  = EdgeMidpoint(nodes[2*idx],nodes[2*j]);
      voxel->nodes[cnt][1]= EdgeMidpoint(nodes[2*idx+1],nodes[2*j+1]);
      cnt++;
      voxel->nodes[cnt][0]  = TriangleBarycenter(nodes[2*idx],nodes[2*j],nodes[2*k]);
      voxel->nodes[cnt][1]= TriangleBarycenter(nodes[2*idx+1],nodes[2*j+1],nodes[2*k+1]);
      cnt++;
      voxel->nodes[cnt][0]  = nodes[2*idx];
      voxel->nodes[cnt][1]= nodes[2*idx+1];
    }
    break;
    case 3: /* simply get all the nodes. */
    cnt = 0;

    for(PetscInt i = gcon->cellPtr[idx]; i < gcon->cellPtr[idx+1]; i++) {

      PetscInt  tetIdx   = gcon->cellIdx[i];
      tetCell_t tet      = gcon->tets[tetIdx];
      PetscInt  indices[3],j,k,l;

      indices[0]=(tet.idx[0]==idx ? tet.idx[3] : tet.idx[0]);
      indices[1]=(tet.idx[1]==idx ? tet.idx[3] : tet.idx[1]);
      indices[2]=(tet.idx[2]==idx ? tet.idx[3] : tet.idx[2]);
      // 3 edges, 3 faces, 1 barycenter: edge -- face -- edge -- face -- edge -- face
      for(PetscInt jj = 0;jj < 3;jj++) {
        PetscInt j = indices[jj];
        voxel->nodes[cnt][0] = EdgeMidpoint(nodes[3*j],nodes[3*idx]); 
        voxel->nodes[cnt][1] = EdgeMidpoint(nodes[3*j+1],nodes[3*idx+1]); 
        voxel->nodes[cnt][2] = EdgeMidpoint(nodes[3*j+2],nodes[3*idx+2]); 
        cnt++;
        if(jj < 2) {
          k = indices[jj+1];
          voxel->nodes[cnt][0] = TriangleBarycenter(nodes[3*j],nodes[3*k],nodes[3*idx]);
          voxel->nodes[cnt][1] = TriangleBarycenter(nodes[3*j+1],nodes[3*k+1],nodes[3*idx+1]);
          voxel->nodes[cnt][2] = TriangleBarycenter(nodes[3*j+2],nodes[3*k+2],nodes[3*idx+2]);
          cnt++;
        }
      }
      j = indices[2];
      k = indices[0];            
      voxel->nodes[cnt][0] = TriangleBarycenter(nodes[3*j],nodes[3*k],nodes[3*idx]);
      voxel->nodes[cnt][1] = TriangleBarycenter(nodes[3*j+1],nodes[3*k+1],nodes[3*idx+1]);
      voxel->nodes[cnt][2] = TriangleBarycenter(nodes[3*j+2],nodes[3*k+2],nodes[3*idx+2]);
      cnt++;
      
      l = indices[1];
      voxel->nodes[cnt][0] = TetBarycenter(nodes[3*j],nodes[3*k],nodes[3*l],nodes[3*idx]);
      voxel->nodes[cnt][1] = TetBarycenter(nodes[3*j+1],nodes[3*k+1],nodes[3*l+1],nodes[3*idx+1]);
      voxel->nodes[cnt][2] = TetBarycenter(nodes[3*j+2],nodes[3*k+2],nodes[3*l+2],nodes[3*idx+2]);
      cnt++;
    } 
    voxel->nodes[cnt][0] = nodes[3*idx];
    voxel->nodes[cnt][1] = nodes[3*idx+1];
    voxel->nodes[cnt][2] = nodes[3*idx+2];
  }

  voxel->length = cnt;
  voxel->i      = idx;

  PetscFunctionReturn(0);
}

/*
PetscErrorCode CRDMEGeometryNodesGetVoxels_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDME j, double *vi, double *vj, int *leni, int *lenj) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  
  if(leni) {
    ierr = CRDMEGeometryNodeGetVoxel_GCON(geom,i,vi,leni);CHKERRQ(ierr);
  } else {
    vi[0] = gcon->nodes[2*i];
    vi[1] = gcon->nodes[2*i+1];
  }
  if(lenj) {
    ierr = CRDMEGeometryNodeGetVoxel_GCON(geom,j,vj,lenj);CHKERRQ(ierr);
  } else {
    vj[0] = gcon->nodes[2*j];
    vj[1] = gcon->nodes[2*j+1];
  }

  PetscFunctionReturn(0);
}
*/

PetscErrorCode CRDMEGeometryNodeGetNumNeighbors_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn) {
  CRDMEGeometry_GCON gcon = geom->data;

  PetscFunctionBegin;

  *nn = gcon->Ap[i+1] - gcon->Ap[i];
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetNeighbors_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, PetscInt *indices) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;

  *nn = gcon->Ap[i+1] - gcon->Ap[i];
  ierr = PetscArraycpy(indices,&gcon->Aj[ gcon->Ap[i] ],*nn);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetEdgeWeights_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, PetscInt *indices, PetscReal *data) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  
  *nn    = gcon->Ap[i+1] - gcon->Ap[i];
  ierr = PetscArraycpy(indices,&gcon->Aj[ gcon->Ap[i] ],*nn);CHKERRQ(ierr);
  ierr = PetscArraycpy(data,&gcon->Ax[ gcon->Ap[i] ],*nn);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryVoxelGetArea_GCON(CRDMEGeometry geom, CRDMEGeomVoxel voxel, PetscReal *area) {
  CRDMEGeometry_GCON gcon = geom->data;

  PetscFunctionBegin;
  
  *area = gcon->mass[voxel.i];
  
  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeometryNodesGetDistanceSqr_GCON(CRDMEGeometry geom,CRDMEGeomGloIdx i,CRDMEGeomGloIdx j,PetscReal *dist) {
  CRDMEGeometry_GCON gcon = geom->data;

  PetscFunctionBegin;
  *dist = 0.0;
  for(PetscInt d=0;d<geom->dim;d++) {
    *dist+=PetscSqr(gcon->nodes[geom->dim*i+d] - gcon->nodes[geom->dim*j+d]);
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryPointsGetDistanceSqr_GCON(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomPoint y, PetscReal *dist) {  

  PetscFunctionBegin;

  *dist=0.0;
  for(PetscInt d=0;d<geom->dim;d++) {
    *dist+=PetscSqr(x[d] - y[d]);
  }
  PetscFunctionReturn(0);
}


PETSC_STATIC_INLINE PetscReal triangle_area(PetscReal *tri) {
    return PetscAbs( tri[0]*(tri[3] - tri[5]) + tri[2]*(tri[5] - tri[1]) + tri[4]*(tri[1] - tri[3]) );
}

PetscErrorCode CRDMEGeometrySampleFromVoxel_GCON_2d(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscReal          r,area,sum=0.0,s,t;
  PetscInt           j,k; 
  PetscReal          tri[6];

  PetscFunctionBegin;
  
  area = gcon->mass[i];
  r = 6*UNI*area;
  tri[0]=gcon->nodes[2*i];
  tri[1]=gcon->nodes[2*i+1];
  for(PetscInt jj = gcon->Ap[i]; jj < gcon->Ap[i+1]-1; jj++) {
    j=gcon->Aj[jj];
    k=gcon->Aj[jj+1];

    tri[2] = gcon->nodes[2*j];
    tri[3] = gcon->nodes[2*j+1];
    tri[4] = gcon->nodes[2*k];
    tri[5] = gcon->nodes[2*k+1];

    sum += PetscSqr(geom->hmin);//triangle_area(tri);
    if(sum > r) {
      r = UNI;
      s = UNI;
      if(r + s > 1.0) {
        r = 1.0 - r;
        s = 1.0 - s;
      } 
      t = 1.0 - r - s;
      s *= .5;
      t *= .3333333333333;
      if(KISS % 2) {
        x[0] = (r+s+t)*tri[0] + (s+t)*tri[2] + t*tri[4];
        x[1] = (r+s+t)*tri[1] + (s+t)*tri[3] + t*tri[5];
      } else {
        x[0] = (r+s+t)*tri[0] + (s+t)*tri[4] + t*tri[2];
        x[1] = (r+s+t)*tri[1] + (s+t)*tri[5] + t*tri[3];
      }
      PetscFunctionReturn(0);
    }
  }
  j=gcon->Aj[gcon->Ap[i+1]-1];
  k=gcon->Aj[gcon->Ap[i]];
  
  tri[2] = gcon->nodes[2*j];
  tri[3] = gcon->nodes[2*j+1];
  tri[4] = gcon->nodes[2*k];
  tri[5] = gcon->nodes[2*k+1];

  r = UNI;
  s = UNI;
  if(r + s > 1.0) {
    r = 1.0 - r;
    s = 1.0 - s;
  } 
  t = 1.0 - r - s;
  s *= .5;
  t *= .3333333333333;

  if(KISS % 2) {
    x[0] = (r+s+t)*tri[0] + (s+t)*tri[2] + t*tri[4];
    x[1] = (r+s+t)*tri[1] + (s+t)*tri[3] + t*tri[5];
  } else {
    x[0] = (r+s+t)*tri[0] + (s+t)*tri[4] + t*tri[2];
    x[1] = (r+s+t)*tri[1] + (s+t)*tri[5] + t*tri[3];
  }
  
  PetscFunctionReturn(0);
}


PetscReal tet_orientation(CRDMEGeomPoint tet[4]) {
    CRDMEGeomPoint tri[3],cross;

    for(PetscInt i = 0; i < 3; i++) {
      for(PetscInt j = 0; j < 3; j++) {
        tri[i][j] = tet[i+1][j] - tet[0][j];
      }
    }

    cross[0] = tri[1][1]*tri[2][2] - tri[1][2]*tri[2][1];
    cross[1] = tri[1][2]*tri[2][0] - tri[1][0]*tri[2][2];
    cross[2] = tri[1][0]*tri[2][1] - tri[1][1]*tri[2][0];

  return cross[0]*tri[0][0] + cross[1]*tri[0][1] + cross[2]*tri[0][2];
}

PETSC_STATIC_INLINE PetscReal tet_volume(PetscReal *tet) {
    return PetscAbs( tet_orientation(tet) ) / 6;
}

void SampleBarycentricCoordinatesSimplex_3d(CRDMEGeomPoint w) {
  PetscReal tmp;
  
  w[0] = UNI;
  w[1] = UNI;
  if(w[0] + w[1] > 1.0) {
    w[0] = 1.0 - w[0];
    w[1] = 1.0 - w[1];
  } 
  w[2] = UNI;
  if(w[1] + w[2] > 1.0) {
    tmp = w[2];
    w[2] = 1.0 - w[0] - w[1];
    w[1] = 1.0 - tmp;
  } else if(w[0] + w[1] + w[2] > 1.0) {
    tmp = w[2];
    w[2] = w[0] + w[1] + w[2] - 1.0;
    w[0] = 1.0 - w[1] - tmp;
  }
}

PetscErrorCode CRDMEGeometrySampleFromVoxel_GCON_3d(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  CRDMEGeometry_GCON gcon = geom->data;
  CRDMEGeomPoint     w;
  PetscReal          r,area,sum=0.0,*p0,*p1,*p2,*p3,w3;
  PetscInt           j,k,l,r2; 
  tetCell_t          tet;

  PetscFunctionBegin;
  
  area = gcon->mass[i];
  r = 4*UNI*area;
  for(PetscInt jj = gcon->cellPtr[i]; jj < gcon->cellPtr[i+1]; jj++) {
    tet = gcon->tets[gcon->cellIdx[jj]];
    j=(tet.idx[0]==i ? tet.idx[3] : tet.idx[0]);
    k=(tet.idx[1]==i ? tet.idx[3] : tet.idx[1]);
    l=(tet.idx[2]==i ? tet.idx[3] : tet.idx[2]);
    sum += tet.vol;
    if(sum > r) {
      SampleBarycentricCoordinatesSimplex_3d(w);
      w3    = 1.0 - w[0] - w[1] - w[2];
      w[2] /= 2;
      w[1] /= 3;
      w[0] /= 4;
      r2 = KISS % 6;
      p0 = &gcon->nodes[3*i];
      switch(r2) {
        case 0:
        p1 = &gcon->nodes[3*j];
        p2 = &gcon->nodes[3*k];
        p3 = &gcon->nodes[3*l];
        break;
        case 1:
        p1 = &gcon->nodes[3*j];
        p2 = &gcon->nodes[3*l];
        p3 = &gcon->nodes[3*k];
        break;
        case 2:
        p1 = &gcon->nodes[3*k];
        p2 = &gcon->nodes[3*j];
        p3 = &gcon->nodes[3*l];
        break;
        case 3:
        p1 = &gcon->nodes[3*k];
        p2 = &gcon->nodes[3*l];
        p3 = &gcon->nodes[3*j];
        break;
        case 4:
        p1 = &gcon->nodes[3*l];
        p2 = &gcon->nodes[3*j];
        p3 = &gcon->nodes[3*k];
        break;
        case 5:
        p1 = &gcon->nodes[3*l];
        p2 = &gcon->nodes[3*k];
        p3 = &gcon->nodes[3*j];
        break;
      }
      x[0] = (w[0] + w[1] + w[2] + w3)*p0[0]
                + (w[0] + w[1] + w[2])*p1[0]
                + (w[0] + w[1])*p2[0]
                + w[0]*p3[0];
      x[1] = (w[0] + w[1] + w[2] + w3)*p0[1]
                + (w[0] + w[1] + w[2])*p1[1]
                + (w[0] + w[1])*p2[1]
                + w[0]*p3[1];
      x[2] = (w[0] + w[1] + w[2] + w3)*p0[2]
                + (w[0] + w[1] + w[2])*p1[2]
                + (w[0] + w[1])*p2[2]
                + w[0]*p3[2];
      PetscFunctionReturn(0);
    }
  }
  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Error sampling from voxel; unreachable code\n");
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONSetBoundary_2d(CRDMEGeometry);

PetscErrorCode CRDMEGeomGCONSetBoundary(CRDMEGeometry geom) {
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  switch(geom->dim) {
    case(2):
      ierr = CRDMEGeomGCONSetBoundary_2d(geom);CHKERRQ(ierr);
      break;
    default:
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Dimension %d GCON not yet supported\n",geom->dim);
  }

  PetscFunctionReturn(0);
}

static PetscInt Count3Cycles(PetscInt i,PetscInt e,PetscInt *Ap,PetscInt *Aj) {
  PetscInt j=Aj[e],k,l,kk,ll;
  PetscInt count=0;
  for(kk=Ap[i];kk<Ap[i+1];kk++) {
    k=Aj[kk];
    if(k!=j) {
      for(ll=Ap[j];ll<Ap[j+1];ll++) {
        l=Aj[ll];
        if(l==k) {
          count++;
        }
      }
    }
  }
  return count;
}

static PetscInt GetNext3Cycle(PetscInt i,PetscInt j,PetscInt prev,PetscInt *Ap,PetscInt *Aj) {
  PetscInt k,l;
  for(PetscInt kk=Ap[i];kk<Ap[i+1];kk++) {
    k=Aj[kk];
    if(k!=j && k!=prev) {
      for(PetscInt ll=Ap[j];ll<Ap[j+1];ll++) {
        l=Aj[ll];
        if(l==k) {
          return k;
        }
      }
    }
  }
  return -1;
}

PetscErrorCode CRDMEGeomGCON2dSortEdges(CRDMEGeometry geom) {

  CRDMEGeometry_GCON gcon = geom->data;
  PetscReal *nodes=gcon->nodes;
  PetscInt  *Ap   =gcon->Ap;
  PetscInt  *Aj   =gcon->Aj;
  PetscReal *Ax   =gcon->Ax;

}


PetscErrorCode CRDMEGeomGCONSetBoundary_2d(CRDMEGeometry geom) {

  CRDMEGeometry_GCON gcon = geom->data;
  PetscInt  startj=0,prevj,idx_min;
  PetscInt  j=0,k,jj,cnt,i;
  PetscInt  cell_count = 0, cell_found = 0;
  PetscInt  det,len;
  PetscReal w=0.0;
  PetscReal *nodes=gcon->nodes;
  PetscInt  *Ap   =gcon->Ap;
  PetscInt  *Aj   =gcon->Aj;
  PetscReal *Ax   =gcon->Ax;

  PetscFunctionBegin;

  for(i = 0; i < geom->N; i++) {
    len = Ap[i + 1] - Ap[i];
    PetscInt  order[len];
    PetscReal weights[len];
    if(!gcon->boundarySetup) {
      gcon->bndry[i] = PETSC_FALSE;
      for(jj = Ap[i]; jj < Ap[i+1]; jj++) {
        j = Aj[jj];
        w = Ax[jj];
        startj = Aj[jj];
        cell_count = Count3Cycles(i,jj,Ap,Aj);
        if(cell_count < 1) {
          return 1;
        }
        if(cell_count < 2) {
          gcon->bndry[i] = PETSC_TRUE;
          break;
        }
      }
    } else {
      for(jj = Ap[i]; jj < Ap[i+1]; jj++) {
        j = Aj[jj];
        w = Ax[jj];
        startj = Aj[jj];
        if(gcon->bndry[j]) {  
          break;
        }
      }
    }

    idx_min = 0;
    prevj  = -1;
    cell_found = 1;
    cnt = 0;
    while(cell_found || j != startj) {
      cell_found = 0;
      order[cnt]   = j;
      weights[cnt] = w;
      if(nodes[2*j+1] < nodes[2*order[idx_min]+1]) {
        idx_min = cnt;
      } else if(nodes[2*j+1] == nodes[2*order[idx_min]+1]) {
        if(nodes[2*j] > nodes[2*order[idx_min]]) {
          idx_min = cnt;
        }
      }
      cnt++;
      k = GetNext3Cycle(i,j,prevj,Ap,Aj);
      for(jj = Ap[i]; jj < Ap[i+1]; jj++) {
        if(Aj[jj] == k) {
          w = Ax[jj];
          break;
        }
      }
      cell_found = (k >= 0);
      prevj = j;
      j = k;
      if(!cell_found || (j == startj)) {
        break;
      }
    }

    PetscInt a,b,c;
    if(idx_min == 0) {
    	a = order[len-1];
    	b = order[0];
    	c = order[1];
    } else if(idx_min == len-1) {
    	a = order[len-2];
    	b = order[len-1];
    	c = order[0];
    } else {
    	a = order[idx_min-1];
    	b = order[idx_min];
    	c = order[idx_min+1];
    }
    det = (nodes[2*b] - nodes[2*a])*(nodes[2*c+1] - nodes[2*b+1]) < (nodes[2*c] - nodes[2*b])*(nodes[2*b+1]-nodes[2*a+1]);
    if(det) {
    	PetscInt  temp[len];
    	PetscReal temp2[len];
    	for(j = 0; j < len; j++) {
    		temp[j] = order[len - 1 - j];
    		temp2[j] = weights[len - 1 - j];
    	}
    	for(j = 0; j < len; j++) {
    		order[j] = temp[j];
    		weights[j] = temp2[j];
    	}
    }
    for(jj = Ap[i],j=0; jj < Ap[i+1]; jj++,j++) {
    	Aj[jj] = order[j];
    	Ax[jj] = weights[j];
    }
  }

	PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySampleFromVoxel_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  switch(geom->dim) {
    case(2):
      CHKERRQ( CRDMEGeometrySampleFromVoxel_GCON_2d(geom,i,x) );
      break;
    case(3):
      CHKERRQ( CRDMEGeometrySampleFromVoxel_GCON_3d(geom,i,x) );
      break;
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Sample from voxel not implemented for geometry GCON in dimension %d",geom->dim);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONSetLaplacian(CRDMEGeometry geom, const PetscInt *Ap, const PetscInt *Aj, const PetscReal *Ax) {

  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;

  ierr = PetscMalloc1(geom->N,&gcon->bndry);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->N+1,&gcon->Ap);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->NEdges,&gcon->Aj);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->NEdges,&gcon->Ax);CHKERRQ(ierr);
  ierr = PetscArraycpy(gcon->Ap,Ap,geom->N+1);CHKERRQ(ierr);
  ierr = PetscArraycpy(gcon->Aj,Aj,geom->NEdges);CHKERRQ(ierr);
  ierr = PetscArraycpy(gcon->Ax,Ax,geom->NEdges);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONSetMass(CRDMEGeometry geom, const PetscReal *mass) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;

  ierr = PetscMalloc1(geom->N,&gcon->mass);CHKERRQ(ierr);
  ierr = PetscArraycpy(gcon->mass,mass,geom->N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONSetNodes(CRDMEGeometry geom, const PetscReal *nodes) {
  CRDMEGeometry_GCON gcon = geom->data;
  PetscErrorCode     ierr;

  PetscFunctionBegin;

  ierr = PetscMalloc1(geom->dim*geom->N,&gcon->nodes);CHKERRQ(ierr);
  ierr = PetscArraycpy(gcon->nodes,nodes,geom->dim*geom->N);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryDestroy_GCON(CRDMEGeometry *geom) {
  CRDMEGeometry_GCON gcon=(*geom)->data;
  PetscErrorCode     ierr;
  
  PetscFunctionBegin;

  ierr = PetscFree(gcon->Ap);CHKERRQ(ierr);
  ierr = PetscFree(gcon->Aj);CHKERRQ(ierr);
  ierr = PetscFree(gcon->Ax);CHKERRQ(ierr);
  ierr = PetscFree(gcon->nodes);CHKERRQ(ierr);
  ierr = PetscFree(gcon->mass);CHKERRQ(ierr);
  ierr = PetscFree(gcon->bndry);CHKERRQ(ierr);
  ierr = PetscFree(gcon);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySetUp_GCON(CRDMEGeometry geom) {
  CRDMEGeometry_GCON gcon=geom->data;
  PetscInt           j,nn,dim=geom->dim,N=geom->N;
  CRDMEGeomPoint     x;
  PetscReal          dx,maxElem;
  PetscErrorCode     ierr;

  PetscFunctionBegin;

  geom->hmax = 0.0;
  geom->hmin =  1e30;

  for(PetscInt i = 0; i < dim; i++) {
    geom->coord_min[i] =  1e30;
    geom->coord_max[i] = -1e30;
  }
  geom->maxNodeDegree=0;
  maxElem=0;
  for(PetscInt i = 0; i < N; i++) {
    for(PetscInt d=0;d<dim;d++) {
      x[d] = gcon->nodes[dim*i + d];
    }
    for(PetscInt d=0;d<dim;d++) {
      geom->coord_min[d] = PetscMin(x[d],geom->coord_min[d]);
      geom->coord_max[d] = PetscMax(x[d],geom->coord_max[d]);
    }
    for(PetscInt jj = gcon->Ap[i]; jj < gcon->Ap[i+1]; jj++) {
      j = gcon->Aj[jj];
      ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dx);CHKERRQ(ierr);
      geom->hmax = PetscMax(PetscSqrtReal(dx),geom->hmax);
      geom->hmin = PetscMin(PetscSqrtReal(dx),geom->hmin);
    }

    ierr = CRDMEGeometryNodeGetNumNeighbors(geom,i,&nn);CHKERRQ(ierr);
    geom->maxNodeDegree = PetscMax(geom->maxNodeDegree,nn);
    if(dim==3) maxElem = PetscMax(maxElem,gcon->cellPtr[i+1]-gcon->cellPtr[i]);
  }

  if(dim==2) {
    ierr = PetscMalloc1(dim*(geom->maxNodeDegree+1),&geom->work_voxel[0].nodes);CHKERRQ(ierr);
    ierr = PetscMalloc1(dim*(geom->maxNodeDegree+1),&geom->work_voxel[1].nodes);CHKERRQ(ierr);
  } else if(dim==3) {
    ierr = PetscMalloc1(7*maxElem+1,&geom->work_voxel[0].nodes);CHKERRQ(ierr);
    ierr = PetscMalloc1(7*maxElem+1,&geom->work_voxel[1].nodes);CHKERRQ(ierr);
  }

  if(dim==2) {
    ierr = CRDMEGeomGCONSetBoundary(geom);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONCreateFromFolder(CRDMEGeometry geom, const char* folder) {
  FILE               *f;
  PetscInt           N,Nnodes,Nedges;
  PetscInt           *Ap,*Aj;
  PetscReal          *Ax,*mass,*nodes;
  char               fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode     ierr;
  
  PetscFunctionBegin;

  ierr = PetscSNPrintf(fname,sizeof(fname),"%s/Ap",folder);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  fread(&N,sizeof(PetscInt),1,f);
  Nnodes = N-1;
  geom->N = Nnodes;
  ierr = PetscMalloc1(Nnodes+1,&Ap);CHKERRQ(ierr);
  fread(Ap,sizeof(PetscInt),Nnodes+1,f);
  fclose(f);
  ierr = PetscSNPrintf(fname,sizeof(fname),"%s/Aj",folder);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  fread(&Nedges,sizeof(PetscInt),1,f);
  geom->NEdges = Nedges;
  ierr = PetscMalloc1(Nedges,&Aj);CHKERRQ(ierr);
  fread(Aj,sizeof(PetscInt),Nedges,f);
  fclose(f);
  ierr = PetscSNPrintf(fname,sizeof(fname),"%s/Ax",folder);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nedges,&Ax);CHKERRQ(ierr);
  fread(Ax,sizeof(PetscReal),Nedges,f);
  fclose(f);
  ierr = PetscSNPrintf(fname,sizeof(fname),"%s/nodes",folder);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(2*Nnodes,&nodes);CHKERRQ(ierr);
  fread(nodes,sizeof(PetscReal),2*Nnodes,f);
  fclose(f);
  ierr = PetscSNPrintf(fname,sizeof(fname),"%s/mass",folder);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nnodes,&mass);CHKERRQ(ierr);
  fread(mass,sizeof(PetscReal),Nnodes,f);
  fclose(f);
  
  ierr = CRDMEGeomGCONSetLaplacian(geom,Ap,Aj,Ax);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONSetMass(geom,mass);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONSetNodes(geom,nodes);CHKERRQ(ierr);

  ierr = PetscFree(Ap);CHKERRQ(ierr);
  ierr = PetscFree(Aj);CHKERRQ(ierr);
  ierr = PetscFree(Ax);CHKERRQ(ierr);
  ierr = PetscFree(nodes);CHKERRQ(ierr);
  ierr = PetscFree(mass);CHKERRQ(ierr);

  ierr = CRDMEGeometrySetUp(geom);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

void g(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                                             const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                                             const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                                             PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[]) {
PetscInt d;
for (d = 0; d < dim; ++d) g3[d*dim+d] = 1.0;                                
}

PetscErrorCode CRDMEGeomGCONCreateFromDMPlex(CRDMEGeometry geom,DM dm) {
  PetscErrorCode     ierr;
  CRDMEGeometry_GCON gcon = geom->data;
  Vec                coords,mass,invmass;
  PetscFE            fe;
  PetscDS            ds;
  Mat                A;
  SNES               snes;
  DMLabel            label;
  PetscInt           cStart,cEnd,dim,N,id,bd,nn;
  PetscBool          isInterpolated,hasLabel=PETSC_FALSE;
  const PetscReal    *nodes,*mass_array,*Ax;
  const PetscInt     *Aj;

  PetscFunctionBegin;

  ierr = DMPlexIsInterpolated(dm, &isInterpolated);
  if(!isInterpolated) {
    DM dmi;
    ierr = DMPlexInterpolate(dm,&dmi);CHKERRQ(ierr);
    ierr = DMDestroy(&dm);CHKERRQ(ierr);
    dm = dmi;
  }

  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  geom->dim = dim;

  ierr = DMClearFields(dm);CHKERRQ(ierr);
  ierr = DMClearDS(dm);CHKERRQ(ierr);

  ierr = PetscFECreateLagrange(PETSC_COMM_SELF,dim,1,PETSC_TRUE,1,PETSC_DETERMINE,&fe);CHKERRQ(ierr);
  ierr = DMSetField(dm,0,NULL,(PetscObject)fe);CHKERRQ(ierr);
  ierr = DMCreateDS(dm);CHKERRQ(ierr);

  ierr = DMHasLabel(dm,"boundary",&hasLabel);CHKERRQ(ierr);
  if(hasLabel) {
    ierr = DMRemoveLabel(dm,"boundary",PETSC_NULL);CHKERRQ(ierr);
  }
  ierr = DMCreateLabel(dm, "boundary");CHKERRQ(ierr);
  ierr = DMGetLabel(dm, "boundary", &label);CHKERRQ(ierr);
  ierr = DMPlexMarkBoundaryFaces(dm, 1, label);CHKERRQ(ierr);
  ierr = DMAddBoundary(dm, DM_BC_NATURAL, "wall", label, 1, &id, 0, 0, NULL, NULL, NULL, PETSC_NULL, &bd);CHKERRQ(ierr);
  ierr = DMPlexSetSNESLocalFEM(dm,NULL,NULL,NULL);CHKERRQ(ierr);

  ierr = DMCreateMassMatrixLumped(dm,&mass);CHKERRQ(ierr);
  ierr = VecGetSize(mass,&N);CHKERRQ(ierr);
  geom->N = N;

  ierr = VecDuplicate(mass,&invmass);CHKERRQ(ierr);
  ierr = VecCopy(mass,invmass);CHKERRQ(ierr);
  ierr = VecReciprocal(invmass);CHKERRQ(ierr);

  ierr = VecGetArrayRead(mass,&mass_array);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONSetMass(geom,mass_array);CHKERRQ(ierr); 
  ierr = VecRestoreArrayRead(mass,&mass_array);CHKERRQ(ierr);
  
  ierr = DMGetCoordinates(dm,&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coords,&nodes);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONSetNodes(geom,nodes);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coords,&nodes);CHKERRQ(ierr);

  ierr = DMGetDS(dm,&ds);
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(ds, 0, 0, NULL, NULL, NULL, g);

  ierr = SNESCreate(PETSC_COMM_SELF,&snes);CHKERRQ(ierr);
  ierr = SNESSetDM(snes,dm);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes, A, A, NULL, NULL);CHKERRQ(ierr);
  ierr = SNESComputeJacobian(snes, invmass, A, A);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);

  ierr = VecSet(mass,0.0);CHKERRQ(ierr);
  ierr = MatDiagonalSet(A,mass,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatDiagonalScale(A,invmass,PETSC_NULL);CHKERRQ(ierr);
  ierr = MatScale(A,-1.0);CHKERRQ(ierr);

  ierr = DMPlexGetHeightStratum(dm,dim-1,&cStart,&cEnd);CHKERRQ(ierr);
  geom->NEdges=2*(cEnd-cStart);
  ierr = PetscMalloc1(N+1,&gcon->Ap);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->NEdges,&gcon->Aj);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->NEdges,&gcon->Ax);CHKERRQ(ierr);

  gcon->Ap[0] = 0;
  for(PetscInt i=0;i<N;i++) {
    ierr = MatGetRow(A,i,&nn,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    gcon->Ap[i+1] = gcon->Ap[i] + nn - 1; 
    ierr = MatRestoreRow(A,i,&nn,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  }

  if(geom->NEdges!=gcon->Ap[N]) {
    printf("Number of edges %d from adjacency matrix does not match number from DMPlex %d\n",gcon->Ap[N],geom->NEdges);
    return 1;
  }

  for(PetscInt i=0;i<N;i++) {
    ierr = MatGetRow(A,i,&nn,&Aj,&Ax);CHKERRQ(ierr);
    for(PetscInt jj=gcon->Ap[i],e=0;jj<gcon->Ap[i+1];jj++,e++) {
      if(Aj[e] != i) {
        gcon->Aj[jj] = Aj[e];
        if(Ax[e] < -1e-14) {
          gcon->Ax[jj] = 0.0;
        } else {
          gcon->Ax[jj] = PetscMax(Ax[e],0.0);
        }
        //gcon->Ax[jj] = PetscMax(Ax[e],0.0);
      } else {
        e++; /* skip self edge */
        gcon->Aj[jj] = Aj[e];
        if(Ax[e] < -1e-14) {
          gcon->Ax[jj] = 0.0;
        } else {
          gcon->Ax[jj] = PetscMax(Ax[e],0.0);
        }
        //gcon->Ax[jj] = PetscMax(Ax[e],0.0);
      }
    }
    ierr = MatRestoreRow(A,i,&nn,&Aj,&Ax);CHKERRQ(ierr);
  }
  ierr = MatDestroy(&A);CHKERRQ(ierr);

  if(dim == 3) {
    PetscSection  section;
    PetscInt      offset,count;
    PetscInt      numindices,*indices;
    PetscReal     tet[12];

    ierr = PetscMalloc1(N+1,&gcon->cellPtr);CHKERRQ(ierr);
    ierr = PetscArrayzero(gcon->cellPtr,N+1);CHKERRQ(ierr);

    ierr = DMGetLocalSection(dm,&section);CHKERRQ(ierr);
    ierr = DMPlexGetHeightStratum(dm,0,&cStart,&cEnd);CHKERRQ(ierr);
    geom->NPrimalCells=cEnd-cStart;
    ierr = PetscMalloc1(geom->NPrimalCells,&gcon->tets);CHKERRQ(ierr);
    for(PetscInt c=cStart,i=0; c<cEnd; c++,i++) {
      ierr = DMPlexGetClosureIndices(dm,section,section,c,PETSC_TRUE,&numindices,&indices,NULL,NULL);CHKERRQ(ierr);
      for(PetscInt j=0;j<numindices;j++) {
        gcon->tets[i].idx[j] = indices[j];
        gcon->cellPtr[indices[j]+1]++;
        for(PetscInt d=0;d<dim;d++) {
          tet[dim*j + d] = gcon->nodes[dim*indices[j] + d];
        }
      }
      gcon->tets[i].vol = tet_volume(tet);
      ierr = DMPlexRestoreClosureIndices(dm,section,section,c,PETSC_TRUE,&numindices,&indices,NULL,NULL);CHKERRQ(ierr);
    }

    for(PetscInt i=0;i<N;i++) {
      gcon->cellPtr[i+1] += gcon->cellPtr[i];
    }

    ierr = PetscMalloc1(gcon->cellPtr[N],&gcon->cellIdx);CHKERRQ(ierr);
    ierr = PetscArrayzero(gcon->cellIdx,gcon->cellPtr[N]);CHKERRQ(ierr);

    for(PetscInt c=cStart,i=0; c<cEnd; c++,i++) {
      ierr = DMPlexGetClosureIndices(dm,section,section,c,PETSC_TRUE,&numindices,&indices,NULL,NULL);CHKERRQ(ierr);
      for(PetscInt j=0;j<4;j++) {
        offset = gcon->cellPtr[indices[j]];
        count  = gcon->cellIdx[gcon->cellPtr[indices[j]+1]-1];
        if(offset + count != gcon->cellPtr[indices[j]+1]-1) {
          gcon->cellIdx[gcon->cellPtr[indices[j]+1]-1]++;
        }
        gcon->cellIdx[offset+count] = i;
      }
      if(gcon->tets[i].vol < 1e-14 || PetscAbs(tet_orientation(tet)) < 1e-14) { 
        return 1;
      }
      ierr = DMPlexRestoreClosureIndices(dm,section,section,c,PETSC_TRUE,&numindices,&indices,NULL,NULL);CHKERRQ(ierr);
    }

    for(PetscInt i=0;i<N;i++) {
      PetscReal area = 0.0;
      for(PetscInt jj=gcon->cellPtr[i];jj<gcon->cellPtr[i+1];jj++) {
        PetscInt j = gcon->cellIdx[jj];
        PetscBool isThere = PETSC_FALSE;
        area += gcon->tets[j].vol / 4.;
        for(PetscInt k=0;k<4;k++) {
          if(gcon->tets[j].idx[k] == i) {
            isThere = PETSC_TRUE;
          }
        }
        if(!isThere) {
          printf("%d %d %d\n",i,jj,j);
          printf("  %d %d %d %d\n",gcon->tets[j].idx[0],gcon->tets[j].idx[1],gcon->tets[j].idx[2],gcon->tets[j].idx[3]);
          return 1;
        }
      }
      if(area - gcon->mass[i] > 1e-14) {
        printf("%1.7f %1.7f\n",area,gcon->mass[i]);
        return 1;
      }
    }
  }

  ierr = PetscMalloc1(N,&gcon->bndry);CHKERRQ(ierr);
  PetscInt isBndry;
  ierr = DMPlexGetHeightStratum(dm,dim,&cStart,&cEnd);CHKERRQ(ierr);
  for(PetscInt c = cStart,i=0; c < cEnd; c++,i++) {
    ierr = DMGetLabelValue(dm,"boundary",c,&isBndry);CHKERRQ(ierr);
    if(isBndry > 0) {
      gcon->bndry[i] = PETSC_TRUE;
    } else {
      gcon->bndry[i] = PETSC_FALSE;
    }
  }
  gcon->boundarySetup = PETSC_TRUE;

  ierr = CRDMEGeometrySetUp(geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONGmshCreateFromFile(CRDMEGeometry geom, const char* filename) {
  DM             dm;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
  ierr = DMPlexCreateGmshFromFile(PETSC_COMM_SELF,filename,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONCreateFromDMPlex(geom,dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomGCONCreateBoxMesh(CRDMEGeometry geom,PetscInt m[3]) {
  DM             dm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
  ierr = DMPlexCreateBoxMesh(PETSC_COMM_SELF,3,PETSC_TRUE,m,PETSC_NULL,PETSC_NULL,PETSC_NULL,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = CRDMEGeomGCONCreateFromDMPlex(geom,dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeomGCONShiftScaleCoordinates(CRDMEGeometry geom,CRDMEGeomPoint shift,PetscScalar scale) {
  CRDMEGeometry_GCON    gcon = geom->data;
  PetscInt              *Ap=gcon->Ap;
  PetscReal             *Ax=gcon->Ax;
  PetscReal             *mass=gcon->mass;
  PetscReal             *nodes=gcon->nodes;
  PetscInt              dim=geom->dim;

  PetscFunctionBegin;

  for(PetscInt i = 0; i < geom->N; i++) {
    for(PetscInt d=0;d<dim;d++) {
      nodes[dim*i+d] += shift[d];
      nodes[dim*i+d] *= scale;
    }
  }
  geom->hmax*=scale;
  geom->hmin*=scale;

  for(PetscInt d= 0; d < dim; d++) {
    geom->coord_min[d] += shift[d];
    geom->coord_max[d] += shift[d];
    geom->coord_min[d] *= scale;
    geom->coord_max[d] *= scale;
  }
  for(PetscInt i = 0; i < geom->N; i++) {
    mass[i] *= PetscPowRealInt(scale,dim);
    for(PetscInt jj = Ap[i]; jj < Ap[i+1]; jj++) {
      Ax[jj] /= PetscSqr(scale);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetVoxelArea_GCON(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *area) {
  CRDMEGeometry_GCON gcon  = geom->data;
  *area = gcon->mass[i];
  return 0;
} 


//PetscErrorCode orient(double *v, int ilo, int ihi, int idx) {
//  int a,c;
//  int det;
//  double tmp[2*(ihi - ilo)];
//
//  if(idx == ilo) {
//    a = ihi - 1;
//    c = ilo + 1;
//  } else if(idx == ihi - 1) {
//    a = ihi - 2;
//    c = ilo;
//  } else {
//    a = idx - 1;
//    c = idx + 1;
//  }
//
//  det = (v[2*idx] - v[2*a])*(v[2*c+1] - v[2*idx+1]) < (v[2*c] - v[2*idx])*(v[2*idx+1]-v[2*a+1]);
//  if(det) {
//    for(int i = 0; i < ihi - ilo; i++) {
//      tmp[2*i]   =  v[2*(ihi-i-1)];
//      tmp[2*i+1] =  v[2*(ihi-i-1)+1];
//    }
//
//    for(int i = 0; i < 2*(ihi - ilo); i++) {
//       v[2*ilo + i] = tmp[i];
//    }
//  }
//  return 0;
//}
//
//PetscErrorCode CRDMEGeometryNodeGetVoxel_GCON(CRDMEGeometry geom,int idx,double *voxel,int *len) {
//  int startj, prevj, idx_min;
//  CRDMEGeometry_GCON gcon = geom->data;
//  double             *nodes=gcon->nodes;
//  int                *Ap   =gcon->Ap;
//  int                *Aj   =gcon->Aj;
//  PetscErrorCode     ierr;
//  PetscFunctionBegin;
//  /* length of nodes should be 2*sum_{idx < #voxels} 2*(geom->Ap[idx + 1] - geom->Ap[idx]) */
//  /* one dual node for every edge, and one for every face between edges */
//  /* (also one for each boundary vertex) */
//
//  /* length of offsets should be #nodes */
//  /* length of is_bndry should be #nodes */
//
//  /* compute an ordering for the dual nodes */
//  int j, k, cnt = 0;
//  int cell_count = 0, cell_found = 0;
//  for(int jj = Ap[idx]; jj < Ap[idx + 1]; jj++) { /* we're going to check and see which edges belong to multiple cells */
//    j = Aj[jj];
//    cell_count = 0;
//    startj = j;
//    for(int kk = Ap[j]; kk < Ap[j+1]; kk++) { /* look at all sequences of distinct edges idx -- j -- k. see if k -- idx */
//      k = Aj[kk];
//      if(k != idx) {
//        for(int ll = Ap[k]; ll < Ap[k + 1]; ll++) { /* see if k -- idx */
//          if(Aj[ll] == idx) {
//            cell_count++;
//            break;
//          }
//        }
//      }
//      if(cell_count > 1) {
//        break;
//      }
//    }
//    if(cell_count < 2) {
//      if(cell_count < 1) {
//        return 1;
//      }
//      voxel[0] = nodes[2*idx];
//      voxel[1] = nodes[2*idx+1];
//      cnt++;
//      break;
//    }
//  }
//
//  idx_min = 0;
//  prevj  = -1;
//  cell_found = 1;
//  while(cell_found || j != startj) {                                /* loop until we come back to the starting edge */
//    cell_found = 0;                                                                /* or we can't find another cell */
//    voxel[2*cnt]   = .5*(nodes[2*idx] + nodes[2*j]);  /* add edge idx -- j */
//    voxel[2*cnt+1] = .5*(nodes[2*idx + 1] + nodes[2*j + 1]);
//    if(voxel[2*cnt+1] < voxel[2*idx_min+1]) {
//      idx_min = cnt;
//    } else if(voxel[2*cnt+1] == voxel[2*idx_min+1]) {
//      if(voxel[2*cnt] > voxel[2*idx_min]) {
//        idx_min = cnt;
//      }
//    }
//    cnt++;
//    for(int kk = Ap[j]; kk < Ap[j + 1] && !cell_found; kk++) {    /* loop over edges idx -- j -- k */
//      k = Aj[kk];
//      if(k != prevj && k != idx) {                                              /* make sure we don't find the previous cell */
//        for(int ll = Ap[k]; ll < Ap[k + 1] && !cell_found; ll++) { /* loop over edges idx -- j -- k -- Aj[ll] */
//          if(Aj[ll] == idx) {                                             /* if Aj[ll] == idx, we've found a cell */
//            cell_found = 1;
//            voxel[2*cnt]   = (nodes[2*idx] + nodes[2*j] + nodes[2*k])/3.;
//            voxel[2*cnt+1] = (nodes[2*idx+1] + nodes[2*j+1] + nodes[2*k+1])/3.;
//            if(voxel[2*cnt+1] < voxel[2*idx_min+1]) {
//              idx_min = cnt;
//            } else if(voxel[2*cnt+1] == voxel[2*idx_min+1]) {
//              if(voxel[2*cnt] > voxel[2*idx_min]) {
//                idx_min = cnt;
//              }
//            }
//            cnt++;
//            prevj = j;
//            j = k;                                                            /* start again with edge idx -- k */
//          }
//        }
//      }
//    }
//
//    if(!cell_found || (j == startj)) {
//      break;
//    }
//  }
//  ierr = orient(voxel,0,cnt,idx_min);CHKERRQ(ierr);
//  *len = cnt;
//  voxel[2*cnt]   = nodes[2*idx];
//  voxel[2*cnt+1] = nodes[2*idx+1];
//  return 0;
//}
