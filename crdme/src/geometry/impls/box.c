/*
 * box.c
 *
 *  Created on: Jul 14, 2021
 *      Author: maxheldman
 */

#include <petsc.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <rng.h>
#include <util.h>

PETSC_STATIC_INLINE void lex_to_coords(PetscInt *m, CRDMEGeomGloIdx i,  PetscInt *ijk, PetscInt dim) {
  switch(dim) {
    case(1):
      *ijk=i;
      break;
    case(2):
      ijk[1] = i / m[0];
      ijk[0] = i - m[0]*ijk[1];
      break;
    case(3):
      ijk[2] = i / (m[0]*m[1]);
      ijk[1] = (i - m[0]*m[1]*ijk[2]) / m[0];
      ijk[0] = i - m[0]*m[1]*ijk[2] - m[0]*ijk[1];
  }
}

PETSC_STATIC_INLINE CRDMEGeomGloIdx coords_to_lex(PetscInt *m, PetscInt *ijk, PetscInt dim) {
  switch(dim) {
    case(1):
      return ijk[0];
      break;
    case(2):
      return ijk[1]*m[0]+ijk[0];
      break;
    case(3):
      return ijk[2]*m[0]*m[1] + ijk[1]*m[0] + ijk[0];
      break;
    default: 
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_OUTOFRANGE,"Wrong dimension %d in coords to lex; reached unreachable code\n",dim);
  }
}

PETSC_STATIC_INLINE PetscBool NodeIsBndry(PetscInt i, PetscInt m) {
	return (i==0 || i==m-1);
}

PETSC_STATIC_INLINE PetscBool NodeIsInterior(PetscInt i, PetscInt m) {
	return !NodeIsBndry(i,m);
}


PetscErrorCode CRDMEGeometryNodeGetCoordinates_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          ijk[geom->dim];

  lex_to_coords(box->m,i,ijk,geom->dim);
  for(PetscInt d=0;d<geom->dim;d++) {
    x[d] = geom->coord_min[d] + box->h[d]*ijk[d];
  }

  return(0);
}

/*
PetscErrorCode _p_CRDMEGeomBoxNodeGetNumNeighbors(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          a[geom->dim];

  lex_to_coords(box->m,i,a,geom->dim);
  *nn=0;
  for(PetscInt d=0; d<geom->dim; d++) {
    *nn += (box->periodicity[d] || NodeIsInterior(a[d],box->m[d]) ? 2 : 1);
  }
  
  return(0);
}

PetscErrorCode _p_CRDMEGeomBoxNodeGetNeighbors(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          a[dim],b[dim],n=0,d;

  lex_to_coords(box->m,i,a,dim);
  PetscArraycpy(b,a,dim);

  for(d=0; d<dim; d++) {
    if(box->periodicity[d]) {
      b[d]= a[d]+1 >= box->m[d] ? a[d]+1 - box->m[d] : a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      b[d]=!a[d] ? box->m[d] + a[d]-1 : a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      n+=2;
      b[d]=a[d];
    } else if(NodeIsInterior(a[d],box->m[d])) {
      b[d]=a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      b[d]=a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      n+=2;
      b[d]=a[d];
    } else {
      b[d] = a[d] == 0 ? a[d]+1 : a[d]-1;
      indices[n]=coords_to_lex(box->m,b,dim);
      n++;
      b[d]=a[d];
    }
  }
  *nn=n;
  
  return(0);
}

PetscErrorCode _p_CRDMEGeomBoxNodeGetEdgeWeights(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices, PetscReal *data) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          d,a[dim],b[dim],n=0;

  lex_to_coords(box->m,i,a,dim);
  PetscArraycpy(b,a,dim);

  for(d=0; d<dim; d++) {
    if(box->periodicity[d]) {
      b[d]= a[d]+1 >= box->m[d] ? a[d]+1 - box->m[d] : a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      data[n]     =box->edge_weights[d];
      b[d]=!a[d] ? box->m[d] + a[d]-1 : a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      data[n+1]   =box->edge_weights[d];
      n+=2;
      b[d]=a[d];
    } else if(NodeIsInterior(a[d],box->m[d])) {
      b[d]=a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      data[n]     =box->edge_weights[d];
      b[d]=a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      data[n+1]   =box->edge_weights[d];
      n+=2;
      b[d]=a[d];
    } else {
      b[d] = a[d] == 0 ? a[d]+1 : a[d]-1;
      indices[n]=coords_to_lex(box->m,b,dim);
      data[n]   =2*box->edge_weights[d];
      n++;
      b[d]=a[d];
    }
  }
  *nn=n;

  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetEdgeWeights_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices, PetscReal *data) {
  CRDMEGeometry_Box box = geom->data;

  *nn = box->Ap[i+1]-box->Ap[i];
  PetscArraycpy(indices,&box->Aj[box->Ap[i]],*nn);
  PetscArraycpy(data,&box->Ax[box->Ap[i]],*nn);

  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetNeighbors_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices) {
  CRDMEGeometry_Box box = geom->data;

  *nn = box->Ap[i+1]-box->Ap[i];
  PetscArraycpy(indices,&box->Aj[box->Ap[i]],*nn);

  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetNumNeighbors_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn) {
  CRDMEGeometry_Box box = geom->data;

  *nn = box->Ap[i+1]-box->Ap[i];

  return(0);
}
*/
PetscErrorCode CRDMEGeometryNodeGetNumNeighbors_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          a[geom->dim];

  lex_to_coords(box->m,i,a,geom->dim);
  *nn=0;
  for(PetscInt d=0; d<geom->dim; d++) {
    *nn += (box->periodicity[d] || NodeIsInterior(a[d],box->m[d]) ? 2 : 1);
  }
  
  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetNeighbors_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          a[dim],b[dim],n=0,d;

  lex_to_coords(box->m,i,a,dim);
  PetscArraycpy(b,a,dim);

  for(d=0; d<dim; d++) {
    if(box->periodicity[d]) {
      b[d]= a[d]+1 >= box->m[d] ? a[d]+1 - box->m[d] : a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      b[d]=!a[d] ? box->m[d] + a[d]-1 : a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      n+=2;
      b[d]=a[d];
    } else if(NodeIsInterior(a[d],box->m[d])) {
      b[d]=a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      b[d]=a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      n+=2;
      b[d]=a[d];
    } else {
      b[d] = a[d] == 0 ? a[d]+1 : a[d]-1;
      indices[n]=coords_to_lex(box->m,b,dim);
      n++;
      b[d]=a[d];
    }
  }
  *nn=n;
  
  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetEdgeWeights_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices, PetscReal *data) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          d,a[dim],b[dim],n=0;

  lex_to_coords(box->m,i,a,dim);
  PetscArraycpy(b,a,dim);

  for(d=0; d<dim; d++) {
    if(box->periodicity[d]) {
      b[d]= a[d]+1 >= box->m[d] ? a[d]+1 - box->m[d] : a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      data[n]     =box->edge_weights[d];
      b[d]=!a[d] ? box->m[d] + a[d]-1 : a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      data[n+1]   =box->edge_weights[d];
      n+=2;
      b[d]=a[d];
    } else if(NodeIsInterior(a[d],box->m[d])) {
      b[d]=a[d]+1;
      indices[n]  =coords_to_lex(box->m,b,dim);
      data[n]     =box->edge_weights[d];
      b[d]=a[d]-1;
      indices[n+1]=coords_to_lex(box->m,b,dim);
      data[n+1]   =box->edge_weights[d];
      n+=2;
      b[d]=a[d];
    } else {
      b[d] = a[d] == 0 ? a[d]+1 : a[d]-1;
      indices[n]=coords_to_lex(box->m,b,dim);
      data[n]   =2*box->edge_weights[d];
      n++;
      b[d]=a[d];
    }
  }
  *nn=n;

  return(0);
}

PetscErrorCode CRDMEGeometryDestroy_Box(CRDMEGeometry *geom) {
  CRDMEGeometry_Box box=(*geom)->data;
  PetscErrorCode    ierr;

  PetscFunctionBegin;

  ierr = PetscFree(box);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySetUp_Box(CRDMEGeometry geom) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          nn;
  PetscErrorCode    ierr;

  PetscFunctionBegin;

  geom->N = 1;
  for(PetscInt d = 0; d < geom->dim; d++) {
    box->L[d]            = geom->coord_max[d] - geom->coord_min[d];
    box->h[d]            = box->periodicity[d] ?  box->L[d] / box->m[d] 
                                               :  box->L[d] / (box->m[d]-1);
    box->hsqr[d]         = PetscSqr(box->h[d]);
    box->edge_weights[d] = 1.0/box->hsqr[d];
    geom->N             *= box->m[d];
  }

  switch(geom->dim) {
      case(1):
        geom->hmin = box->h[0];
        geom->hmax = box->h[0];
        break;
      case(2):
        geom->hmin = PetscMin(box->h[0],box->h[1]);
        geom->hmax = PetscMax(box->h[0],box->h[1]);
        break;
      case(3):
        geom->hmin = PetscMin(box->h[0],PetscMin(box->h[1],box->h[2]));
        geom->hmax = PetscMax(box->h[0],PetscMax(box->h[1],box->h[2]));
        break;
      default:
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Unreachable code. Geometry dimension %d is not valid.",geom->dim);
  }

  geom->voxelNodesSize = PetscPowInt(2,geom->dim);
  ierr = PetscMalloc1(geom->voxelNodesSize,&geom->work_voxel[0].nodes);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->voxelNodesSize,&geom->work_voxel[1].nodes);CHKERRQ(ierr);

  geom->maxNodeDegree  = 2*geom->dim;

  //ierr = PetscMalloc1(geom->N+1,&box->Ap);CHKERRQ(ierr);
  geom->NEdges=0;
  //box->Ap[0] = 0;
  for(PetscInt i=0;i<geom->N;i++) {
    //ierr = _p_CRDMEGeomBoxNodeGetNumNeighbors(geom,i,&nn);CHKERRQ(ierr);
    ierr = CRDMEGeometryNodeGetNumNeighbors(geom,i,&nn);CHKERRQ(ierr);
    geom->NEdges+=nn;
    //box->Ap[i+1]=geom->NEdges;
  }

  //ierr = PetscMalloc1(geom->NEdges,&box->Aj);CHKERRQ(ierr);
  //ierr = PetscMalloc1(geom->NEdges,&box->Ax);CHKERRQ(ierr);
  /*
  for(PetscInt i=0;i<geom->N;i++) {
    ierr = _p_CRDMEGeomBoxNodeGetEdgeWeights(geom,i,&nn,&box->Aj[box->Ap[i]],&box->Ax[box->Ap[i]]);CHKERRQ(ierr);
  }
  */
  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeomBoxSet(CRDMEGeometry geom, PetscReal *x, PetscInt *m, PetscBool *periodicity) {
  CRDMEGeometry_Box box = geom->data;
  PetscErrorCode    ierr;
  PetscInt          d=2*geom->dim;
  PetscBool         flg;

  PetscFunctionBegin;

  if(geom->dim < 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ORDER,"CRDMEGeomBoxSetDomain called before setting geometry dimension.");
  }

  ierr = PetscOptionsGetRealArray(PETSC_NULL,PETSC_NULL,"-crdme_geometry_box_set_limits",x,&d,&flg);CHKERRQ(ierr);
  d = geom->dim;
  ierr = PetscOptionsGetIntArray(PETSC_NULL,PETSC_NULL,"-crdme_geometry_box_set_grid_spacing",m,&d,&flg);CHKERRQ(ierr);
  d = geom->dim;
  ierr = PetscOptionsGetBoolArray(PETSC_NULL,PETSC_NULL,"-crdme_geometry_box_periodicity",periodicity,&d,&flg);CHKERRQ(ierr);

  for(PetscInt i = 0; i < geom->dim; i++) {
    geom->coord_min[i]  = x[2*i];
    geom->coord_max[i]  = x[2*i+1];
    box->periodicity[i] = periodicity[i];
    box->m[i]           = m[i]; /* number of independent unknowns in each direction */
  }
  
  ierr = CRDMEGeometrySetUp(geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeometryIterateVoxelNbhd_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscScalar radius, VoxelNbhdCallback f, void *ctx) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim=geom->dim;
  PetscInt          extx,exty,extz;
  PetscInt          xmin,xmax,ymin,ymax,zmin,zmax;
  PetscInt          ij[dim],kl[dim];
  PetscBool         exit=PETSC_FALSE,cap=PETSC_FALSE;
  PetscScalar       rsqr = PetscSqr(radius);
  CRDMEGeomGloIdx   j;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  
  switch(geom->dim) {
    case(1):
      extx=((PetscInt)(radius / box->h[dim-1])) + 1;
      xmin = box->periodicity[0] ? i - extx : PetscMax(0,i - extx);
      xmax = box->periodicity[0] ? i + extx : PetscMin(box->m[0]-1,i + extx);
      for(PetscInt x=xmin; x<=xmax&&!exit; x++) {
        j    = box->periodicity[0] ? PerIndex(x, box->m[0]) : x; /* translate to global index */
        ierr = f(geom,i,j,radius,ctx,&cap,&exit);CHKERRQ(ierr);
      }
      break;
    case(2):
      lex_to_coords(box->m, i, ij, geom->dim);
      exty=((PetscInt)(radius / box->h[dim-1])) + 1;
      ymin = box->periodicity[1] ? ij[1] - exty : PetscMax(0,ij[1] - exty);
      ymax = box->periodicity[1] ? ij[1] + exty : PetscMin(box->m[1]-1,ij[1] + exty);
      for(PetscInt y=ymin; y<=ymax&&!exit; y++) {
        extx  = ( (PetscInt) ( PetscSqrtReal(rsqr - PetscSqr(ij[1] - y)*box->hsqr[1]) / box->h[0] )) + 1;
        xmin  = box->periodicity[0] ? ij[0] - extx : PetscMax(0,ij[0] - extx);
        xmax  = box->periodicity[0] ? ij[0] + extx : PetscMin(box->m[0]-1,ij[0] + extx);
        kl[1] = box->periodicity[1] ? PerIndex(y, box->m[1]) : y;
        for(PetscInt x=xmin; x<=xmax&&!exit; x++) {
          kl[0] = box->periodicity[0] ? PerIndex(x, box->m[0]) : x;
          j     = coords_to_lex(box->m,kl,2); /* translate to global index */
          ierr  = f(geom,i,j,radius,ctx,&cap,&exit);CHKERRQ(ierr);
        }
      }
      break;
    case(3):
      lex_to_coords(box->m, i, ij, geom->dim);
      extz=((PetscInt)(radius / box->h[dim-1])) + 1;
      zmin  = box->periodicity[2] ? ij[2] - extz : PetscMax(0,ij[2] - extz);
      zmax  = box->periodicity[2] ? ij[2] + extz : PetscMin(box->m[2]-1,ij[2] + extz);
      for(PetscInt z=zmin; z<=zmax&&!exit; z++) {
        exty  = ( (PetscInt) ( PetscSqrtReal(rsqr - PetscSqr(ij[2] - z)*box->hsqr[2]) / box->h[1] )) + 1;
        ymin  = box->periodicity[1] ? ij[1] - exty : PetscMax(0,ij[1] - exty);
        ymax  = box->periodicity[1] ? ij[1] + exty : PetscMin(box->m[1]-1,ij[1] + exty);
        kl[2] = box->periodicity[2] ? PerIndex(z, box->m[2]) : z;
        for(PetscInt y=ymin; y<=ymax&&!exit; y++) {
          extx  = ( (PetscInt) ( PetscSqrtReal(rsqr - PetscSqr(ij[2] - z)*box->hsqr[2] - PetscSqr(ij[1] - y)*box->hsqr[1]) / box->h[0] )) + 1;
          xmin  = box->periodicity[0] ? ij[0] - extx : PetscMax(0,ij[0] - extx);
          xmax  = box->periodicity[0] ? ij[0] + extx : PetscMin(box->m[0]-1,ij[0] + extx);
          kl[1] = box->periodicity[1] ? PerIndex(y, box->m[1]) : y;
          for(PetscInt x=xmin; x<=xmax&&!exit; x++) {
            kl[0] = box->periodicity[0] ? PerIndex(x, box->m[0]) : x;
            j     = coords_to_lex(box->m,kl,3); /* translate to global index */
            ierr  = f(geom,i,j,radius,ctx,&cap,&exit);CHKERRQ(ierr);
          }
        }
      }
      break;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetVoxel_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomVoxel *voxel) {
  CRDMEGeometry_Box box  = geom->data;
  PetscInt          a[geom->dim];

  PetscFunctionBegin;

  voxel = &geom->work_voxel[0];
  lex_to_coords(box->m,i,a,geom->dim);
  for(PetscInt d=0;d<geom->dim;d++) {
    if(box->periodicity[d]) {
      voxel->nodes[0][d] = geom->coord_min[d] + (a[d] - .5)*box->h[d];
      voxel->nodes[1][d] = geom->coord_min[d] + (a[d] + .5)*box->h[d];
      voxel->nodes[2][d] = geom->coord_min[d] +  a[d]*box->h[d];
    } else {
      voxel->nodes[0][d] = a[d] == 0           ? geom->coord_min[d] :  geom->coord_min[d] + (a[d] - .5)*box->h[d];
      voxel->nodes[1][d] = a[d] == box->m[d]-1 ? geom->coord_max[d] :  geom->coord_min[d] + (a[d] + .5)*box->h[d];
      voxel->nodes[2][d] = geom->coord_min[d] + a[d]*box->h[d];
    }
  }
  voxel->i = i;

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodesGetDistanceSqr_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *dist) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          a[dim],b[dim];

  lex_to_coords(box->m,i,a,dim); 
  lex_to_coords(box->m,j,b,dim);

  *dist=0.0;
  for(PetscInt d = 0; d < dim; d++) {
    *dist += box->periodicity[d] ? PerDistSqr(a[d],b[d],box->m[d])*box->hsqr[d] : PetscSqr(a[d]-b[d])*box->hsqr[d];
  }

  return 0;
}

PetscErrorCode CRDMEGeometryPointsGetDistanceSqr_Box(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomPoint y, PetscReal *dist) {  
  CRDMEGeometry_Box box = geom->data;

  *dist=0.0;
  for(PetscInt d = 0; d < geom->dim; d++) {
    *dist += box->periodicity[d] ? PerDistSqr(x[d],y[d],box->L[d]) : PetscSqr(x[d]-y[d]);
  }

  return 0;
}

PetscErrorCode CRDMEGeometryNodeGetVoxelArea_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *area) {
  CRDMEGeometry_Box box  = geom->data;
  PetscInt          a[geom->dim];

  lex_to_coords(box->m,i,a,geom->dim);
  *area=1.0;
  for(PetscInt d=0;d<geom->dim;d++) {
    if(box->periodicity[d]) {
      *area *= box->h[d];
    } else {
      *area *= (NodeIsBndry(a[d],box->m[d]) ? .5*box->h[d] : box->h[d]);
    }
  }

  return 0;
} 

PetscErrorCode CRDMEGeometryVoxelGetArea_Box(CRDMEGeometry geom, CRDMEGeomVoxel voxel, PetscReal *area) {
  
  *area=1.0;
  for(PetscInt d=0;d<geom->dim;d++) {
    *area *= (voxel.nodes[1][d] - voxel.nodes[0][d]);
  }

  return(0);
} 

PetscErrorCode CRDMEGeometrySampleFromVoxel_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  PetscErrorCode ierr;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];

  PetscFunctionBegin;
  
  ierr = CRDMEGeometryNodeGetVoxel(geom,i,voxel);CHKERRQ(ierr);

  for(PetscInt d=0; d<geom->dim; d++) {
    x[d] = voxel->nodes[0][d] + UNI*(voxel->nodes[1][d] - voxel->nodes[0][d]);
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomBoxIteratePeriodicStencil(CRDMEGeometry geom,CRDMEGeomGloIdx i,SpatIaStencil stencil,VoxelNbhdCallback f,PetscInt n,void *ctx) {
  CRDMEGeometry_Box box=geom->data;
  PetscInt          dim=geom->dim,a[dim],b[dim],*ijk,d,jj,kk;
  CRDMEGeomGloIdx   j;
  PetscReal         val;
  PetscBool         cap,exit=PETSC_FALSE;

  PetscFunctionBegin;

  lex_to_coords(box->m,i,a,dim);
  for(jj=0,kk=0;jj<stencil->stencilLen;jj++,kk+=dim) {
    ijk = &stencil->indices[kk];
    for(d=0;d < dim;d++) {
      b[d] = PerIndex(a[d]+ijk[d],box->m[d]);
    }
    j = coords_to_lex(box->m,b,dim);
    val = stencil->vals[n] ? stencil->vals[n][jj] : stencil->constantVals[n];
    f(geom,i,j,val,ctx,&cap,&exit);
    if(exit) {
      break;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode computeStencilIndicesCallback_PeriodicBox(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  computeStencilIndicesCtx *c=ctx;  
  PetscBool                in=PETSC_FALSE;
  PetscErrorCode           ierr;
  PetscInt                 dim=geom->dim;
  PetscInt                 ijk[dim];

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = c->VoxelIsInStencil(geom,i,j,&in,c->ctx);CHKERRQ(ierr);
  if(in) {
    CRDMEGeometry_Box box = geom->data;
    lex_to_coords(box->m,j,ijk,dim);
    for(PetscInt d=0;d<dim;d++) {
      if(ijk[d] >= box->m[d] / 2) {
        ijk[d] = ijk[d] - box->m[d];
      }
      c->stencil->indices[dim*c->count+d] = ijk[d];
    }
    c->count++;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeomBoxSearchPeriodicStencil(CRDMEGeometry geom,SpatIaStencil stencil,CRDMEGeomGloIdx i,PetscReal val,CRDMEGeomGloIdx *j) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt dim=geom->dim,jj,*ijk,a[dim];
  PetscErrorCode ierr;
  ierr = BinarySearchReal(stencil->vals[1],0,stencil->stencilLen,val,&jj);CHKERRQ(ierr);
  ijk = &stencil->indices[dim*jj];
  lex_to_coords(box->m,i,a,dim);
  for(PetscInt d=0;d < dim;d++) {
    a[d] = PerIndex(a[d]+ijk[d],box->m[d]);
  }
  *j = coords_to_lex(box->m,a,dim);

  return (0);
} 




/* 
PetscErrorCode CRDMEGeometryIterateVoxelNbhd_Box_3d(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscScalar radius, VoxelNbhdCallback f, void *ctx) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim;
  PetscInt          extz=( (PetscInt) ((radius + .5*box->h[dim-1]) / box->h[dim-1] )) + 1;
  PetscInt          jlo,jhi,icnt=0;
  PetscInt          ij[geom->dim],kl[geom->dim];
  PetscScalar       rsqr = (radius+.5*box->h[1])*(radius+.5*box->h[1]);
  CRDMEGeomGloIdx   j;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  
  lex_to_coords(box->m, i, ij, geom->dim);
  jlo = box->periodicity[1] ? ij[1] - extz : PetscMax(0,ij[1] - extz);
  jhi = box->periodicity[1] ? ij[1] + extz : PetscMin(0,ij[1] + extz);
  
  for(PetscInt o = jlo; o <= jhi; o++) {
    kl[1]  = box->periodicity[1] ? PerIndex(o,box->m[1]) : o;
    j      = kl[1]*box->m[0]+ij[0];
    ierr   = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt-.5)) < rsqr) {
      kl[0]= box->periodicity[0] ? PerIndex(ij[0]+icnt,box->m[0]) : ij[0]+icnt;
      if(kl[0] >= box->m[0]) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt+.5)) < rsqr) {
      kl[0] = box->periodicity[0] ? PerIndex(ij[0]-icnt,box->m[0]) : ij[0]-icnt;
      if(kl[0] < 0) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    rsqr -= box->hsqr[1];
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryIterateVoxelNbhd_Box_2d(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscScalar radius, VoxelNbhdCallback f, void *ctx) {
  CRDMEGeometry_Box box = geom->data;
  PetscInt          extj=( (PetscInt) ((radius + .5*box->h[1]) / box->h[1] )) + 1;
  PetscInt          jlo,jhi,icnt=0;
  PetscInt          ij[geom->dim],kl[geom->dim];
  PetscScalar       rsqr = (radius+.5*box->h[1])*(radius+.5*box->h[1]);
  CRDMEGeomGloIdx   j;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  
  lex_to_coords(box->m, i, ij, geom->dim);
  jlo = box->periodicity[1] ? ij[1] - extj : PetscMax(0,ij[1] - extj);
  jhi = box->periodicity[1] ? ij[1] + extj : PetscMin(0,ij[1] + extj);
  
  for(PetscInt o = jlo; o <= jhi; o++) {
    kl[1]  = box->periodicity[1] ? PerIndex(o,box->m[1]) : o;
    j      = kl[1]*box->m[0]+ij[0];
    ierr   = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt-.5)) < rsqr) {
      kl[0]= box->periodicity[0] ? PerIndex(ij[0]+icnt,box->m[0]) : ij[0]+icnt;
      if(kl[0] >= box->m[0]) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt+.5)) < rsqr) {
      kl[0] = box->periodicity[0] ? PerIndex(ij[0]-icnt,box->m[0]) : ij[0]-icnt;
      if(kl[0] < 0) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    rsqr -= box->hsqr[1];
  }

  PetscFunctionReturn(0);
}



  CRDMEGeometry_Box box = geom->data;
  PetscInt          dim = geom->dim,d=dim;
  PetscInt          ext=( (PetscInt) ((radius + .5*box->h[dim-1]) / box->h[dim-1] )) + 1;
  PetscInt          extx,exty;
  PetscInt          xmin,xmax,ymin,ymax,zmin,zmax;
  PetscInt          ij[dim-1],kl[dim-1];
  PetscScalar       rsqr = PetscSqr(radius+.5*box->h[dim-1]);
  CRDMEGeomGloIdx   j;
  PetscErrorCode    ierr;

  lex_to_coords(box->m, i, ij, geom->dim);

  for(PetscInt )

  for(PetscInt d=dim-1;d > -1;d--) {
    rsqr = 
  }
  jlo = box->periodicity[1] ? ij[1] - extz : PetscMax(0,ij[1] - extz);
  jhi = box->periodicity[1] ? ij[1] + extz : PetscMin(0,ij[1] + extz);
  
  for(PetscInt o = jlo; o <= jhi; o++) {
    kl[1]  = box->periodicity[1] ? PerIndex(o,box->m[1]) : o;
    j      = kl[1]*box->m[0]+ij[0];
    ierr   = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt-.5)) < rsqr) {
      kl[0]= box->periodicity[0] ? PerIndex(ij[0]+icnt,box->m[0]) : ij[0]+icnt;
      if(kl[0] >= box->m[0]) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    icnt   = 1;
    while(PetscSqr(box->h[0]*(icnt+.5)) < rsqr) {
      kl[0] = box->periodicity[0] ? PerIndex(ij[0]-icnt,box->m[0]) : ij[0]-icnt;
      if(kl[0] < 0) {
        break;
      }
      j    = kl[1]*box->m[0]+kl[0];
      ierr = f(geom,i,j,radius,ctx);CHKERRQ(ierr);
      icnt++;
    }
    rsqr -= box->hsqr[1];
  }

  PetscErrorCode _p_CRDMEGeomDimensionRecursion(CRDMEGeometry geom, PetscInt d) {
    switch()
  }

*/

/*
PetscErrorCode CRDMEGeometryNodeGetRelativeVoxel_Box(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, CRDMEGeomVoxel voxel) {
  CRDMEGeometry_Box box  = geom->data;
  PetscInt          s,dim=geom->dim;
  PetscInt          a[dim],b[dim];
  PetscScalar       h,min,max,c;

  PetscFunctionBegin;

  lex_to_coords(box->m,i,a,dim);
  lex_to_coords(box->m,j,b,dim);
  for(PetscInt d=0;d<geom->dim;d++) {
    h = box->h[d];
    if(box->periodicity[d]) {
      s              = PeriodicDist(a[d],b[d],box->m[d]);
      voxel[d]       = (a[d] + s - .5)*h;
      voxel[d+dim]   = (a[d] + s + .5)*h;
      voxel[d+2*dim] = (a[d] + s     )*h;
    } else {
      voxel[d]       = b[d]==0           ? geom->coord_min[d] : geom->coord_min[d] + (b[d]-.5)*h;
      voxel[d+dim]   = b[d]==box->m[d]-1 ? geom->coord_max[d] : geom->coord_min[d] + (b[d]+.5)*h;
      voxel[d+2*dim] = geom->coord_min[d] + b[d]*h;
  }

  PetscFunctionReturn(0);
} 
*/