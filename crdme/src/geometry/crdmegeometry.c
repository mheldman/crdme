/*
 * crdmegeometry.c
 *
 *  Created on: Jul 13, 2021
 *      Author: maxheldman
 */

#include <petsc.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <crdmegeomgcon.h>
#include <rng.h>
#include <overlap.h>

PetscErrorCode CRDMEGeometryCreate(CRDMEGeometry *geom) {
  
  PetscErrorCode ierr;
  CRDMEGeometry  g;

  PetscFunctionBegin;

  ierr = PetscMalloc1(1,&g);CHKERRQ(ierr);

  g->data                               = NULL;
  g->type                               = GEOMETRY_TYPE_GCON;
  g->ops.crdmegeom_destroy              = NULL;
  g->ops.crdmegeom_nodegetcoords        = NULL;
  g->ops.crdmegeom_nodegetvoxel         = NULL;
  g->ops.crdmegeom_nodegetedgeweights   = NULL;
  g->ops.crdmegeom_nodegetnumneighbors  = NULL;
  g->ops.crdmegeom_nodegetneighbors     = NULL;
  g->ops.crdmegeom_nodesgetdistancesqr  = NULL;
  g->ops.crdmegeom_pointsgetdistancesqr = NULL;
  g->ops.crdmegeom_setup                = NULL;

  g->dim    = -1;
  g->hmax   = 1.0;
  g->hmin   = 0.0;
  g->N      = 0;
  g->NEdges = 0;
  g->coarse = NULL;
  g->qArr   = NULL;
  g->visitedArr = NULL;
  g->setupCalled=PETSC_FALSE;

  for(PetscInt d = 0; d < 3; d++) {
    g->coord_min[d] = 0.0;
    g->coord_max[d] = 0.0;
  }

  g->work_voxel[0].i = -1; /* indicate data in voxel is junk */
  g->work_voxel[1].i = -1;

  *geom = g;

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryDestroy(CRDMEGeometry *geom) {
  
  PetscFunctionBegin;

  if((*geom)->ops.crdmegeom_destroy) {
    CHKERRQ( (*geom)->ops.crdmegeom_destroy(geom) );
  }
  if((*geom)->coarse) {
    CHKERRQ( (*geom)->coarse->ops.crdmegeom_destroy(&(*geom)->coarse) );
  }
  PetscFree(*geom);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySetType(CRDMEGeometry geom, CRDMEGeometryType type) {
  
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  if(type==GEOMETRY_TYPE_GCON) {
    CRDMEGeometry_GCON   gcon;
    geom->type=type;
    geom->ops.crdmegeom_destroy              = CRDMEGeometryDestroy_GCON;
    geom->ops.crdmegeom_nodegetcoords        = CRDMEGeometryNodeGetCoordinates_GCON;
    geom->ops.crdmegeom_nodegetvoxel         = CRDMEGeometryNodeGetVoxel_GCON;
    geom->ops.crdmegeom_nodegetvoxelarea     = CRDMEGeometryNodeGetVoxelArea_GCON;
    geom->ops.crdmegeom_nodegetedgeweights   = CRDMEGeometryNodeGetEdgeWeights_GCON;
    geom->ops.crdmegeom_nodegetneighbors     = CRDMEGeometryNodeGetNeighbors_GCON;
    geom->ops.crdmegeom_nodegetnumneighbors  = CRDMEGeometryNodeGetNumNeighbors_GCON;
    geom->ops.crdmegeom_voxelgetarea         = CRDMEGeometryVoxelGetArea_GCON;
    geom->ops.crdmegeom_nodesgetdistancesqr  = CRDMEGeometryNodesGetDistanceSqr_GCON;
    geom->ops.crdmegeom_pointsgetdistancesqr = CRDMEGeometryPointsGetDistanceSqr_GCON;
    geom->ops.crdmegeom_samplefromvoxel      = CRDMEGeometrySampleFromVoxel_GCON;
    geom->ops.crdmegeom_iteratevoxelnbhd     = CRDMEGeometryIterateVoxelNbhd;
    geom->ops.crdmegeom_setup                = CRDMEGeometrySetUp_GCON;
    ierr = PetscMalloc1(1,&gcon);CHKERRQ(ierr);
    geom->data = gcon;
  } else if(type==GEOMETRY_TYPE_BOX) {
    CRDMEGeometry_Box  box;
    geom->type=type;
    geom->ops.crdmegeom_destroy              = CRDMEGeometryDestroy_Box;
    geom->ops.crdmegeom_nodegetcoords        = CRDMEGeometryNodeGetCoordinates_Box;
    geom->ops.crdmegeom_nodegetvoxel         = CRDMEGeometryNodeGetVoxel_Box;
    geom->ops.crdmegeom_nodegetvoxelarea     = CRDMEGeometryNodeGetVoxelArea_Box;
    geom->ops.crdmegeom_nodegetedgeweights   = CRDMEGeometryNodeGetEdgeWeights_Box;
    geom->ops.crdmegeom_nodegetneighbors     = CRDMEGeometryNodeGetNeighbors_Box;
    geom->ops.crdmegeom_nodegetnumneighbors  = CRDMEGeometryNodeGetNumNeighbors_Box;
    geom->ops.crdmegeom_voxelgetarea         = CRDMEGeometryVoxelGetArea_Box;
    geom->ops.crdmegeom_nodesgetdistancesqr  = CRDMEGeometryNodesGetDistanceSqr_Box;
    geom->ops.crdmegeom_pointsgetdistancesqr = CRDMEGeometryPointsGetDistanceSqr_Box;
    geom->ops.crdmegeom_samplefromvoxel      = CRDMEGeometrySampleFromVoxel_Box;
    geom->ops.crdmegeom_iteratevoxelnbhd     = CRDMEGeometryIterateVoxelNbhd;
    geom->ops.crdmegeom_setup                = CRDMEGeometrySetUp_Box;
    ierr       = PetscMalloc1(1,&box);CHKERRQ(ierr);
    geom->data = box;
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Geometry type %d not implemented\n",type);CHKERRQ(ierr); /* TODO: should be a string */
    return 1;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySetUp(CRDMEGeometry geom) { 
  
  PetscFunctionBegin;

  geom->setupCalled=PETSC_TRUE;
  if(geom->ops.crdmegeom_setup) {
    CHKERRQ( geom->ops.crdmegeom_setup(geom) ); 
  } else {
    PetscPrintf(PETSC_COMM_SELF,"Set up not implemented\n");
    return 1;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometrySetDimension(CRDMEGeometry geom,PetscInt dim) {

  PetscFunctionBegin;

  geom->dim = dim;
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetCoordinates(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) { 
  
  CHKERRQ( geom->ops.crdmegeom_nodegetcoords(geom,i,x) ); 

  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetVoxel(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomVoxel *voxel) { 
  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(geom->ops.crdmegeom_nodegetvoxel) {
    if(geom->work_voxel[0].i==i) {
      ierr = PetscMemcpy(voxel,&geom->work_voxel[0],sizeof(CRDMEGeomVoxel));CHKERRQ(ierr);
    } else if(geom->work_voxel[1].i==i) {
      ierr = PetscMemcpy(voxel,&geom->work_voxel[1],sizeof(CRDMEGeomVoxel));CHKERRQ(ierr);
    } else {
      CHKERRQ( geom->ops.crdmegeom_nodegetvoxel(geom,i,voxel) ); 
    }
    CHKERRQ( geom->ops.crdmegeom_nodegetvoxel(geom,i,voxel) ); 
  } else { 
    PetscPrintf(PETSC_COMM_WORLD,"Get voxel not implemented\n");
    return 1;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetEdgeWeights(CRDMEGeometry geom, PetscInt i, PetscInt *nn, PetscInt *indices, PetscReal *data) { 
  
  CHKERRQ( geom->ops.crdmegeom_nodegetedgeweights(geom,i,nn,indices,data) ); 

  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetNumNeighbors(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn) { 
  
  CHKERRQ( geom->ops.crdmegeom_nodegetnumneighbors(geom,i,nn) ); 
  
  return(0);
}

PetscErrorCode CRDMEGeometryNodeGetNeighbors(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscInt *nn, CRDMEGeomGloIdx *indices) { 
  
  PetscFunctionBegin;

  CHKERRQ( geom->ops.crdmegeom_nodegetneighbors(geom,i,nn,indices) ); 

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryVoxelGetArea(CRDMEGeometry geom, CRDMEGeomVoxel voxel, PetscReal *area) {
  
  PetscFunctionBegin;
  
  CHKERRQ( geom->ops.crdmegeom_voxelgetarea(geom,voxel,area) ); 

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryNodeGetVoxelArea(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *area) {
  
  PetscFunctionBegin;
  
  CHKERRQ( geom->ops.crdmegeom_nodegetvoxelarea(geom,i,area) ); 

  PetscFunctionReturn(0);
}


inline PetscErrorCode CRDMEGeometryNodesGetDistanceSqr(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *dist) {
  
  CHKERRQ( geom->ops.crdmegeom_nodesgetdistancesqr(geom,i,j,dist) ); 

  return(0);
}

PetscErrorCode CRDMEGeometryPointsGetDistanceSqr(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomPoint xb, PetscReal *dist) {
    
  CHKERRQ( geom->ops.crdmegeom_pointsgetdistancesqr(geom,x,xb,dist) ); 

  return(0);
}

PetscErrorCode CRDMEGeometrySampleFromVoxel(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomPoint x) {
  
  PetscFunctionBegin;
  
  if( geom->ops.crdmegeom_samplefromvoxel) {
    CHKERRQ( geom->ops.crdmegeom_samplefromvoxel(geom,i,x) );
  } else {
    PetscPrintf(PETSC_COMM_WORLD,"Sample from voxel not implemented\n");
    return 1;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryGetDomainVolume(CRDMEGeometry geom,PetscReal *V) {
  
  PetscErrorCode ierr;
  PetscReal      mass;
  PetscFunctionBegin;
  
  *V = 0.0;
  for(PetscInt i = 0; i < geom->N; i++) {
    ierr = CRDMEGeometryNodeGetVoxelArea(geom,i,&mass);CHKERRQ(ierr);
    *V += mass;
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea_1d(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) {
  PetscReal v[2] = {voxel.nodes[0][0]-x[0],voxel.nodes[1][0]-x[0]}; /* shift the coordinates, then do the interesection of [v[0],v[1]] with [-r,r] */

  PetscFunctionBegin;

  if(v[0] > radius || v[1] < -radius) {
    *area = 0;
    PetscFunctionReturn(0);
  } else if(v[0] > -radius && v[1] < radius) {
    *area = v[1] - v[0];
    PetscFunctionReturn(0);
  } else if(v[0] <= -radius && v[1] >= radius) {
    *area = 2*radius;
    PetscFunctionReturn(0);
  } else if(v[1] < radius) {
    *area = v[1] + radius;
    PetscFunctionReturn(0);
  } else { 
    *area = radius - v[0];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
} 

/* 

Minimizers of ||x - [bt + (1-t)a]||^2

*/

PetscReal MinDistanceSqrSegment_2d(CRDMEGeomPoint x, CRDMEGeomPoint a, CRDMEGeomPoint b) {
  PetscReal dot,nm,xmin,ymin,t;
  PetscReal xshift0,xshift1,bshift0,bshift1;

  xshift0 = x[0] - a[0];
  xshift1 = x[1] - a[1];
  bshift0 = b[0] - a[0];
  bshift1 = b[1] - a[1];
  dot = xshift0*bshift0 + xshift1*bshift1;
  nm  = PetscSqr(bshift0) + PetscSqr(bshift1);
  if(dot <= 0.0) {
      return PetscSqr(xshift0) + PetscSqr(xshift1);
  } else if(dot >= nm) {
      return PetscSqr(xshift0 - bshift0) + PetscSqr(xshift1 - bshift1);
  } else { 
      t = dot / nm;
      xmin = t*bshift0;
      ymin = t*bshift1;
      return PetscSqr(xmin - xshift0) + PetscSqr(ymin - xshift1);
  }
}

PetscErrorCode MinDistanceSqr_2d(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomVoxel voxel, PetscReal *dist) {
  
  PetscInt len = voxel.length;

  PetscFunctionBegin;

  *dist=PETSC_MAX_REAL;
  for(PetscInt l=0;l<len-1;l++) {
    *dist=PetscMin(*dist,MinDistanceSqrSegment_2d(x,voxel.nodes[l],voxel.nodes[l+1]));
    if(*dist<PETSC_SMALL) {
      PetscFunctionReturn(0);
    }
  }
  *dist=PetscMin(*dist,MinDistanceSqrSegment_2d(x,voxel.nodes[len-1],voxel.nodes[0]));

  PetscFunctionReturn(0);
}

PETSC_STATIC_INLINE void PointMinus(CRDMEGeomPoint x, CRDMEGeomPoint y, CRDMEGeomPoint z) {
  z[0] = x[0] - y[0];
  z[1] = x[1] - y[1];
  z[2] = x[2] - y[2];
}

PETSC_STATIC_INLINE PetscReal PointDot(CRDMEGeomPoint x, CRDMEGeomPoint y) {
  return x[0]*y[0] + x[1]*y[1] + x[2]*y[2];
}

PetscReal MinDistanceSqr_Triangle_3d(CRDMEGeomPoint x0, CRDMEGeomPoint p0, CRDMEGeomPoint q0, CRDMEGeomPoint r0) {

  CRDMEGeomPoint p,q,r;
  PetscReal      a,b,c,d,e,f,s,t,det,numer,denom,tmp0,tmp1;

  PointMinus(p0,r0,p);
  PointMinus(q0,r0,q);
  PointMinus(r0,x0,r);
  
  a = PointDot(p,p);
  b = PointDot(p,q);
  c = PointDot(q,q);
  d = PointDot(p,r);
  e = PointDot(q,r);
  f = PointDot(r,r);

  s   = b*e - c*d;
  t   = b*d - a*e;
  det = PetscAbs(a*c - b*b);

  if(s + t <= det) {
    if(s < 0) {
      if(t < 0) { 
        //region 4
        if(d < 0) {
          t = 0;
          if(a + d <= 0) {
            s = 1;
          } else {
            s = -d / a;
          }
        } else {
          s = 0;
          if(e >= 0) {
            t = 0;
          } else if(-e >= c) {
            t = 1;
          } else {
            t = -e / c;
          }
        }
      } else {
        //region 3
        s = 0;
        if(e >= 0) {
          t = 0;
        } else if(-e >= c) {
          t = 1;
        } else {
          t = -e / c;
        }
      }
    } else if(t < 0) {
      //region 5
      t = 0;
      if(d >= 0) {
        s = 0;
      } else if(a + d <= 0) {
        s = 1;
      } else {
        s = -d / a;
      }
    } else {
      //region 0
      s /= det;
      t /= det;
    }
  } else {
    if(s < 0) {
      //region 2
      tmp0 = b + d;
      tmp1 = c + e;
      if(tmp1 > tmp0) {
        numer = tmp1 - tmp0;
        denom = a - 2*b + c;
        if(numer >= denom) {
          s = 1;
        } else {
          s = numer / denom;
        }
        t = 1 - s;
      } else {
        s = 0;
        if(tmp1 <= 0) {
          t = 1;
        } else if(e >= 0) {
          t = 0;
        } else {
          t = -e / c;
        }
      }
    } else if(t < 0) {
      //region 6
      tmp0 = b + e;
      tmp1 = a + d;
      if(tmp1 > tmp0) {
        numer = tmp1 - tmp0;
        denom = a - 2*b + c;
        if(numer >= denom) {
          t = 1;
        } else {
          t = numer / denom;
        }
        s = 1 - t;
      } else {
        t = 0;
        if(tmp1 <= 0) {
          s = 1;
        } else if(d >= 0) {
          s = 0;
        } else {
          s = -d / a;
        }
      }
    } else {
      //region 1
      numer = (c + e) - (b + d);
      if(numer <= 0) {
        s = 0;
      } else {
        denom = a - 2*b + c;
        if(numer >= denom) {
          s = 1;
        } else {
          s = numer / denom;
        }
      }
      t = 1 - s;
    }
  }

  //printf("%1.15f %1.15f %1.15f\n",s,t,a*s*s + 2*b*s*t + c*t*t + 2*d*s + 2*e*t + f);
  return a*s*s + 2*b*s*t + c*t*t + 2*d*s + 2*e*t + f;
}

PetscErrorCode MinDistanceSqr_3d(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomVoxel voxel, PetscReal *dist) {
  
  PetscInt       len = voxel.length;

  PetscFunctionBegin;

  *dist=PETSC_MAX_REAL;
  for(PetscInt l=0;l<len-1;l++) {
    if((l+1) % 7) {
      if((l+2)%7) {
        *dist=PetscMin(*dist,MinDistanceSqr_Triangle_3d(x,voxel.nodes[l],voxel.nodes[l+1],voxel.nodes[7*(l / 7) + 6]));
      } else {
        *dist=PetscMin(*dist,MinDistanceSqr_Triangle_3d(x,voxel.nodes[l],voxel.nodes[l-5],voxel.nodes[7*(l / 7) + 6]));
      }
     if(*dist<PETSC_SMALL) {
        PetscFunctionReturn(0);
      }
    }
  }

  PetscFunctionReturn(0);
}

/* return d = x' - y where x' is the closest periodioc rep of x to y*/

PETSC_STATIC_INLINE PetscReal SignedPerDistReal(PetscReal x, PetscReal y, PetscReal p) {
  PetscReal dist=PetscAbs(x-y),pdist=p-dist;

  if(x <= y && dist <= pdist) {  /* -|x - y| = x - y */
    return -dist;
  } else if(x > y && dist <= pdist) {
    return dist;
  } else if(x <= y) { /* p - |x - y| = p - x + y */
    return pdist;
  } else {
    return -pdist;
  }
}

/* return x' where x' is the closest periodioc rep of x to y */

PETSC_STATIC_INLINE PetscReal ClosestPerCoordReal(PetscReal x, PetscReal y, PetscReal p) {
  PetscReal dist=PetscAbs(x-y),pdist=p-dist;

  if(dist <= pdist) { 
    return x;
  }
  return x + (x <= y ? p : -p);
}


PetscErrorCode MinDistanceSqr_Box(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomVoxel voxel, PetscReal *dist) {
  CRDMEGeometry_Box box = geom->data;
  PetscReal min,max;

  PetscFunctionBegin;

  *dist = 0.0;
  for(PetscInt d=0;d<geom->dim;d++) {
    min=voxel.nodes[0][d];
    max=voxel.nodes[1][d];
    if(!box->periodicity[d]) {
      *dist += PetscSqr(PetscMin(x[d] - min,0.0));
      *dist += PetscSqr(PetscMax(x[d] - max,0.0));
    } else {
      *dist += PetscSqr(PetscMin(SignedPerDistReal(x[d],min,box->L[d]),0.0));
      *dist += PetscSqr(PetscMax(SignedPerDistReal(x[d],max,box->L[d]),0.0));
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryMinDistanceSqr(CRDMEGeometry geom, CRDMEGeomPoint x, CRDMEGeomVoxel voxel, PetscReal *dist) {
  
  PetscErrorCode ierr;

  PetscFunctionBegin; 

  if(geom->type==GEOMETRY_TYPE_BOX) {
    ierr = MinDistanceSqr_Box(geom,x,voxel,dist);CHKERRQ(ierr);
  } else if(geom->dim==2) {
    if(PetscAbs(x[0] - voxel.nodes[voxel.length][0]) + PetscAbs(x[1] - voxel.nodes[voxel.length][1]) < 1e-12) {
      *dist=0.0;
      PetscFunctionReturn(0);
    } else {
      ierr = MinDistanceSqr_2d(geom,x,voxel,dist);CHKERRQ(ierr);
    }
  } else if(geom->dim==3) {
    if(PetscAbs(x[0] - voxel.nodes[voxel.length][0]) + PetscAbs(x[1] - voxel.nodes[voxel.length][1]) +  PetscAbs(x[2] - voxel.nodes[voxel.length][2])< 1e-12) {
      *dist = 0.0;
      PetscFunctionReturn(0);
    }
    ierr = MinDistanceSqr_3d(geom,x,voxel,dist);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_SELF,"Min distance squared not implemented for unstructured mesh in dimension %d\n",geom->dim);CHKERRQ(ierr); /* TODO: should be a string */
    PetscFunctionReturn(1);
  }

  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea_2d(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) { /* rename: polygon circle intersection area? */
  PetscInt   len=voxel.length;
  PetscReal  intersections[4*len],angles[2*len+1];
  PetscReal  A=0.0;
  PetscReal  x1,x2,y1,y2,dx1,dx2,a,b,c,det,t1,t2,t,n1,n2,m1,m2,i1,i2,ang;
  PetscReal  rsqr=PetscSqr(radius);
  PetscInt   int_cnt=0,i,j,m=0,n=0;
  PetscBool  inside;
  PetscBool  b1,b2;

  PetscFunctionBegin;
  x[0] += PETSC_PI*1e-12; /* perturb the center randomly to avoid weirdness */
  x[1] += PETSC_PI*1e-12;
  x1 = voxel.nodes[len-1][0]-x[0];
  x2 = voxel.nodes[len-1][1]-x[1];
  inside = PetscSqr(x1) + PetscSqr(x2) <= rsqr;
  for(i = 0; i < len; i++) {
    if(i > 0) {
      x1 = voxel.nodes[i-1][0]-x[0];
      x2 = voxel.nodes[i-1][1]-x[1];
    }
    y1  = voxel.nodes[i][0]-x[0];
    y2  = voxel.nodes[i][1]-x[1];
    dx1 = y1 - x1;
    dx2 = y2 - x2;
    b = x1*dx1 + x2*dx2;
    a = dx1*dx1 + dx2*dx2;
    c = x1*x1 + x2*x2 - rsqr;
    det = b*b - a*c;
    if(det >= 0) {
      det = PetscSqrtReal(det);
      if(b <= 0) { /* numerically stable quadratic formula: avoid subtracting nearly equal quantities. */
        b  = det - b;
        t1 =  c/b;     /* t1 < t2 */
        t2  = b/a;      
      } else {
        b  = -(b + det);
        t1 = b/a;
        t2 = c/b;
      }
      b1 = (t1 >= 0.0 && t1 <= 1.0);
      b2 = (t2 >= 0.0 && t2 <= 1.0 && t1 != t2);
      if(b1) {
        intersections[2*int_cnt]   = x1 + t1*dx1;
        intersections[2*int_cnt+1] = x2 + t1*dx2;
        ang = PetscAtan2Real(-intersections[2*int_cnt+1], -intersections[2*int_cnt] ) + PETSC_PI;
        angles[int_cnt] = ang;
        if(int_cnt > 0) {
          for(j = int_cnt-1; j > -1; j--) {
            if(ang < angles[j]) {
              angles[j+1] = angles[j];
              if(j == 0) {
                angles[0] = ang;
                m = int_cnt;
                n = i;
                break;
              }
            } else {
              angles[j+1] = ang;
              break;
            }
          }
        } else {
          n = i;
        }
 
        int_cnt++;

      }
      if(b2) {
        intersections[2*int_cnt]   = x1 + t2*dx1;
        intersections[2*int_cnt+1] = x2 + t2*dx2;
        ang = PetscAtan2Real(-intersections[2*int_cnt+1], -intersections[2*int_cnt] ) + PETSC_PI;
        angles[int_cnt] = ang;
        if(int_cnt > 0) {
          for(j = int_cnt-1; j > -1; j--) {
            if(ang < angles[j]) {
              angles[j+1] = angles[j];
              if(j == 0) {
                angles[0] = ang;
                m = int_cnt;
                n = i;
                break;
              }
            } else {
              angles[j+1] = ang;
              break;
            }
          }
        } else {
          n = i;
        }
        int_cnt++;
      }
      if(b1 && b2) {
        n1 = dx2;
        n2 = -dx1;
        m1 =  intersections[2*int_cnt - 4] + intersections[2*int_cnt - 2];
        m2 =  intersections[2*int_cnt - 3] + intersections[2*int_cnt - 1];
        A += .5*PetscAbsReal(t1 - t2)*(m1*n1 + m2*n2); 
        inside = 0;
      } else if( b1 || b2 ) {
        t  = b1 ? t1 : t2;
        n1 = dx2;
        n2 = -dx1;
        i1 = intersections[2*int_cnt - 2];
        i2 = intersections[2*int_cnt - 1];
        if(inside) {
          m1 = i1 + x1;
          m2 = i2 + x2;
          inside = 0;
        } else {
          m1 = i1 + y1;
          m2 = i2 + y2;
          t = 1.0-t;
          inside=1;
        }
        A += .5*t*(m1*n1 + m2*n2); 
      } else if(inside) {
        n1 = dx2;
        n2 = -dx1;
        m1 = y1 + x1;
        m2 = y2 + x2;
        A += .5*(m1*n1 + m2*n2);
      }
    } else if(inside) {
      n1 = dx2;
      n2 = -dx1;
      m1 = y1 + x1;
      m2 = y2 + x2;
      A += .5*(m1*n1 + m2*n2);
    }
  }
  
  if(!int_cnt) {
    if(inside) {
      *area = .5*A;
      PetscFunctionReturn(0);
    } else {
      *area = -1.0;
      PetscFunctionReturn(0);
    }
  } else {
    inside = 0;
    angles[int_cnt] = angles[0] + 2.0*PETSC_PI;
    i1 = intersections[2*m];
    i2 = intersections[2*m+1];
    x1 = voxel.nodes[n][0] - x[0];
    x2 = voxel.nodes[n][1] - x[1];
    dx1 = x1 - i1;
    dx2 = x2 - i2;
    if(i1*dx1 + i2*dx2 >= 0.0) {
      inside = 1;
    }
    for(i = 0; i < int_cnt; i++) {
      if(inside) {
        A += rsqr*(angles[i+1] - angles[i]);
      }
      inside = !inside;
    }
  }
  
  *area = .5*A;

  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea_3d_gcon(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) { 

  PetscInt       len = voxel.length;
  CRDMEGeomPoint tet[4];

  PetscFunctionBegin;

  tet[0][0] = voxel.nodes[len][0]; tet[0][1] = voxel.nodes[len][1]; tet[0][2] = voxel.nodes[len][2];
  *area = 0.0;
  for(PetscInt l=0;l<len-1;l++) {
    if((l+1) % 7) {
      tet[1][0] = voxel.nodes[l][0]; tet[1][1] = voxel.nodes[l][1]; tet[1][2] = voxel.nodes[l][2];
      tet[2][0] = voxel.nodes[7*(l / 7) + 6][0]; tet[2][1] = voxel.nodes[7*(l / 7) + 6][1]; tet[2][2] = voxel.nodes[7*(l / 7) + 6][2];
      if((l+2)%7) {
        tet[3][0] = voxel.nodes[l+1][0]; tet[3][1] = voxel.nodes[l+1][1]; tet[3][2] = voxel.nodes[l+1][2];
      } else {
        tet[3][0] = voxel.nodes[l-5][0]; tet[3][1] = voxel.nodes[l-5][1]; tet[3][2] = voxel.nodes[l-5][2];
      }
      
      *area += sphereTetIntersection(x,radius,tet);
    }
  }

  PetscFunctionReturn(0);
}

/*
PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea_3d_gcon(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) { 

  PetscInt       len = voxel.length;
  CRDMEGeomPoint pent[5];

  PetscFunctionBegin;

  pent[0][0] = voxel.nodes[len][0]; pent[0][1] = voxel.nodes[len][1]; pent[0][2] = voxel.nodes[len][2];
  *area = 0.0;
  for(PetscInt l=0; l < len / 7; l+=7) {
    pent[4][0] = voxel.nodes[l+6][0]; pent[4][1] = voxel.nodes[l+6][1]; pent[4][2] = voxel.nodes[l+6][2]; 
    for(PetscInt i = 0; i < 5; i+=2) {
      pent[1][0] = voxel.nodes[l+(i%6)][0]; pent[1][1] = voxel.nodes[l+(i%6)][1]; pent[1][2] = voxel.nodes[l+(i%6)][2]; 
      pent[3][0] = voxel.nodes[l+((i+2)%6)][0]; pent[3][1] = voxel.nodes[l+((i+2)%6)][1]; pent[3][2] = voxel.nodes[l+((i+2)%6)][2]; 
      pent[2][0] = voxel.nodes[l+i+1][0]; pent[2][1] = voxel.nodes[l+i+1][1]; pent[2][2] = voxel.nodes[l+i+1][2]; 
      for(int i =0;i<5;i++) {
        printf("  %f %f %f\n",pent[i][0],pent[i][1],pent[i][2]);
      }
      *area += spherePentIntersection(x,radius,pent);
    }
  }

  PetscFunctionReturn(0);
}
*/
PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea_3d_box(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) { 

  PetscFunctionBegin;

  *area = sphereBoxIntersection(x,radius,voxel.nodes);

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea(CRDMEGeometry geom, CRDMEGeomPoint x, PetscScalar radius, CRDMEGeomVoxel voxel, PetscReal *area) {

  PetscErrorCode ierr;

  PetscFunctionBegin;

  switch(geom->dim) {
    case(1):
      if(geom->type==GEOMETRY_TYPE_BOX) {
        CRDMEGeometry_Box box = geom->data;
        PetscReal d=voxel.nodes[1][0] - voxel.nodes[0][0];
        voxel.nodes[0][0]=box->periodicity[0] ? ClosestPerCoordReal(voxel.nodes[0][0],x[0],box->L[0]) : voxel.nodes[0][0];        
        voxel.nodes[1][0]=voxel.nodes[0][0] + d;
      }
      ierr = CRDMEGeometryVoxelSphereIntersectionArea_1d(geom,x,radius,voxel,area);CHKERRQ(ierr);
      break;
    case(2):
      if(geom->type==GEOMETRY_TYPE_BOX) {
        CRDMEGeometry_Box box = geom->data;
        CRDMEGeomVoxel    v;
        CRDMEGeomPoint    vn[geom->voxelNodesSize];

        v.length=geom->voxelNodesSize;
        v.nodes =vn;
      
        vn[0][0]=box->periodicity[0] ? ClosestPerCoordReal(voxel.nodes[0][0],x[0],box->L[0]) : voxel.nodes[0][0];        
        vn[1][0]=vn[0][0] + voxel.nodes[1][0] - voxel.nodes[0][0];
        vn[2][0]=vn[1][0];
        vn[3][0]=vn[0][0];

        vn[0][1]=box->periodicity[1] ? ClosestPerCoordReal(voxel.nodes[0][1],x[1],box->L[1]) : voxel.nodes[0][1];        
        vn[1][1]=vn[0][1];
        vn[2][1]=vn[0][1] + voxel.nodes[1][1] - voxel.nodes[0][1];
        vn[3][1]=vn[2][1];
        ierr = CRDMEGeometryVoxelSphereIntersectionArea_2d(geom,x,radius,v,area);CHKERRQ(ierr); /* rename: polygon circle intersection area? */
      } else {
        ierr = CRDMEGeometryVoxelSphereIntersectionArea_2d(geom,x,radius,voxel,area);CHKERRQ(ierr); /* rename: polygon circle intersection area? */
      }
      break;
    case(3):
    if(geom->type==GEOMETRY_TYPE_BOX) {
      ierr = CRDMEGeometryVoxelSphereIntersectionArea_3d_box(geom,x,radius,voxel,area);CHKERRQ(ierr); /* rename: polygon circle intersection area? */
    } else if(geom->type==GEOMETRY_TYPE_GCON) {
      ierr = CRDMEGeometryVoxelSphereIntersectionArea_3d_gcon(geom,x,radius,voxel,area);CHKERRQ(ierr); /* rename: polygon circle intersection area? */
    }
    break;
    default:
      ierr = PetscPrintf(PETSC_COMM_SELF,"Voxel sphere intersection not implmeented in dimension %d for geometry type %d\n",geom->dim,geom->type);CHKERRQ(ierr); /* TODO: should be a string */
      return 1;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode _p_BFS(CRDMEGeometry geom, PetscInt begin, PetscInt end, PetscInt *count, PetscReal radius, VoxelNbhdCallback f, void *ctx) {
  PetscInt           Aj[geom->maxNodeDegree];
  PetscInt           nn,j,c=0,idx;
  PetscReal          d,rsqr=PetscSqr(radius+2*geom->hmax); 
  PetscBool          exit=PETSC_FALSE,cap=PETSC_FALSE;
  PetscBool          *visited=geom->visitedArr;
  CRDMEGeomGloIdx    *q      =geom->qArr;
  PetscErrorCode     ierr;
  
  PetscFunctionBegin;  

  *count+=end-begin;
  for(PetscInt i=begin;i<end;i++) {
    idx  = q[i];
    ierr = f(geom,q[0],idx,radius,ctx,&cap,&exit);CHKERRQ(ierr);
    if(exit) {
      *count+=c;
      PetscFunctionReturn(0);
    } else if(!cap) {
      ierr = CRDMEGeometryNodeGetNeighbors(geom,idx,&nn,Aj);CHKERRQ(ierr);
      for(PetscInt e=0;e<nn;e++) {
        j = Aj[e];
        ierr = CRDMEGeometryNodesGetDistanceSqr(geom,q[0],j,&d);CHKERRQ(ierr);
        if(!visited[j] && d < rsqr) {
          visited[j]=PETSC_TRUE;
          q[end+c]  =j;/*queue it*/
          c++;
        }
      }
    } 
  }

  if(c) {
    ierr=_p_BFS(geom,end,end+c,count,radius,f,ctx);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode CRDMEGeometryIterateVoxelNbhd(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscScalar radius, VoxelNbhdCallback f, void *ctx) {
  CRDMEGeomGloIdx *q=geom->qArr;
  PetscBool       *visited=geom->visitedArr;
  PetscInt        count=0;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  if(!visited) {
    ierr = PetscMalloc1(geom->N,&geom->visitedArr);CHKERRQ(ierr);
    ierr = PetscMalloc1(geom->N,&geom->qArr);CHKERRQ(ierr);
    ierr = PetscArrayzero(geom->visitedArr,geom->N);CHKERRQ(ierr);
    q=geom->qArr;
    visited=geom->visitedArr;
  }

  q[0]       = i;
  visited[i] = PETSC_TRUE;
  ierr = _p_BFS(geom,0,1,&count,radius,f,ctx);CHKERRQ(ierr);
  for(PetscInt k=0;k<count;k++) {
    visited[q[k]] = PETSC_FALSE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryIterateVoxelStencil(CRDMEGeometry geom, CRDMEGeomGloIdx i, SpatIaStencil stencil, VoxelNbhdCallback f, PetscInt n, void *ctx) {
  CRDMEGeomGloIdx j;
  PetscErrorCode  ierr;
  PetscBool       cap=PETSC_FALSE,exit=PETSC_FALSE;
  PetscScalar     val;

  PetscFunctionBegin;
  
  if(stencil->boxPeriodic) {
    ierr = CRDMEGeomBoxIteratePeriodicStencil(geom,i,stencil,f,n,ctx);CHKERRQ(ierr);
  } else {
    for(PetscInt jj=stencil->rowPtr[i];jj<stencil->rowPtr[i+1];jj++) {
      j = stencil->indices[jj];
      val = stencil->vals[n] ? stencil->vals[n][jj] : stencil->constantVals[n];
      ierr = f(geom,i,j,val,ctx,&cap,&exit);CHKERRQ(ierr);
      if(exit) {
        break;
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode computeStencilSizeCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  computeStencilIndicesCtx *c=ctx;  
  PetscBool                in=PETSC_FALSE;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = c->VoxelIsInStencil(geom,i,j,&in,c->ctx);CHKERRQ(ierr);
  if(in) {
    c->count++;
  } else {
    *cap=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode computeStencilIndicesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  computeStencilIndicesCtx *c=ctx;  
  PetscBool                in=PETSC_FALSE;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = c->VoxelIsInStencil(geom,i,j,&in,c->ctx);CHKERRQ(ierr);
  if(in) {
    c->stencil->indices[c->count + c->offset]=j;
    c->count++;
  } else {
    *cap=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryComputeStencilIndices(CRDMEGeometry geom, SpatIaStencil stencil, PetscScalar radius, MeshIaFnBool isInStencil, void *isInStencilCtx) {

  PetscErrorCode              ierr;
  PetscInt                    N=geom->N;
  computeStencilIndicesCtx    ctx;

  PetscFunctionBegin;

  ctx.stencil          = stencil;
  ctx.VoxelIsInStencil = isInStencil;
  ctx.ctx              = isInStencilCtx;
  ctx.count            = 0;
  stencil->stencilLen  = 0;
  stencil->boxPeriodic = PETSC_TRUE;

  if(geom->type==GEOMETRY_TYPE_BOX) {
    CRDMEGeometry_Box box = geom->data;
    for(PetscInt d = 0; d < geom->dim; d++) {
      stencil->boxPeriodic = (box->periodicity[d] && stencil->boxPeriodic);
    }
  } else {
    stencil->boxPeriodic = PETSC_FALSE;
  }

  if(!stencil->boxPeriodic) {
    PetscInt                    Np,Nf,ntot,locsize;
    PetscInt                    offset=0;
    MPI_Comm                    nodecomm,glob_comm;
    PetscShmComm                petsc_nodecomm;
    PetscMPIInt                 windisp,np,rank,noderank,nodesize;
    MPI_Aint                    winsize;

    ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&np);CHKERRQ(ierr);

    ierr = PetscCommDuplicate(PETSC_COMM_WORLD,&glob_comm,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscShmCommGet(glob_comm,&petsc_nodecomm);CHKERRQ(ierr);
    ierr = PetscShmCommGetMpiShmComm(petsc_nodecomm,&nodecomm);CHKERRQ(ierr);
    ierr = MPI_Comm_rank(nodecomm, &noderank);
    ierr = MPI_Comm_size(nodecomm, &nodesize);

    locsize = ( noderank == 0 ? N+1 : 0);
    MPI_Info_create(&stencil->win_info);
    MPI_Info_set(stencil->win_info, "alloc_shared_noncontig", "true");
    stencil->rowPtr = PETSC_NULL;
    ierr = MPI_Win_allocate_shared(locsize*sizeof(PetscInt), sizeof(PetscInt),
                stencil->win_info,nodecomm,&stencil->rowPtr,&stencil->rowPtrWin);CHKERRQ(ierr);

    if(noderank){
      ierr = MPI_Win_shared_query(stencil->rowPtrWin,0,&winsize,&windisp,&stencil->rowPtr);CHKERRQ(ierr);
    }

    if(!stencil->rowPtr) {
      PetscPrintf(PETSC_COMM_SELF,"error: memory could not be allocated for stencil row pointer.\n");
      return 1;
    }

    ierr = MPI_Win_fence(0,stencil->rowPtrWin);CHKERRQ(ierr);

    Np = N / nodesize;
    Nf = (noderank == nodesize - 1 ? N : Np*(noderank+1) );

    for(PetscInt i = Np*noderank; i < Nf; i++) {
      if(i==0) {
        stencil->rowPtr[i]=0;
      }
      ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,radius,computeStencilSizeCallback,&ctx);CHKERRQ(ierr);
      stencil->rowPtr[i+1]=ctx.count;
      stencil->stencilLen =PetscMax(stencil->rowPtr[i+1]-stencil->rowPtr[i],stencil->stencilLen);
    }

    MPI_Win_fence(0,stencil->rowPtrWin);
    for(PetscInt i = 1; i < noderank+1; i++) {
      offset += stencil->rowPtr[i*Np];
    }
    MPI_Win_fence(0,stencil->rowPtrWin);
    for(PetscInt i = Np*noderank; i < Nf; i++) stencil->rowPtr[i+1] += offset;
    MPI_Win_fence(0,stencil->rowPtrWin);

    ntot = stencil->rowPtr[N];
    locsize = ( noderank == 0 ? ntot : 0 );
    ierr = MPI_Win_allocate_shared(locsize*sizeof(CRDMEGeomGloIdx),sizeof(CRDMEGeomGloIdx),
                stencil->win_info,nodecomm,&stencil->indices,&stencil->indicesWin);CHKERRQ(ierr);

    if(noderank) {
      ierr = MPI_Win_shared_query(stencil->indicesWin,0,&winsize,&windisp,&stencil->indices);CHKERRQ(ierr);
    }

    if(!stencil->indices) {
      PetscPrintf(PETSC_COMM_SELF,"error: memory could not be allocated for stencil indices.\n");
      return 1;
    }

    ierr = MPI_Win_fence(0,stencil->indicesWin);CHKERRQ(ierr);

    ctx.offset = offset;
    ctx.count  = 0;
    for(PetscInt i=Np*noderank;i<Nf;i++) {
      ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,radius,computeStencilIndicesCallback,&ctx);CHKERRQ(ierr);
    }
    ierr = MPI_Win_fence(0,stencil->indicesWin);CHKERRQ(ierr);
  } else {
    stencil->rowPtr=PETSC_NULL;
    ierr = CRDMEGeometryIterateVoxelNbhd(geom,0,radius,computeStencilSizeCallback,&ctx);CHKERRQ(ierr);
    stencil->stencilLen=ctx.count;
    ctx.count  = 0;
    ctx.offset = 0;
    ierr = PetscMalloc1(geom->dim*stencil->stencilLen,&stencil->indices);CHKERRQ(ierr);
    ierr = CRDMEGeometryIterateVoxelNbhd(geom,0,radius,computeStencilIndicesCallback_PeriodicBox,&ctx);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode computeStencilValuesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar nullval, void *ctx, PetscBool *cap, PetscBool *exit) {
  computeStencilValuesCtx *c=ctx;  
  PetscScalar              rsqr,val;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  if(!c->boxPeriodic || i==0) {
    if(c->meshFn) {
      ierr = c->meshFn(geom,i,j,&val,c->meshFnCtx);CHKERRQ(ierr);
    } else if(c->spatIaFn) {
      ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
      val = c->spatIaFn(rsqr,c->spatIaFnCtx);
    } else {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_NULL,"Could not compute stencil values; no function handle given to compute values.\n");
    }
    c->vals[c->count + c->offset] = val;
    c->count++;
  } else {
    *exit = PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryComputeStencilValues(CRDMEGeometry geom, SpatIaStencil stencil, MeshIaFn meshFn, void *meshFnCtx, SpatialIaFn spatIaFn, void *spatIaFnCtx, PetscInt n, PetscBool symmetrize, PetscBool accumulate) {
  
  PetscErrorCode         ierr;
  PetscInt               N=geom->N;
  CRDMEGeomGloIdx        j;
  PetscReal              mass;
  PetscBool              match;
  computeStencilValuesCtx ctx;

  PetscFunctionBegin;

  ctx.meshFn=meshFn;
  ctx.meshFnCtx=meshFnCtx;
  ctx.spatIaFn=spatIaFn;
  ctx.spatIaFnCtx = spatIaFnCtx;
  ctx.boxPeriodic  = stencil->boxPeriodic;

  if(!stencil->boxPeriodic) {
    PetscInt                    Np,Nf,locsize;
    MPI_Comm                    nodecomm,glob_comm;
    PetscShmComm                petsc_nodecomm;
    PetscMPIInt                 windisp,noderank,nodesize;
    MPI_Aint                    winsize;

    ierr = PetscCommDuplicate(PETSC_COMM_WORLD,&glob_comm,NULL);CHKERRQ(ierr);
    ierr = PetscShmCommGet(glob_comm,&petsc_nodecomm);CHKERRQ(ierr);
    ierr = PetscShmCommGetMpiShmComm(petsc_nodecomm,&nodecomm);CHKERRQ(ierr);
    ierr = MPI_Comm_rank(nodecomm, &noderank);
    ierr = MPI_Comm_size(nodecomm, &nodesize);

    locsize = ( noderank == 0 ? stencil->rowPtr[N] : 0 );
    ierr = MPI_Win_allocate_shared(locsize*sizeof(PetscReal),sizeof(PetscReal),
            stencil->win_info,nodecomm,&stencil->vals[n],&stencil->valsWin[n]);CHKERRQ(ierr);

    if(noderank) {
      ierr = MPI_Win_shared_query(stencil->valsWin[n],0,&winsize,&windisp,&stencil->vals[n]);CHKERRQ(ierr);
    }

    if(!stencil->vals[n]) {
      PetscPrintf(PETSC_COMM_SELF,"error: memory could not be allocated for stencil values %d.\n",n);
     return 1;
    }

    Np = N / nodesize;
    Nf = (noderank == nodesize - 1 ? N : Np*(noderank+1) );
    ctx.vals        = stencil->vals[n];
    ctx.count       = 0;
    ctx.offset = stencil->rowPtr[Np*noderank];
    ierr = MPI_Win_fence(0,stencil->valsWin[n]);CHKERRQ(ierr);
    for(PetscInt i=Np*noderank; i<Nf; i++) {
      ierr = CRDMEGeometryIterateVoxelStencil(geom,i,stencil,computeStencilValuesCallback,n,&ctx);CHKERRQ(ierr);
    }

    ierr = MPI_Win_fence(0,stencil->valsWin[n]);CHKERRQ(ierr);
    if(accumulate) {
      for(PetscInt i=Np*noderank; i<Nf; i++) {
        ierr = CRDMEGeometryNodeGetVoxelArea(geom,i,&mass);CHKERRQ(ierr);
        stencil->vals[n][stencil->rowPtr[i]] *= mass;
        for(PetscInt jj=stencil->rowPtr[i]+1;jj<stencil->rowPtr[i+1];jj++) {
          j = stencil->indices[jj];
          ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
          stencil->vals[n][jj] = stencil->vals[n][jj-1] + mass*stencil->vals[n][jj];
        }
      }
    }

    ierr = MPI_Win_fence(0,stencil->valsWin[n]);CHKERRQ(ierr);
    if(symmetrize) {
      for(PetscInt i=Np*noderank; i<Nf; i++) {
        for(PetscInt jj=stencil->rowPtr[i]+1;jj<stencil->rowPtr[i+1];jj++) {
          j = stencil->indices[jj];
          if(j < i) {
            match=PETSC_FALSE;
            for(PetscInt kk=stencil->rowPtr[j];kk<stencil->rowPtr[j+1];kk++) {
              if(stencil->indices[kk]==i) {
                stencil->vals[n][jj] = .5*(stencil->vals[n][jj] + stencil->vals[n][kk]);
                stencil->vals[n][kk] = stencil->vals[n][jj];
                match=PETSC_TRUE;
                break;
              }
            }
            if(!match) {
              ierr = PetscPrintf(PETSC_COMM_WORLD,"Requested stencil value symmetrization but stencil indices are not symmetric, index %d and index %d.\n",i,j);CHKERRQ(ierr);
              return 1;
            }
          }
        }
      }
    }
  } else {
    ierr = PetscMalloc1(stencil->stencilLen,&stencil->vals[n]);CHKERRQ(ierr);
    ctx.vals        = stencil->vals[n];
    ctx.count       = 0;
    ctx.offset      = 0;
    ierr = CRDMEGeometryIterateVoxelStencil(geom,0,stencil,computeStencilValuesCallback,n,&ctx);CHKERRQ(ierr);
    if(accumulate) {
      ierr = CRDMEGeometryNodeGetVoxelArea(geom,0,&mass);CHKERRQ(ierr);
      stencil->vals[n][0] *= mass;
      for(PetscInt jj=1;jj<stencil->stencilLen;jj++) {
        j = stencil->indices[jj];
        stencil->vals[n][jj] = stencil->vals[n][jj-1] + mass*stencil->vals[n][jj];
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode CRDMEGeometryView_ASCII(CRDMEGeometry geom) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Dimension %d CRDME geometry type %s with %d voxels and %d edges.\n",geom->dim,geom->type==GEOMETRY_TYPE_BOX ? "box" : "gcon",geom->N,geom->NEdges);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   Minimum and maximum x-coords: (%1.14f,%1.14f)\n",geom->coord_min[0],geom->coord_max[0]);CHKERRQ(ierr);
  if(geom->dim > 1) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"   Minimum and maximum y-coords: (%1.14f,%1.14f)\n",geom->coord_min[1],geom->coord_max[1]);CHKERRQ(ierr);
  }
  if(geom->dim > 2) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"   Minimum and maximum z-coords: (%1.14f,%1.14f)\n",geom->coord_min[2],geom->coord_max[2]);CHKERRQ(ierr);
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   hmin = %1.14f.\n",geom->hmin);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   hmax = %1.14f.\n",geom->hmax);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   maximum node degree is %d.\n",geom->maxNodeDegree);CHKERRQ(ierr);

  if(geom->type == GEOMETRY_TYPE_BOX) {
    CRDMEGeometry_Box box = geom->data;
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Box geometry with periodicity ");
    for(PetscInt d=0;d<geom->dim;d++) {
      ierr = PetscPrintf(PETSC_COMM_WORLD,"%s ",box->periodicity[0] ? "true" : "false");CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_WORLD,"and mx = %d",box->m[0]);
    if(geom->dim > 1) {
      ierr = PetscPrintf(PETSC_COMM_WORLD,", my = %d",box->m[1]);
    }
    if(geom->dim > 2) {
      ierr = PetscPrintf(PETSC_COMM_WORLD,", mz = %d",box->m[2]);
    }
    ierr = PetscPrintf(PETSC_COMM_WORLD,".\n");CHKERRQ(ierr);
  }

  return(0);
}




