/* 

reactionsystem: test0
Create and print a reaction system.

*/

#include <petsc.h>
#include <reactionsystem.h>

static inline PetscReal f(PetscScalar r, void* ctx) {
  return 0.0;
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Create a reaction systenm and print the result");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create reaction system.\n");CHKERRQ(ierr);
  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Create reaction system successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species A.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species A successful.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species B successful.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species C successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant A.\n");CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",1.0);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant A sucessful.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"B",2.0);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant B sucessful.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",3.0);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add species diffusion constant C sucessful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add creation reaction C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddCreation(rs,"C",4.2,"0->C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add creation reaction C successful.\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add creation reaction A.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddCreation(rs,"A",PETSC_PI,"0->A");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add creation reaction A successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add decay reaction A.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddDecay(rs,"A",42,"A->0");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add decay reaction A successful.\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add decay reaction B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddDecay(rs,"B",PETSC_PI,"B->0");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add decay reaction B successful.\n\n");CHKERRQ(ierr);
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add conversion reaction B,C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddConversion(rs,"B","C",PETSC_PI*PETSC_PI,"B->C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add conversion reaction B,C successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add fusion reaction A+B->C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddFusion(rs,"A","B","C",f,5.0,1.0,"A+B->C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add fusion reaction A+B->C successful.\n\n");CHKERRQ(ierr);  
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi fusion reaction A+C->B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddFusion(rs,"A","B","C",PETSC_NULL,31.0,1.0,"A+C->B");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add fusion reaction A+C->B successful.\n\n");CHKERRQ(ierr);  

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add fission reaction B->C+A.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddFission(rs,"B","C","A",f,5.0,1.0,"B->C+A");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi fusion reaction B->C+A successful.\n\n");CHKERRQ(ierr);  
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi fission reaction C->A+B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddFission(rs,"C","A","B",PETSC_NULL,5.0,1.0,"C->A+B");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi fusion reaction C->A+B successful.\n\n");CHKERRQ(ierr);  

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add annihilation reaction A,B.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddAnnihilation(rs,"A","B",f,5.0,1.0,"A+B->0");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add annihilation reaction A,B successful.\n\n");CHKERRQ(ierr);  
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi annihilation reaction B,C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddAnnihilation(rs,"B","C",PETSC_NULL,2.0,1.0,"B+C->0");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add annihilation reaction B,C successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add enzymatic reaction A+B->A+C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddEnzymatic(rs,"B","C","A",f,1.0,1.0,"A+B->A+C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add enzymatic reaction B->C+A successful.\n\n");CHKERRQ(ierr);  
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add Doi enzymatic reaction B+C->A+C.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddEnzymatic(rs,"B","A","C",PETSC_NULL,1.0,1.0,"B+C->A+C");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add enzymatic reaction B+C->A+C successful.\n\n");CHKERRQ(ierr);  

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add A,B pair potential.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"A","B",1.0,f);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add A,B pair potential successful.\n\n");CHKERRQ(ierr);  
 
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add B,C pair potential.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"B","C",1.0,f);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add B,C pair potential successful.\n\n");CHKERRQ(ierr);  

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add B,B pair potential.\n");CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"B","B",2.0,f);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Add B,B pair potential successful.\n\n");CHKERRQ(ierr); 
  
  ierr = PetscPrintf(PETSC_COMM_WORLD,"\nView reaction system.\n");CHKERRQ(ierr);
  ierr = ReactionSystemView(rs);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"View reaction system successful.\n\n");CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Reaction system destroy.\n");CHKERRQ(ierr);
  ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Destroy reaction system successful.\n\n");CHKERRQ(ierr);

  PetscFunctionReturn(0);
  
}