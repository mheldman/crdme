/*
 * reactionsystem.c
 *
 *  Created on: Jan 14, 2021
 *      Author: maxheldman
 */

#define _XOPEN_SOURCE
#include <reactionsystem.h>
#include <petsc.h>
#include <stdlib.h>
#include <stdio.h>
#include <crdmegeometry.h>

PetscErrorCode ReactionSystemCreate(ReactionSystem *rs) {

  ReactionSystem a;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscMalloc1(1,&a);CHKERRQ(ierr);
  a->nSpecies        = 0;
  a->spec            = NULL;
  a->nReactions      = 0;
  a->reac            = NULL;
  a->nPairPotentials = 0;
  a->pairPotentials  = NULL;
  a->data            = NULL;
  a->ctx             = NULL;
  *rs = a;
  PetscFunctionReturn(0);
}

PetscErrorCode SpeciesDestroy(RSSpecies *s) {

  PetscFunctionBegin;

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemDestroy(ReactionSystem *rs) {
  
  PetscInt       M = (*rs)->nSpecies;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  for(PetscInt i = 0; i < M; i++) {
    ierr = SpeciesDestroy(&(*rs)->spec[i]);CHKERRQ(ierr);
  }
  PetscFree((*rs)->spec);
  PetscFree((*rs)->reac);
  PetscFree((*rs)->data);
  PetscFree((*rs)->ctx);
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemSetContext(ReactionSystem rs, void *ctx) {

  PetscFunctionBegin;
  
  rs->ctx = ctx;
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddSpecies(ReactionSystem rs,const char *speciesName) {

  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(!rs->nSpecies) {
    ierr = PetscMalloc1(1,&rs->spec);CHKERRQ(ierr);
  } else {
    ierr = PetscRealloc(sizeof(RSSpecies)*(rs->nSpecies+1),&rs->spec);CHKERRQ(ierr);
  }
  ierr = PetscSNPrintf(rs->spec[rs->nSpecies].name,sizeof(rs->spec[rs->nSpecies].name),speciesName);CHKERRQ(ierr);

  rs->spec[rs->nSpecies].idx                 = rs->nSpecies;
  rs->spec[rs->nSpecies].D                   = 0.0;
  rs->spec[rs->nSpecies].backgroundPotential = NULL;

  rs->nSpecies++;
  
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemSpeciesGetIndex(ReactionSystem rs,const char *speciesName,PetscInt *idx) {
  PetscErrorCode ierr;
  PetscBool      sp;

  PetscFunctionBegin;

  for(PetscInt i = 0; i < rs->nSpecies; i++) {
    ierr = PetscStrcmp(speciesName,rs->spec[i].name,&sp);CHKERRQ(ierr);
    if(sp) {
      *idx = i;
      PetscFunctionReturn(0);
    }
  }
  *idx = -1;

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemReactionGetIndex(ReactionSystem rs,const char *rxName,PetscInt *idx) {
  PetscErrorCode ierr;
  PetscBool      sp;

  PetscFunctionBegin;

  for(PetscInt i = 0; i < rs->nReactions; i++) {
    ierr = PetscStrcmp(rxName,rs->reac[i].name,&sp);CHKERRQ(ierr);
    if(sp) {
      *idx = i;
      PetscFunctionReturn(0);
    }
  }
  *idx = -1;

  PetscFunctionReturn(0);
}



PetscErrorCode ReactionSystemSpeciesSetDiffusionConstant(ReactionSystem rs,const char *speciesName,PetscReal D) {

  PetscErrorCode ierr;
  PetscInt       idx;

  PetscFunctionBegin;
  ierr = ReactionSystemSpeciesGetIndex(rs,speciesName,&idx);CHKERRQ(ierr);
  if(idx < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding diffusion constant; no species named %s found\n",speciesName);
  }
  rs->spec[idx].D = D;
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemSpeciesAddBackgroundPotential(ReactionSystem rs,const char *speciesName,SpatialFn f) {

  PetscErrorCode ierr;
  PetscInt       idx;

  PetscFunctionBegin;

  ierr = ReactionSystemSpeciesGetIndex(rs,speciesName,&idx);CHKERRQ(ierr);
  if(idx < 0){
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding background potential; no species named %s found\n",speciesName);  
  }
  rs->spec[idx].backgroundPotential = f;
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddPairPotential(ReactionSystem rs,const char *species1,const char *species2,PetscScalar radius,SpatialIaFn kern) {

  PetscErrorCode ierr;
  PetscInt      sp1,sp2;

  PetscFunctionBegin;
  ierr = ReactionSystemSpeciesGetIndex(rs,species1,&sp1);CHKERRQ(ierr);
  if(sp1 < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding pair potential; no species named %s found\n",species1);
  }
  ierr = ReactionSystemSpeciesGetIndex(rs,species2,&sp2);CHKERRQ(ierr);
  if(sp2 < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding pair potential; no species named %s found\n",species2);
  }
  
  if(!rs->nPairPotentials) {
    ierr = PetscMalloc1(1,&rs->pairPotentials);CHKERRQ(ierr);
  } else {
    ierr = PetscRealloc(sizeof(pairPotential)*(rs->nPairPotentials+1),&rs->pairPotentials);CHKERRQ(ierr);
  }

  rs->pairPotentials[rs->nPairPotentials].species[0] = sp1;
  rs->pairPotentials[rs->nPairPotentials].species[1] = sp2;
  rs->pairPotentials[rs->nPairPotentials].potFn      = kern;
  rs->pairPotentials[rs->nPairPotentials].radius     = radius;
  rs->pairPotentials[rs->nPairPotentials].data       = PETSC_NULL;

  rs->nPairPotentials++;
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddReaction(ReactionSystem rs,PetscInt nReactants,PetscInt *reactants,
                                                           PetscInt nProducts,PetscInt* products,
                                                           SpatialIaFn kern,PetscScalar rate,PetscScalar radius,const char* rx_name) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  /* error check to see if reaction of same name already exists? */

  if(!rs->nReactions) {
    ierr = PetscMalloc1(1,&rs->reac);CHKERRQ(ierr);
  } else {
    ierr = PetscRealloc(sizeof(RSReaction)*(rs->nReactions+1),&rs->reac);CHKERRQ(ierr);
  }
  
  ierr = PetscSNPrintf(rs->reac[rs->nReactions].name,sizeof(rs->reac[rs->nReactions].name),rx_name);CHKERRQ(ierr);

  rs->reac[rs->nReactions].nReactants=nReactants;
  for(PetscInt i=0; i < nReactants; i++) {
    rs->reac[rs->nReactions].reactants[i] = reactants[i];
  }  

  rs->reac[rs->nReactions].nProducts=nProducts;
  for(PetscInt i=0; i < nProducts; i++) {
    rs->reac[rs->nReactions].products[i] = products[i];
  }  

  rs->reac[rs->nReactions].idx          = rs->nReactions;
  rs->reac[rs->nReactions].rate         = rate;
  rs->reac[rs->nReactions].rateFn       = kern;
  rs->reac[rs->nReactions].isDoi        = (!kern && (nProducts > 1 || nReactants > 1)) ? PETSC_TRUE : PETSC_FALSE;
  rs->reac[rs->nReactions].isReversible = PETSC_FALSE;
  rs->reac[rs->nReactions].radius       = radius;
  rs->reac[rs->nReactions].data         = PETSC_NULL;

  rs->nReactions++;
  
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddReverseReaction(ReactionSystem rs,const char* name,PetscScalar bwdRate,const char* reverseName) {
  PetscInt       i;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = ReactionSystemReactionGetIndex(rs,name,&i);CHKERRQ(ierr);
  if(i < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error creating backward reaction; no reaction named %s found\n",name);
  }

  ierr=ReactionSystemAddReaction(rs,rs->reac[i].nProducts,rs->reac[i].products,
                                 rs->reac[i].nReactants,rs->reac[i].reactants,
                                 rs->reac[i].rateFn,bwdRate,rs->reac[i].radius,reverseName);CHKERRQ(ierr);
  
  rs->reac[i].isReversible                = PETSC_TRUE;
  rs->reac[rs->nReactions-1].isReversible = PETSC_TRUE;
  
  rs->reac[i].reverseRx                = &rs->reac[rs->nReactions-1];
  rs->reac[rs->nReactions-1].reverseRx = &rs->reac[i];

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddCreation(ReactionSystem rs,const char* product,PetscScalar rate,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp;

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,product,&sp);CHKERRQ(ierr);
  if(sp < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",product);
  } else {
    ierr = ReactionSystemAddReaction(rs,0,PETSC_NULL,1,&sp,PETSC_NULL,rate,0.0,rx_name);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddDecay(ReactionSystem rs,const char* reactant,PetscScalar rate,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp;

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant,&sp);CHKERRQ(ierr);
  if(sp < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant);
  } else {
    ierr = ReactionSystemAddReaction(rs,1,&sp,0,PETSC_NULL,PETSC_NULL,rate,0,rx_name);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddAnnihilation(ReactionSystem rs,const char* reactant1,const char* reactant2,SpatialIaFn kern,PetscScalar rate,PetscScalar radius,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp[2];

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant1,&sp[0]);CHKERRQ(ierr);
  if(sp[0] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant1);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,reactant2,&sp[1]);CHKERRQ(ierr);
  if(sp[1] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant2);
  }

  ierr = ReactionSystemAddReaction(rs,2,sp,0,PETSC_NULL,kern,rate,radius,rx_name);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddConversion(ReactionSystem rs,const char* reactant,const char* product,PetscScalar rate,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp[2];

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant,&sp[0]);CHKERRQ(ierr);
  if(sp[0] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,product,&sp[1]);CHKERRQ(ierr);
  if(sp[1] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",product);
  }

  ierr = ReactionSystemAddReaction(rs,1,&sp[0],1,&sp[1],PETSC_NULL,rate,0.0,rx_name);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddFusion(ReactionSystem rs,const char* reactant1, const char* reactant2, const char* product,
                                          SpatialIaFn kern,PetscScalar rate,PetscScalar radius,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp[3];

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant1,&sp[0]);CHKERRQ(ierr);
  if(sp[0] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant1);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,reactant2,&sp[1]);CHKERRQ(ierr);
  if(sp[1] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant2);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,product,&sp[2]);CHKERRQ(ierr);
  if(sp[2] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",product);
  }

  ierr = ReactionSystemAddReaction(rs,2,&sp[0],1,&sp[2],kern,rate,radius,rx_name);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemAddFission(ReactionSystem rs,const char* reactant, const char* product1, const char* product2,
                                          SpatialIaFn kern,PetscScalar rate,PetscScalar radius,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp[3];

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant,&sp[0]);CHKERRQ(ierr);
  if(sp[0] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,product1,&sp[1]);CHKERRQ(ierr);
  if(sp[1] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",product1);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,product2,&sp[2]);CHKERRQ(ierr);
  if(sp[2] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",product2);
  }

  ierr = ReactionSystemAddReaction(rs,1,&sp[0],2,&sp[1],kern,rate,radius,rx_name);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode ReactionSystemAddEnzymatic(ReactionSystem rs,const char* reactant, const char* product, const char* enzyme,
                                          SpatialIaFn kern,PetscScalar rate,PetscScalar radius,const char* rx_name) {
    
  PetscErrorCode ierr; 
  PetscInt       sp[4];

  PetscFunctionBegin;
  
  ierr = ReactionSystemSpeciesGetIndex(rs,reactant,&sp[0]);CHKERRQ(ierr);
  if(sp[0] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",reactant);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,product,&sp[2]);CHKERRQ(ierr);
  if(sp[2] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",enzyme);
  }

  ierr = ReactionSystemSpeciesGetIndex(rs,enzyme,&sp[1]);CHKERRQ(ierr);
  if(sp[1] < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error adding creation reaction; no species named %s found\n",enzyme);
  }
  sp[3] = sp[1];


  ierr = ReactionSystemAddReaction(rs,2,&sp[0],2,&sp[2],kern,rate,radius,rx_name);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode ReactionSystemSpeciesView(ReactionSystem rs,PetscInt i) {
  PetscErrorCode ierr;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"    Species %s\n",rs->spec[i].name);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"      Diffusion constant %1.14f\n",rs->spec[i].D);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"      Background potential? %s\n",rs->spec[i].backgroundPotential ? "yes" : "no");CHKERRQ(ierr);
  
  return(0);
}

PetscErrorCode ReactionSystemReactionView(ReactionSystem rs,PetscInt i) {
  PetscErrorCode ierr;
  PetscInt j;

  if(rs->reac[i].isReversible) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"    Reversible reaction %s-%s\n",rs->reac[i].name,rs->reac[i].reverseRx->name);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"    Reaction %s\n",rs->reac[i].name);CHKERRQ(ierr);
  }
  if(rs->reac[i].nReactants > 0) {
    j = rs->reac[i].reactants[0];
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      %s",rs->spec[j].name);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      0");CHKERRQ(ierr);
  }
  if(rs->reac[i].nReactants > 1) {
    j = rs->reac[i].reactants[1];
    ierr = PetscPrintf(PETSC_COMM_WORLD," + %s",rs->spec[j].name);CHKERRQ(ierr);
  } 
  PetscPrintf(PETSC_COMM_WORLD," %s",rs->reac[i].isReversible ? "<-->" : "->");CHKERRQ(ierr);
  if(rs->reac[i].nProducts > 0) {
    j = rs->reac[i].products[0];
    ierr = PetscPrintf(PETSC_COMM_WORLD," %s",rs->spec[j].name);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD," 0");CHKERRQ(ierr);
  }
  if(rs->reac[i].nProducts > 1) {
    j = rs->reac[i].products[1];
    ierr = PetscPrintf(PETSC_COMM_WORLD," + %s\n",rs->spec[j].name);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);
  }

  if(rs->reac[i].isReversible) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      forward rate: %1.14f\n",rs->reac[i].rate);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      backward rate: %1.14f\n",rs->reac[i].reverseRx->rate);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      reaction rate: %1.14f\n",rs->reac[i].rate);CHKERRQ(ierr);
  }
  if((rs->reac[i].nProducts > 1) || (rs->reac[i].nReactants > 1)) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      reaction radius: %1.14f\n",rs->reac[i].radius);CHKERRQ(ierr);
  }

  if(rs->reac[i].isDoi) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"      Using Doi reaction kernel\n");CHKERRQ(ierr);
  } 

  return(0);
}

PetscErrorCode ReactionSystemPairPotentialView(ReactionSystem rs,PetscInt i) {
  PetscErrorCode ierr;
  PetscInt       sp1,sp2;

  sp1 = rs->pairPotentials[i].species[0];
  sp2 = rs->pairPotentials[i].species[1];
  ierr = PetscPrintf(PETSC_COMM_WORLD,"    pair potential %d between species %s and %s with radius %1.14f\n",i,rs->spec[sp1].name,rs->spec[sp2].name,rs->pairPotentials[i].radius);CHKERRQ(ierr);

  return(0);
}

PetscErrorCode ReactionSystemView(ReactionSystem rs) {

  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(rs->nSpecies) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Reaction system with %d species and %d reactions\n",rs->nSpecies,rs->nReactions);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  I. Species\n");CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Empty reaction system. Add a species to begin.\n");
  }
  for(PetscInt i=0; i < rs->nSpecies; i++) {
    ierr = ReactionSystemSpeciesView(rs,i);CHKERRQ(ierr);
  }
  if(rs->nReactions) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  II. Reactions\n");CHKERRQ(ierr);
  }
  for(PetscInt i=0; i < rs->nReactions; i++) {
    ierr = ReactionSystemReactionView(rs,i);CHKERRQ(ierr);
  }
  
  if(rs->nPairPotentials) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  III. Pair potentials\n");CHKERRQ(ierr);
    for(PetscInt i = 0; i < rs->nPairPotentials; i++) { 
      ierr = ReactionSystemPairPotentialView(rs,i);CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}

/*
PetscErrorCode CRDMESetNspecies(CRDME crdme, int Nspecies) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  crdme->Nspecies = Nspecies;
  ierr = PetscMalloc1(Nspecies,&crdme->species);CHKERRQ(ierr);
  for(int i = 0; i < Nspecies; i++) {
    ierr = PetscMalloc1(1,&crdme->species[i]);CHKERRQ(ierr);
    ierr = PetscSNPrintf(crdme->species[i]->name,sizeof(crdme->species[i]->name),"%d",i);CHKERRQ(ierr);

    crdme->species[i]->D               = 0.0;
    crdme->species[i]->ctx             = NULL;
    crdme->species[i]->data            = NULL;
    crdme->species[i]->pair_potentials = NULL;
    crdme->species[i]->pair_rxs        = NULL;
    crdme->species[i]->potential       = NULL;
    crdme->species[i]->rx_decay        = NULL;
    crdme->species[i]->rx_cre          = NULL;
    crdme->species[i]->n_unbnd         = 0;    
    crdme->species[i]->n_convert       = 0;
    
    ierr = PetscMalloc1(Nspecies,&crdme->species[i]->pair_potentials);CHKERRQ(ierr);
    ierr = PetscMalloc1(Nspecies,&crdme->species[i]->pair_rxs);CHKERRQ(ierr);
    
    for(int j = 0; j < Nspecies; j++) {
      crdme->species[i]->pair_potentials[j] = NULL;
      crdme->species[i]->pair_rxs[j]        = NULL;
    }

  }
  return 0;
}
//
//PetscErrorCode CRDMESpeciesSetNumPairIntx(CRDME crdme, int speciesNumber, int nIntx[CRDME_NUM_RX_TYPES]) {
//   PetscErrorCode ierr;
//   CRDMESpecies   s = crdme->species[speciesNumber];
//   PetscFunctionBegin;
//   
//   if(speciesNumber >= crdme->Nspecies) {
//     PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);
//     return 1;
//   }
//   
//   s->npix[PAIR_POT]   = nIntx[PAIR_POT];
//   s->npix[PAIR_ANN]   = nIntx[PAIR_ANN];
//   s->npix[PAIR_BND]   = nIntx[PAIR_BND];
//   s->npix[PAIR_UNBND] = nIntx[PAIR_UNBND];
//
//
//   ierr = PetscMalloc1(nIntx[PAIR_UNBND],&s->rx_unbnd);CHKERRQ(ierr);
//    
//   
//    *    ierr = PetscMalloc1(nIntx[PAIR_POT],&s->pair_potentials);CHKERRQ(ierr);
//   ierr = PetscMalloc1(nIntx[PAIR_ANN],&s->rx_pair_ann);CHKERRQ(ierr);
//   ierr = PetscMalloc1(nIntx[PAIR_BND],&s->rx_bnd);CHKERRQ(ierr);
//   for(int i = 0; i < s->npix[PAIR_POT]; i++) {
//     ierr = PetscMalloc1(1,&s->pair_potentials[i]);CHKERRQ(ierr);
//     s->pair_potentials[i]->Ip = NULL;
//     s->pair_potentials[i]->Ij = NULL;
//   }
//   
//   for(int i = 0; i < s->npix[PAIR_ANN]; i++) {
//     ierr = PetscMalloc1(1,&s->rx_pair_ann[i]);CHKERRQ(ierr);
//     s->rx_pair_ann[i]->Ip = NULL;
//     s->rx_pair_ann[i]->Ij = NULL;
//   }
//   
//   for(int i = 0; i < s->npix[PAIR_BND]; i++) {
//     ierr = PetscMalloc1(1,&s->rx_bnd[i]);CHKERRQ(ierr);
//     s->rx_bnd[i]->Ip = NULL;
//     s->rx_bnd[i]->Ij = NULL;
//   }
//   */
//   
//   for(int i = 0; i < s->npix[PAIR_UNBND]; i++) {
//     ierr = PetscMalloc1(1,&s->rx_unbnd[i]);CHKERRQ(ierr);
//     s->rx_unbnd[i]->Ip = NULL;
//     s->rx_unbnd[i]->Ij = NULL;
//   }
//
//   return 0;
//}

// PetscErrorCode CRDMESetSpeciesName(CRDME crdme, int speciesNumber, const char *name) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;
    
//    if(speciesNumber >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);
//      return 1;
//    }
//    ierr = PetscSNPrintf(crdme->species[speciesNumber]->name,sizeof(crdme->species[speciesNumber]->name),name);CHKERRQ(ierr);
//    return 0;
// }

// PetscErrorCode CRDMESpeciesSetDiffusionConstant(CRDME crdme, int speciesNumber, double D) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;
    
//    if(speciesNumber >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);CHKERRQ(ierr);
//      return 1;
//    }
//    crdme->species[speciesNumber]->D = D;
//    return 0;
// }

// PetscErrorCode CRDMESpeciesSetDecayPropensity(CRDME crdme, int speciesNumber, SpatFn_2D k) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;
    
//    if(speciesNumber >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);
//      return 1;
//    }
//    crdme->species[speciesNumber]->rx_decay = k;
//    return 0;
// }

// PetscErrorCode CRDMESpeciesSetCreationPropensity(CRDME crdme, int speciesNumber, SpatFn_2D k) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;
    
//    if(speciesNumber >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);
//      return 1;
//    }
//    crdme->species[speciesNumber]->rx_cre = k;
//    return 0;
// }

// PetscErrorCode CRDMESpeciesSetBackgroundPotential(CRDME crdme, int speciesNumber, SpatFn_2D psi) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;
    
//    if(speciesNumber >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", speciesNumber, crdme->Nspecies);
//      return 1;
//    }
//    crdme->species[speciesNumber]->potential = psi;
//    return 0;
// }

// PetscErrorCode CRDMESpeciesAddConversion(CRDME crdme, int Reactant, int Product, double c) {
//    PetscErrorCode ierr;
//    PetscFunctionBegin;

//    if(Reactant >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", Reactant, crdme->Nspecies);
//      return 1;
//    }

//    if(Product >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", Product, crdme->Nspecies);
//      return 1;
//    }

//    ierr = PetscMalloc1(1,&crdme->species[Reactant]->conv_rxs[crdme->species[Reactant]->n_convert]);CHKERRQ(ierr);
//    crdme->species[Reactant]->conv_rxs[crdme->species[Reactant]->n_convert]->species[0] = Reactant;
//    crdme->species[Reactant]->conv_rxs[crdme->species[Reactant]->n_convert]->species[1] = Product;

//    crdme->species[Reactant]->conv_rxs[crdme->species[Reactant]->n_convert]->c = c;
//    crdme->species[Reactant]->n_convert++;
//    return 0;
// }

// PetscErrorCode CRDMEAddPairPotential(CRDME crdme, int nA, int nB, SpatInteractionFn_2D psiAB, double r) {
//    PetscErrorCode ierr;
//    PetscBool      flg=PETSC_FALSE;
//    PetscFunctionBegin;

//    if(nA >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      ierr = PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }
   
//    ierr = PetscMalloc1(1,&crdme->species[nA]->pair_potentials[nB]);CHKERRQ(ierr);
//    ierr = PetscMalloc1(1,&crdme->species[nB]->pair_potentials[nA]);CHKERRQ(ierr);

//    crdme->species[nA]->pair_potentials[nB]->species[0] = nA;// = { speciesA, speciesB };
//    crdme->species[nA]->pair_potentials[nB]->species[1] = nB;
//    crdme->species[nA]->pair_potentials[nB]->psiAB = psiAB;
//    crdme->species[nA]->pair_potentials[nB]->radius = r;
  
//    if(nA != nB) {
//      crdme->species[nB]->pair_potentials[nA]->species[0] = nB;// = { speciesA, speciesB };
//      crdme->species[nB]->pair_potentials[nA]->species[1] = nA;
//      crdme->species[nB]->pair_potentials[nA]->psiAB = psiAB;
//      crdme->species[nB]->pair_potentials[nA]->radius = r;

//    }
   
//    ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_pot_interaction_stencil",&flg,NULL);CHKERRQ(ierr);

//    if(flg) {
//      int            N   = crdme->geom->N;
//      double         *p;
//      double         rsqr;
//      int            i,j,jj;
//      int            *Ip,*Ij;

//      ierr = PetscMalloc1(N+1,&crdme->species[nA]->pair_potentials[nB]->Ip);CHKERRQ(ierr);
//      ierr = CRDMEComputePointStencil(crdme,crdme->species[nA]->pair_potentials[nB]->Ip,
//                                 &crdme->species[nA]->pair_potentials[nB]->Ij,
//                                 crdme->species[nA]->pair_potentials[nB]->radius+crdme->geom->hmax);CHKERRQ(ierr);
//      Ip = crdme->species[nA]->pair_potentials[nB]->Ip;
//      Ij = crdme->species[nA]->pair_potentials[nB]->Ij;
//      ierr = PetscMalloc1(Ip[N],&crdme->species[nA]->pair_potentials[nB]->p);CHKERRQ(ierr);
//      p = crdme->species[nA]->pair_potentials[nB]->p;
//      for(i = 0; i < N; i++) {
//        for(jj = Ip[i]; jj < Ip[i+1]; jj++) {
//          j = Ij[jj];
//          ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
//          p[jj] = psiAB(rsqr,crdme->ctx);
//        }
//      }
     
//      if(nA != nB) {
//        ierr = PetscMalloc1(N+1,&crdme->species[nB]->pair_potentials[nA]->Ip);CHKERRQ(ierr);
//        ierr = PetscMalloc1(Ip[N],
//                            &crdme->species[nB]->pair_potentials[nA]->Ij);CHKERRQ(ierr); /* can just give them the same pointer instead */
//        ierr = PetscMalloc1(Ip[crdme->geom->N],
//                            &crdme->species[nB]->pair_potentials[nA]->p);CHKERRQ(ierr); /* can just give them the same pointer instead */
//        ierr = PetscArraycpy(crdme->species[nB]->pair_potentials[nA]->Ip,
//                             crdme->species[nA]->pair_potentials[nB]->Ip,(size_t) N+1);CHKERRQ(ierr);
//        ierr = PetscArraycpy(crdme->species[nB]->pair_potentials[nA]->Ij,
//                             crdme->species[nA]->pair_potentials[nB]->Ij,
//                             (size_t) Ip[N]);CHKERRQ(ierr);
//        ierr = PetscArraycpy(crdme->species[nB]->pair_potentials[nA]->p,
//                             crdme->species[nA]->pair_potentials[nB]->p,
//                             (size_t) Ip[N]);CHKERRQ(ierr);
//      }
        
//    } else {
//      crdme->species[nA]->pair_potentials[nB]->Ip = NULL;
//      crdme->species[nA]->pair_potentials[nB]->Ij = NULL;
//      crdme->species[nA]->pair_potentials[nB]->p  = NULL;
//      crdme->species[nB]->pair_potentials[nA]->Ip = NULL;
//      crdme->species[nB]->pair_potentials[nA]->Ij = NULL;
//      crdme->species[nB]->pair_potentials[nA]->p  = NULL;
//    }

//    return 0;
// }

// PetscErrorCode CRDMEAddPairAnnihilation(CRDME crdme, int nA, int nB, SpatInteractionFn_2D kAB, double r) {
//    PetscErrorCode ierr;
//    double         rsqr;
//    int            i,j,jj;
//    PetscBool      flg=0;
//    PetscFunctionBegin;

//    if(nA >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }

//    ierr = PetscMalloc1(1,&crdme->species[nA]->pair_rxs[nB]);CHKERRQ(ierr);
   
//    crdme->species[nA]->pair_rxs[nB]->species[0] = nA;// = { speciesA, speciesB };
//    crdme->species[nA]->pair_rxs[nB]->species[1] = nB;
//    crdme->species[nA]->pair_rxs[nB]->kAB = kAB;
//    crdme->species[nA]->pair_rxs[nB]->radius = r;
//    crdme->species[nA]->pair_rxs[nB]->rtype  = RX_ANN;
   
//    ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_annihilation_stencil",&flg,NULL);CHKERRQ(ierr);

//    if(flg) {
//      ierr = PetscMalloc1(crdme->geom->N+1,&crdme->species[nA]->pair_rxs[nB]->Ip);CHKERRQ(ierr);
//      ierr = CRDMEComputePointStencil(crdme,crdme->species[nA]->pair_rxs[nB]->Ip,
//                              &crdme->species[nA]->pair_rxs[nB]->Ij,
//                              crdme->species[nA]->pair_rxs[nB]->radius);CHKERRQ(ierr);
     
//      ierr = PetscMalloc1(crdme->species[nA]->pair_rxs[nB]->Ip[crdme->geom->N],
//                       &crdme->species[nA]->pair_rxs[nB]->rates);CHKERRQ(ierr);
//      for(i = 0; i < crdme->geom->N; i++) {
//        for(jj = crdme->species[nA]->pair_rxs[nB]->Ip[i]; jj < crdme->species[nA]->pair_rxs[nB]->Ip[i+1]; jj++) {
//          j  = crdme->species[nA]->pair_rxs[nB]->Ij[jj];
//          ierr =  CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
//          crdme->species[nA]->pair_rxs[nB]->rates[jj] = crdme->species[nA]->pair_rxs[nB]->kAB(rsqr,NULL);
//        }
//      }
//    } else {
//      crdme->species[nA]->pair_rxs[nB]->Ip = NULL;
//      crdme->species[nA]->pair_rxs[nB]->Ij = NULL;
//      crdme->species[nA]->pair_rxs[nB]->rates = NULL;
//    }
                                                                                
//    return 0;
// }

// static double w_duv25[25] = {0.090817990382754, 0.036725957756467, 0.045321059435528, 0.036725957756467,
//                              0.045321059435528, 0.036725957756467, 0.045321059435528, 0.072757916845420,
//                              0.028327242531057, 0.009421666963733, 0.072757916845420, 0.028327242531057,
//                              0.009421666963733, 0.072757916845420, 0.028327242531057, 0.009421666963733,
//                              0.072757916845420, 0.028327242531057, 0.009421666963733, 0.072757916845420,
//                              0.028327242531057, 0.009421666963733, 0.072757916845420, 0.028327242531057, 0.009421666963733};

// static double p_duv25[50] = {0.333333333333333, 0.333333333333333, 0.485577633383657, 0.028844733232686,
//                             0.109481575485037, 0.781036849029926, 0.028844733232686, 0.485577633383657,
//                              0.781036849029926, 0.109481575485037, 0.485577633383657, 0.485577633383657,
//                                 0.109481575485037, 0.109481575485037, 0.307939838764121, 0.550352941820999,
//                                 0.246672560639903, 0.728323904597411, 0.066803251012200, 0.923655933587501,
//                                 0.141707219414880, 0.307939838764121, 0.025003534762686, 0.246672560639903,
//                                 0.009540815400299, 0.066803251012200, 0.550352941820999, 0.141707219414880,
//                                 0.728323904597411, 0.025003534762686, 0.923655933587501, 0.009540815400299,
//                                 0.141707219414880, 0.550352941820999, 0.025003534762686, 0.728323904597411,
//                                 0.009540815400299, 0.923655933587501, 0.307939838764121, 0.141707219414880,
//                                 0.246672560639903, 0.025003534762686, 0.066803251012200, 0.009540815400299,
//                                 0.550352941820999, 0.307939838764121, 0.728323904597411, 0.246672560639903,
//                                 0.923655933587501, 0.066803251012200};


// static double w_duv7[7] = {0.225000000000000, 0.132394152788506, 0.125939180544827, 0.132394152788506, 
//                              0.125939180544827, 0.132394152788506, 0.125939180544827};
// static double p_duv7[14]  =  {0.333333333333333, 0.333333333333333, 0.470142064105100, 0.059715871789800, 0.101286507323456, 
//                               0.797426985353088, 0.059715871789800, 0.470142064105100, 0.797426985353088, 0.101286507323456, 
//                               0.470142064105100, 0.470142064105100, 0.101286507323456, 0.101286507323456}; 

// static double w_centr[1] = {1.0};
// static double p_centr[2]  =  {0.333333333333333, 0.333333333333333}; 

// PetscErrorCode CRDMEAddPairReactionDoi(CRDME crdme, int nA, int nB, int nC, double c, double r,
// 																			 pair_rx_type rtype, doi_type doi_type, rx_mechanism me_type, placement_type pl_type) {
//   PetscErrorCode ierr;
//   int            j,dtype,mtype;
//   PetscBool            flg=0;
//   PetscFunctionBegin;

//   if(nA >= crdme->Nspecies) {
//     PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//     return 1;
//   }
//   if(nB >= crdme->Nspecies) {
//     PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//     return 1;
//   }
//   ierr = PetscMalloc1(1,&crdme->species[nA]->pair_rxs[nB]);CHKERRQ(ierr);

//   crdme->species[nA]->pair_rxs[nB]->species[0] = nA;// = { speciesA, speciesB };
//   crdme->species[nA]->pair_rxs[nB]->species[1] = nB;
//   if(rtype != RX_ANN) {
//     crdme->species[nA]->pair_rxs[nB]->species[2] = nC;
//     if(nC >= crdme->Nspecies) {
//       PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nC, crdme->Nspecies);
//       return 1;
//     }
//   }
//   crdme->species[nA]->pair_rxs[nB]->kAB    = NULL;
//   crdme->species[nA]->pair_rxs[nB]->c = c;
//   crdme->species[nA]->pair_rxs[nB]->radius = r;



//   /* ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_annihilation_stencil",&flg,NULL);CHKERRQ(ierr); */
//   /* for now, require the stencil */
//   /* later, allow for rates computed on the fly */
//   dtype = doi_type;
//   mtype = me_type;
//   ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_pair_stencil",&flg,NULL);CHKERRQ(ierr);
//   ierr =  PetscOptionsGetInt(NULL,NULL,"-crdme_doi_type",&dtype,NULL);CHKERRQ(ierr);

//   crdme->species[nA]->pair_rxs[nB]->rtype  = rtype;
//   if(flg) {
//     crdme->species[nA]->pair_rxs[nB]->me_type  = STENCIL;
//   } else {
//     ierr =  PetscOptionsGetInt(NULL,NULL,"-crdme_doi_mechanism_type",&mtype,NULL);CHKERRQ(ierr);
//     crdme->species[nA]->pair_rxs[nB]->me_type  = mtype;
//   }
//   crdme->species[nA]->pair_rxs[nB]->pl_type  = pl_type;
//   crdme->doi_type=dtype;
//   if(dtype==DOI_STANDARD) {
//     double min_area = PetscPowReal(4.0,-7.0)*crdme->geom->hmax*crdme->geom->hmax; /* seven refinements below minimum mesh length scale */
//     double max_area = 1e-4;
//     int sch1,sch2;
//     PetscBool flg_sch1,flg_sch2;
//     ierr = PetscMalloc1(1,&crdme->q);CHKERRQ(ierr);
//     ierr = PetscMalloc1(1,&crdme->q->sch1);CHKERRQ(ierr);
//     ierr = PetscMalloc1(1,&crdme->q->sch2);CHKERRQ(ierr);
//     ierr = PetscOptionsGetReal(NULL,NULL,"-crdme_doi_min_area",&min_area,NULL);CHKERRQ(ierr);
//     ierr = PetscOptionsGetReal(NULL,NULL,"-crdme_doi_max_area",&max_area,NULL);CHKERRQ(ierr);
//     crdme->q->max_area = max_area;
//     crdme->q->min_area = min_area;
//     crdme->q->sch1->n = 25;
//     crdme->q->sch1->weights = w_duv25;
//     crdme->q->sch1->points = p_duv25;
//     crdme->q->sch2->n = 7;
//     crdme->q->sch2->weights = w_duv7;
//     crdme->q->sch2->points = p_duv7;
//     ierr = PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_scheme1",&sch1,&flg_sch1);CHKERRQ(ierr);
//     if(flg_sch1) {
//       switch(sch1) {
//       case 0:
//         crdme->q->sch1->n = 1;
//         PetscMalloc1(1,&crdme->q->sch1->weights);
//         PetscMalloc1(2,&crdme->q->sch1->points);
//         PetscArraycpy(crdme->q->sch1->weights,w_centr,1);
//         PetscArraycpy(crdme->q->sch1->points,p_centr,2);
//         break;
//       case 1:
//         crdme->q->sch1->n = 7;
//         PetscMalloc1(7,&crdme->q->sch1->weights);
//         PetscMalloc1(14,&crdme->q->sch1->points);
//         PetscArraycpy(crdme->q->sch1->weights,w_duv7,7);
//         PetscArraycpy(crdme->q->sch1->points,p_duv7,7);
//         break;
//       default:
//         crdme->q->sch1->n = 25;
//         PetscMalloc1(25,&crdme->q->sch1->weights);
//         PetscMalloc1(50,&crdme->q->sch1->points);
//         PetscArraycpy(crdme->q->sch1->weights,w_duv25,25);
//         PetscArraycpy(crdme->q->sch1->points,p_duv25,50);
//       }
//     }
//     ierr = PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_scheme1",&sch2,&flg_sch2);CHKERRQ(ierr);
//     if(flg_sch2) {
//       switch(sch2) {
//       case 0:
//         crdme->q->sch2->n = 1;
//         PetscMalloc1(1,&crdme->q->sch2->weights);
//         PetscMalloc1(2,&crdme->q->sch2->points);
//         PetscArraycpy(crdme->q->sch2->weights,w_centr,1);
//         PetscArraycpy(crdme->q->sch2->points,p_centr,2);
//         break;
//       case 2:
//         crdme->q->sch2->n = 25;
//         PetscMalloc1(25,&crdme->q->sch2->weights);
//         PetscMalloc1(50,&crdme->q->sch2->points);
//         PetscArraycpy(crdme->q->sch2->weights,w_duv25,25);
//         PetscArraycpy(crdme->q->sch2->points,p_duv25,50);
//       default:
//         crdme->q->sch2->n = 7;
//         PetscMalloc1(7,&crdme->q->sch2->weights);
//         PetscMalloc1(14,&crdme->q->sch2->points);
//         PetscArraycpy(crdme->q->sch2->weights,w_duv7,7);
//         PetscArraycpy(crdme->q->sch2->points,p_duv7,7);
//       }
//     }
//   } else if(dtype==DOI_STANDARD_SQUARE) {
//     double rtol = 1e-8;
//     double atol = 1e-14;
//     int    max_eval = 0;
//     ierr = PetscMalloc1(1,&crdme->p);CHKERRQ(ierr);
//     PetscOptionsGetReal(NULL,NULL,"-crdme_doi_quad_rtol",&rtol,NULL);CHKERRQ(ierr);
//     PetscOptionsGetReal(NULL,NULL,"-crdme_doi_quad_atol",&rtol,NULL);CHKERRQ(ierr);
//     PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_max_eval",&max_eval,NULL);CHKERRQ(ierr);
//     crdme->p->atol = atol;
//     crdme->p->rtol = rtol;
//     crdme->p->max_eval = max_eval;
//   }
//   if(flg || mtype==STENCIL || mtype==DOI_REJECT) {
//     int rank,nodesize,noderank;
//     MPI_Comm nodecomm;

//     ierr = CRDMEComputePointStencilDoi(crdme,&crdme->species[nA]->pair_rxs[nB]->Ip,
//                                 &crdme->species[nA]->pair_rxs[nB]->Ij,
//                                 &crdme->species[nA]->pair_rxs[nB]->rates,
//                                 crdme->species[nA]->pair_rxs[nB]->radius,crdme->species[nA]->pair_rxs[nB]->me_type==STENCIL,dtype);CHKERRQ(ierr);

//     MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
//     MPI_Barrier(PETSC_COMM_WORLD);
//     MPI_Comm_split_type(PETSC_COMM_WORLD, MPI_COMM_TYPE_SHARED,rank,
//                 MPI_INFO_NULL, &nodecomm);
//     MPI_Comm_size(nodecomm, &nodesize);
//     MPI_Comm_rank(nodecomm, &noderank);

//     MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
//     if(!crdme->species[nA]->pair_rxs[nB]->Ip) {
//       PetscPrintf(PETSC_COMM_SELF,"Error: pointer array not allocated properly on process %d\n",rank);
//       return 1;
//     }
//     MPI_Barrier(PETSC_COMM_WORLD);
//     if(!noderank && crdme->species[nA]->pair_rxs[nB]->me_type==STENCIL) {
//     	PetscBool fnd=0;
//       for(int i = 0; i < crdme->geom->N; i++) {
//         for(int jj = crdme->species[nA]->pair_rxs[nB]->Ip[i]; jj < crdme->species[nA]->pair_rxs[nB]->Ip[i+1]; jj++) {
//           j =  crdme->species[nA]->pair_rxs[nB]->Ij[jj];
//           crdme->species[nA]->pair_rxs[nB]->rates[jj]*=c;
//           if(i > j) {
//             fnd=0;
//             for(int kk=crdme->species[nA]->pair_rxs[nB]->Ip[j]; kk < crdme->species[nA]->pair_rxs[nB]->Ip[j+1]; kk++) {
//               int k = crdme->species[nA]->pair_rxs[nB]->Ij[kk];
//               if(k == i) {
//               	fnd=1;
//                 crdme->species[nA]->pair_rxs[nB]->rates[jj] =  .5*(crdme->species[nA]->pair_rxs[nB]->rates[jj] + crdme->species[nA]->pair_rxs[nB]->rates[kk]);
//                 crdme->species[nA]->pair_rxs[nB]->rates[kk] = crdme->species[nA]->pair_rxs[nB]->rates[jj];
//                 break;
//               }
//             }
//             if(!fnd) {
//             	PetscPrintf(PETSC_COMM_SELF,"error: stencil not symmetric, nodes %d and %d\n",i,j);
//             	return 1;
//             }
//           }

//         }
//       }
//     }
//     MPI_Barrier(PETSC_COMM_WORLD);
//   }  else {
//     crdme->species[nA]->pair_rxs[nB]->Ip = NULL;
//     crdme->species[nA]->pair_rxs[nB]->Ij = NULL;
//     crdme->species[nA]->pair_rxs[nB]->rates = NULL;
//   }

//   return 0;
// }


// PetscErrorCode CRDMEAddPairAnnihilationDoi(CRDME crdme, int nA, int nB, double c, double r, doi_type doi_type, rx_mechanism me_type, placement_type pl_type) {
//    PetscErrorCode ierr;

//    PetscFunctionBegin;

//    ierr = CRDMEAddPairReactionDoi(crdme,nA,nB,-1,c,r,RX_ANN,doi_type,me_type,pl_type);CHKERRQ(ierr);
//    return 0;
// }

// PetscErrorCode CRDMEAddPairBindingDoi(CRDME crdme, int nA, int nB, int nC, double c, double r, doi_type doi_type, rx_mechanism me_type, placement_type pl_type) {
//    PetscErrorCode ierr;

//    PetscFunctionBegin;

//    ierr = CRDMEAddPairReactionDoi(crdme,nA,nB,nC,c,r,RX_BND,doi_type,me_type,pl_type);CHKERRQ(ierr);
//    return 0;
// }

// PetscErrorCode CRDMEAddPairEnzymaticDoi(CRDME crdme, int nA, int nB, int nC, double c, double r, doi_type doi_type, rx_mechanism me_type, placement_type pl_type) {
//    PetscErrorCode ierr;

//    PetscFunctionBegin;

//    ierr = CRDMEAddPairReactionDoi(crdme,nA,nB,nC,c,r,RX_ENZ,doi_type,me_type,pl_type);CHKERRQ(ierr);
//    return 0;
// }


// PetscErrorCode CRDMEAddPairBinding(CRDME crdme, int Reactant1, int Reactant2, int Product,
//                                                     SpatInteractionFn_2D kAB, double r) {
//    PetscErrorCode ierr;
//    double         rsqr;
//    int            i,j,jj;
//    PetscBool      flg=0;
//    int            nA=Reactant1,nB=Reactant2,nC=Product;
//    PetscFunctionBegin;

//    if(nA >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }

//    ierr = PetscMalloc1(1,&crdme->species[nA]->pair_rxs[nB]);CHKERRQ(ierr);
   
//    crdme->species[nA]->pair_rxs[nB]->species[0] = nA;// = { speciesA, speciesB };
//    crdme->species[nA]->pair_rxs[nB]->species[1] = nB;
//    crdme->species[nA]->pair_rxs[nB]->species[2] = nC;
//    crdme->species[nA]->pair_rxs[nB]->pl_type      =  SSA_C_PLACEMENT_SMOOTH; /* TODO */
//    crdme->species[nA]->pair_rxs[nB]->kAB = kAB;
//    crdme->species[nA]->pair_rxs[nB]->radius = r;
//    crdme->species[nA]->pair_rxs[nB]->rtype  = RX_BND;
   
//    ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_binding_stencil",&flg,NULL);CHKERRQ(ierr);
//    if(flg) {
//      ierr = PetscMalloc1(crdme->geom->N+1,&crdme->species[nA]->pair_rxs[nB]->Ip);CHKERRQ(ierr);
//      ierr = CRDMEComputePointStencil(crdme,crdme->species[nA]->pair_rxs[nB]->Ip,
//                              &crdme->species[nA]->pair_rxs[nB]->Ij,
//                              crdme->species[nA]->pair_rxs[nB]->radius);CHKERRQ(ierr);
     
//      ierr = PetscMalloc1(crdme->species[nA]->pair_rxs[nB]->Ip[crdme->geom->N],
//                       &crdme->species[nA]->pair_rxs[nB]->rates);CHKERRQ(ierr);
     
//      for(i = 0; i < crdme->geom->N; i++) {
//        for(jj = crdme->species[nA]->pair_rxs[nB]->Ip[i]; jj < crdme->species[nA]->pair_rxs[nB]->Ip[i+1]; jj++) {
//          j  = crdme->species[nA]->pair_rxs[nB]->Ij[jj];
//          ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
//          crdme->species[nA]->pair_rxs[nB]->rates[jj] = crdme->species[nA]->pair_rxs[nB]->kAB(rsqr,NULL);
//        }
//      }
//    } else {
//      crdme->species[nA]->pair_rxs[nB]->Ip = NULL;
//      crdme->species[nA]->pair_rxs[nB]->Ij = NULL;
//      crdme->species[nA]->pair_rxs[nB]->rates = NULL;
//    }
                                                             
//    return 0;
// }

// PetscErrorCode CRDMEAddPairEnzymatic(CRDME crdme, int Reactant, int Enzyme, int Product, SpatInteractionFn_2D kAB, double r) {
//    PetscErrorCode ierr;
//    double         rsqr;
//    int            i,j,jj;
//    PetscBool      flg=0;
//    int            nA=Reactant,nB=Enzyme,nC=Product;
//    PetscFunctionBegin;

//    if(nA >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }

//    ierr = PetscMalloc1(1,&crdme->species[nA]->pair_rxs[nB]);CHKERRQ(ierr);
   
//    crdme->species[nA]->pair_rxs[nB]->species[0] = nA;// = { speciesA, speciesB };
//    crdme->species[nA]->pair_rxs[nB]->species[1] = nB;
//    crdme->species[nA]->pair_rxs[nB]->species[2] = nC;
//    crdme->species[nA]->pair_rxs[nB]->pl_type      =  SSA_C_PLACEMENT_SMOOTH; /* TODO */
//    crdme->species[nA]->pair_rxs[nB]->kAB = kAB;
//    crdme->species[nA]->pair_rxs[nB]->radius = r;
//    crdme->species[nA]->pair_rxs[nB]->rtype  = RX_ENZ;
   
//    ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_enzymatic_rx_stencil",&flg,NULL);CHKERRQ(ierr);

//    if(flg) {
//      ierr = PetscMalloc1(crdme->geom->N+1,&crdme->species[nA]->pair_rxs[nB]->Ip);CHKERRQ(ierr);
//      ierr = CRDMEComputePointStencil(crdme,crdme->species[nA]->pair_rxs[nB]->Ip,
//                                &crdme->species[nA]->pair_rxs[nB]->Ij,
//                                crdme->species[nA]->pair_rxs[nB]->radius);CHKERRQ(ierr);
     
//      ierr = PetscMalloc1(crdme->species[nA]->pair_rxs[nB]->Ip[crdme->geom->N],
//                       &crdme->species[nA]->pair_rxs[nB]->rates);CHKERRQ(ierr);
     
//      for(i = 0; i < crdme->geom->N; i++) {
//        for(jj = crdme->species[nA]->pair_rxs[nB]->Ip[i]; jj < crdme->species[nA]->pair_rxs[nB]->Ip[i+1]; jj++) {
//          j  = crdme->species[nA]->pair_rxs[nB]->Ij[jj];
//          ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
//          crdme->species[nA]->pair_rxs[nB]->rates[jj] = crdme->species[nA]->pair_rxs[nB]->kAB(rsqr,NULL);
//        }
//      } 
     
//    }  else {
//      crdme->species[nA]->pair_rxs[nB]->Ip = NULL;
//      crdme->species[nA]->pair_rxs[nB]->Ij = NULL;
//      crdme->species[nA]->pair_rxs[nB]->rates = NULL;
//    }                                                                    
//    return 0;
// }

// PetscErrorCode CRDMEAddPairUnbinding(CRDME crdme, int nReactant, int nProduct1, int nProduct2,
//                                                 SpatInteractionFn_2D kAB, double r) {
//    PetscErrorCode ierr;
//    double         rsqr,a;
//    int            i,j,jj;
//    int            nA=nReactant,nB=nProduct1,nC=nProduct2;
//    int            n=crdme->species[nA]->n_unbnd;
//    PetscFunctionBegin;
    
//    crdme->species[nA]->n_unbnd++;
//    if(nA >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }
//    if(nC >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }
//    n = crdme->species[nA]->n_unbnd;
//    if(n >= CRDME_MAX_UNBIND) {
//      PetscPrintf(PETSC_COMM_WORLD,"Cannot add unbinding reaction. Unbinding reaction count already exceeds limit %d\n", CRDME_MAX_UNBIND);
//      return 1;
//    }
//    ierr = PetscMalloc1(1,&crdme->species[nA]->rx_unbnd[n]);CHKERRQ(ierr);
   
//    crdme->species[nA]->rx_unbnd[n]->species[0] = nA;
//    crdme->species[nA]->rx_unbnd[n]->species[1] = nB;
//    crdme->species[nA]->rx_unbnd[n]->species[2] = nC;
//    crdme->species[nA]->rx_unbnd[n]->kAB        = kAB;
//    crdme->species[nA]->rx_unbnd[n]->ptype      = SSA_C_PLACEMENT_SMOOTH; /* TODO */
//    crdme->species[nA]->rx_unbnd[n]->radius     = r;
//    crdme->species[nA]->rx_unbnd[n]->c          = 0.0;
   
//    //ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_enzymatic_rx_stencil",&flg,NULL);CHKERRQ(ierr);
//    //ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_enzymatic_rx_stencil",&flg,NULL);CHKERRQ(ierr);

//    if(1) {
//      //ierr = PetscMalloc1(crdme->geom->N+1,&crdme->species[nA]->rx_unbnd[n]->Ip);CHKERRQ(ierr);
//      ierr = CRDMEComputePointStencil(crdme,crdme->species[nA]->rx_unbnd[n]->Ip,
//                                &crdme->species[nA]->rx_unbnd[n]->Ij,
//                                crdme->species[nA]->rx_unbnd[n]->radius);CHKERRQ(ierr);
     
//      ierr = PetscMalloc1(crdme->species[nA]->rx_unbnd[n]->Ip[crdme->geom->N],
//                       &crdme->species[nA]->rx_unbnd[n]->rates);CHKERRQ(ierr);
     
//      for(i = 0; i < crdme->geom->N; i++) {
//        ierr = CRDMEGeometryNodeGetVoxelArea(crdme->geom,i,&a);CHKERRQ(ierr);
//        crdme->species[nA]->rx_unbnd[n]->rates[crdme->species[nA]->rx_unbnd[n]->Ip[i]] = a*crdme->species[nA]->rx_unbnd[n]->kAB(0.0,NULL);
//        for(jj = crdme->species[nA]->rx_unbnd[n]->Ip[i] + 1; jj < crdme->species[nA]->rx_unbnd[n]->Ip[i+1]; jj++) {
//          j  = crdme->species[nA]->rx_unbnd[n]->Ij[jj];
//          ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
//          ierr = CRDMEGeometryNodeGetVoxelArea(crdme->geom,j,&a);CHKERRQ(ierr);
//          crdme->species[nA]->rx_unbnd[n]->rates[jj] = crdme->species[nA]->rx_unbnd[n]->rates[jj-1]
//                                                    + a*crdme->species[nA]->rx_unbnd[n]->kAB(rsqr,NULL);
//        }
//      }
//    }   
//    crdme->species[nA]->n_unbnd++;
//    return 0;
// }

// PetscErrorCode CRDMEAddPairUnbindingDoi(CRDME crdme, int nReactant, int nProduct1, int nProduct2, double c, SpatInteractionFn_2D psi, double r, doi_type doi_type, placement_type ptype) {
//    PetscErrorCode ierr;
//    int            i,j,jj;
//    int            nA=nReactant,nB=nProduct1,nC=nProduct2;
//    int            n;
//    double         a;
//    PetscFunctionBegin;

//    crdme->species[nA]->n_unbnd++;
//    if(nA >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nA, crdme->Nspecies);
//      return 1;
//    }
//    if(nB >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }
//    if(nC >= crdme->Nspecies) {
//      PetscPrintf(PETSC_COMM_WORLD,"Specified species number %d greater than total number of species %d\n", nB, crdme->Nspecies);
//      return 1;
//    }
//    n = crdme->species[nA]->n_unbnd;
//    if(n >= CRDME_MAX_UNBIND) {
//      PetscPrintf(PETSC_COMM_WORLD,"Cannot add unbinding reaction. Unbinding reaction count already exceeds limit %d\n", CRDME_MAX_UNBIND);
//      return 1;
//    }
//    n = crdme->species[nA]->n_unbnd-1;
//    ierr = PetscMalloc1(1,&crdme->species[nA]->rx_unbnd[n]);CHKERRQ(ierr);
//    crdme->species[nA]->rx_unbnd[n]->species[0] = nA;// = { speciesA, speciesB };
//    crdme->species[nA]->rx_unbnd[n]->species[1] = nB;
//    crdme->species[nA]->rx_unbnd[n]->species[2] = nC;

//    crdme->species[nA]->rx_unbnd[n]->kAB    = NULL;
//    crdme->species[nA]->rx_unbnd[n]->c = c;
//    crdme->species[nA]->rx_unbnd[n]->radius = r;

//    /* ierr =  PetscOptionsGetBool(NULL,NULL,"-crdme_compute_annihilation_stencil",&flg,NULL);CHKERRQ(ierr); */
//    /* for now, require the stencil */
//    /* later, allow for rates computed on the fly */
//    ierr =  PetscOptionsGetInt(NULL,NULL,"-crdme_doi_type",&doi_type,NULL);CHKERRQ(ierr);
//    crdme->species[nA]->rx_unbnd[n]->doi_type  = doi_type;
//    ptype = SSA_AB_PLACEMENT_SMOOTH;
//    ierr =  PetscOptionsGetInt(NULL,NULL,"-crdme_unbinding_doi_placement_type",&ptype,NULL);CHKERRQ(ierr);
//    crdme->species[nA]->rx_unbnd[n]->ptype      = ptype; /* TODO */
//    if(ptype == SSA_AB_PLACEMENT_SMOOTH) {
//      crdme->doi_type=doi_type;
//      if(doi_type==DOI_STANDARD) {
//        double min_area = PetscPowReal(4.0,-7.0)*crdme->geom->hmax*crdme->geom->hmax; /* seven refinements below minimum mesh length scale */
//        double max_area = 1e-4;
//        int sch1,sch2;
//        PetscBool flg_sch1,flg_sch2;
//        ierr = PetscMalloc1(1,&crdme->q);CHKERRQ(ierr);
//        ierr = PetscMalloc1(1,&crdme->q->sch1);CHKERRQ(ierr);
//        ierr = PetscMalloc1(1,&crdme->q->sch2);CHKERRQ(ierr);
//        ierr = PetscOptionsGetReal(NULL,NULL,"-crdme_doi_min_area",&min_area,NULL);CHKERRQ(ierr);
//        ierr = PetscOptionsGetReal(NULL,NULL,"-crdme_doi_max_area",&max_area,NULL);CHKERRQ(ierr);
//        crdme->q->max_area = max_area;
//        crdme->q->min_area = min_area;
//        crdme->q->sch1->n = 25;
//        crdme->q->sch1->weights = w_duv25;
//        crdme->q->sch1->points = p_duv25;
//        crdme->q->sch2->n = 7;
//        crdme->q->sch2->weights = w_duv7;
//        crdme->q->sch2->points = p_duv7;
//        ierr = PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_scheme1",&sch1,&flg_sch1);CHKERRQ(ierr);
//        if(flg_sch1) {
//          switch(sch1) {
//          case 0: 
//            crdme->q->sch1->n = 1;
//            PetscMalloc1(1,&crdme->q->sch1->weights);
//            PetscMalloc1(2,&crdme->q->sch1->points);
//            PetscArraycpy(crdme->q->sch1->weights,w_centr,1);
//            PetscArraycpy(crdme->q->sch1->points,p_centr,2);
//            break;
//          case 1:
//            crdme->q->sch1->n = 7;
//            PetscMalloc1(7,&crdme->q->sch1->weights);
//            PetscMalloc1(14,&crdme->q->sch1->points);
//            PetscArraycpy(crdme->q->sch1->weights,w_duv7,7);
//            PetscArraycpy(crdme->q->sch1->points,p_duv7,7);
//            break;
//          default:
//            crdme->q->sch1->n = 25;
//            PetscMalloc1(25,&crdme->q->sch1->weights);
//            PetscMalloc1(50,&crdme->q->sch1->points);
//            PetscArraycpy(crdme->q->sch1->weights,w_duv25,25);
//            PetscArraycpy(crdme->q->sch1->points,p_duv25,50);
//          }
//        }
//        ierr = PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_scheme1",&sch2,&flg_sch2);CHKERRQ(ierr);
//        if(flg_sch2) {
//          switch(sch2) {
//          case 0: 
//            crdme->q->sch2->n = 1;
//            PetscMalloc1(1,&crdme->q->sch2->weights);
//            PetscMalloc1(2,&crdme->q->sch2->points);
//            PetscArraycpy(crdme->q->sch2->weights,w_centr,1);
//            PetscArraycpy(crdme->q->sch2->points,p_centr,2);
//            break;
//          case 2:
//            crdme->q->sch2->n = 25;
//            PetscMalloc1(25,&crdme->q->sch2->weights);
//            PetscMalloc1(50,&crdme->q->sch2->points);
//            PetscArraycpy(crdme->q->sch2->weights,w_duv25,25);
//            PetscArraycpy(crdme->q->sch2->points,p_duv25,50);
//          default:
//            crdme->q->sch2->n = 7;
//            PetscMalloc1(7,&crdme->q->sch2->weights);
//            PetscMalloc1(14,&crdme->q->sch2->points);
//            PetscArraycpy(crdme->q->sch2->weights,w_duv7,7);
//            PetscArraycpy(crdme->q->sch2->points,p_duv7,7);
//          }
//        }
//      } else if(doi_type==DOI_STANDARD_SQUARE) {
//        double rtol = 1e-8;
//        double atol = 1e-14;
//        int    max_eval = 0;
//        ierr = PetscMalloc1(1,&crdme->p);CHKERRQ(ierr);
//        PetscOptionsGetReal(NULL,NULL,"-crdme_doi_quad_rtol",&rtol,NULL);CHKERRQ(ierr);
//        PetscOptionsGetReal(NULL,NULL,"-crdme_doi_quad_atol",&rtol,NULL);CHKERRQ(ierr);
//        PetscOptionsGetInt(NULL,NULL,"-crdme_doi_quad_max_eval",&max_eval,NULL);CHKERRQ(ierr);
//        crdme->p->atol = atol;
//        crdme->p->rtol = rtol;
//        crdme->p->max_eval = max_eval;
//      }
//      ierr = CRDMEComputePointStencilDoi(crdme,&crdme->species[nA]->rx_unbnd[n]->Ip,
//                                  &crdme->species[nA]->rx_unbnd[n]->Ij,
//                                  &crdme->species[nA]->rx_unbnd[n]->rates,
//                                  crdme->species[nA]->rx_unbnd[n]->radius,1,doi_type);CHKERRQ(ierr);
//      int rank,nodesize,noderank;
//      MPI_Comm nodecomm;
     
//      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
//      if(!crdme->species[nA]->rx_unbnd[n]->Ip) {
//        PetscPrintf(PETSC_COMM_SELF,"Error: pointer array not allocated properly on process %d\n",rank);
//        return 1;
//      }
//      MPI_Barrier(PETSC_COMM_WORLD);
//      MPI_Comm_split_type(PETSC_COMM_WORLD, MPI_COMM_TYPE_SHARED,rank,
//                  MPI_INFO_NULL, &nodecomm);
//      MPI_Comm_size(nodecomm, &nodesize);
//      MPI_Comm_rank(nodecomm, &noderank);
//      if(!noderank) {
//        for(int jj = 0; jj < crdme->species[nA]->rx_unbnd[n]->Ip[crdme->geom->N]; jj++) {
//          crdme->species[nA]->rx_unbnd[n]->rates[jj]*= (ptype == SSA_AB_PLACEMENT_POINT ? c : c/(PETSC_PI*r*r) );
//        } 
//     	 double r;
//        for(i = 0; i < crdme->geom->N; i++) {
//       	 jj = crdme->species[nA]->rx_unbnd[n]->Ip[i];
//          ierr = CRDMEGeometryNodeGetVoxelArea(crdme->geom,i,&a);CHKERRQ(ierr);
//          crdme->species[nA]->rx_unbnd[n]->rates[jj] *= a;
//          if(psi) {
//            crdme->species[nA]->rx_unbnd[n]->rates[jj] *= psi(0.0,crdme->ctx);
//          }
//          for(jj = crdme->species[nA]->rx_unbnd[n]->Ip[i] + 1; jj < crdme->species[nA]->rx_unbnd[n]->Ip[i+1]; jj++) {
//            j  = crdme->species[nA]->rx_unbnd[n]->Ij[jj];
//            if(psi) {
//              ierr = CRDMEGeometryNodeGetVoxelArea(crdme->geom,j,&a);CHKERRQ(ierr);
//              ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&r);CHKERRQ(ierr);
//              crdme->species[nA]->rx_unbnd[n]->rates[jj] = crdme->species[nA]->rx_unbnd[n]->rates[jj-1]
//                                                             +  a*psi(r,crdme->ctx)*crdme->species[nA]->rx_unbnd[n]->rates[jj];
//            } else {
//              ierr = CRDMEGeometryNodeGetVoxelArea(crdme->geom,j,&a);CHKERRQ(ierr);
//              crdme->species[nA]->rx_unbnd[n]->rates[jj] = crdme->species[nA]->rx_unbnd[n]->rates[jj-1]
//                                                             +  a*crdme->species[nA]->rx_unbnd[n]->rates[jj];
//            }

//          }
//        }  
//      }
//      MPI_Barrier(PETSC_COMM_WORLD);
//    } else if(ptype == SSA_AB_PLACEMENT_POINT) {
//      crdme->species[nA]->rx_unbnd[n]->Ip = NULL;
//      crdme->species[nA]->rx_unbnd[n]->Ij = NULL;
//      crdme->species[nA]->rx_unbnd[n]->rates = NULL;
//    } else {
//      PetscPrintf(PETSC_COMM_WORLD, "Unrecognized or unsupported placement type %d\n",ptype);
//      PetscPrintf(PETSC_COMM_WORLD, "Supported types are: %d, reversible placement mechanism and %d, point unbinding\n",SSA_AB_PLACEMENT_SMOOTH,SSA_AB_PLACEMENT_POINT);
//      return 1;
//    }
//    return 0;
// }


// PetscErrorCode CRDMEView_ascii(CRDME crdme) {
  
//   CRDMESpecies   s;
//   int            max=0;
//   PetscErrorCode ierr;
  
//   PetscFunctionBegin;

//   ierr = PetscPrintf(PETSC_COMM_WORLD,"CRDME with %d species\n",crdme->Nspecies);CHKERRQ(ierr);
//   ierr = PetscPrintf(PETSC_COMM_WORLD,"CRDME geometry with %d edges and %d voxels\n",crdme->geom->num_edges,crdme->geom->N);CHKERRQ(ierr);
//   ierr = PetscPrintf(PETSC_COMM_WORLD,"maximum mesh width %1.14f\n",crdme->geom->hmax);CHKERRQ(ierr);
//   for(int i = 0; i < crdme->Nspecies; i++) {
//     s = crdme->species[i];
//     PetscPrintf(PETSC_COMM_WORLD,"CRDME species %s\n",s->name);
//     PetscPrintf(PETSC_COMM_WORLD,"  Diffusion constant %1.6f\n",s->D);
//     for(int j = 0; j < crdme->Nspecies; j++) {
//       if(s->pair_potentials[j]) {
//         PetscPrintf(PETSC_COMM_WORLD,"  pair potential with species %s\n",crdme->species[j]->name);
//         PetscPrintf(PETSC_COMM_WORLD,"    interaction radius %1.6f\n",s->pair_potentials[j]->radius);
//         if(s->pair_potentials[j]->Ip) {
//           for(int k = 0; k < crdme->geom->N; k++) {
//             if(s->pair_potentials[j]->Ip[k+1]-s->pair_potentials[j]->Ip[k]>max){
//               max=s->pair_potentials[j]->Ip[k+1]-s->pair_potentials[j]->Ip[k];
//             }
//           }
//           PetscPrintf(PETSC_COMM_WORLD,"    maximum stencil width %d\n",max);
//         } else {
//           PetscPrintf(PETSC_COMM_WORLD,"    interaction stencil not set\n",max);
//         }
//       }
//     }
//     for(int j = 0; j < crdme->Nspecies; j++) {
//       max=0;
//       if(s->pair_rxs[j]) {
//         if(s->pair_rxs[j]->me_type!=CALLBACK) {
//           if(s->pair_rxs[j]->rtype == RX_ANN) {
//             PetscPrintf(PETSC_COMM_WORLD,"  Doi-type pair annihilation with species %s\n",crdme->species[j]->name);
//           } else if(s->pair_rxs[j]->rtype == RX_BND) {
//             PetscPrintf(PETSC_COMM_WORLD,"  Doi-type pair binding with species %s\n",crdme->species[j]->name);
//             PetscPrintf(PETSC_COMM_WORLD,"  producing species %s\n",crdme->species[s->pair_rxs[j]->species[2]]->name);
//           } else if(s->pair_rxs[j]->rtype == RX_ENZ) {
//             PetscPrintf(PETSC_COMM_WORLD,"  Doi-type enzymatic reaction with species %s\n",crdme->species[j]->name);
//             PetscPrintf(PETSC_COMM_WORLD,"  producing species %s\n",crdme->species[s->pair_rxs[j]->species[2]]->name);
//           }
//           PetscPrintf(PETSC_COMM_WORLD,"    interaction radius %1.6f\n",s->pair_rxs[j]->radius);
//           PetscPrintf(PETSC_COMM_WORLD,"    reaction rate %1.6f\n",s->pair_rxs[j]->c);

//           if(s->pair_rxs[j]->Ip) {
//             for(int k = 0; k < crdme->geom->N; k++) {
//               if(s->pair_rxs[j]->Ip[k+1]-s->pair_rxs[j]->Ip[k]>max){
//                 max=s->pair_rxs[j]->Ip[k+1]-s->pair_rxs[j]->Ip[k];
//               }
//             }
//             PetscPrintf(PETSC_COMM_WORLD,"    maximum stencil width %d\n",max);
//           } else {
//             PetscPrintf(PETSC_COMM_WORLD,"    interaction stencil not set\n",max);
//           }
//         } else {
//           if(s->pair_rxs[j]->rtype == RX_ANN) {
//             PetscPrintf(PETSC_COMM_WORLD,"  pair annihilation with species %s\n",crdme->species[j]->name);
//           } else if(s->pair_rxs[j]->rtype == RX_BND) {
//             PetscPrintf(PETSC_COMM_WORLD,"  pair binding with species %s\n",crdme->species[j]->name);
//             PetscPrintf(PETSC_COMM_WORLD,"  producing species %s\n",crdme->species[s->pair_rxs[j]->species[2]]->name);
//           } else if(s->pair_rxs[j]->rtype == RX_ENZ) {
//             PetscPrintf(PETSC_COMM_WORLD,"  enzymatic reaction with species %s\n",crdme->species[j]->name);
//             PetscPrintf(PETSC_COMM_WORLD,"  producing species %s\n",crdme->species[s->pair_rxs[j]->species[2]]->name);
//           }
//           PetscPrintf(PETSC_COMM_WORLD,"    interaction radius %1.6f\n",s->pair_rxs[j]->radius);
//           if(s->pair_rxs[j]->Ip) {
//             for(int k = 0; k < crdme->geom->N; k++) {
//               if(s->pair_rxs[j]->Ip[k+1]-s->pair_rxs[j]->Ip[k]>max){
//                 max=s->pair_rxs[j]->Ip[k+1]-s->pair_rxs[j]->Ip[k];
//               }
//             }
//             PetscPrintf(PETSC_COMM_WORLD,"    maximum stencil width %d\n",max);
//           } else {
//             PetscPrintf(PETSC_COMM_WORLD,"    interaction stencil not set\n",max);
//           }
//         }
//       }

//     }
//     for(int j = 0; j < s->n_unbnd; j++) {
//       PetscPrintf(PETSC_COMM_WORLD,"  unbinding reaction producing species %s and species %s\n",crdme->species[s->rx_unbnd[j]->species[1]]->name,
//                                       crdme->species[s->rx_unbnd[j]->species[2]]->name);
//       PetscPrintf(PETSC_COMM_WORLD,"    interaction radius %1.6f\n",s->rx_unbnd[j]->radius);
//       PetscPrintf(PETSC_COMM_WORLD,"    unbinding rate %1.6f\n",s->rx_unbnd[j]->c);
//       switch(s->rx_unbnd[j]->ptype) {
//       case SSA_AB_PLACEMENT_POINT:
//         PetscPrintf(PETSC_COMM_WORLD,"    using point placement mechanism\n");
//         break;
//       default:
//         PetscPrintf(PETSC_COMM_WORLD,"    using reversible placement mechanism\n");
//         for(int k = 0; k < crdme->geom->N; k++) {
//            if(s->rx_unbnd[j]->Ip[k+1]-s->rx_unbnd[j]->Ip[k]>max){
//              max=s->rx_unbnd[j]->Ip[k+1]-s->rx_unbnd[j]->Ip[k];
//            }
//          }
//         PetscPrintf(PETSC_COMM_WORLD,"    maximum placement stencil width %d\n",max);
//       }


//     }
    

//   }
//   return 0;
// }

//
//PetscErrorCode CRDMEGeomView_vtk(CRDME crdme, const char *filename) {
//  FILE *f = NULL;
//  CRDMEGeometry geom = crdme->geom;
//  char vtk_filename[50];
//  int offset = 0;
//  int Np, Nc;
//  int bytes;
//  int *cells, *offsets;
//  double *nodes;
//  uint8_t *types;
//  PetscErrorCode ierr;
//   
//  PetscFunctionBegin;
//  int num_dual_nodes = 0;
//  for(int i = 0; i < geom->N; i++) {
//     num_dual_nodes += 2*(geom->Ap[i + 1] - geom->Ap[i]); 
//  }
//  
//  Np = num_dual_nodes;
//  Nc = geom->N;
//  
//  ierr  = PetscMalloc1(Np,&cells);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(3*Np,&nodes);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(Nc,&types);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(Nc,&offsets);CHKERRQ(ierr);
//
//  snprintf(vtk_filename, sizeof(vtk_filename), "%s.vtu", filename);
//  f = fopen(vtk_filename, "wb");
//  fprintf(f, "<?xml version=\"1.0\"?>\n");
//  fprintf(f, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"%s\">\n", "LittleEndian");
//  fprintf(f, "  <UnstructuredGrid>\n");
//
//  fprintf(f, "    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\n", Np, Nc);
//  fprintf(f, "      <Points>\n");
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"Position\""
//    " NumberOfComponents=\"3\" format=\"appended\" offset=\"%D\" />\n", "Float64", offset);
//
//  offset += 4;
//  offset += 3 * sizeof(double) * Np;
//
//  fprintf(f, "      </Points>\n");
//  fprintf(f, "      <Cells>\n");
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"connectivity\""
//    " format=\"%s\" offset=\"%D\" />\n", "Int32", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(int) * Np;
//
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"offsets\""
//    " format=\"%s\"  offset=\"%D\" />\n", "Int32", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(int) * Nc;
//
//  fprintf(f, "        <DataArray type=\"UInt8\" Name=\"types\""
//    " format=\"%s\" offset=\"%D\" />\n", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(uint8_t) * Nc;
//
//  fprintf(f, "      </Cells>\n");
//  fprintf(f, "    </Piece>\n");
//
//  fprintf(f, "  </UnstructuredGrid>\n");
//
//  fprintf(f, "  <AppendedData encoding=\"raw\">\n");
//  fprintf(f, "_");
//  
//  compute_dual_mesh_vtk(geom,cells,nodes,offsets,types);
//  bytes = 3*sizeof(double)*Np;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(nodes, sizeof(double), 3*Np, f);
//  
//  bytes = sizeof(int)*Np;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(cells, sizeof(int), Np, f);
//
//  bytes = sizeof(int)*Nc;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(offsets,sizeof(int),Nc,f);
//
//  bytes = sizeof(uint8_t)*Nc;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(&(types[0]),sizeof(uint8_t),Nc,f);
//  
//  fprintf(f, "\n");
//  fprintf(f, "  </AppendedData>\n");
//  fprintf(f, "</VTKFile>");
//  fclose(f);
//
//  PetscFree(cells);
//  PetscFree(nodes);
//  PetscFree(types);
//  PetscFree(offsets);
//  return 0;
//}
/*

this is not really needed right now.

void crdme_create(CRDME_t *crdme, int nspecies, int nppotentials, int nbpotentials, int nreactionsA0,
                                  int nreactionsAB0, int nreactionsABC, int nreactionsCAB) {
    
    crdme = (CRDME_t *) malloc(sizeof(CRDME_t));
    (*crdme)->geom = NULL;
}
*/
