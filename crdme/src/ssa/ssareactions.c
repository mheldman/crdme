#include <ssareactions.h>
#include <crdmegeometry.h>
#include <ssa.h>
#include <rng.h>

/* Specialized rate functions */

PetscErrorCode DoiIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Doi not yet implemented\n");CHKERRQ(ierr); 
  return 1;

  PetscFunctionReturn(0);
}

PetscErrorCode DoiLumpIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint x;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  PetscReal      mass,rsqr;
  DoiIaFnCtx     *c=ctx;
  
  PetscFunctionBegin;
  
  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryVoxelSphereIntersectionArea(geom,x,c->r,*voxel,rate);CHKERRQ(ierr);

  ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
  if(*rate < 0.0) {
    *rate = (i == j ? c->kap*PETSC_PI*PetscSqr(c->r) / mass : 0.0);
  } else {
    *rate /= mass;
    *rate *= c->kap;
  }

  if(c->w) {
    ierr    = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
    *rate  *= PetscExpReal(-c->w(rsqr,c->ctx));
  }


  PetscFunctionReturn(0);
}

PetscErrorCode SymmetricDoiLumpIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *result, void *ctx) {
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  if(i==j) {
    ierr = DoiLumpIaFn(geom,i,j,result,ctx);CHKERRQ(ierr);
  } else {
    PetscReal      temp;
    *result  = 0.0;
    ierr     = DoiLumpIaFn(geom,i,j,&temp,ctx);CHKERRQ(ierr);
    *result += .5*temp;
    ierr     = DoiLumpIaFn(geom,j,i,&temp,ctx);CHKERRQ(ierr);
    *result += .5*temp;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode DoiLumpRejectIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint x;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  PetscReal      dist;
  DoiIaFnCtx     *c=ctx;
  
  PetscFunctionBegin;

  if(i==j) {
    *rate = c->kap;
    if(c->w) {
      *rate *= PetscExpReal(-c->w(0.0,c->ctx));
    }
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);

  *rate = (dist < PetscSqr(c->r) ? c->kap : 0.0);
  if(c->w && dist < PetscSqr(c->r)) {
    ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
    *rate *= PetscExpReal(-c->w(dist,c->ctx));
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SymmetricDoiLumpRejectIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint x;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  PetscReal      dist;
  DoiIaFnCtx     *c=ctx;
  
  PetscFunctionBegin;

  if(i==j) {
    *rate = c->kap;
    if(c->w) {
      *rate *= PetscExpReal(-c->w(0.0,c->ctx));
    }
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);
  if(dist < PetscSqr(c->r)) {
    *rate = c->kap;
    if(c->w) {
      ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
      *rate *= PetscExpReal(-c->w(dist,c->ctx));
    }
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,j,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,i,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);

  *rate = (dist < PetscSqr(c->r) ? c->kap : 0.0);
  if(c->w) {
    ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
    *rate *= PetscExpReal(-c->w(dist,c->ctx));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode DoiRejectIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint xi,xj;
  CRDMEGeomVoxel *voxeli=&geom->work_voxel[0];
  CRDMEGeomVoxel *voxelj=&geom->work_voxel[1];
  PetscReal      dist,rsqr;
  DoiIaFnCtx     *c=ctx;
  
  PetscFunctionBegin;

  if(i == j) {
    *rate=c->kap;
    if(c->w) {
      *rate *= PetscExpReal(-c->w(0,c->ctx));
    }
    PetscFunctionReturn(0);
  }
  
  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,xi);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,i,voxeli);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetCoordinates(geom,j,xj);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxelj);CHKERRQ(ierr);

  *rate=0.0;
  rsqr = PetscSqr(c->r);
  for(PetscInt n=0;n<voxelj->length;n++) {
    ierr = CRDMEGeometryMinDistanceSqr(geom,voxelj->nodes[n],*voxeli,&dist);CHKERRQ(ierr);
    if(dist < rsqr) {
      *rate=c->kap;
      if(c->w) {
        ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
        *rate *= PetscExpReal(-c->w(dist,c->ctx));
      }
      PetscFunctionReturn(0);
    }
  }

  for(PetscInt n=0;n<voxeli->length;n++) {
    ierr = CRDMEGeometryMinDistanceSqr(geom,voxeli->nodes[n],*voxelj,&dist);CHKERRQ(ierr);
    if(dist < rsqr) {
      *rate=c->kap;
      if(c->w) {
        ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
        *rate *= PetscExpReal(-c->w(dist,c->ctx));
      }
      PetscFunctionReturn(0);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SmoothIaFn(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscReal *rate, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      rsqr;
  SmoothIaFnCtx  *c=ctx;
  
  PetscFunctionBegin;

  ierr  = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
  *rate = c->kap*c->k(rsqr,c->ctx);

  PetscFunctionReturn(0);
}

/* Cheap functions for checking one node is in the stencil of another */

PetscErrorCode VoxelIsInStencil_Doi(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscBool *in, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      *rsqr=ctx;
  CRDMEGeomVoxel *voxel1=&geom->work_voxel[0];
  CRDMEGeomVoxel *voxel2=&geom->work_voxel[1];
  PetscReal      dist;

  PetscFunctionBegin;

  if(i==j) {
    *in=PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetVoxel(geom,i,voxel1);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel2);CHKERRQ(ierr);

  for(PetscInt i=0;i<voxel1->length;i++) {
    ierr = CRDMEGeometryMinDistanceSqr(geom,voxel1->nodes[i],*voxel2,&dist);CHKERRQ(ierr);
    if(dist < *rsqr) {
      *in = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }

  for(PetscInt i=0;i<voxel2->length;i++) {
    ierr = CRDMEGeometryMinDistanceSqr(geom,voxel2->nodes[i],*voxel1,&dist);CHKERRQ(ierr);
    if(dist < *rsqr) {
      *in = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode VoxelIsInStencil_DoiLump(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscBool *in, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      *rsqr=ctx;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  CRDMEGeomPoint x;
  PetscReal      dist;

  PetscFunctionBegin;

  *in = PETSC_FALSE;
  if(i==j) {
    *in=PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);

  if(dist < *rsqr) {
    *in = PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode VoxelIsInStencil_SymmetricDoiLump(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscBool *in, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      *rsqr=ctx;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  CRDMEGeomPoint x;
  PetscReal      dist;

  PetscFunctionBegin;

  *in = PETSC_FALSE;
  if(i==j) {
    *in=PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,j,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);

  if(dist < *rsqr) {
    *in = PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodeGetCoordinates(geom,j,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryNodeGetVoxel(geom,i,voxel);CHKERRQ(ierr);
  ierr = CRDMEGeometryMinDistanceSqr(geom,x,*voxel,&dist);CHKERRQ(ierr);

  if(dist < *rsqr) {
    *in = PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode VoxelIsInStencil_Smooth(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscBool *in, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      *rsqr=ctx;
  PetscReal      dist;

  PetscFunctionBegin;
  
  if(i==j) {
    *in = PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
  if(dist < *rsqr) {
    *in = PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode VoxelIsInStencil_Edge(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscBool *in, void *ctx) {
  PetscErrorCode ierr;
  PetscReal      *rsqr=ctx;
  PetscReal      dist;

  PetscFunctionBegin;
  
  if(i==j) {
    *in = PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&dist);CHKERRQ(ierr);
  if(dist < *rsqr) {
    *in = PETSC_TRUE;
    PetscFunctionReturn(0);
  }

  PetscInt        nn;
  CRDMEGeomGloIdx Aj[geom->maxNodeDegree],k;
  ierr = CRDMEGeometryNodeGetNeighbors(geom,j,&nn,Aj);CHKERRQ(ierr);

  for(PetscInt jj=0;jj<nn;jj++) {
    k = Aj[jj];
    ierr = CRDMEGeometryNodesGetDistanceSqr(geom,k,i,&dist);CHKERRQ(ierr);
    if(dist < *rsqr) {
      *in = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }

  PetscFunctionReturn(0);
}

/* Specialized rejection mechanisms */

PetscErrorCode DoiLumpPairRejectionFn(SpatialSSA ssa, void *ctx) {
  PetscErrorCode ierr;
  SSAState       state=ssa->state;
  PetscReal      *radius=ctx;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomPoint x,y;
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,state->lastEvent.remLoc[0],x);CHKERRQ(ierr);
  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.remLoc[1],y);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);

  state->lastEvent.reject = (dist > PetscSqr(*radius));

  PetscFunctionReturn(0);
}

PetscErrorCode DoiPairRejectionFn(SpatialSSA ssa, void *ctx) {
  PetscErrorCode ierr;
  SSAState       state=ssa->state;
  PetscReal      *radius=ctx;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomPoint x,y;
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.remLoc[0],x);CHKERRQ(ierr);
  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.remLoc[1],y);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);

  state->lastEvent.reject = (dist > PetscSqr(*radius));

  PetscFunctionReturn(0);
}

PetscErrorCode SymmetricDoiLumpPairRejectionFn(SpatialSSA ssa, void *ctx) {
  PetscErrorCode ierr;
  SSAState       state=ssa->state;
  PetscReal      *radius=ctx;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomPoint x,y;
  PetscReal      dist;
  
  PetscFunctionBegin;

  if(KISS % 2) {
    ierr = CRDMEGeometryNodeGetCoordinates(geom,state->lastEvent.remLoc[0],x);CHKERRQ(ierr);
    ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.remLoc[1],y);CHKERRQ(ierr);
    ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);
    state->lastEvent.reject = (dist > PetscSqr(*radius));
  } else {
    ierr = CRDMEGeometryNodeGetCoordinates(geom,state->lastEvent.remLoc[1],x);CHKERRQ(ierr);
    ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.remLoc[0],y);CHKERRQ(ierr);
    ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);
    state->lastEvent.reject = (dist > PetscSqr(*radius));
  }

  PetscFunctionReturn(0);
}

PetscErrorCode DoiLumpUnbindRejectionFn(SpatialSSA ssa, void *ctx) {
  PetscErrorCode ierr;
  SSAState       state=ssa->state;
  PetscReal      *radius=ctx;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomPoint x,y;
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,state->lastEvent.addLoc[0],x);CHKERRQ(ierr);
  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.addLoc[1],y);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);
  state->lastEvent.reject = (dist > PetscSqr(*radius));

  PetscFunctionReturn(0);
}

PetscErrorCode DoiUnbindRejectionFn(SpatialSSA ssa, void *ctx) {
  PetscErrorCode ierr;
  SSAState       state=ssa->state;
  PetscReal      *radius=ctx;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomPoint x,y;
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.addLoc[0],x);CHKERRQ(ierr);
  ierr = CRDMEGeometrySampleFromVoxel(geom,state->lastEvent.addLoc[1],y);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);

  state->lastEvent.reject = (dist > PetscSqr(*radius));

  PetscFunctionReturn(0);
}

PetscErrorCode ReversibleReactionRejectionFn(SpatialSSA ssa) {
  PetscInt       sp,spb,loc,locb;
  SSAState       state=ssa->state;
  SSASpecies     s;
  PetscReal      energyChange=0.0,rsqr;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  for(PetscInt i=0;i<state->lastEvent.nRem;i++) {
    sp=state->lastEvent.remSpec[i];
    s =state->spec[sp];
    if(state->spec[sp]->hasPot) {
      loc=state->lastEvent.remLoc[i];
      energyChange-=s->potentials.potEnergy[loc];
      if(state->spec[sp]->potentials.pp[sp]) { /* correction for self potential */
        energyChange += state->spec[sp]->potentials.pp[sp]->potFn(0,ssa->ctx);
      }
      if(state->lastEvent.type == REACTION_ORD_2 && i==0) { /* force between associating particles already accounted for */      
        spb=state->lastEvent.remSpec[1];
        if(state->spec[sp]->potentials.pp[spb]) {
          locb=state->lastEvent.remLoc[1];
          ierr = CRDMEGeometryNodesGetDistanceSqr(ssa->geom,loc,locb,&rsqr);CHKERRQ(ierr);
          energyChange += 2*state->spec[sp]->potentials.pp[spb]->potFn(rsqr,ssa->ctx);
        }
      }
    }
  }


  for(PetscInt i=0;i<state->lastEvent.nAdd;i++) {
    sp=state->lastEvent.addSpec[i];
    s =state->spec[sp];
    if(state->spec[sp]->hasPot) {
      loc=state->lastEvent.addLoc[i];
      energyChange+=s->potentials.potEnergy[loc];
      for(PetscInt j=0;j<state->lastEvent.nRem;j++) { /* correction for removed particles */
        spb=state->lastEvent.remSpec[j];
        if(s->potentials.pp[spb]) {
          locb=state->lastEvent.remLoc[j];
          ierr = CRDMEGeometryNodesGetDistanceSqr(ssa->geom,loc,locb,&rsqr);CHKERRQ(ierr);
          energyChange-=s->potentials.pp[spb]->potFn(rsqr,ssa->ctx);
        }
      }
    }
  }

  if(energyChange <= 0.0) {
    state->lastEvent.revReject=PETSC_FALSE;
  } else {
    state->lastEvent.revReject=(UNI > PetscExpReal(-energyChange));
  }

  PetscFunctionReturn(0);
}

/* Initial conditions */

PetscErrorCode uniformDistribution(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *val, void *ctx) {

  PetscFunctionBegin;

  *val=1.0;

  PetscFunctionReturn(0);
}
