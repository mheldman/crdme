#define _XOPEN_SOURCE
#include <ssa.h>
#include <ssansm.h>
#include <ssanbhditerators.h>
#include <heap.h>
#include <stdlib.h> 


PetscErrorCode SSASetUp_NSM(SpatialSSA ssa) {
  SpatialSSA_NSM  *nsm  = ssa->data;
  PetscInt        N=ssa->geom->N;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  ierr = PetscMalloc1(1,&nsm->queue);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->queue->index);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->queue->nextSubvolume);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->queue->voxelNRT);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->queuedForUpdate);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->updateQueue);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&nsm->voxelRateDiff);CHKERRQ(ierr);
  nsm->queue->N = N;

  ssa->itContexts.hoppingRatesUpdateCtx.data=nsm;
  ssa->itContexts.nbhdRatesUpdateCtx.data=nsm;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAGetNextSubvolume_NSM(SpatialSSA ssa) {
  SpatialSSA_NSM  *nsm      = ssa->data;
  CRDMEGeomGloIdx loc       = nsm->queue->nextSubvolume[0];

  PetscFunctionBegin;

	ssa->state->lastEvent.loc=loc;
  ssa->state->rand         =erand48(ssa->seed)*ssa->state->voxelRatesum[loc];

  nsm->queuedForUpdate[loc]=PETSC_TRUE;
  nsm->updateQueue[nsm->nQueuedForUpdate]=loc;
  nsm->nQueuedForUpdate++;

  if(ssa->state->voxelRatesum[loc] < 1e-13) {
    CHKERRQ( PriorityQueueView(nsm->queue) );
    CHKERRQ( PriorityQueueTestParity(nsm->queue) );
    return 1;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SSAStateInitialize_NSM(SpatialSSA ssa) {
  SpatialSSA_NSM *nsm      =ssa->data;  
  PriorityQueue  queue     =nsm->queue;
  PetscReal      *voxelRatesum=ssa->state->voxelRatesum;
  PetscInt       N=ssa->geom->N;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  for(PetscInt i=0;i<N;i++) {
    queue->voxelNRT[i]     = (voxelRatesum[i] > PETSC_SMALL ? -PetscLogReal(erand48(ssa->seed)) / voxelRatesum[i] : ssa->Tf+1.0);
    queue->index[i]        =i;
    queue->nextSubvolume[i]=i;
  }

  ierr = PetscArrayzero(nsm->queuedForUpdate,N);CHKERRQ(ierr);
  ierr = PetscArrayzero(nsm->voxelRateDiff,N);CHKERRQ(ierr);
  nsm->nQueuedForUpdate=0;

  ierr = PriorityQueueInitialize(queue);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode SSAGetNextEventTime_NSM(SpatialSSA ssa) {
  SpatialSSA_NSM           *nsm=ssa->data; 
  PriorityQueue            queue=nsm->queue;
  SSAState                 state=ssa->state;
  PetscInt                 loc;
  PetscReal                t;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  for(PetscInt i=0;i<nsm->nQueuedForUpdate;i++) {
    loc  =nsm->updateQueue[i];
    if(state->voxelRatesum[loc] < PETSC_SMALL) {
      t=ssa->Tf+1.0;
    } else if(1 || i==0 || queue->voxelNRT[queue->index[loc]] == ssa->Tf+1.0) {
      t=state->curTime - PetscLogReal(erand48(ssa->seed)) / state->voxelRatesum[loc];
    } else if(PetscAbs(nsm->voxelRateDiff[loc]) > PETSC_SMALL) {
      t = state->curTime +
     (state->voxelRatesum[loc] - nsm->voxelRateDiff[loc])*(queue->voxelNRT[queue->index[loc]]-state->curTime)/state->voxelRatesum[loc];
    } 

    if(1 || i==0 || PetscAbs(nsm->voxelRateDiff[loc]) > PETSC_SMALL) {
      ierr = PriorityQueueUpdate(queue,loc,t);CHKERRQ(ierr);
    }
    nsm->queuedForUpdate[loc]=PETSC_FALSE;
    nsm->voxelRateDiff[loc]=0.0;
  }
  nsm->nQueuedForUpdate=0;

  ssa->state->prevTime=ssa->state->curTime;
  ssa->state->curTime =nsm->queue->voxelNRT[0];

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesAddToVoxel_NSM(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {
  SpatialSSA_NSM  *nsm      = ssa->data;

  PetscFunctionBegin;
  
  if(!nsm->queuedForUpdate[i]) {
    nsm->queuedForUpdate[i]=PETSC_TRUE;
    nsm->updateQueue[nsm->nQueuedForUpdate]=i;
    nsm->nQueuedForUpdate++;
    if(ssa->state->lastEvent.loc!=i) {
      nsm->voxelRateDiff[i] += ssa->state->ratesum - ssa->state->oldRatesum;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesRemoveFromVoxel_NSM(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {  
  SpatialSSA_NSM  *nsm      = ssa->data;

  PetscFunctionBegin;
  
  if(!nsm->queuedForUpdate[i]) {
    nsm->queuedForUpdate[i]=PETSC_TRUE;
    nsm->updateQueue[nsm->nQueuedForUpdate]=i;
    nsm->nQueuedForUpdate++;
    if(ssa->state->lastEvent.loc!=i) {
      nsm->voxelRateDiff[i] += ssa->state->ratesum - ssa->state->oldRatesum;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdateHoppingRatesCallback_NSM(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx) {
  SSAUpdateHoppingRatesCtx *c  =(SSAUpdateHoppingRatesCtx *)ctx;
  SpatialSSA_NSM           *nsm= c->data;

  PetscFunctionBegin;

  if(!nsm->queuedForUpdate[j]) {
    nsm->queuedForUpdate[j]=PETSC_TRUE;
    nsm->updateQueue[nsm->nQueuedForUpdate]=j;
    nsm->nQueuedForUpdate++;
    if(c->ssa->state->lastEvent.loc!=j) {
      nsm->voxelRateDiff[j] += c->rateDiff;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdatePairRatesCallback_NSM(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx) {
  SSAUpdateNbhdRatesCtx  *c=(SSAUpdateNbhdRatesCtx *)ctx;
  SpatialSSA_NSM         *nsm=c->data;

  PetscFunctionBegin;
  
  if(!nsm->queuedForUpdate[j]) {
    nsm->queuedForUpdate[j]=PETSC_TRUE;
    nsm->updateQueue[nsm->nQueuedForUpdate]=j;
    nsm->nQueuedForUpdate++;
    if(c->ssa->state->lastEvent.loc!=j) {
      nsm->voxelRateDiff[j] += c->rate;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSADestroy_NSM(SpatialSSA *ssa) {
  SpatialSSA_NSM  *nsm  = (*ssa)->data;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  ierr = PetscFree(nsm->queue->index);CHKERRQ(ierr);
  ierr = PetscFree(nsm->queue->nextSubvolume);CHKERRQ(ierr);
  ierr = PetscFree(nsm->queue->voxelNRT);CHKERRQ(ierr);
  ierr = PetscFree(nsm->queuedForUpdate);CHKERRQ(ierr);
  ierr = PetscFree(nsm->updateQueue);CHKERRQ(ierr);
  ierr = PetscFree(nsm->queue);CHKERRQ(ierr);
  ierr = PetscFree(nsm);CHKERRQ(ierr);

  (*ssa)->data=PETSC_NULL;

  PetscFunctionReturn(0);
}





