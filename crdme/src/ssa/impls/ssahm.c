#define _XOPEN_SOURCE
#include <ssa.h>
#include <ssahm.h>
#include <ssanbhditerators.h>
#include <stdlib.h>

PetscErrorCode SSACreate_HM(SpatialSSA *ssa) {

  PetscFunctionBegin; 

  PetscFunctionReturn(0);
}

PetscBool isPowerOfTwo(PetscInt n) {
  return (n & (n - 1)) == 0;
}

PetscErrorCode SSASetGeometry_HM(SpatialSSA ssa, CRDMEGeometry geom) {

  PetscFunctionBegin;

  if(!isPowerOfTwo(geom->N)) {
    /* this is an unnecessary constraint but it will take extra work to remove */
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Trying to set SSA HM geometry with %d dofs, but only powers of two are supported\n",geom->N);
  }

  PetscFunctionReturn(0);
} 

PetscErrorCode SSASpeciesSetUp_HM(SpatialSSA ssa, PetscInt sp) {
  PetscFunctionBegin;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetUp_HM(SpatialSSA ssa) {
  SpatialSSA_HM   *hm  = ssa->data;
  PetscInt        nL=0,N=ssa->geom->N;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  
  while(N > 1) {
    N /= 2;
    nL++;
  }

  hm->nLevels=nL;
  hm->totVoxels=2*(ssa->geom->N-1);

  ierr = PetscFree(ssa->state->voxelRatesum);CHKERRQ(ierr);
  ierr = PetscMalloc1(nL,&hm->hmVoxelRatesum);CHKERRQ(ierr);
  ierr = PetscMalloc1(hm->totVoxels,&hm->hmVoxelRatesum[0]);CHKERRQ(ierr);

  for(PetscInt l=1;l<nL;l++) {
    hm->hmVoxelRatesum[l]=hm->hmVoxelRatesum[l-1]+PetscPowInt(2,l);
  }
  ssa->state->voxelRatesum=hm->hmVoxelRatesum[nL-1];

  ssa->itContexts.hoppingRatesUpdateCtx.data=hm;
  ssa->itContexts.nbhdRatesUpdateCtx.data=hm;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAGetNextSubvolume_HM(SpatialSSA ssa) {
  SpatialSSA_HM *hm      = ssa->data;
  SSAState      state   = ssa->state;
  PetscReal     r       = state->ratesum*erand48(ssa->seed);
  PetscReal     **lr    = hm->hmVoxelRatesum;
  PetscInt      nL      = hm->nLevels;
  PetscInt      next_sv = 0;

  PetscFunctionBegin;

	for(PetscInt l=0; l < nL-1; l++) {
		if(r > lr[l][next_sv]) {
			r -= lr[l][next_sv];
			next_sv++;
		} 
	  next_sv <<= 1;
	}

  if(r > lr[nL-1][next_sv]) {
	  r -= lr[nL-1][next_sv];
		next_sv++;
  }

  state->lastEvent.loc=next_sv;
  state->rand         =r;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAStateInitialize_HM(SpatialSSA ssa) {
  SpatialSSA_HM  *hm     = ssa->data;
  PetscInt       nL      = hm->nLevels;
  PetscInt       N       = ssa->geom->N;
  PetscInt       v;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscArrayzero(hm->hmVoxelRatesum[0],N-2);CHKERRQ(ierr);
  for(CRDMEGeomGloIdx i=0; i<N; i++) {
    v=i;
	  for(PetscInt l = nL-2; l > -1; l--) {
      v >>= 1;
			hm->hmVoxelRatesum[l][v] += ssa->state->voxelRatesum[i];
	  }	
	}

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesAddToVoxel_HM(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {
  SpatialSSA_HM   *hm      = ssa->data;
  SSAState        state   = ssa->state;
  SSASpecies      s       = state->spec[sp];
  PetscInt        v=i,nL=hm->nLevels;

  PetscFunctionBegin;
  
  for(PetscInt l=nL-2; l>-1; l--) {
    v >>= 1;
    /* for each reaction (don't update rates yet) */
    hm->hmVoxelRatesum[l][v] += s->hoppingRates.totDiffusionRate[i];
    if(s->reactions.Ord0) {
      hm->hmVoxelRatesum[l][v] += s->reactions.Ord0->rate;
    }
    for(PetscInt rn=0;rn<s->reactions.nOrd1; rn++) {
      hm->hmVoxelRatesum[l][v] += s->reactions.Ord1[rn]->rate;
    }
    for(PetscInt rn=0;rn<s->reactions.nUnbind; rn++) {
      hm->hmVoxelRatesum[l][v] += s->reactions.unbindRates[rn][i];
    }
    for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
      hm->hmVoxelRatesum[l][v] += s->reactions.pairRates[rn][i];
    }
  }	

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesRemoveFromVoxel_HM(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {
  SpatialSSA_HM   *hm      = ssa->data;
  SSASpecies      s       = ssa->state->spec[sp];
  PetscInt        v=i,nL=hm->nLevels;

  PetscFunctionBegin;
  
  for(PetscInt l=nL-2; l>-1; l--) {
    /* for each reaction (don't update rates yet) */
    v >>= 1;
    hm->hmVoxelRatesum[l][v] -= s->hoppingRates.totDiffusionRate[i];
    if(s->reactions.Ord0) {
      hm->hmVoxelRatesum[l][v] -= s->reactions.Ord0->rate;
    }
    for(PetscInt rn=0;rn<s->reactions.nOrd1; rn++) {
      hm->hmVoxelRatesum[l][v] -= s->reactions.Ord1[rn]->rate;
    }
    for(PetscInt rn=0;rn<s->reactions.nUnbind; rn++) {
      hm->hmVoxelRatesum[l][v] -= s->reactions.unbindRates[rn][i];
    }
    for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
      hm->hmVoxelRatesum[l][v] -= s->reactions.pairRates[rn][i];
    }
  }	

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdateHoppingRatesCallback_HM(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx) {
  SSAUpdateHoppingRatesCtx *c  =(SSAUpdateHoppingRatesCtx *)ctx;
  SpatialSSA_HM            *hm=c->data; 
  PetscInt                 v=j,nL=hm->nLevels;

  PetscFunctionBegin;

  for(PetscInt l=nL-2; l>-1; l--) {
    v >>= 1;
    hm->hmVoxelRatesum[l][v] += c->rateDiff;
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdatePairRatesCallback_HM(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx) {
  SSAUpdateNbhdRatesCtx    *c=(SSAUpdateNbhdRatesCtx *)ctx;
  SpatialSSA_HM            *hm=c->data; 
  PetscInt                 v=j,nL=hm->nLevels;

  PetscFunctionBegin;

  for(PetscInt l=nL-2; l>-1; l--) {
    v >>= 1;
    hm->hmVoxelRatesum[l][v] += c->rate;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSADestroy_HM(SpatialSSA *ssa) {
  SpatialSSA_HM  *hm = (*ssa)->data;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscFree(hm->hmVoxelRatesum[0]);CHKERRQ(ierr);
  ierr = PetscFree(hm->hmVoxelRatesum);CHKERRQ(ierr);
  ierr = PetscFree(hm);CHKERRQ(ierr);
  (*ssa)->state->voxelRatesum=PETSC_NULL;

  PetscFunctionReturn(0);
}


