#include <ssa.h>
#include <reactionsystem.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <crdmegeomgcon.h>
#include <statistics.h>
#include <util.h>

#define rA 1.000000000
#define rB 1.000000000
#define rC 1.4
#define DA (5.0*rA)
#define DB (5.0*rB)
#define DC (5.0*rC)
#define rAA (2*rA)
#define rBB (2*rB)
#define rCC (2*rC)
#define rAB (rA+rB)
#define rAC (rA+rC)
#define rBC (rB+rC)
#define rAA_squared PetscSqr(rAA)
#define rBB_squared PetscSqr(rBB)
#define rCC_squared PetscSqr(rCC)
#define rAB_squared PetscSqr(rAB)
#define rAC_squared PetscSqr(rAC)
#define rBC_squared PetscSqr(rBC)
#define kappa (.5*10.0)
#define koff  1e-3
#define Kd    (2.0*1e-2)
#define kon   (koff / Kd)
#define coordMin 0.0
#define coordMax 20.0  

PETSC_STATIC_INLINE PetscReal psiAA(PetscReal rsqr,void *ctx) {
  return (rsqr < rAA_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rAA) : 0.0);
}

PETSC_STATIC_INLINE PetscReal psiBB(PetscReal rsqr,void *ctx) {
  return (rsqr < rBB_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rBB) : 0.0);
}

PETSC_STATIC_INLINE PetscReal psiCC(PetscReal rsqr,void *ctx) {
  return (rsqr < rCC_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rCC) : 0.0);
}

PETSC_STATIC_INLINE PetscReal psiAC(PetscReal rsqr,void *ctx) {
  return (rsqr < rAC_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rAC) : 0.0);
}

PETSC_STATIC_INLINE PetscReal psiAB(PetscReal rsqr,void *ctx) {
  return (rsqr < rAB_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rAB) : 0.0);
}

PETSC_STATIC_INLINE PetscReal psiBC(PetscReal rsqr,void *ctx) {
  return (rsqr < rBC_squared ? kappa*PetscSqr(PetscSqrtReal(rsqr) - rBC) : 0.0);
}

typedef enum {SPECIES_A,SPECIES_B,SPECIES_C} ssa_species;

PetscErrorCode Monitor(SpatialSSA ssa, PetscBool *end, PetscBool isChkpt, void *ctx) {
  SSAState        state=ssa->state;
  PetscInt        *trials=ctx;
  
  PetscFunctionBegin;

  // if(state->curTime == 0.0) PetscPrintf(PETSC_COMM_SELF,"Simulation %d start\n",ssa->curSimulationNumber);

  if(isChkpt) {
     //ierr = PetscPrintf(PETSC_COMM_SELF,"  %d %1.14f %d\n",state->nextCheckpoint,ssa->checkpoints[state->nextCheckpoint],state->spec[SPECIES_C]->pop);CHKERRQ(ierr);
    trials[state->nextCheckpoint*ssa->nTrials+ssa->curSimulationNumber] = state->spec[SPECIES_C]->pop;
  }

  if(state->curTime >= ssa->Tf) {
    //trials[ssa->curSimulationNumber] = state->spec[SPECIES_C]->pop;
    //PetscPrintf(PETSC_COMM_SELF,"Simulation %d end\n",ssa->curSimulationNumber);
    trials[state->nextCheckpoint*ssa->nTrials+ssa->curSimulationNumber] = state->spec[SPECIES_C]->pop;
    *end=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SetCheckpoints(PetscInt nCheckpoints, PetscReal *checkpoints, PetscReal Tf) {

  for(PetscInt i=0;i<nCheckpoints;i++) {
    checkpoints[i] = (1.0 / (nCheckpoints - 1))*i;
  }
  for(PetscInt i=0;i<nCheckpoints;i++) {
    checkpoints[i] = PetscPowRealInt(checkpoints[i],1);
  }
  for(PetscInt i=0;i<nCheckpoints;i++) {
    checkpoints[i] *= Tf;
  }

  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  CRDMEGeometry  geom;
  SpatialSSA     ssa;
  PetscInt       lvl=4,mx=PetscPowInt(2,lvl);
  PetscInt       dim=2,m[3]={mx,mx,mx};
  PetscBool      periodicity[3]={1,1,1},flg;
  PetscReal      limits[6]={coordMin,coordMax,coordMin,coordMax,coordMin,coordMax};
  PetscReal      shift[3]={1.0+limits[0],1.0+limits[2],1.0+limits[4]},scale=.5*limits[1];
  PetscBool      structured=PETSC_TRUE,reject=PETSC_FALSE;
  PetscReal      eps=rAB;
  PetscInt       gamma=1000;
  PetscInt       *trials;
  PetscInt       nCheckpoints=1001;
  PetscReal      checkPoints[nCheckpoints],chkptMean[nCheckpoints],chkptStd[nCheckpoints];
  char           filename[PETSC_MAX_PATH_LEN];
  PetscInt       output_info[4];
  PetscInt       seed;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Implement a reversible fusion/fission reaction. Check that the equilibrium solution is the same as the analytical equilibrium.");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);

  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"C");CHKERRQ(ierr);

  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",DA);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"B",DB);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",DC);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-dimension",&dim,&flg);CHKERRQ(ierr);
  
  ierr = ReactionSystemAddPairPotential(rs,"A","A",rAA,psiAA);CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"B","B",rBB,psiBB);CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"C","C",rCC,psiCC);CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"A","B",rAB,psiAB);CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"A","C",rAC,psiAC);CHKERRQ(ierr);
  ierr = ReactionSystemAddPairPotential(rs,"B","C",rBC,psiBC);CHKERRQ(ierr);

  ierr = ReactionSystemAddFusion(rs,"A","B","C",PETSC_NULL,kon,rAB,"fusion");CHKERRQ(ierr);
  ierr = ReactionSystemAddReverseReaction(rs,"fusion",koff,"fission");CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-refine_lvl",&lvl,&flg);CHKERRQ(ierr);

  if(flg) {
    m[0]=PetscPowInt(2,lvl+1);
    m[1]=PetscPowInt(2,lvl+1);
    m[2]=PetscPowInt(2,lvl+1);
  }
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-structured",&structured,&flg);CHKERRQ(ierr);

  if(structured) {
    ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
    ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
    ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);
  } else {
    char mesh[PETSC_MAX_PATH_LEN];
    ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
    ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
    ierr = PetscSNPrintf(mesh,sizeof(mesh),"/home/heldmanm/crdme/crdme/test/mesh_data_binary/uniform_mesh/mesh%d",lvl);CHKERRQ(ierr);
    ierr = CRDMEGeomGCONSetFromFolder(geom,mesh);CHKERRQ(ierr);
    ierr = CRDMEGeomGCONShiftScaleCoordinates(geom,shift,scale);CHKERRQ(ierr);
  }

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  if(structured) {
    ierr = SSASetType(ssa,SSA_TYPE_HM);CHKERRQ(ierr);
  } else {
    ierr = SSASetType(ssa,SSA_TYPE_NSM);CHKERRQ(ierr);
  }

  ierr = SSASetReactionSystem(ssa,rs);CHKERRQ(ierr);
  ierr = SSASetGeometry(ssa,geom);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-reject",&reject,&flg);CHKERRQ(ierr);
  if(reject) {
    ierr = SSAReactionSetDoiType(ssa,"fusion",DOI_LUMP_CONSTANT);CHKERRQ(ierr);
    ierr = SSAReactionSetRejectionFn(ssa,"fusion",SymmetricDoiLumpPairRejectionFn,&eps);CHKERRQ(ierr);
    ierr = SSAReactionSetDoiType(ssa,"fission",DOI_LUMP_CONSTANT);CHKERRQ(ierr);
    ierr = SSAReactionSetRejectionFn(ssa,"fission",DoiLumpUnbindRejectionFn,&eps);CHKERRQ(ierr);
  }

  PetscPrintf(PETSC_COMM_WORLD,"Precomputing reaction rates..\n");
  ierr = SSAReactionPrecomputeRates(ssa,"fusion");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing AA potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"A","A");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing BB potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"B","B");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing CC potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"C","C");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing AB potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"A","B");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing AC potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"A","C");CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Precomputing BC potential values..\n");
  ierr = SSAPairPotentialPrecompute(ssa,"B","C");CHKERRQ(ierr);

  ierr = SSASetNumTrials(ssa,10);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,100.0);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);

  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-gamma",&gamma,&flg);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_A,gamma);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_B,gamma);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_C,0);CHKERRQ(ierr);

  ierr = PetscMalloc1(ssa->nTrials*nCheckpoints,&trials);CHKERRQ(ierr);
  ierr = SSASetMonitorContext(ssa,trials);
  ierr = SSAView(ssa);CHKERRQ(ierr);
  ierr = SetCheckpoints(nCheckpoints,checkPoints,ssa->Tf);CHKERRQ(ierr);
  ierr = SSASetCheckpoints(ssa,nCheckpoints,checkPoints);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Simulation start.\n");
   
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-seed",&seed,&flg);CHKERRQ(ierr);
  if(flg) {
    ssa->seed[0] = (seed-1)*ssa->np + ssa->rank;
  }

  ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,"-output_path",filename,PETSC_MAX_PATH_LEN,&flg);CHKERRQ(ierr);
  if(flg && !ssa->rank) {
    output_info[0] = ssa->nTrials;
    output_info[1] = ssa->np;
    output_info[2] = nCheckpoints;
    output_info[3] = (PetscInt) (ssa->Tf + .5);
    ierr = WriteBinaryDataToFile(filename,-1,output_info,4,PETSC_INT);CHKERRQ(ierr);
  }

  ierr = SSASolve(ssa);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"processors: %d; batch number: %d\n",ssa->np,seed-1);CHKERRQ(ierr);
  if(flg) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"writing output to file %s%d.dat\n",filename,(seed-1)*ssa->np + ssa->rank);CHKERRQ(ierr);
    ierr = WriteBinaryDataToFile(filename,(seed-1)*ssa->np + ssa->rank,trials,ssa->nTrials*nCheckpoints,PETSC_INT);CHKERRQ(ierr);
  }

  for(PetscInt i=0;i<nCheckpoints;i++) {
    if(i%100==0) {
      ierr = ComputeArrayMeanInt(&trials[i*ssa->nTrials],ssa->nTrials,&chkptMean[i]);CHKERRQ(ierr);
      ierr = ComputeArrayStdDeviationInt(&trials[i*ssa->nTrials],ssa->nTrials,chkptMean[i],&chkptStd[i]);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"Time %1.14f\n",checkPoints[i]);CHKERRQ(ierr);
      chkptMean[i] /= gamma;
      ierr = PetscPrintf(PETSC_COMM_WORLD,"  mean: %1.14f\n",chkptMean[i]);CHKERRQ(ierr);
      chkptStd[i] *= (2.0 / gamma);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"  2std: %1.14f\n",chkptStd[i]);CHKERRQ(ierr);
    }
  }

  /*
  PetscReal xlim[2]={0.0,ssa->Tf};
  PetscReal ylim[2]={0.0,1.0};
  ierr = PlotDataArray("test4_result.ppm","mean population type C","time","population",nCheckpoints,checkPoints,xlim,chkptMean,ylim);CHKERRQ(ierr);
  */

  ierr = SSADestroy(&ssa);CHKERRQ(ierr);
  ierr = ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* time mpirun -n 8 ./test4 -ssa_num_trials 2 -ssa_set_final_time 1.0 -dimension 3  -refine_lvl 5 -structured 1 -gamma 50 */