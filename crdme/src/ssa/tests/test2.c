#include <ssa.h>
#include <reactionsystem.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <statistics.h>

PetscErrorCode gaussianDistribution(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *val, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint x,y={0.0,0.0,0.0};
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);
  *val = PetscExpReal( -dist / (2*PetscSqr(.05)) ) / PetscSqrtReal(2.*PETSC_PI*PetscSqr(.05));

  PetscFunctionReturn(0);
}

PetscErrorCode Monitor(SpatialSSA ssa, PetscBool *end, void *ctx) {
  PetscErrorCode  ierr;  
  SSAState        state=ssa->state;
  PetscReal       *trials=ctx;
  
  PetscFunctionBegin;

  if(state->curTime >= ssa->Tf || !state->spec[0]->pop) {
    trials[ssa->curSimulationNumber] = state->curTime;
    *end=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  CRDMEGeometry  geom;
  SpatialSSA     ssa;
  PetscInt       lvl=4,mx=PetscPowInt(2,lvl);
  PetscInt       dim=1,m[3]={mx,mx,mx};
  PetscBool      periodicity[3]={0,0,0},flg;
  PetscReal      limits[6]={0,.2,0.0,.2,0.0,.2};
  PetscReal      *trials,mean,std,error,eps=.001;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Implement a global pair potential. For two particles, compare to solution from Julia.");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);

  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);

  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",10.0);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"B",10.0);CHKERRQ(ierr);
  //ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",1.0);CHKERRQ(ierr);

  ierr = ReactionSystemAddAnnihilation(rs,"A","B",PETSC_NULL,1e9,eps,"annihilation");CHKERRQ(ierr);
  ierr = ReactionSystemView(rs);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-dimension",&dim,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-refine_lvl",&lvl,&flg);CHKERRQ(ierr);
  if(flg) {
    m[0]=PetscPowInt(2,lvl);
    m[1]=PetscPowInt(2,lvl);
    m[2]=PetscPowInt(2,lvl);
  }
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
  ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_HM);CHKERRQ(ierr);

  ierr = SSASetReactionSystem(ssa,rs);CHKERRQ(ierr);
  ierr = SSASetGeometry(ssa,geom);CHKERRQ(ierr);

  PetscBool reject=PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-reject",&reject,&flg);CHKERRQ(ierr);
  if(reject) {
    ierr = SSAReactionSetDoiType(ssa,"annihilation",DOI_LUMP_CONSTANT);CHKERRQ(ierr);
    ierr = SSAReactionSetRejectionFn(ssa,"annihilation",DoiLumpPairRejectionFn,&eps);CHKERRQ(ierr);
  }
  ierr = SSAReactionPrecomputeRates(ssa,"annihilation");CHKERRQ(ierr);
  ierr = SSASetNumTrials(ssa,10);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,10);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  //ierr = SSASetInitialCondition(ssa,"A",gaussianDistribution,"gaussian");CHKERRQ(ierr);
  //ierr = SSASetInitialCondition(ssa,"B",gaussianDistribution,"gaussian");CHKERRQ(ierr);

  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);

  ierr = PetscMalloc1(ssa->nTrials,&trials);CHKERRQ(ierr);
  ierr = SSASetMonitorContext(ssa,trials);
  ierr = SSASolve(ssa);CHKERRQ(ierr);

  ierr = ComputeArrayMeanReal(trials,ssa->nTrials,&mean);CHKERRQ(ierr);
  ierr = ComputeArrayStdDeviationReal(trials,ssa->nTrials,mean,&std);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"eps / h: %1.15f\n",.001/geom->hmax);
  PetscPrintf(PETSC_COMM_WORLD,"mean: %1.14f\n",mean);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"std:  %1.14f\n",std);CHKERRQ(ierr);
  error = 0.0;
  switch(dim) {
    case 1: 
      error = PetscAbs(0.00174558954434  - mean);
      break;
    case 2:
      error = PetscAbs(0.00174558954434 - mean);
      break;
    case 3:
      error = PetscAbs(0.00174558954434  - mean);
  }

  PetscPrintf(PETSC_COMM_WORLD,"error: %1.14f\n",error);CHKERRQ(ierr);

  ierr = SSADestroy(&ssa);CHKERRQ(ierr);
  ierr = ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*

Vary refinement level and number of trials to test spatial and statistical accuracy.
Vary dimension to see convergence rates for the same problem posed in 1, 2, and 3 dimensions.
produce convergence plots?

./test2 -ssa_init_population_species_A 1 -ssa_init_population_species_B 1 \
                       -ssa_num_trials 10000 -dimension 2 -refine_lvl 3

*/