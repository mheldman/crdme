#include <ssa.h>
#include <reactionsystem.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <crdmegeomgcon.h>
#include <statistics.h>

typedef enum {SPECIES_A,SPECIES_B,SPECIES_C} ssa_species;

PetscErrorCode Monitor(SpatialSSA ssa, PetscBool *end, void *ctx) {
  PetscErrorCode  ierr;  
  SSAState        state=ssa->state;
  PetscInt        *trials=ctx;
  
  PetscFunctionBegin;

  if(state->curTime >= ssa->Tf) {
    trials[ssa->curSimulationNumber] = state->spec[SPECIES_C]->pop;
    *end=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  CRDMEGeometry  geom;
  SpatialSSA     ssa;
  PetscInt       lvl=4,mx=PetscPowInt(2,lvl);
  PetscInt       dim=2,m[3]={mx,mx,mx};
  PetscBool      periodicity[3]={1,1,1},flg;
  PetscReal      limits[6]={0.0,1.0,0.0,1.0,0.0,1.0};
  PetscReal      shift[3]={1.0+limits[0],1.0+limits[2],1.0+limits[4]},scale=.5*limits[1];
  PetscBool      structured=PETSC_TRUE,reject=PETSC_FALSE;
  PetscReal      mean,std,error,pi_ab,pi_c,kp=1.0,km=.05,vol,Kd,eps=PetscPowRealInt(2.,-3);
  PetscReal      A;
  PetscInt       gamma=1000;
  PetscInt       *trials;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Implement a reversible fusion/fission reaction. Check that the equilibrium solution is the same as the analytical equilibrium.");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);

  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"C");CHKERRQ(ierr);

  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",1.0);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"B",.5);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",.1);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-dimension",&dim,&flg);CHKERRQ(ierr);
  switch(dim) {
    case 1:
      A   = 2*eps;
      break;  
    case 2:
      A   = PetscSqr(eps)*PETSC_PI;
      break;
    case 3:
      A   = (4./3.)*PetscSqr(eps)*eps*PETSC_PI;
  }
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-gamma",&gamma,&flg);CHKERRQ(ierr);
  ierr = ReactionSystemAddFusion(rs,"A","B","C",PETSC_NULL,kp / (A*gamma),eps,"fusion");CHKERRQ(ierr);
  ierr = ReactionSystemAddReverseReaction(rs,"fusion",km / A,"fission");CHKERRQ(ierr);
  ierr = ReactionSystemView(rs);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-refine_lvl",&lvl,&flg);CHKERRQ(ierr);

  if(flg) {
    m[0]=PetscPowInt(2,lvl+1);
    m[1]=PetscPowInt(2,lvl+1);
    m[2]=PetscPowInt(2,lvl+1);
  }
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-structured",&structured,&flg);CHKERRQ(ierr);

  if(structured) {
    ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
    ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
    ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);
  } else {
    char mesh[PETSC_MAX_PATH_LEN];
    ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
    ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
    ierr = PetscSNPrintf(mesh,sizeof(mesh),"/home/heldmanm/crdme/crdme/test/mesh_data_binary/uniform_mesh/mesh%d",lvl);CHKERRQ(ierr);
    ierr = CRDMEGeomGCONSetFromFolder(geom,mesh);CHKERRQ(ierr);
    ierr = CRDMEGeomGCONShiftScaleCoordinates(geom,shift,scale);CHKERRQ(ierr);
  }

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  if(structured) {
    ierr = SSASetType(ssa,SSA_TYPE_HM);CHKERRQ(ierr);
  } else {
    ierr = SSASetType(ssa,SSA_TYPE_NSM);CHKERRQ(ierr);
  }

  ierr = SSASetReactionSystem(ssa,rs);CHKERRQ(ierr);
  ierr = SSASetGeometry(ssa,geom);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-reject",&reject,&flg);CHKERRQ(ierr);
  if(reject) {
    ierr = SSAReactionSetDoiType(ssa,"fusion",DOI_LUMP_CONSTANT);CHKERRQ(ierr);
    ierr = SSAReactionSetRejectionFn(ssa,"fusion",SymmetricDoiLumpPairRejectionFn,&eps);CHKERRQ(ierr);
    ierr = SSAReactionSetDoiType(ssa,"fission",DOI_LUMP_CONSTANT);CHKERRQ(ierr);
    ierr = SSAReactionSetRejectionFn(ssa,"fission",DoiLumpUnbindRejectionFn,&eps);CHKERRQ(ierr);
  }

  PetscPrintf(PETSC_COMM_WORLD,"Precomputing reaction rates..\n");
  if(dim < 4) ierr = SSAReactionPrecomputeRates(ssa,"fusion");CHKERRQ(ierr);
  
  ierr = SSASetNumTrials(ssa,10);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,1.0);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);

  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_A,gamma);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_B,gamma);CHKERRQ(ierr);
  ierr = SSASpeciesSetInitialPopulation(ssa,SPECIES_C,0);CHKERRQ(ierr);

  ierr = PetscMalloc1(ssa->nTrials,&trials);CHKERRQ(ierr);
  ierr = SSASetMonitorContext(ssa,trials);
  PetscPrintf(PETSC_COMM_WORLD,"Simulation start.\n");
  ierr = SSASolve(ssa);CHKERRQ(ierr);

  ierr = ComputeArrayMeanInt(trials,ssa->nTrials,&mean);CHKERRQ(ierr);
  ierr = ComputeArrayStdDeviationInt(trials,ssa->nTrials,mean,&std);CHKERRQ(ierr);

  mean /= ssa->state->spec[SPECIES_A]->initCondition->pop;
  std  /= ssa->state->spec[SPECIES_A]->initCondition->pop;
  PetscPrintf(PETSC_COMM_WORLD,"eps / h: %1.15f\n",eps/geom->hmax);
  PetscPrintf(PETSC_COMM_WORLD,"mean: %1.14f\n",mean);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"std:  %1.14f\n",std);CHKERRQ(ierr);
  error = 0.0;
  switch(dim) {
    case 1: 
      vol = limits[1] - limits[0];
      Kd  = km / kp;
      pi_ab = vol*Kd / (1.0 + Kd*vol);
      pi_c = 1.0 / (1.0 + Kd*vol);
      PetscPrintf(PETSC_COMM_WORLD,"analytical:  %1.14f\n",pi_c);CHKERRQ(ierr);
      pi_c = .800253;
      error = PetscAbs(pi_c - mean);
      break;
    case 2:
      vol = (limits[1] - limits[0])*(limits[3] - limits[2]);
      Kd  = km / kp;
      pi_ab = vol*Kd / (1.0 + Kd*vol);
      pi_c = 1.0 / (1.0 + Kd*vol);
      PetscPrintf(PETSC_COMM_WORLD,"analytical:  %1.14f\n",pi_c);CHKERRQ(ierr);
      pi_c = .800253;
      error = PetscAbs(pi_c - mean);
      break;
    case 3:
      vol = (limits[1] - limits[0])*(limits[3] - limits[2])*(limits[5] - limits[4]);
      Kd  = km / kp;
      pi_ab = vol*Kd / (1.0 + Kd*vol);
      pi_c = 1.0 / (1.0 + Kd*vol);
      PetscPrintf(PETSC_COMM_WORLD,"analytical:  %1.14f\n",pi_c);CHKERRQ(ierr);
      pi_c=.800253;
      error = PetscAbs(pi_c - mean);
      break;
  }

  PetscPrintf(PETSC_COMM_WORLD,"error: %1.14f\n",error);CHKERRQ(ierr);

  ierr = SSADestroy(&ssa);CHKERRQ(ierr);
  ierr = ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* 