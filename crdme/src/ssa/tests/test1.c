#include <ssa.h>
#include <reactionsystem.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>
#include <statistics.h>

PetscErrorCode deltaDistribution(CRDMEGeometry geom, CRDMEGeomGloIdx i, PetscReal *val, void *ctx) {
  PetscErrorCode ierr;
  CRDMEGeomPoint x,y={0.0,0.0,0.0};
  PetscReal      dist;
  
  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
  ierr = CRDMEGeometryPointsGetDistanceSqr(geom,x,y,&dist);CHKERRQ(ierr);
  if(dist <= geom->dim*.25*PetscSqr(geom->hmax)) {
    *val=1.0;
  } else {
    *val=0.0;
  }

  PetscFunctionReturn(0);
}

PETSC_STATIC_INLINE PetscReal f(PetscScalar r, void* ctx) {
  return r < 4.0 ? 100.0*PetscExpReal(-10.0*r) : 0.0;
}

PetscErrorCode Monitor(SpatialSSA ssa, PetscBool *end, void *ctx) {
  PetscErrorCode  ierr;  
  SSAState        state=ssa->state;
  PetscReal       *trials=ctx;
  CRDMEGeomGloIdx locA=-1,locB=-1;
  
  PetscFunctionBegin;

  if(state->curTime >= ssa->Tf) {
    for(CRDMEGeomGloIdx i=0;i<ssa->geom->N;i++) {
      if(state->spec[0]->voxelPop[i] == 2) {
        locA=i;
        locB=i;
        break;
      } else if(state->spec[0]->voxelPop[i] == 1) {
        locA=i;
        break;
      }
    }
    if(locB < 0) {
      for(CRDMEGeomGloIdx i=0;i<ssa->geom->N;i++) {
        if(state->spec[0]->voxelPop[i] > 0 && i!=locA) {
          locB=i;
          break;
        }
      }
    }
    /*
    for(CRDMEGeomGloIdx i=0;i<ssa->geom->N;i++) {
      if(state->spec[1]->voxelPop[i] > 0) {
        locB=i;
        break;
      }
    }
    */
    if(locA < 0 || locB < 0) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"error: location for A or B not found\n");CHKERRQ(ierr);
      ierr = SSAPrintState(ssa);CHKERRQ(ierr);
      return 1;
    }
    ierr = CRDMEGeometryNodesGetDistanceSqr(ssa->geom,locA,locB,&trials[ssa->curSimulationNumber]);CHKERRQ(ierr);
    trials[ssa->curSimulationNumber] = PetscSqrtReal(trials[ssa->curSimulationNumber]);
    *end=PETSC_TRUE;
  }
  if(0) {
    ierr = SSAPrintState(ssa);CHKERRQ(ierr);
    if(state->curTime >= ssa->Tf) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"location A: %d; location B: %d\n",locA,locB);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_SELF,"result: %1.14f\n",trials[ssa->curSimulationNumber]);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  CRDMEGeometry  geom;
  SpatialSSA     ssa;
  PetscInt       lvl=4,mx=PetscPowInt(2,lvl);
  PetscInt       dim=3,m[3]={mx,mx,mx};
  PetscBool      periodicity[3]={0,0,0},flg;
  PetscReal      limits[6]={-1.0,1.0,-1.0,1.0,-1.0,1.0};
  PetscReal      *trials,mean,std,error;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Implement a global pair potential. For two particles, compare to solution from Julia.");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);

  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  //ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);
  //ierr = ReactionSystemAddSpecies(rs,"C");CHKERRQ(ierr);

  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",1.0);CHKERRQ(ierr);
  //ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",1.0);CHKERRQ(ierr);
  //ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",3.0);CHKERRQ(ierr);

  ierr = ReactionSystemAddPairPotential(rs,"A","A",2.0,f);CHKERRQ(ierr);
  //ierr = ReactionSystemAddFusion(rs,"A","B","C",PETSC_NULL,100000.0,.05,"A+B->C");CHKERRQ(ierr);
  //ierr = ReactionSystemAddReverseReaction(rs,"A+B->C",10.0,"C->A+B");CHKERRQ(ierr);
  ierr = ReactionSystemView(rs);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-dimension",&dim,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-refine_lvl",&lvl,&flg);CHKERRQ(ierr);
  if(flg) {
    m[0]=PetscPowInt(2,lvl);
    m[1]=PetscPowInt(2,lvl);
    m[2]=PetscPowInt(2,lvl);
  }
  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
  ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_HM);CHKERRQ(ierr);

  ierr = SSASetReactionSystem(ssa,rs);CHKERRQ(ierr);
  ierr = SSASetGeometry(ssa,geom);CHKERRQ(ierr);
  ierr = SSASetNumTrials(ssa,10);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,.01);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  ierr = SSASetInitialCondition(ssa,"A",deltaDistribution,"delta");CHKERRQ(ierr);
  //ierr = SSASetInitialCondition(ssa,"B",deltaDistribution,"delta");CHKERRQ(ierr);

  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);
  ierr = SSAPairPotentialPrecompute(ssa,"A","A");CHKERRQ(ierr);
  ierr = PetscMalloc1(ssa->nTrials,&trials);CHKERRQ(ierr);
  ierr = SSASetMonitorContext(ssa,trials);
  ierr = SSASolve(ssa);CHKERRQ(ierr);

  ierr = ComputeArrayMeanReal(trials,ssa->nTrials,&mean);CHKERRQ(ierr);
  ierr = ComputeArrayStdDeviationReal(trials,ssa->nTrials,mean,&std);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"mean: %1.14f\n",mean);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"std: %1.14f\n",std);CHKERRQ(ierr);

  switch(dim) {
    case 1: 
      error = PetscAbs(0.7919305706091406 - mean);
      break;
    case 2:
      error = PetscAbs(0.8062531541666479 - mean);
      break;
    case 3:
      error = PetscAbs(0.8196920808814053 - mean);
  }

  PetscPrintf(PETSC_COMM_WORLD,"error: %1.14f\n",error);CHKERRQ(ierr);

  ierr = SSADestroy(&ssa);CHKERRQ(ierr);
  ierr = ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*

Vary refinement level and number of trials to test spatial and statistical accuracy.
Vary dimension to see convergence rates for the same problem posed in 1, 2, and 3 dimensions.
produce convergence plots?

./test1 -ssa_init_population_species_A 2 -ssa_num_trials 1000 -dimension 2 -refine_lvl 3

*/