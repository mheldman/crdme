#include <ssa.h>
#include <reactionsystem.h>
#include <crdmegeometry.h>
#include <crdmegeombox.h>

PETSC_STATIC_INLINE PetscReal f(PetscScalar r, void* ctx) {
  return r < .0025 ? PetscSqr(PetscSqrtReal(r) - .05) : 0.0;
}

PetscErrorCode Monitor(SpatialSSA ssa, PetscBool *end, void *ctx) {
  PetscErrorCode ierr;  
  
  PetscFunctionBegin;

  ierr = SSAPrintState(ssa);CHKERRQ(ierr);
  if(ssa->state->nEvents > 5) {
    *end=PETSC_TRUE;
    printf("\n");
  }

  PetscFunctionReturn(0);
}

int main(int argc,char **argv) {
  ReactionSystem rs;
  CRDMEGeometry  geom;
  SpatialSSA     ssa;
  PetscInt       mx=PetscPowInt(2,2);
  PetscInt       dim=2,m[3]={mx,mx,mx};
  PetscBool      periodicity[3]={0,0,0};
  PetscReal      limits[6]={0.0,1.0,0.0,1.0,0.0,1.0};
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscInitialize(&argc,&argv,(char*)0,"Create an SSA object, initialize the state, and add and remove a particle.");if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);

  ierr = ReactionSystemCreate(&rs);CHKERRQ(ierr);

  ierr = ReactionSystemAddSpecies(rs,"A");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"B");CHKERRQ(ierr);
  ierr = ReactionSystemAddSpecies(rs,"C");CHKERRQ(ierr);

  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"A",1.0);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"B",2.0);CHKERRQ(ierr);
  ierr = ReactionSystemSpeciesSetDiffusionConstant(rs,"C",3.0);CHKERRQ(ierr);

  ierr = ReactionSystemAddPairPotential(rs,"A","B",.05,f);CHKERRQ(ierr);
  ierr = ReactionSystemAddFusion(rs,"A","B","C",PETSC_NULL,10.0,.05,"A+B->C");CHKERRQ(ierr);
  ierr = ReactionSystemAddReverseReaction(rs,"A+B->C",10000.0,"C->A+B");CHKERRQ(ierr);
  ierr = ReactionSystemView(rs);CHKERRQ(ierr);

  ierr = CRDMEGeometryCreate(&geom);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetType(geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetDimension(geom,dim);CHKERRQ(ierr);
  ierr = CRDMEGeomBoxSet(geom,limits,m,periodicity);CHKERRQ(ierr);

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_HM);CHKERRQ(ierr);
  ierr = SSASetReactionSystem(ssa,rs);CHKERRQ(ierr);
  ierr = SSASetGeometry(ssa,geom);CHKERRQ(ierr);
  ierr = SSASetNumTrials(ssa,10);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,1.0);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);

  ierr = SSAStateInitialize(ssa);CHKERRQ(ierr);
  ierr = SSAPrintState(ssa);CHKERRQ(ierr);
  ierr = SSAAddToLocation(ssa,1,3);CHKERRQ(ierr);
  ierr = SSARemoveFromLocation(ssa,1,3);CHKERRQ(ierr);
  ierr = SSAPrintState(ssa);CHKERRQ(ierr);

  ierr = SSADestroy(&ssa);CHKERRQ(ierr);
  ierr = ReactionSystemDestroy(&rs);CHKERRQ(ierr);
  ierr = CRDMEGeometryDestroy(&geom);CHKERRQ(ierr);

  PetscFunctionReturn(0);
  
}