///*
// * ssa_dm.c
// *
// *  Created on: Jan 15, 2021
// *      Author: maxheldman
// */
//
#define _XOPEN_SOURCE 
#include <petsc.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <rng.h>
#include "ssa.h"
#include "ssa_dm.h"
#include "dual_mesh.h" //TODO: edit this file 

PetscLogEvent SSA_DM_UpdateRates;
PetscLogEvent SSA_DM_RatesCompute;
PetscLogEvent SSA_DM_FindNextEvent;
PetscLogEvent SSA_DM_StateInitialize;


static inline double bernoulli(double z) {
  return (PetscAbsReal(z) < 1e-8 ? 1.0 / (1.0 + .5 * z) : z / expm1(z));
}

int bin_search(double *arr, int l, int h, double r) {
  
  if(r < arr[l]) {
    return l;
  }
  if(h - l >= 1) {
    int mid = l + (h - l) / 2;
    if(arr[mid] < r && arr[mid+1] > r) {
      return mid+1;
    }
    
    if(r > arr[mid]) {
      return bin_search(arr,mid+1,h,r);
    }
    
    return bin_search(arr,l,mid,r);
  }
  return -1;
}

PetscErrorCode SSADestroy_DM(SpatialSSA *ssa) {
  return 0; 
}

PetscErrorCode _p_SSASpeciesStateSetUp_DM(SpatialSSA ssa, int sp) {
  
  PetscErrorCode      ierr;
  SSA_DM              *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState          state = ssa_dm->state;
  SSADMSpeciesState   s;
  CRDME               crdme = ssa->crdme;
  CRDMESpecies        cs=crdme->species[sp];
  int                 M=crdme->Nspecies;
  int                 N=crdme->geom->N;
  int                 i,j,idx;
  double              x[2],xe[2],ax[ssa_dm->max_edge];
  int                 aj[ssa_dm->max_edge],len;
  rx_CAB              rx;
  double              D = cs->D;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&s);CHKERRQ(ierr);
  
  s->max             = 0;
  s->num             = 0;
  s->num_v           = 0;
  s->loc             = NULL;
  s->pot_type        = SSA_POT_TYPE_NONE;
  s->n_pair          = 0;
  s->n_unbnd         = 0;
  
  for(i = 0; i < M; i++) {
    if(cs->pair_rxs[i]) {
      s->n_pair++;
    } 
    if(cs->pair_potentials[i]) {
      s->pot_type = SSA_POT_TYPE_PAIR; 
    }
    s->n_unbnd=cs->n_unbnd;
  }
  
  if(cs->potential) {
    SpatFn_2D psi = crdme->species[sp]->potential;
    ierr = PetscMalloc1(crdme->geom->num_edges,&s->background_rate);CHKERRQ(ierr);
    if(s->pot_type != SSA_POT_TYPE_PAIR) {
      s->pot_type = SSA_POT_TYPE_BACKGROUND;    
      for(int i = 0, k =0; i < N; i++) {
        CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
        CRDMEGeometryNodeGetEdges(crdme->geom,i,&len,aj,ax);CHKERRQ(ierr);
        for(int e = 0; e < len; e++) {
          j = aj[e];
          CRDMEGeometryNodeGetCoordinates(crdme->geom,j,xe);CHKERRQ(ierr);
          s->background_rate[k] = D*ax[e]*bernoulli(psi(xe[0],xe[1],ssa->ctx) - psi(x[0],x[1],ssa->ctx));
        }
      }
    } else {
      for(int i = 0, k =0; i < N; i++) {
        CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
        CRDMEGeometryNodeGetEdges(crdme->geom,i,&len,aj,ax);CHKERRQ(ierr);
        for(int e = 0; e < len; e++,k++) {
          j = aj[e];
          CRDMEGeometryNodeGetCoordinates(crdme->geom,j,xe);CHKERRQ(ierr);
          s->background_rate[k] = psi(xe[0],xe[1],ssa->ctx) - psi(x[0],x[1],ssa->ctx); /* precompute edge potentials */
        }
      }
    }
  } else {
    s->background_rate = NULL;
  }
  
  ierr = PetscMalloc1(N,&s->pop);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&s->first);CHKERRQ(ierr);
  
  for(int i = 0;i<N;i++) {
    s->first[i]=-1;
  }

  if(cs->rx_cre) {
    ierr = PetscMalloc1(N,&s->cre_rates);CHKERRQ(ierr);
    SpatFn_2D k = cs->rx_cre;
    for(int i = 0; i < N; i++) {
      CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
      s->cre_rates[i] = k(x[0],x[1],ssa->ctx);
    }
  } else {
    s->cre_rates = NULL;
  }
  
  if(cs->rx_decay) {
    ierr = PetscMalloc1(N,&s->dec_rates);CHKERRQ(ierr);
    SpatFn_2D k = cs->rx_decay;
    for(int i = 0; i < N; i++) {
      CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
      s->dec_rates[i] = k(x[0],x[1],ssa->ctx); /* TODO: these should pass a context */
    }
  } else {
    s->dec_rates = NULL;
  }
  
  ierr = PetscMalloc1(crdme->geom->N*s->n_unbnd,&s->unbnd_ratesum);CHKERRQ(ierr);
  for(int m = 0; m < s->n_unbnd; m++) {
    rx  = cs->rx_unbnd[m];
    for(int i = 0; i < N; i++) {
      idx = i*cs->n_unbnd + m;
      switch(rx->ptype) {
      case SSA_AB_PLACEMENT_POINT:
        s->unbnd_ratesum[idx] = rx->rates[rx->Ip[i+1]-1];
        break;
      default:
        s->unbnd_ratesum[idx] = rx->rates[rx->Ip[i+1]-1];
      }
    }
  }

  state->species[sp] = s;
  return 0;
}

PetscErrorCode _p_SSAStateSetUp_DM(SpatialSSA ssa) {
  PetscErrorCode ierr;
  SSA_DM         *ssa_dm = (SSA_DM *) ssa->data;
  CRDME          crdme = ssa->crdme;
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&ssa_dm->state);CHKERRQ(ierr);
  ssa_dm->state->ratesum    = 0.0;
  ssa_dm->state->last_event = -1;
  ssa_dm->state->time       = 0.0;
  if(crdme) {
    ierr = PetscMalloc1(crdme->Nspecies,&ssa_dm->state->species);CHKERRQ(ierr);
    for(int i = 0; i < crdme->Nspecies; i++) {
      ierr = _p_SSASpeciesStateSetUp_DM(ssa,i);CHKERRQ(ierr);
      if(!ssa_dm->state->species[i]) {
        PetscPrintf(PETSC_COMM_WORLD,"species %d not allocated properly.\n");
      }
    }
  }
  return 0;
}


PetscErrorCode SSASetUp_DM(SpatialSSA ssa) {
  
  PetscErrorCode ierr;
  int            num_neighbors;
  SSA_DM         *ssa_dm = (SSA_DM*) ssa->data;
  CRDME          crdme = ssa->crdme;
  int            N = crdme->geom->N;
  PetscFunctionBegin;
  ssa_dm->max_edge = 0;
  if(ssa->crdme) {
    CRDME crdme = ssa->crdme;
      if(crdme->geom) {
        for(int i = 0; i < N; i++) {
          ierr = CRDMEGeometryNodeGetNumNeighbors(crdme->geom,i,&num_neighbors);CHKERRQ(ierr);
          ssa_dm->max_edge = PetscMax(num_neighbors,ssa_dm->max_edge);
        }
      } else {
      PetscPrintf(PETSC_COMM_WORLD,"warning: setup called on SSA DM with null CRDME geometry\n");
    }
  }  else {
    PetscPrintf(PETSC_COMM_WORLD,"warning: setup called on SSA DM with null CRDME\n");
  }
  ierr = _p_SSAStateSetUp_DM(ssa);CHKERRQ(ierr);
  return 0; 
}

PetscErrorCode SSADMSetMaxParticles(SpatialSSA ssa, int *max) {
  PetscErrorCode    ierr;
  CRDME             crdme = ssa->crdme;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s;
  
  PetscFunctionBegin;
  for(int i = 0; i < crdme->Nspecies; i++) {
    s=state->species[i];
    s->max = max[i];
    ierr = PetscMalloc1(s->max,&s->loc);CHKERRQ(ierr);
    ierr = PetscMalloc1(s->max,&s->hr);CHKERRQ(ierr);
    ierr = PetscMalloc1(s->max*s->n_pair,&s->pair_ratesum);CHKERRQ(ierr);
    ierr = PetscMalloc1(crdme->Nspecies,&s->rx_num);CHKERRQ(ierr);
    ierr = PetscArrayzero(s->pair_ratesum,s->max*s->n_pair);CHKERRQ(ierr); 
    for(int j = 0; j < s->max; j++) {
      ierr = PetscMalloc1(ssa_dm->max_edge,&s->hr[j].edge_rate);CHKERRQ(ierr); /*TODO: want to use PetscCalloc or do each individually */
      ierr = PetscMalloc1(ssa_dm->max_edge,&s->hr[j].edge_pot);CHKERRQ(ierr);
      ierr = PetscArrayzero(s->hr[j].edge_rate,ssa_dm->max_edge);CHKERRQ(ierr); /*TODO: want to use PetscCalloc or do each individually */
      ierr = PetscArrayzero(s->hr[j].edge_pot,ssa_dm->max_edge);CHKERRQ(ierr); /*TODO: want to use PetscCalloc or do each individually */
      s->hr[j].voxel_rate=0.0;
    }
  }
  return 0;
}

PetscErrorCode SSADMICFunctionSet(SpatialSSA ssa, StateInit init) {
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  
  PetscFunctionBegin;
  ssa_dm->init = init;
  return 0;
}

PetscErrorCode SSADMResetFunctionSet(SpatialSSA ssa, StateReset reset) {
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  
  PetscFunctionBegin;
  ssa_dm->reset = reset;
  return 0;
}

PetscErrorCode SSADMGetState(SpatialSSA ssa, SSADMState *state) {
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  
  PetscFunctionBegin;
  *state = ssa_dm->state;
  return 0;
}

PetscErrorCode SSADMStateCompare(SpatialSSA ssa, SSADMState s1, SSADMState s2, PetscBool *b) {
  CRDME             crdme = ssa->crdme;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  CRDMESpecies      cs;
  int               M=crdme->Nspecies;
  SSADMSpeciesState s,v;
  
  PetscFunctionBegin;
  *b=1;
  if(s1->last_event!=s2->last_event) {
    *b=0;
    return 0;
  }
  if(s1->ratesum!=s2->ratesum) {
    *b=0;
    return 0;
  }
  if(s1->time!=s2->time) {
    *b=0;
    return 0;
  }
  

  for(int i = 0; i < M; i++) {
    v=s1->species[i];
    cs=crdme->species[i];
    s=s2->species[i];
    if(s->num!=v->num) {
      *b=0;
      return 0;
    }
    if(s->max!=v->max) {
      *b=0;
      return 0;
    }

    for(int j=0; j<s->max;j++) {
      if(s->hr[j].voxel_rate!=v->hr[j].voxel_rate) {
        *b=0;
        printf("voxel rates %1.14f %1.14f not equal \n",s->hr[j].voxel_rate,v->hr[j].voxel_rate);
        return 0;
      }
      for(int k=0;k<ssa_dm->max_edge;k++) {
        if(v->hr[j].edge_pot) {
          if(s->hr[j].edge_pot[k]!=v->hr[j].edge_pot[k]) {
            *b=0;
            return 0;
          }
        }
        if(s->hr[j].edge_rate[k]!=v->hr[j].edge_rate[k]) {
            *b=0;
            return 0;
        }
      }
    }
    
    /* need to check reaction rates too, but too much work for now */
  }                                  /* it's convenient to do each species separately, though inefficient */
  return 0;
}

/*
PetscErrorCode _p_SSADMComputeHoppingRatesStencil(SpatialSSA ssa, int sp1, int sp2, int e, int i, int j, pair_potential pp) {
  CRDME                crdme = ssa->crdme;
  int                  vl,vlb;
  double               rsqre,rsqr;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp1],sb=state->species[sp2];
  PetscErrorCode       ierr;

  for(int m = pp->Ip[vl]; m < pp->Ip[vl+1]; m++) {
    vlb = pp->Ij[m];
    if(sb->pop[vlb] && ((sp1 != sp2) || (vlb != vl))) {
       ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
       ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
       s->hr[i].edge_pot[e] +=sb->pop[vlb]*(pp->psiAB(rsqre, ssa->ctx)
                                            - pp->psiAB(rsqr, ssa->ctx));
    }
  }
  return 0;
}

PetscErrorCode _p_SSADMComputeHoppingRatesBFS(SpatialSSA ssa, int sp1, int sp2, int e, int i, int j, pair_potential pp) {
  CRDME                crdme = ssa->crdme;
  int                  vl,vlb;
  double               rsqre,rsqr;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp1],sb=state->species[sp2];
  PetscErrorCode       ierr;

  for(int m = pp->Ip[vl]; m < pp->Ip[vl+1]; m++) {
    vlb = pp->Ij[m];
    if(sb->pop[vlb] && ((sp1 != sp2) || (vlb != vl))) {
       ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
       ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
       s->hr[i].edge_pot[e] +=sb->pop[vlb]*(pp->psiAB(rsqre, ssa->ctx)
                                                 - pp->psiAB(rsqr, ssa->ctx));
    }
  }
  return 0;
}
*/

PetscErrorCode _p_SSADMSpeciesHoppingRatesInitialize(SpatialSSA ssa, int sp) {
   PetscErrorCode       ierr;
   CRDME                crdme = ssa->crdme;
   pair_potential       pp;
   int                  vl,vlb,j;
   SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
   SSADMState           state = ssa_dm->state;
   SSADMSpeciesState    s  = state->species[sp],sb;
   CRDMESpecies         cs = crdme->species[sp];
   double               D=cs->D;
   double               rsqr,rsqre;
   int                  nn,Aj[ssa_dm->max_edge];
   double               Ax[ssa_dm->max_edge]; 
   PetscBool            self;
   
   PetscFunctionBegin;
   
   for(int i = 0; i < s->num_v; i++) {
   
     vl = s->loc[i];
     s->hr[i].voxel_rate = 0.0;
     ierr = PetscArrayzero(s->hr[i].edge_pot,ssa_dm->max_edge);CHKERRQ(ierr);
     ierr = PetscArrayzero(s->hr[i].edge_rate,ssa_dm->max_edge);CHKERRQ(ierr);
     
     /* if it's the first one.. */  
     //ierr = CRDMEGeometryNodeGetCoordinates(crdme->geom,vl,x);CHKERRQ(ierr);
     ierr = CRDMEGeometryNodeGetEdges(crdme->geom,vl,&nn,Aj,Ax);CHKERRQ(ierr);
     if(cs->potential) { 
       PetscPrintf(PETSC_COMM_WORLD,"background potential not implemented\n");
       return 1;
       if(s->pot_type!=SSA_POT_TYPE_PAIR) {
         for(int e=0; e < nn; e++) {
           state->ratesum += 0.0;//s->pop[vl]*s->background_rate[jj];
         }
       } else {
         //SpatFn_2D k = cs->potential;
         for(int e=0; e<nn; e++) {
           j  = Aj[e];
           s->background_rate[0] = 0.0;//k(xe[0], xe[1], ssa->ctx) - k(x[0], x[1], ssa->ctx);
           s->hr[i].edge_pot[e] += 0.0;//s->background_rate[0]; /* TODO: need edge offset for this */
           for(int l = 0; l < crdme->Nspecies; l++) {
             pp = cs->pair_potentials[l];
             self=(l==sp);
             if(pp) {
            	 /*
               if(pp->Ip && sb->num_v > pp->Ip[vl+1]- pp->Ip[vl]) {
              	 ierr = _p_SSADMComputeHoppingRatesStencil(ssa,sp,l,e,i,j,pp);CHLERRQ(ierr);
               } else {
              	 ierr = _p_SSADMComputeHoppingRates(ssa,sp,l,e,i,j,pp);CHLERRQ(ierr);
               }
               */
               sb  = state->species[l];
               if(pp->Ip && sb->num_v > pp->Ip[vl+1]- pp->Ip[vl]) {
                 for(int m = pp->Ip[vl]; m < pp->Ip[vl+1]; m++) {
                   vlb = pp->Ij[m];
                   if(sb->pop[vlb]) {
                     ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
                     ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
                     s->hr[i].edge_pot[e] +=((self && vlb==vl) ? sb->pop[vlb]-1 :sb->pop[vlb]) *(pp->psiAB(rsqre, ssa->ctx)
                                                         - pp->psiAB(rsqr, ssa->ctx));       
                   }
                 }
               } else {
                 for(int m = 0; m < sb->num_v; m++) {
                    vlb = sb->loc[m];
                    ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
                    ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
                    s->hr[i].edge_pot[e] +=((self && vlb==vl) ? sb->pop[vlb]-1 :sb->pop[vlb]) *(pp->psiAB(rsqre, ssa->ctx)
                                                         - pp->psiAB(rsqr, ssa->ctx));
                 }
               }
             }          
           }
           s->hr[i].edge_rate[e] = D*Ax[e]*bernoulli(s->hr[i].edge_pot[e]);
           s->hr[i].voxel_rate += s->hr[i].edge_rate[e];
         }
         state->ratesum    +=   s->pop[vl]*s->hr[i].voxel_rate;
       }
     } else if(s->pot_type == SSA_POT_TYPE_PAIR){
       for(int e=0; e<nn; e++) {
         j  = Aj[e];
         for(int l = 0; l < crdme->Nspecies; l++) {
           pp  = cs->pair_potentials[l];
           self=(l==sp);
           if(pp) {
             sb  = state->species[l];
             if(pp->Ip) {
               for(int m = pp->Ip[vl]; m < pp->Ip[vl+1]; m++) {
                 vlb = pp->Ij[m];
                 if(sb->pop[vlb]) {
                   ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
                   ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
                   s->hr[i].edge_pot[e] += ((self && vlb==vl) ? sb->pop[vlb]-1 :sb->pop[vlb])*(pp->psiAB(rsqre, ssa->ctx)
                                                       - pp->psiAB(rsqr, ssa->ctx));                     }
               }
             } else {
               for(int m = 0; m < sb->num_v; m++) {
                 vlb = sb->loc[m];
              	 ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,vlb,&rsqr);CHKERRQ(ierr);
              	 ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,vlb,&rsqre);CHKERRQ(ierr);
              	 s->hr[i].edge_pot[e] += ((self && vlb==vl) ? sb->pop[vlb]-1 :sb->pop[vlb])*(pp->psiAB(rsqre, ssa->ctx)
                                                     - pp->psiAB(rsqr, ssa->ctx));
               }
             }
           }
         }
         s->hr[i].edge_rate[e] = D*Ax[e]*bernoulli(s->hr[i].edge_pot[e]);
         s->hr[i].voxel_rate += s->hr[i].edge_rate[e];
       }
       state->ratesum+=s->pop[vl]*s->hr[i].voxel_rate;
     } else {
       for(int e=0; e<nn; e++) {
         s->hr[i].edge_rate[e] = D*Ax[e];
         s->hr[i].voxel_rate += s->hr[i].edge_rate[e];
       }
       state->ratesum    += s->pop[vl]*s->hr[i].voxel_rate;

     }
   }
   return 0;
}

PetscErrorCode _p_SSADMPairRatesInitialize_Reject(SpatialSSA ssa, pair_rx rx) {
   int                  sp=rx->species[0],spb=rx->species[1];
   int                  vl,i,j,jj,idx,tot;
   SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
   SSADMState           state = ssa_dm->state;
   SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
   PetscFunctionBegin;
   for(i = 0; i < s->num_v; i++) {
	   sb  = state->species[spb];
		 vl = s->loc[i];
		 idx = s->n_pair*i + s->rx_num[spb];
		 tot = 0;
		 for(jj = rx->Ip[vl]; jj <rx->Ip[vl+1]; jj++) {
			 j= rx->Ij[jj];
			 tot += sb->pop[j];
	   }
		 s->pair_ratesum[idx] = tot*rx->c;
		 state->ratesum  += s->pop[vl]*s->pair_ratesum[idx];
   }
   return 0;
}

PetscErrorCode _p_SSADMPairRatesInitialize_Stencil(SpatialSSA ssa, pair_rx rx) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  vl,i,j,jj,idx;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];

  for(i = 0; i < s->num_v; i++) {
		 vl = s->loc[i];
		 idx = s->n_pair*i + s->rx_num[spb];
		 s->pair_ratesum[idx] = 0.0;
		 for(jj = rx->Ip[vl]; jj <rx->Ip[vl+1]; jj++) {
			 j= rx->Ij[jj];
			 if(sb->pop[j]) {
				 s->pair_ratesum[idx] += sb->pop[j]*rx->rates[jj];
			 }
	   }
		 state->ratesum  += s->pop[vl]*s->pair_ratesum[idx];
  }
  return 0;
}


PetscErrorCode _p_SSADMPairRatesInitialize_Doi(SpatialSSA ssa, pair_rx rx) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  double               rate,rsqr,r,rh;
  int                  vl,i,j,jj,idx;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  r = rx->radius*rx->radius;
  rh = (crdme->doi_type == DOI_LUMP) ? (rx->radius + crdme->geom->hmax)*(rx->radius + crdme->geom->hmax) :
  																		 (rx->radius + 2*crdme->geom->hmax)*(rx->radius + 2*crdme->geom->hmax);
  for(i = 0; i < s->num_v; i++) {
  	vl = s->loc[i];
  	idx = s->n_pair*i + s->rx_num[spb];
  	s->pair_ratesum[idx] = 0.0;
	  for(jj = 0; jj < sb->num_v; jj++) {
		  j = sb->loc[jj];
		  ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,j,&rsqr);CHKERRQ(ierr);
		  if(rsqr < rh) {
	     ierr = CRDMEDoiComputeRate(crdme,vl,j,r,&rate,rx->doi_type);CHKERRQ(ierr);
			 s->pair_ratesum[idx] += sb->pop[j]*rx->c*rate;
		  }
	  }
		state->ratesum  += s->pop[vl]*s->pair_ratesum[idx];
  }
  return 0;
}

PetscErrorCode _p_SSADMPairRatesInitialize_Callback(SpatialSSA ssa, pair_rx rx) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  double               rsqr;
  int                  vl,i,j,jj,idx;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  for(i = 0; i < s->num_v; i++) {
  	vl = s->loc[i];
  	idx = s->n_pair*i + s->rx_num[spb];
  	s->pair_ratesum[idx] = 0.0;
	  for(jj = 0; jj < sb->num_v; jj++) {
		  j = sb->loc[jj];
		  ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,j,&rsqr);CHKERRQ(ierr);
			s->pair_ratesum[idx] += sb->pop[j]*rx->kAB(rsqr,crdme->ctx);
	  }
		state->ratesum  += s->pop[vl]*s->pair_ratesum[idx];
  }
  return 0;
}


PetscErrorCode _p_SSADMSpeciesPairRatesInitialize(SpatialSSA ssa, int sp) {
   PetscErrorCode       ierr;
   CRDME                crdme = ssa->crdme;
   pair_rx              rx;
   int                  m,rn=0;
   SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
   SSADMState           state = ssa_dm->state;
   SSADMSpeciesState    s  = state->species[sp];
   CRDMESpecies         cs = crdme->species[sp];

   PetscFunctionBegin;
   for(m = 0; m < crdme->Nspecies; m++) {
     rx  = cs->pair_rxs[m];
     if(rx) {
       s->rx_num[m] = rn;
       switch(rx->me_type) {
     	 case STENCIL:
    	   ierr = _p_SSADMPairRatesInitialize_Stencil(ssa,rx);CHKERRQ(ierr);
    	   break;
     	 case DOI:
    		 ierr = _p_SSADMPairRatesInitialize_Doi(ssa,rx);CHKERRQ(ierr);
    		 break;
     	 case CALLBACK:
   		   ierr = _p_SSADMPairRatesInitialize_Callback(ssa,rx);CHKERRQ(ierr);
   		   break;
     	 case DOI_REJECT:
   		   ierr = _p_SSADMPairRatesInitialize_Reject(ssa,rx);CHKERRQ(ierr);
   		   break;
       }
       rn++;
     }  else {
       s->rx_num[m] = -1;
     }
   }
   return 0;
}

PetscErrorCode _p_SSADMSpeciesUnbindRatesInitialize(SpatialSSA ssa, int sp) {

  CRDME                crdme = ssa->crdme;
   int                  idx;
   SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
   SSADMState           state = ssa_dm->state;
   SSADMSpeciesState    s  = state->species[sp];
   CRDMESpecies         cs = crdme->species[sp];
   
   PetscFunctionBegin;

   for(int m = 0; m < s->n_unbnd; m++) {
     for(int i = 0; i < s->num_v; i++) {
       idx = s->loc[i]*cs->n_unbnd + m;
       state->ratesum  += s->pop[s->loc[i]]*s->unbnd_ratesum[idx];
     }
   }
   return 0;
}

PetscErrorCode _p_SSADMSpeciesConversionRatesInitialize(SpatialSSA ssa, int sp) {

  CRDME                crdme = ssa->crdme;
   SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
   SSADMState           state = ssa_dm->state;
   SSADMSpeciesState    s  = state->species[sp];
   CRDMESpecies         cs = crdme->species[sp];

   PetscFunctionBegin;

   for(int m = 0; m < cs->n_convert; m++) {
  	 state->ratesum += s->num*cs->conv_rxs[m]->c;
   }
   return 0;
}



PetscErrorCode _p_SSADMSpeciesRatesInitialize(SpatialSSA ssa, int sp) {
  
  PetscErrorCode       ierr;
  PetscFunctionBegin;
  /* TODO: initialize rates for creation and decay reactions */
  ierr = _p_SSADMSpeciesHoppingRatesInitialize(ssa,sp);CHKERRQ(ierr);
  ierr = _p_SSADMSpeciesPairRatesInitialize(ssa,sp);CHKERRQ(ierr);
  ierr = _p_SSADMSpeciesUnbindRatesInitialize(ssa,sp);CHKERRQ(ierr);
  ierr = _p_SSADMSpeciesConversionRatesInitialize(ssa,sp);CHKERRQ(ierr);

  return 0;
}

PetscErrorCode SSADMStateInitialize(SpatialSSA ssa, ic_type type, int *num) {
  PetscErrorCode    ierr;
  CRDME             crdme = ssa->crdme;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  int               M=crdme->Nspecies;
  SSADMSpeciesState s;
  PetscFunctionBegin;
  
  state->time = 0.0;
  state->ratesum = 0.0;
  state->last_event = -1;
  for(int i = 0; i < M; i++) {
    s=state->species[i];
    s->num=num[i]; 
  }
  if(type==SSA_IC_CUSTOM) {
      if(!ssa_dm->init) {
        PetscPrintf(PETSC_COMM_WORLD,"Error: SSA DM initialization function not set.\n");
        return 1;
      }
      else {
        ierr = ssa_dm->init(ssa,state->species);CHKERRQ(ierr);
        for(int i = 0; i < M; i++) {
          s=state->species[i];
          s->num_v=0;
          int loc[s->max];
          ierr = PetscArrayzero(s->pop,crdme->geom->N);CHKERRQ(ierr);
          for(int j = 0;j<crdme->geom->N;j++) {
            s->first[i]=-1;
          }
          for(int j = 0; j < s->max; j++) {
	    if(j < s->num) {
              if(!s->pop[s->loc[j]]) {
                loc[s->num_v]=s->loc[j];
                s->first[s->loc[j]] = s->num_v;
                s->num_v++;
              }
              s->pop[s->loc[j]]++;
            }	   
          }
          PetscArraycpy(s->loc,loc,s->num_v);
        }
      }
  } else {
    PetscPrintf(PETSC_COMM_WORLD,"Error: initializations other than custom not implemented.\n");
    return 1;
  }
  for(int i = 0; i < M; i++) {
    ierr = _p_SSADMSpeciesRatesInitialize(ssa,i);CHKERRQ(ierr); /* locations are set, now initialize the rates */
  }      
  /*
  PetscBool b=1;
  ierr=SSADMStateDuplicate(ssa,&ssa_dm->start);CHKERRQ(ierr);
  ierr=SSADMStateCompare(ssa,ssa_dm->state,ssa_dm->start,&b);CHKERRQ(ierr);
  if(!b) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: state copies not equal.\n");
    return 1;
  } */
  /* it's convenient to do each species separately, though inefficient */
  return 0;
}

PetscErrorCode SSADMStateDuplicate(SpatialSSA ssa, SSADMState *copy) {
  PetscErrorCode    ierr;
  CRDME             crdme = ssa->crdme;
  int               N=crdme->geom->N;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  CRDMESpecies      cs;
  int               M=crdme->Nspecies,w;
  SSADMSpeciesState s,v;
  
  PetscFunctionBegin;
  ierr=PetscMalloc1(1,copy);CHKERRQ(ierr);
  ierr=PetscMalloc1(M,&(*copy)->species);CHKERRQ(ierr);
  (*copy)->last_event = state->last_event;
  (*copy)->ratesum    = state->ratesum;
  (*copy)->time       = state->time;
  for(int i = 0; i < M; i++) {
    cs=crdme->species[i];
    v=state->species[i];
    ierr=PetscMalloc1(1,&(*copy)->species[i]);CHKERRQ(ierr);
    s = (*copy)->species[i];
    s->num = v->num;
    s->max = v->max;
    s->num_v=v->num_v;
    s->pot_type=v->pot_type;
    s->n_pair=v->n_pair;
    s->n_unbnd=v->n_unbnd;
    ierr=PetscMalloc1(s->max,&s->loc);CHKERRQ(ierr);
    ierr=PetscMalloc1(N,&s->pop);CHKERRQ(ierr);
    ierr=PetscMalloc1(N,&s->first);CHKERRQ(ierr);
    ierr=PetscArraycpy(s->loc,v->loc,s->max);CHKERRQ(ierr);
    ierr=PetscArraycpy(s->pop,v->pop,N);CHKERRQ(ierr);
    ierr=PetscArraycpy(s->first,v->first,N);CHKERRQ(ierr);

    s->background_rate=state->species[i]->background_rate;
    s->pot_type=v->pot_type;
    
    PetscMalloc1(s->max,&(*copy)->species[i]->hr);
    for(int j=0; j<s->max;j++) {
      ierr=PetscMalloc1(ssa_dm->max_edge,&(*copy)->species[i]->hr[j].edge_pot);CHKERRQ(ierr);
      ierr=PetscMalloc1(ssa_dm->max_edge,&(*copy)->species[i]->hr[j].edge_rate);CHKERRQ(ierr);
      ierr=PetscArraycpy( (*copy)->species[i]->hr[j].edge_pot,v->hr[j].edge_pot,ssa_dm->max_edge);CHKERRQ(ierr);
      ierr=PetscArraycpy( (*copy)->species[i]->hr[j].edge_rate,v->hr[j].edge_rate,ssa_dm->max_edge);CHKERRQ(ierr);
      (*copy)->species[i]->hr[j].voxel_rate = v->hr[j].voxel_rate;
    }
    
    (*copy)->species[i]->cre_rates=v->cre_rates;
    (*copy)->species[i]->dec_rates=v->dec_rates;
    
    if(s->n_pair) {
      w = s->max*s->n_pair;
      ierr=PetscMalloc1(w,&(*copy)->species[i]->pair_ratesum);CHKERRQ(ierr);
      ierr=PetscArraycpy((*copy)->species[i]->pair_ratesum,v->pair_ratesum,w);CHKERRQ(ierr);
    } else {
      s->pair_ratesum = NULL;
    }
    (*copy)->species[i]->unbnd_ratesum=v->unbnd_ratesum;
  }

  //*copy = c;       
  /* it's convenient to do each species separately, though inefficient */
  return 0;
}

PetscErrorCode SSADMSetState(SpatialSSA ssa, SSADMState copy) {
  PetscErrorCode    ierr;
  CRDME             crdme = ssa->crdme;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state,c=copy;
  int               M=crdme->Nspecies,w;
  SSADMSpeciesState s,v;
  
  state->last_event=c->last_event;
  state->ratesum=c->ratesum;
  state->time=c->time;

  for(int i = 0; i < M; i++) {
    v=c->species[i];
    s=state->species[i];
    s->num = v->num;


    s->max = v->max;
    s->num_v = v->num_v;
    ierr=PetscArraycpy(s->loc,v->loc,s->max);CHKERRQ(ierr);
    ierr=PetscArraycpy(s->pop,v->pop,crdme->geom->N);CHKERRQ(ierr);
    ierr=PetscArraycpy(s->first,v->first,crdme->geom->N);CHKERRQ(ierr);

    s->background_rate=v->background_rate;
    s->pot_type=v->pot_type;
    for(int j=0; j<s->num_v;j++) {
      if(v->hr[j].edge_pot) {
        ierr=PetscArraycpy(s->hr[j].edge_pot,v->hr[j].edge_pot,ssa_dm->max_edge);CHKERRQ(ierr);
      }
      ierr=PetscArraycpy(s->hr[j].edge_rate,v->hr[j].edge_rate,ssa_dm->max_edge);CHKERRQ(ierr);
      s->hr[j].voxel_rate = v->hr[j].voxel_rate;
    }
    
    s->cre_rates=v->cre_rates;
    s->dec_rates=v->dec_rates;

    s->n_pair=v->n_pair;
    s->n_unbnd=v->n_unbnd;
    s->unbnd_ratesum = v->unbnd_ratesum;

    if(s->n_pair) {
      w = s->max*s->n_pair;
      ierr=PetscArraycpy(s->pair_ratesum,v->pair_ratesum,w);CHKERRQ(ierr);
    }

  } 
  /* it's convenient to do each species separately, though inefficient */
  return 0;
}


PetscErrorCode _p_SSADMComputePairRates_Stencil(SpatialSSA ssa, pair_rx rx, int i) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  vl,j,jj,idx;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];

  vl = s->loc[i];
  idx = s->n_pair*i + s->rx_num[spb];
  s->pair_ratesum[idx] = 0.0;
  for(jj=rx->Ip[vl];jj<rx->Ip[vl+1];jj++) {
     j = rx->Ij[jj];
     if(sb->pop[j]) {
       s->pair_ratesum[idx] += sb->pop[j]*rx->rates[jj];
     }
  }
  state->ratesum += s->pair_ratesum[idx];
  return 0;
}

PetscErrorCode _p_SSADMComputePairRates_Reject(SpatialSSA ssa, pair_rx rx, int i) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  vl,j,jj,idx,tot;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];

  vl = s->loc[i];
  idx = s->n_pair*i + s->rx_num[spb];
  tot = 0;
  for(jj=rx->Ip[vl];jj<rx->Ip[vl+1];jj++) {
     j = rx->Ij[jj];
     tot += sb->pop[j];
  }
  s->pair_ratesum[idx] = tot*rx->c;
  state->ratesum += s->pair_ratesum[idx];
  return 0;
}

PetscErrorCode _p_SSADMComputePairRates_Doi(SpatialSSA ssa, pair_rx rx, int i) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  vl,j,jj,ri,idx;
  double               r,rh,rsqr,rate;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  vl = s->loc[i];
  ri = s->rx_num[spb];
  idx = s->n_pair*i + ri;
  s->pair_ratesum[idx] = 0.0;
  r = rx->radius*rx->radius;
  rh = (crdme->doi_type == DOI_LUMP) ? (rx->radius + crdme->geom->hmax)*(rx->radius + crdme->geom->hmax)
  																	 : (rx->radius + 2*crdme->geom->hmax)*(rx->radius + 2*crdme->geom->hmax);
  for(jj=0;jj<sb->num_v;jj++) {
    j=sb->loc[jj];
    ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,j,&rsqr);CHKERRQ(ierr);
    if(rsqr < rh) {
      ierr = CRDMEDoiComputeRate(crdme,vl,j,r,&rate,rx->doi_type);CHKERRQ(ierr);
      s->pair_ratesum[idx]   += sb->pop[j]*rx->c*rate;
    }
  }
  state->ratesum += s->pair_ratesum[idx];
  return 0;
}

PetscErrorCode _p_SSADMComputePairRates_Callback(SpatialSSA ssa, pair_rx rx, int i) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  vl,j,jj,idx;
  double               rsqr;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  vl = s->loc[i];
  idx = s->n_pair*i + s->rx_num[spb];
  s->pair_ratesum[idx] = 0.0;
  for(jj=0;jj<sb->num_v;jj++) {
    j=sb->loc[jj];
    ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,vl,j,&rsqr);CHKERRQ(ierr);
    s->pair_ratesum[idx]   += sb->pop[j]*rx->kAB(rsqr,crdme->ctx);
  }
  state->ratesum += s->pair_ratesum[idx];
  return 0;
}




PetscErrorCode _p_SSADMComputeRates(SpatialSSA ssa, int sp, int n) {
  
  CRDME             crdme=ssa->crdme;
  CRDMESpecies      cs=crdme->species[sp];
  int               M =crdme->Nspecies;
  pair_rx           rx;
  pair_potential    pp;
  int               *Ip,*Ij;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s=state->species[sp],sb;
  int               j,k,ll,l,m,sr,ei,nn;
  double            rh,rsqr,rsqre;
  double            Ax[ssa_dm->max_edge];
  int               Aj[ssa_dm->max_edge];
  PetscBool         pot_is_zero=1,self=0;
  PetscErrorCode    ierr;
  
  j = s->loc[n];
  ierr = CRDMEGeometryNodeGetEdges(crdme->geom,j,&nn,Aj,Ax);CHKERRQ(ierr);
  if(s->pot_type == SSA_POT_TYPE_PAIR) {
  
   /* compute potential over each edge */
   s->hr[n].voxel_rate   = 0.0;
   for(ei=0;ei<nn;ei++) {
     s->hr[n].edge_pot[ei] = 0.0;//cs->potential ? s->background_rate[kk] : 0.0;
     pot_is_zero=1;
     k = Aj[ei];
     for(sr=0;sr<M;sr++) {
       pp = cs->pair_potentials[sr];
       if(pp) {
      	 self=(sr==sp);
         sb=state->species[sr];
         if(pp->Ip) {
           Ip=pp->Ip;
           Ij=pp->Ij;
           for(ll=Ip[j];ll<Ip[j+1];ll++) {
             l = Ij[ll];
             if(sb->pop[l]) {
            	 pot_is_zero=0;
               s->hr[n].edge_pot[ei] -= ( (self && l==j) ? sb->pop[l] - 1 :  sb->pop[l])*pp->p[ll];//(pp->psiAB(xe,ye,xb,yb,NULL) - pp->psiAB(x,y,xb,yb,NULL));
             }
           }
           for(ll=Ip[k];ll<Ip[k+1];ll++) {
             l = Ij[ll];
             if(sb->pop[l]) {
            	 pot_is_zero=0;
               s->hr[n].edge_pot[ei] += ( (self && l==j) ? sb->pop[l] - 1 :  sb->pop[l])*pp->p[ll];//(pp->psiAB(xe,ye,xb,yb,NULL) - pp->psiAB(x,y,xb,yb,NULL));
             }
           }
         } else {
           rh = (pp->radius + crdme->geom->hmax)*(pp->radius + crdme->geom->hmax);
           for(ll=0;ll<sb->num_v;ll++) {
             l = sb->loc[ll];
             ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,l,&rsqr);CHKERRQ(ierr);
             if(rsqr < rh) {
               ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,k,l,&rsqre);CHKERRQ(ierr);
               s->hr[n].edge_pot[ei] += ( (!self || !(l==j)) ? sb->pop[l] : sb->pop[l]-1)*(pp->psiAB(rsqre,ssa->ctx) - pp->psiAB(rsqr,ssa->ctx));
               pot_is_zero = 0;
             }
           }
         }
       }  
     }
     if(pot_is_zero) {
       s->hr[n].edge_rate[ei] = cs->D*Ax[ei];
     } else {
       s->hr[n].edge_rate[ei] = cs->D*Ax[ei]*bernoulli(s->hr[n].edge_pot[ei]);
     }
     s->hr[n].voxel_rate    += s->hr[n].edge_rate[ei];
   }
   state->ratesum += s->hr[n].voxel_rate;
  } else {
   s->hr[n].voxel_rate   = 0.0;
   for(ei=0;ei<nn;ei++) {
     s->hr[n].edge_rate[ei]   = cs->D*Ax[ei];
     s->hr[n].voxel_rate += s->hr[n].edge_rate[ei]; 
   }
   state->ratesum+=s->hr[n].voxel_rate;
  }

  for(m=0;m<M;m++) {
   rx=cs->pair_rxs[m];
   if(rx) {
  	 switch(rx->me_type) {
  	 case DOI:
  		 ierr = _p_SSADMComputePairRates_Doi(ssa,rx,n);CHKERRQ(ierr);
  		 break;
  	 case DOI_REJECT:
  		 ierr = _p_SSADMComputePairRates_Reject(ssa,rx,n);CHKERRQ(ierr);
  		 break;
  	 case STENCIL:
  		 ierr = _p_SSADMComputePairRates_Stencil(ssa,rx,n);CHKERRQ(ierr);
  		 break;
  	 case CALLBACK:
  		 ierr = _p_SSADMComputePairRates_Callback(ssa,rx,n);CHKERRQ(ierr);
  		 break;
  	 }
   }
 }
 return 0; 
}

PetscErrorCode _p_SSADMRemoveFromLocation(SpatialSSA ssa, int sp, int loc) {
  CRDME             crdme=ssa->crdme;
  CRDMESpecies      cs=crdme->species[sp];
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s=state->species[sp];
  int               i,l,n;
  PetscErrorCode    ierr;

  n = s->first[loc];
  s->pop[loc]--;
  s->num--;

  state->ratesum -= s->hr[n].voxel_rate;
  for(i=0;i<s->n_pair;i++) {
    state->ratesum -= s->pair_ratesum[s->n_pair*n+i];
  }
  for(i=0;i<cs->n_convert;i++) {
    state->ratesum -= cs->conv_rxs[i]->c;
  }

  if(!s->pop[loc]) {
    s->num_v--;
    s->first[loc] = -1;
    if(n < s->num_v) {
      l = s->loc[s->num_v]; /* move the last particle to the n'th position */
      s->loc[n]=l;
      s->first[l]=n;
        /* copy over data */
      ierr = PetscArraycpy(s->hr[n].edge_pot,s->hr[s->num_v].edge_pot,ssa_dm->max_edge);CHKERRQ(ierr);
      ierr = PetscArraycpy(s->hr[n].edge_rate,s->hr[s->num_v].edge_rate,ssa_dm->max_edge);CHKERRQ(ierr);
      s->hr[n].voxel_rate=s->hr[s->num_v].voxel_rate;
      for(i=0;i<s->n_pair;i++) {
        s->pair_ratesum[s->n_pair*n+i] = s->pair_ratesum[s->n_pair*s->num_v+i]; 
      }
    }
  }

  for(i=0;i<s->n_unbnd;i++) {
     state->ratesum -= s->unbnd_ratesum[s->n_unbnd*loc+i];
   }
   
   if(cs->rx_decay) {
     state->ratesum -= s->dec_rates[loc];
   }
  return 0;
}

PetscErrorCode _p_SSADMAddToLocation(SpatialSSA ssa, int sp, int loc) {

	CRDME             crdme=ssa->crdme;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  CRDMESpecies      cs=crdme->species[sp];
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s=state->species[sp];
  int               n,i;
  PetscErrorCode    ierr;
  
  if(s->num_v == s->max) {
    PetscPrintf(PETSC_COMM_WORLD,"error: cannot add species %d to location %d because particle count is at maximum %d.\n",sp,loc,s->max);
    return 1;
  }

  s->num++;
  s->pop[loc]++;
  if(s->pop[loc] == 1) {
    s->first[loc] = s->num_v;
    s->loc[s->num_v] = loc;
    s->num_v++;
    ierr = _p_SSADMComputeRates(ssa,sp,s->num_v-1);CHKERRQ(ierr);
  } else {
    n = s->first[loc];
    state->ratesum += s->hr[n].voxel_rate;
    for(i=0;i<s->n_pair;i++) {
      state->ratesum += s->pair_ratesum[s->n_pair*n+i]; 
    }
    
  }

  for(i=0;i<s->n_unbnd;i++) {
    state->ratesum += s->unbnd_ratesum[s->n_unbnd*loc+i];
  }
  for(i=0;i<cs->n_convert;i++) {
    state->ratesum += cs->conv_rxs[i]->c;
  }
  
  if(s->dec_rates) {
    state->ratesum += s->dec_rates[loc];
  }
  
  return 0;
}

PetscErrorCode _p_SSADMSwitchLocation(SpatialSSA ssa, int sp, int i, int j) {
  
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s=state->species[sp];
  int               k,n;
  PetscErrorCode    ierr;

  if(s->pop[i] == 1 && s->pop[j] == 0) {
    n = s->first[i];
    s->first[j] = n;
    s->loc[n] = j;
    
    state->ratesum -= s->hr[n].voxel_rate;
    for(k=0;k<s->n_pair;k++) {
      state->ratesum -= s->pair_ratesum[s->n_pair*n+k];
    }
    s->pop[i]--;
    s->pop[j]++;
    ierr = _p_SSADMComputeRates(ssa,sp,n);CHKERRQ(ierr);

    
    for(k=0;k<s->n_unbnd;k++) {
      state->ratesum -= s->unbnd_ratesum[s->n_unbnd*i+k];
      state->ratesum += s->unbnd_ratesum[s->n_unbnd*j+k];
    }
    
    if(s->dec_rates) {
      state->ratesum -= s->dec_rates[j];
      state->ratesum += s->dec_rates[j];
    }
  } else {
    ierr = _p_SSADMRemoveFromLocation(ssa,sp,i);CHKERRQ(ierr);
    ierr = _p_SSADMAddToLocation(ssa,sp,j);CHKERRQ(ierr);
  }
  return 0;
}

PetscErrorCode _p_SSADMUpdatePairRates_Stencil(SpatialSSA ssa, pair_rx rx, int nrm, int *loc_rm, int nadd, int *loc_add) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj,k,kk,idx;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp];

  for(jj = 0; jj < nrm; jj++) {
    j = loc_rm[jj];
    for(kk=rx->Ip[j];kk<rx->Ip[j+1];kk++) {
      k = rx->Ij[kk];
      if(s->pop[k]) {
      	idx = s->n_pair*s->first[k] + s->rx_num[spb];
        s->pair_ratesum[idx] -= rx->rates[kk];
        state->ratesum       -= s->pop[k]*rx->rates[kk];
      }
    }
  }

  for(jj = 0; jj < nadd; jj++) {
    j = loc_add[jj];
    for(kk=rx->Ip[j];kk<rx->Ip[j+1];kk++) {
      k = rx->Ij[kk];
      if(s->pop[k]) {
      	idx = s->n_pair*s->first[k] + s->rx_num[spb];
        s->pair_ratesum[idx] += rx->rates[kk];
        state->ratesum       += s->pop[k]*rx->rates[kk];
      }
    }
  }
  return 0;
}

PetscErrorCode _p_SSADMUpdatePairRates_Reject(SpatialSSA ssa, pair_rx rx, int nrm, int *loc_rm, int nadd, int *loc_add) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj,k,kk;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp];

  for(jj = 0; jj < nrm; jj++) {
    j = loc_rm[jj];
    for(kk=rx->Ip[j];kk<rx->Ip[j+1];kk++) {
      k = rx->Ij[kk];
      if(s->pop[k]) {
        s->pair_ratesum[s->n_pair*s->first[k] + s->rx_num[spb]] -= rx->c;
        state->ratesum -= s->pop[k]*rx->c;
      }
    }
  }

  for(jj = 0; jj < nadd; jj++) {
    j = loc_add[jj];
    for(kk=rx->Ip[j];kk<rx->Ip[j+1];kk++) {
      k = rx->Ij[kk];
      if(s->pop[k]) {
        s->pair_ratesum[s->n_pair*s->first[k] + s->rx_num[spb]] += rx->c;
        state->ratesum += s->pop[k]*rx->c;
      }
    }
  }
  return 0;
}


PetscErrorCode _p_SSADMUpdatePairRates_Doi(SpatialSSA ssa, pair_rx rx, int nrm, int *loc_rm, int nadd, int *loc_add) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  ri,j,jj,k,kk,idx;
  double               r,rh,rsqr,rate,temp;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp];
  PetscErrorCode       ierr;

  ri = s->rx_num[spb];
  r = rx->radius*rx->radius;
  rh = (crdme->doi_type == DOI_LUMP) ? (rx->radius + crdme->geom->hmax)*(rx->radius + crdme->geom->hmax)
  																	 : (rx->radius + 2*crdme->geom->hmax)*(rx->radius + 2*crdme->geom->hmax);
  for(kk=0;kk<s->num_v;kk++) {
  	idx = s->n_pair*kk + ri;
    k=s->loc[kk];
    for(jj=0;jj<nrm; jj++) {
      j=loc_rm[jj];
      ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqr);CHKERRQ(ierr);
      if(rsqr < rh) {
        ierr = CRDMEDoiComputeRate(crdme,k,j,r,&rate,rx->doi_type);CHKERRQ(ierr);
        temp = rx->c*rate;
        s->pair_ratesum[idx]   -= temp;
        state->ratesum -= s->pop[k]*temp;
      }
    }

    for(jj=0;jj<nadd; jj++) {
      j=loc_add[jj];
      ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqr);CHKERRQ(ierr);
      if(rsqr < rh) {
        ierr = CRDMEDoiComputeRate(crdme,k,j,r,&rate,rx->doi_type);CHKERRQ(ierr);
        temp = rx->c*rate;
        s->pair_ratesum[idx]   += temp;
        state->ratesum += s->pop[k]*temp;
      }
    }
  }
  return 0;
}

PetscErrorCode _p_SSADMUpdatePairRates_Callback(SpatialSSA ssa, pair_rx rx, int nrm, int *loc_rm, int nadd, int *loc_add) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  ri,j,jj,k,kk;
  double               rsqr,temp;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp];
  PetscErrorCode       ierr;

  ri = s->rx_num[spb];
  for(kk=0;kk<s->num_v;kk++) {
    k=s->loc[kk];
    for(jj=0;jj<nrm; jj++) {
      j=loc_rm[jj];
      ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqr);CHKERRQ(ierr);
      temp = rx->kAB(rsqr,crdme->ctx);
      s->pair_ratesum[s->n_pair*kk + ri]   -= temp;
      state->ratesum -= s->pop[k]*temp;
    }

    for(jj=0;jj<nadd; jj++) {
      j=loc_add[jj];
      ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqr);CHKERRQ(ierr);
      ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqr);CHKERRQ(ierr);
      temp = rx->kAB(rsqr,crdme->ctx);
      s->pair_ratesum[s->n_pair*kk + ri]   += temp;
      state->ratesum += s->pop[k]*temp;
    }
  }
  return 0;
}



PetscErrorCode _p_SSADMUpdateRates(SpatialSSA ssa, int sp, int nrm, int *loc_rm, int nadd, int *loc_add) {
  CRDME             crdme=ssa->crdme;
  CRDMESpecies      cs=crdme->species[sp],csb;
  int               M =crdme->Nspecies;
  pair_rx           rx;
  pair_potential    pp;
  int               *Ip,*Ij;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState sb;
  int               jj,j,k,ll,l,m,sr,ib,ei;
  double            rh,rsqr,rsqre,pv;
  double            Ax[ssa_dm->max_edge];
  int               Aj[ssa_dm->max_edge];
  int               nn;
  PetscBool         potential_modified,self,same_voxel;
  PetscErrorCode    ierr;
  /* update rates for the species on the other side of this potential */
  for(sr=0;sr<M;sr++) {
    pp = cs->pair_potentials[sr];
    if(pp) {
    	self = (sr == sp);
      csb=crdme->species[sr];
      sb=state->species[sr];
      Ip=pp->Ip;
      Ij=pp->Ij;
      potential_modified=0;
      if(Ip) {
        for(jj=0; jj < nrm; jj++) {
          j = loc_rm[jj];
          for(ll=Ip[j];ll<Ip[j+1];ll++) {
            l=Ij[ll];
            if(sb->pop[l]) {
              ib = sb->first[l];
            	state->ratesum -= sb->pop[l]*sb->hr[ib].voxel_rate;
            	sb->hr[ib].voxel_rate = 0.0;
              ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
          	  ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,l,j,&rsqr);CHKERRQ(ierr);
          	  pv   = pp->psiAB(rsqr,ssa->ctx);
            	for(ei=0;ei<nn;ei++) {
            	  k=Aj[ei];
            	  CRDMEGeometryNodesGetDistanceSqr(crdme->geom,k,j,&rsqre);CHKERRQ(ierr);
            	  sb->hr[ib].edge_pot[ei] -= pp->psiAB(rsqre,ssa->ctx) - pv;
           	  	sb->hr[ib].edge_rate[ei] = csb->D*Ax[ei]*bernoulli(sb->hr[ib].edge_pot[ei]);
           	  	sb->hr[ib].voxel_rate   += sb->hr[ib].edge_rate[ei];
            	}
            	state->ratesum += sb->pop[l]*sb->hr[ib].voxel_rate;
            }
          }
        }
//            for(ei=0;ei<nn;ei++) {
//              k=Aj[ei];
//              CRDMEGeometryNodesGetDistanceSqr(crdme->geom,l,k,&rsqr);CHKERRQ(ierr);
//              CRDMEGeometryNodesGetDistanceSqr(crdme->geom,l,k,&rsqre);CHKERRQ(ierr);
//              if(sb->pop[l]) {
//                sb->hr[ib].edge_pot[ei] += pp->p[ll];
//              }
//              if(sb->pop[k]) {
//                ierr = CRDMEGeometryNodeGetEdges(crdme->geom,k,&nn2,Aj2,Ax2);CHKERRQ(ierr);
//                for(ej=0;ej<nn2;ej++) {
//                  if(Aj2[ej] == l) {
//                    sb->hr[sb->first[k]].edge_pot[ej] -= pp->p[ll];
//                    break;
//                  }
//                }
//              }
//            }
//          }
//
//          for(ll=Ip[j];ll<Ip[j+1];ll++) {
//            l=Ij[ll];
//            if(sb->pop[l]) {
//              ib = sb->first[l];
//              csb=crdme->species[sr];
//              state->ratesum -= sb->pop[l]*sb->hr[ib].voxel_rate;
//              sb->hr[ib].voxel_rate = 0.0;
//              ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
//              for(ei=0;ei<nn;ei++) {
//                sb->hr[ib].edge_rate[ei] = csb->D*Ax[ei]*bernoulli(sb->hr[ib].edge_pot[ei]);
//                sb->hr[ib].voxel_rate   += sb->hr[ib].edge_rate[ei];
//              }
//              state->ratesum += sb->pop[l]*sb->hr[ib].voxel_rate;
//            }
//          }

        
        for(jj=0; jj < nadd; jj++) {
          j = loc_add[jj];
          for(ll=Ip[j];ll<Ip[j+1];ll++) {
            l=Ij[ll];
            if(sb->pop[l]) {
              ib = sb->first[l];
            	state->ratesum -= sb->pop[l]*sb->hr[ib].voxel_rate;
            	sb->hr[ib].voxel_rate = 0.0;
              ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
          	  ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,l,j,&rsqr);CHKERRQ(ierr);
          	  pv   = pp->psiAB(rsqr,ssa->ctx);
          	  for(ei=0;ei<nn;ei++) {
            	  k=Aj[ei];
            	  CRDMEGeometryNodesGetDistanceSqr(crdme->geom,k,j,&rsqre);CHKERRQ(ierr);
            	  sb->hr[ib].edge_pot[ei] += pp->psiAB(rsqre,ssa->ctx) - pv;
           	  	sb->hr[ib].edge_rate[ei] = csb->D*Ax[ei]*bernoulli(sb->hr[ib].edge_pot[ei]);
           	  	sb->hr[ib].voxel_rate   += sb->hr[ib].edge_rate[ei];
            	}
            	state->ratesum += sb->pop[l]*sb->hr[ib].voxel_rate;
            }
          }
        }
//            l=Ij[ll];
//            ib = sb->first[l];
//            ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
//            for(ei=0;ei<nn;ei++) {
//              k=Aj[ei];
//              if(sb->pop[l]) {
//                sb->hr[ib].edge_pot[ei] -= pp->p[ll];
//              }
//              if(sb->pop[k]) {
//                ierr = CRDMEGeometryNodeGetEdges(crdme->geom,k,&nn2,Aj2,Ax2);CHKERRQ(ierr);
//                for(ej=0;ej<nn2;ej++) {
//                  if(Aj[ej] == l) {
//                    sb->hr[sb->first[k]].edge_pot[ej] += pp->p[ll];
//                    break;
//                  }
//                }
//              }
//            }
//          }
//
//          for(ll=Ip[j];ll<Ip[j+1];ll++) {
//            l=Ij[ll];
//            if(sb->pop[l]) {
//              ib = sb->first[l];
//              csb=crdme->species[sr];
//              state->ratesum -= sb->pop[l]*sb->hr[ib].voxel_rate;
//              sb->hr[ib].voxel_rate = 0.0;
//              ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
//              for(ei=0;ei<nn;ei++) {
//                sb->hr[ib].edge_rate[ei] = csb->D*Ax[ei]*bernoulli(sb->hr[ib].edge_pot[ei]);
//                sb->hr[ib].voxel_rate   += sb->hr[ib].edge_rate[ei];
//              }
//              state->ratesum += sb->pop[l]*sb->hr[ib].voxel_rate;
//            }
//          }
      } else {
        rh = (pp->radius + crdme->geom->hmax)*(pp->radius + crdme->geom->hmax);
        for(ll=0;ll<sb->num_v;ll++) {
          l = sb->loc[ll];
          if(nadd == 1) {
            same_voxel = (l == loc_add[0]);
          }
          ierr = CRDMEGeometryNodeGetEdges(crdme->geom,l,&nn,Aj,Ax);CHKERRQ(ierr);
          csb=crdme->species[sr];
          state->ratesum -= sb->pop[l]*sb->hr[ll].voxel_rate;
          sb->hr[ll].voxel_rate = 0.0;
          for(ei=0;ei<nn;ei++) {
            potential_modified = 0;
            k=Aj[ei];
            if(!self || !same_voxel) {
							for(jj=0;jj < nrm;jj++) {
								j = loc_rm[jj];
								ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,l,&rsqr);CHKERRQ(ierr);
								if(rsqr < rh) {
									ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqre);CHKERRQ(ierr);
									potential_modified = 1;
									sb->hr[ll].edge_pot[ei] -= pp->psiAB(rsqre,ssa->ctx) - pp->psiAB(rsqr,ssa->ctx);
								}
							}
            }
            for(jj=0;jj < nadd;jj++) {
              j = loc_add[jj];
              if(!self || !(l == j) || sb->pop[j] > 1) {
                ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,l,&rsqr);CHKERRQ(ierr);
                if(rsqr < rh) {
                  ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,j,k,&rsqre);CHKERRQ(ierr);
                  potential_modified = 1;
                  sb->hr[ll].edge_pot[ei] += pp->psiAB(rsqre,ssa->ctx) - pp->psiAB(rsqr,ssa->ctx);
                }
              }
            }
            if(potential_modified) {
              sb->hr[ll].edge_rate[ei] = csb->D*Ax[ei]*bernoulli(sb->hr[ll].edge_pot[ei]);
            }
            sb->hr[ll].voxel_rate   += sb->hr[ll].edge_rate[ei];
          }
         state->ratesum += sb->pop[l]*sb->hr[ll].voxel_rate;
        }
      }
    }
  }
/* update pair reaction rates for the other species */
  for(m=0;m<M;m++) {
    csb=crdme->species[m];
    rx=csb->pair_rxs[sp];
    if(rx) {
    	switch(rx->me_type) {
    	case DOI:
    		ierr = _p_SSADMUpdatePairRates_Doi(ssa,rx,nrm,loc_rm,nadd,loc_add);CHKERRQ(ierr);
    		break;
    	case DOI_REJECT:
    		ierr = _p_SSADMUpdatePairRates_Reject(ssa,rx,nrm,loc_rm,nadd,loc_add);CHKERRQ(ierr);
    		break;
			case STENCIL:
				ierr = _p_SSADMUpdatePairRates_Stencil(ssa,rx,nrm,loc_rm,nadd,loc_add);CHKERRQ(ierr);
				break;
			case CALLBACK:
				ierr = _p_SSADMUpdatePairRates_Callback(ssa,rx,nrm,loc_rm,nadd,loc_add);CHKERRQ(ierr);
				break;
    	}
    }
  }
 
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdateDiffusion(SpatialSSA ssa, int sp, int i, int j) {
  
  PetscErrorCode    ierr;
  PetscFunctionBegin;
  /* diffusion over edge ij */
  ierr = _p_SSADMSwitchLocation(ssa,sp,i,j);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,sp,1,&i,1,&j);CHKERRQ(ierr);
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdatePair(SpatialSSA ssa, int sp, int spb, int la, int lb) {
  CRDME             crdme=ssa->crdme;
  CRDMESpecies      cs=crdme->species[sp];
  pair_rx           rx=cs->pair_rxs[spb];
  pair_rx_type      rtype=rx->rtype;
  int               spc;
  double            r;
  PetscErrorCode    ierr;

  ierr = _p_SSADMRemoveFromLocation(ssa,sp,la);CHKERRQ(ierr);
  if(rtype == RX_ANN || rtype == RX_BND) { 
    ierr = _p_SSADMRemoveFromLocation(ssa,spb,lb);CHKERRQ(ierr);
  }

  ierr = _p_SSADMUpdateRates(ssa,sp,1,&la,0,NULL);CHKERRQ(ierr);
  if(rtype == RX_ANN || rtype == RX_BND) { 
    ierr = _p_SSADMUpdateRates(ssa,spb,1,&lb,0,NULL);CHKERRQ(ierr);
  }

  if(rtype == RX_BND || rtype == RX_ENZ) {
    spc = rx->species[2];
    r = UNI;
    if(r > .5) { /* should use specified placement mechanism instead */
      ierr = _p_SSADMAddToLocation(ssa,spc,la);CHKERRQ(ierr);
      ierr = _p_SSADMUpdateRates(ssa,spc,0,NULL,1,&la);CHKERRQ(ierr);
    } else {
      ierr = _p_SSADMAddToLocation(ssa,spc,lb);CHKERRQ(ierr);
      ierr = _p_SSADMUpdateRates(ssa,spc,0,NULL,1,&lb);CHKERRQ(ierr);
    }
  }
  
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdateUnbind(SpatialSSA ssa, int sp, int ri, double r, int loc) {
  
  CRDME             crdme=ssa->crdme;
  CRDMESpecies      cs=crdme->species[sp];
  rx_CAB            rx=cs->rx_unbnd[ri];
  int               *Ip = cs->rx_unbnd[ri]->Ip;
  int               *Ij = cs->rx_unbnd[ri]->Ij;
  double            *rates = cs->rx_unbnd[ri]->rates;
  int               sp1,sp2,j;
  double            r1;
  PetscErrorCode    ierr;
  sp1 = rx->species[1];
  sp2 = rx->species[2];
  ierr = _p_SSADMRemoveFromLocation(ssa,sp,loc);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,sp,1,&loc,0,NULL);CHKERRQ(ierr); /* TODO: update potentials first, then rates */
  switch(rx->ptype) {
    case SSA_AB_PLACEMENT_POINT:
      ierr = _p_SSADMAddToLocation(ssa,sp1,loc);CHKERRQ(ierr);
      ierr = _p_SSADMUpdateRates(ssa,sp1,0,NULL,1,&loc);CHKERRQ(ierr);
      ierr = _p_SSADMAddToLocation(ssa,sp2,loc);CHKERRQ(ierr);
      ierr = _p_SSADMUpdateRates(ssa,sp2,0,NULL,1,&loc);CHKERRQ(ierr);
      break;
    default:
      r1   = UNI;
      if(r1 > .5) {
        ierr = _p_SSADMAddToLocation(ssa,sp1,loc);CHKERRQ(ierr);
        ierr = _p_SSADMUpdateRates(ssa,sp1,0,NULL,1,&loc);CHKERRQ(ierr);
        if(Ip[loc+1] - Ip[loc] == 1) {
          ierr = _p_SSADMAddToLocation(ssa,sp2,loc);CHKERRQ(ierr);
          ierr = _p_SSADMUpdateRates(ssa,sp2,0,NULL,1,&loc);CHKERRQ(ierr);
        } else {
          j    = bin_search(rates,Ip[loc],Ip[loc+1],r);
	  if(j < 0) {
            PetscPrintf(PETSC_COMM_SELF,"Error in binary search. Query not found.\n");
            PetscPrintf(PETSC_COMM_SELF,"  Index %d\n",loc);
            for(int i = Ip[loc]; i < Ip[loc+1];i++) {
	      printf("  %d %1.16f\n",Ij[i],rates[i]);
            }
            SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
            SSADMState           state = ssa_dm->state;
            SSADMSpeciesState    s=state->species[sp];
            printf("  %1.16f\n",s->unbnd_ratesum[loc]);
	    return 1;
          }
          j = Ij[j];
          ierr = _p_SSADMAddToLocation(ssa,sp2,j);CHKERRQ(ierr);
          ierr = _p_SSADMUpdateRates(ssa,sp2,0,NULL,1,&j);CHKERRQ(ierr);
        }
      } else {
        ierr = _p_SSADMAddToLocation(ssa,sp2,loc);CHKERRQ(ierr);
        ierr = _p_SSADMUpdateRates(ssa,sp2,0,NULL,1,&loc);CHKERRQ(ierr);
        if(Ip[loc+1] - Ip[loc] == 1) {
          ierr = _p_SSADMAddToLocation(ssa,sp1,loc);CHKERRQ(ierr);
          ierr = _p_SSADMUpdateRates(ssa,sp1,0,NULL,1,&loc);CHKERRQ(ierr);
        } else {
          j    = bin_search(rates,Ip[loc],Ip[loc+1],r);
          if(j < 0) {
            PetscPrintf(PETSC_COMM_SELF,"Error in binary search. Query not found.\n");
	    PetscPrintf(PETSC_COMM_SELF,"  Index %d\n",loc);
	    for(int i = Ip[loc]; i < Ip[loc+1]; i++) {
	      printf("  %d %1.16f\n",Ij[i],rates[i]);
	    }
	    SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
	    SSADMState           state = ssa_dm->state;
	    SSADMSpeciesState    s=state->species[sp];
	    printf("  %1.16f\n",s->unbnd_ratesum[loc]);
	    return 1;
          }
          j = Ij[j];
          ierr = _p_SSADMAddToLocation(ssa,sp1,j);CHKERRQ(ierr);
          ierr = _p_SSADMUpdateRates(ssa,sp1,0,NULL,1,&j);CHKERRQ(ierr);
        }
      }
  }
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdateDecay(SpatialSSA ssa, int sp, int loc) {
  PetscErrorCode ierr;
  ierr = _p_SSADMRemoveFromLocation(ssa,sp,loc);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,sp,1,&loc,0,NULL);CHKERRQ(ierr);
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdateCreate(SpatialSSA ssa, int sp, int loc) {
  PetscErrorCode ierr;
  ierr = _p_SSADMAddToLocation(ssa,sp,loc);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,sp,0,NULL,1,&loc);CHKERRQ(ierr);
  return 0;
}

PetscErrorCode _p_SSADMRatesUpdateConvert(SpatialSSA ssa, int sp, int spb, int loc) {
  PetscErrorCode ierr;
  ierr = _p_SSADMRemoveFromLocation(ssa,sp,loc);CHKERRQ(ierr);
  ierr = _p_SSADMAddToLocation(ssa,spb,loc);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,sp,1,&loc,0,NULL);CHKERRQ(ierr);
  ierr = _p_SSADMUpdateRates(ssa,spb,0,NULL,1,&loc);CHKERRQ(ierr);
  return 0;
}

PetscErrorCode _p_SSADMSamplePairReaction_Callback(SpatialSSA ssa, pair_rx rx, double r, int i, int *loc, PetscBool *fnd) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj;
  double               rsqr,sum=0.0;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  PetscFunctionBegin;
  for(jj=0; jj<sb->num_v && sum<r;jj++) {
    j = sb->loc[jj];
    ierr = CRDMEGeometryNodesGetDistanceSqr(crdme->geom,i,j,&rsqr);CHKERRQ(ierr);
    sum+=s->pop[i]*sb->pop[j]*rx->kAB(rsqr,ssa->ctx);
    if(sum > r) {
    	*loc = j;
    	return 0;
    }
  }
  *fnd=0;
  return 0;
}

PetscErrorCode _p_SSADMSamplePairReaction_Doi(SpatialSSA ssa, pair_rx rx, double r, int i, int *loc, PetscBool *fnd) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj;
	double               sum=0.0,rr=rx->radius*rx->radius,rate;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  PetscFunctionBegin;
  for(jj=0; jj<sb->num_v && sum<r;jj++) {
    j = sb->loc[jj];
    ierr = CRDMEDoiComputeRate(crdme,i,j,rr,&rate,rx->doi_type);
    sum+=s->pop[i]*sb->pop[j]*rx->c*rate;
    if(sum > r) {
    	*loc = j;
    	return 0;
    }
  }
  *fnd=0;
  return 0;
}

PetscErrorCode _p_SSADMSamplePairReaction_Stencil(SpatialSSA ssa, pair_rx rx, double r, int i, int *loc, PetscBool *fnd) {
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj;
	double               sum=0.0;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];

  PetscFunctionBegin;
  for(jj=rx->Ip[i]; jj<rx->Ip[i+1] && sum<r;jj++) {
  	j = rx->Ij[jj];
  	if(sb->pop[j]) {
      sum+=s->pop[i]*sb->pop[j]*rx->rates[jj];
  	}
    if(sum > r) {
    	*loc = j;
    	return 0;
    }
  }
  for(jj=rx->Ip[i]; jj<rx->Ip[i+1] && sum<r;jj++) {
  	j = rx->Ij[jj];
  	if(sb->pop[j]) {
      sum+=s->pop[i]*sb->pop[j]*rx->rates[jj];
  	}
    PetscPrintf(PETSC_COMM_SELF,"%d %d %d %d %1.14f %1.14f %1.14f\n",s->pop[i],sb->pop[j],rx->Ip[i+1] - rx->Ip[i],rx->Ip[j+1] - rx->Ip[j],sum,r-sum,state->ratesum);
  }
  *fnd=0;
  return 0;
}

static inline double fsqr(double x) {
	return x*x;
}

static inline double dist_sqr(double *x, double *xb) {
	return fsqr(x[0] - xb[0]) + fsqr(x[1] - xb[1]);
}

#define FLOAT_TO_INT(x) (int)(x+0.5)

PetscErrorCode _p_SSADMSamplePairReaction_Reject(SpatialSSA ssa, pair_rx rx, double r, int i, int *loc, PetscBool *fnd) {
  CRDME                crdme = ssa->crdme;
  int                  sp=rx->species[0],spb=rx->species[1];
  int                  j,jj,rand,rh,sum=0;
  double               x[2],xb[2],rr=rx->radius*rx->radius;
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  SSADMSpeciesState    s=state->species[sp],sb=state->species[spb];
  PetscErrorCode       ierr;

  PetscFunctionBegin;

  rand = KISS % FLOAT_TO_INT(s->pair_ratesum[s->n_pair*s->first[i] + s->rx_num[spb]] / rx->c);
  rh   = fsqr(rx->radius - crdme->geom->hmax);
  for(jj=rx->Ip[i]; jj<rx->Ip[i+1] && sum<=rand;jj++) {
  	j = rx->Ij[jj];
    sum+=sb->pop[j];
    if(sum > rand) {
  		ierr = CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
  		ierr = CRDMEGeometryNodeGetCoordinates(crdme->geom,j,xb);CHKERRQ(ierr);
    	if(dist_sqr(x,xb) < rh) {
    		*loc = j;
    		*fnd = 1;
    	} else {
      	if(rx->doi_type!=DOI_LUMP) {
        	ierr = CRDMEGeometrySampleFromVoxel(crdme->geom,i,x);CHKERRQ(ierr);
      	}
      	ierr = CRDMEGeometrySampleFromVoxel(crdme->geom,j,xb);CHKERRQ(ierr);
      	if(dist_sqr(x,xb) < rr) {
        	*loc = j;
        	*fnd = 1;
      	} else {
      		*loc = -1;
      		*fnd = 0;
      	}
    	}
    	return 0;
    }
  }
  PetscPrintf(PETSC_COMM_WORLD,"Error sampling pair reaction using rejection: no reaction found\n");
	return 1;
}

PetscErrorCode SSASolve_DM(SpatialSSA ssa) {
  CRDME             crdme = ssa->crdme;
  CRDMESpecies      cs,csb;
  int               M      = crdme->Nspecies;
  SSA_DM            *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState        state = ssa_dm->state;
  SSADMSpeciesState s,sb;
  double            sum=0.0,r,step;
  int               sims,si,ni,ri,sj,i,j,e,c=0;
  int               Aj[ssa_dm->max_edge];
  double            Ax[ssa_dm->max_edge];
  int               nn;
  PetscBool         event_fnd;
  PetscErrorCode    ierr;
  
  PetscFunctionBegin;
  for(sims=0;sims<ssa->ntrials;sims++) {
    while(!ssa->monitor(ssa,sims,NULL) && state->time < ssa->tf && state->ratesum>1e-12) {
      r = state->ratesum*erand48(ssa->seed);
      step = -PetscLogReal(erand48(ssa->seed))/state->ratesum;
      state->time+=step;
      if(state->time >= ssa->tf) {
        state->time = ssa->tf;
	if(ssa->checkpoints) {
	  while(ssa->tf >= ssa->checkpoints[ssa->next_checkpoint] && ssa->next_checkpoint < ssa->num_checkpoints) {
	    ssa->monitor(ssa,sims,NULL);
	    ssa->next_checkpoint++;
	  } 
	} else {
	  ssa->monitor(ssa,sims,NULL);
	}
        break; /* break before the next event happens */
      }
      if(ssa->checkpoints) {
        while(state->time >= ssa->checkpoints[ssa->next_checkpoint]) {
        	ssa->monitor(ssa,sims,NULL);
        	ssa->next_checkpoint++;
        }
      }

      sum=0.0;
      event_fnd=0;
      for(si = 0;si < M && !event_fnd; si++) { /* loop through species */
        s=state->species[si];
        for(ni = 0;ni < s->num_v && !event_fnd;ni++) { /* loop through each member */
          i = s->loc[ni];
          sum+=s->pop[i]*s->hr[ni].voxel_rate;
          if(sum > r) {
            sum-=s->pop[i]*s->hr[ni].voxel_rate;
            ierr = CRDMEGeometryNodeGetEdges(crdme->geom,i,&nn,Aj,Ax);CHKERRQ(ierr);
            for(e=0;e<nn;e++) {
              sum += s->pop[i]*s->hr[ni].edge_rate[e];
              if(sum > r) {
                event_fnd = PETSC_TRUE;
                ierr = _p_SSADMRatesUpdateDiffusion(ssa,si,i,Aj[e]);CHKERRQ(ierr);                
                break;
              }
            }
            if(!event_fnd) {
            	double x[2];
            	ierr = CRDMEGeometryNodeGetCoordinates(crdme->geom,i,x);CHKERRQ(ierr);
            	PetscPrintf(PETSC_COMM_SELF,"Error: no diffusion event found species %s %d\n",crdme->species[si]->name);
            	PetscPrintf(PETSC_COMM_SELF,"time: %1.15f\n",state->time);
            	PetscPrintf(PETSC_COMM_SELF,"sum: %1.15f; r: %1.15f; rate sum: %1.15f\n",sum,r,state->ratesum);
            	PetscPrintf(PETSC_COMM_SELF,"total diffusion rate: %1.15f\n",s->pop[i]*s->hr[ni].voxel_rate);
            	PetscPrintf(PETSC_COMM_SELF,"node number: %d\n",i);
            	PetscPrintf(PETSC_COMM_SELF,"number of neighbors: %d\n",nn);
            	PetscPrintf(PETSC_COMM_SELF,"coordinates: (%1.14f, %1.14f)\n",x[0],x[1]);
            	PetscPrintf(PETSC_COMM_SELF,"edge rates and diffusion rates: \n");
              for(e=0;e<nn;e++) {
              	PetscPrintf(PETSC_COMM_SELF,"  %1.15f %1.15f %1.15f\n",s->pop[i]*s->hr[ni].edge_rate[e],s->hr[ni].edge_pot[e],Ax[e]);
              }
            	return 1;
            }
          }
        }
      }

      
      for(si = 0;si < M && !event_fnd; si++) { /* loop through species */
        s=state->species[si];
        cs=crdme->species[si];
        for(sj=0;sj< M && !event_fnd;sj++) {
          pair_rx rx = cs->pair_rxs[sj];
          if(rx) {              
            ri = s->rx_num[sj];
            csb = crdme->species[sj];
            sb  = state->species[sj];
            for(ni = 0;ni < s->num_v && !event_fnd;ni++) { /* loop through each member */
              i  = s->loc[ni];
              sum+=s->pop[i]*s->pair_ratesum[s->n_pair*ni + ri];
              if(sum > r) {
                event_fnd=1;
                sum-=s->pop[i]*s->pair_ratesum[s->n_pair*ni + ri];
                switch(rx->me_type) {
                case DOI:
                	ierr=_p_SSADMSamplePairReaction_Doi(ssa,rx,r-sum,i,&j,&event_fnd);CHKERRQ(ierr);
                	if(!event_fnd) {
                		PetscPrintf(PETSC_COMM_SELF,"Error on doi pair reaction sample: no event found\n");
                		return 1;
                	} else {
                    ierr = _p_SSADMRatesUpdatePair(ssa,si,sj,i,j);CHKERRQ(ierr);
                    sum = r+1.0;
                	}
                	break;
                case STENCIL:
                	ierr=_p_SSADMSamplePairReaction_Stencil(ssa,rx,r-sum,i,&j,&event_fnd);CHKERRQ(ierr);
                	if(!event_fnd) {
                		PetscPrintf(PETSC_COMM_SELF,"Error on stencil pair reaction sample: no event found\n");
                		PetscPrintf(PETSC_COMM_SELF,"%1.14f\n",r-sum);

                		return 1;
                	} else {
                    ierr = _p_SSADMRatesUpdatePair(ssa,si,sj,i,j);CHKERRQ(ierr);
                    sum = r+1.0;
                	}
                	break;
                case CALLBACK:
                	ierr=_p_SSADMSamplePairReaction_Callback(ssa,rx,r-sum,i,&j,&event_fnd);CHKERRQ(ierr);
                	if(!event_fnd) {
                		PetscPrintf(PETSC_COMM_SELF,"Error on callback pair reaction sample: no event found\n");
                		return 1;
                	} else {
                    ierr = _p_SSADMRatesUpdatePair(ssa,si,sj,i,j);CHKERRQ(ierr);
                    sum = r+1.0;
                	}
                	break;
                case DOI_REJECT:
                	ierr=_p_SSADMSamplePairReaction_Reject(ssa,rx,0,i,&j,&event_fnd);CHKERRQ(ierr);
                	if(event_fnd) {
                    ierr = _p_SSADMRatesUpdatePair(ssa,si,sj,i,j);CHKERRQ(ierr);
                	}
                	event_fnd = 1;
                  sum = r+1.0;
                	break;
                }
              }
            }
          }
        }
      }

      for(si = 0;si < M && sum < r; si++) { /* loop through species */
        s=state->species[si];
        cs=crdme->species[si];
        for(ri=0;ri<s->n_unbnd && sum < r;ri++) {
          csb = crdme->species[sj];
          sb  = state->species[sj];
          for(ni = 0;ni < s->num_v && sum < r;ni++) { /* loop through each member */
            i  = s->loc[ni];
            sum+=s->pop[i]*s->unbnd_ratesum[s->n_unbnd*i + ri];
            if(sum > r) {
              sum-=s->pop[i]*s->unbnd_ratesum[s->n_unbnd*i + ri];
              ierr=_p_SSADMRatesUpdateUnbind(ssa,si,ri,(r-sum)/s->pop[i],i);CHKERRQ(ierr);
              sum+=(s->pop[i]+1)*s->unbnd_ratesum[s->n_unbnd*i + ri];
            }
          }
        }
      }
      
      for(si = 0; si < M && sum < r; si++) {
        s=state->species[si];
        cs=crdme->species[si];
        for(ri=0;ri<cs->n_convert && sum < r;ri++) {
          sum += s->num*cs->conv_rxs[ri]->c;
          if(sum > r) {
          	sum -= s->num*cs->conv_rxs[ri]->c;
            for(ni = 0;ni < s->num_v && sum < r;ni++) { /* loop through each member */
            	i   = s->loc[ni];
            	sum += s->pop[i]*cs->conv_rxs[ri]->c;
            	if(sum > r) {
            		sj  = cs->conv_rxs[ri]->species[1];
            		ierr=_p_SSADMRatesUpdateConvert(ssa,si,sj,i);CHKERRQ(ierr);
            	}
            }
          }
        }
      }
      if(sum < r) {
        PetscPrintf(PETSC_COMM_SELF,"\nError inside SSA DM solve: no event found, rate sums do not agree\n");
        PetscPrintf(PETSC_COMM_SELF,"%f %1.14f %1.14f %1.14f\n",state->time,sum,r,state->ratesum);
        for(int i = 0; i < crdme->Nspecies; i++) {
          PetscPrintf(PETSC_COMM_SELF,"  %d ",state->species[i]->num_v);
        }
        PetscPrintf(PETSC_COMM_SELF,"\n");
        for(int i = 0; i < crdme->Nspecies; i++) {
          PetscPrintf(PETSC_COMM_SELF,"  %d ",state->species[i]->num);
        }
        //return 1;
      }
    }
    if(sims<ssa->ntrials-1){
    	ierr = ssa_dm->reset(ssa,state->species);CHKERRQ(ierr);
    	ssa->next_checkpoint = 0;
    }
    if(sims/(1.0*ssa->ntrials) > c*.05) {
      PetscPrintf(PETSC_COMM_WORLD,"[%d]",c);
      c++;
    }
  }

  PetscPrintf(PETSC_COMM_WORLD,"\n");

 
  return 0; 
}


//
//PetscErrorCode SSADMStateViewVTK(SpatialSSA ssa, const char *filename) {
//
//  FILE           *f = NULL;
//  CRDME          crdme = ssa->crdme;
//  int            N     = crdme->geom->N;
//  int            *Ap   = crdme->geom->Ap;
//  SSA_DM         *ssa_dm = (SSA_DM*) ssa->data;
//  SSADMState     state = ssa_dm->state;
//  char           vtk_filename[50],*ptr;
//  int            offset = 0,num_dual_nodes = 0,i;
//  int            Np, Nc;
//  int            bytes;
//  int            *cells, *offsets;
//  double         *nodes,*dens;
//  int            *types;
//  PetscErrorCode ierr;
//  
//  PetscFunctionBegin;
//  for(int i = 0; i < N; i++) {
//     num_dual_nodes += 2*(Ap[i + 1] - Ap[i]); 
//  }
//  
//  Np = num_dual_nodes;
//  Nc = N;
//  
//  ierr  = PetscMalloc1(Np,&cells);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(3*Np,&nodes);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(Nc,&types);CHKERRQ(ierr);
//  ierr  = PetscMalloc1(Nc,&offsets);CHKERRQ(ierr);
//
//  snprintf(vtk_filename, sizeof(vtk_filename), "%s_%2.1f", filename, state->time);
//  ptr=vtk_filename;
//  i=0;
//  while(*ptr!='\0') { /* remove decimal point. TODO: don't know if this will work for timesteps < .1 (how does pvw treat 01 vs 001?) */
//    vtk_filename[i] = *ptr;
//    if(*ptr != '.') { i++; } 
//    ptr++;
//  }
//  snprintf(vtk_filename, sizeof(vtk_filename), "%s.vtu", vtk_filename);
//
//
//  f = fopen(vtk_filename, "wb");
//  fprintf(f, "<?xml version=\"1.0\"?>\n");
//  fprintf(f, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"%s\">\n", "LittleEndian");
//  fprintf(f, "  <UnstructuredGrid>\n");
//
//  fprintf(f, "    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\n", Np, Nc);
//  fprintf(f, "      <Points>\n");
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"Position\""
//    " NumberOfComponents=\"3\" format=\"appended\" offset=\"%D\" />\n", "Float64", offset);
//
//  offset += 4;
//  offset += 3 * sizeof(double) * Np;
//
//  fprintf(f, "      </Points>\n");
//  fprintf(f, "      <Cells>\n");
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"connectivity\""
//    " format=\"%s\" offset=\"%D\" />\n", "Int32", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(int) * Np;
//
//  fprintf(f, "        <DataArray type=\"%s\" Name=\"offsets\""
//    " format=\"%s\"  offset=\"%D\" />\n", "Int32", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(int) * Nc;
//
//  fprintf(f, "        <DataArray type=\"Int32\" Name=\"types\""
//    " format=\"%s\" offset=\"%D\" />\n", "appended", offset);
//
//  offset += 4;
//  offset += sizeof(int) * Nc;
//
//  fprintf(f, "      </Cells>\n");
//  
//  fprintf(f, "      <CellData>\n");
//  for(int i = 0; i < crdme->Nspecies; i++) {
//    fprintf(f, "        <DataArray type=\"%s\" Name=\"%s\""
//      " NumberOfComponents=\"1\" format=\"%s\" offset=\"%D\" />\n", "Float64", crdme->species[i]->name, "appended", offset);
//    if(i != crdme->Nspecies-1) {
//      offset += 4;
//      offset += sizeof(double) * Nc;
//    }
//  }
//
//  fprintf(f, "      </CellData>\n");
//  fprintf(f, "    </Piece>\n");
//
//  fprintf(f, "  </UnstructuredGrid>\n");
//
//  fprintf(f, "  <AppendedData encoding=\"raw\">\n");
//  fprintf(f, "_");
//  
//  compute_dual_mesh_vtk(crdme->geom,cells,nodes,offsets,types);
//
//  bytes = 3*sizeof(double)*Np;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(nodes, sizeof(double), 3*Np, f);
//  
//  bytes = sizeof(int)*Np;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(cells, sizeof(int), Np, f);
//
//  bytes = sizeof(int)*Nc;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(offsets,sizeof(int),Nc,f);
//
//  bytes = sizeof(int)*Nc;
//  fwrite(&bytes,sizeof(int),1,f);
//  fwrite(types,sizeof(int),Nc,f);
//
//  ierr  = PetscMalloc1(Nc,&dens);CHKERRQ(ierr);
//  for(int i = 0; i < crdme->Nspecies; i++) {
//    for(int j = 0; j < Nc; j++) {
//      dens[j] = state->species[i]->pop[j]/crdme->geom->mass[j];
//    }
//    bytes = sizeof(double)*Nc;
//    fwrite(&bytes,sizeof(int),1,f);
//    fwrite(dens,sizeof(double),Nc,f);
//  }
//  
//  fprintf(f, "\n");
//  fprintf(f, "  </AppendedData>\n");
//  fprintf(f, "</VTKFile>");
//  
//  PetscFree(nodes);
//  PetscFree(cells);
//  PetscFree(offsets);
//  PetscFree(types);
//  fclose(f);
//  return 0;
//}
//
