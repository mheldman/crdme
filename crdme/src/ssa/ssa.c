
#define _XOPEN_SOURCE
#include <stdlib.h>
#include <petsc.h>
#include <ssahm.h>
#include <ssansm.h>
#include <ssa.h>
#include <crdmegeombox.h>
#include <ssanbhditerators.h>
#include <ssareactions.h>
#include <rng.h>
#include <util.h>

PetscErrorCode DefaultMonitor(SpatialSSA ssa, PetscBool *end, PetscBool isChkpt, void *ctx) {
    
    PetscFunctionBegin;

    /* do nothing */

    PetscFunctionReturn(0);
}

PetscErrorCode SSACreate(SpatialSSA *ssa) {
  
  PetscErrorCode ierr;
  SpatialSSA     s;
  PetscMPIInt    rank;

  PetscFunctionBegin;

  ierr = PetscMalloc1(1,&s);CHKERRQ(ierr);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  s->geom     = NULL;
  s->rs       = NULL;
  s->ssa_type = 0;

  s->seed[0] = rank;
  s->seed[1] = 0;
  s->seed[2] = 0;

  s->nTrials      = 1;
  s->Tf           = 1.0;
  s->monitor      = DefaultMonitor;
  s->monitor_ctx  = NULL;
  s->nCheckpoints = 0; 
  s->checkpoints  = NULL;

  s->ops.ssa_destroy = NULL;
  s->ops.ssa_setup   = NULL;
  s->ops.ssa_solve   = NULL;

  s->state = NULL;
  s->trackEmptyVoxelRates=PETSC_TRUE;
  s->ctx   = NULL;
  s->data  = NULL;
  s->setupCalled=PETSC_FALSE;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&s->rank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&s->np);CHKERRQ(ierr);

  *ssa = s;
  
  PetscFunctionReturn(0);
}

PetscErrorCode SSADestroy(SpatialSSA *ssa) {
  /* incomplete */
  PetscErrorCode ierr;
  PetscInt       M=(*ssa)->state->nSpecies;
  PetscBool      match=PETSC_FALSE;
  SSASpecies     s,s2;

  PetscFunctionBegin;

  if((*ssa)->ops.ssa_destroy) {
    ierr = (*ssa)->ops.ssa_destroy(ssa);CHKERRQ(ierr);
  }
  for(PetscInt sp=0;sp<M;sp++) {
    s=(*ssa)->state->spec[sp];
    if(s->hasPot) {
      ierr = PetscFree(s->hoppingRates.edgeDiffusionRates[0]);CHKERRQ(ierr);
      ierr = PetscFree(s->hoppingRates.edgeDiffusionRates);CHKERRQ(ierr);
    }
    ierr = PetscFree(s->hoppingRates.totDiffusionRate);CHKERRQ(ierr);
    ierr = PetscFree(s->initCondition->cumDensity);CHKERRQ(ierr);
    for(PetscInt sp2=sp+1;sp2<M;sp2++) {
      s2=(*ssa)->state->spec[sp2];
      ierr = PetscStrcmp(s->initCondition->name,s2->initCondition->name,&match);CHKERRQ(ierr);
      if(match) {
        s2->initCondition->cumDensity=PETSC_NULL;
      }
    }
    ierr = PetscFree(s->initCondition);CHKERRQ(ierr);
    for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
      ierr = PetscFree(s->reactions.pairKernels[rn].kernCtx);CHKERRQ(ierr);
      //ierr = PetscFree(s->reactions.pairKernels[rn].rejectCtx);CHKERRQ(ierr);
      if(s->reactions.pairKernels[rn].stencil) {
        //ierr = PetscFree(s->reactions.pairKernels[rn].stencil->re);CHKERRQ(ierr);
        //ierr = PetscFree(s->reactions.pairKernels[rn].stencil->indices);CHKERRQ(ierr);
        //ierr = PetscFree(s->reactions.pairKernels[rn].stencil->vals);CHKERRQ(ierr);
        if(s->reactions.Ord2[rn]->isReversible) {
          
        }
      }

    }
    ierr = PetscFree(s->reactions.pairEffects);CHKERRQ(ierr);
    if(s->reactions.nOrd2) { ierr = PetscFree(s->reactions.pairRates[0]);CHKERRQ(ierr); }
    ierr = PetscFree(s->reactions.pairRates);CHKERRQ(ierr);
    ierr = PetscFree(s->reactions.pairKernels);CHKERRQ(ierr);
    for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
      ierr = PetscFree(s->reactions.unbindKernels[rn].kernCtx);CHKERRQ(ierr);
      //ierr = PetscFree(s->reactions.unbindKernels[rn].rejectCtx);CHKERRQ(ierr);
    }
    ierr = PetscFree(s->reactions.unbindKernels);CHKERRQ(ierr);
    if(s->reactions.nUnbind) { ierr = PetscFree(s->reactions.unbindRates[0]);CHKERRQ(ierr); }
    ierr = PetscFree(s->reactions.unbindRates);CHKERRQ(ierr);
    
    if(s->hasPot) { ierr = PetscFree(s->potentials.potEnergy);CHKERRQ(ierr); }
    ierr = PetscFree(s->voxelPop);CHKERRQ(ierr);
    ierr = PetscFree(s);CHKERRQ(ierr);
  }
  ierr = PetscFree((*ssa)->state->spec);CHKERRQ(ierr);
  ierr = PetscFree((*ssa)->state->voxelRatesum);CHKERRQ(ierr);
  ierr = PetscFree((*ssa)->state);CHKERRQ(ierr);

  (*ssa)->geom = NULL; 
  (*ssa)->ctx  = NULL;
  (*ssa)->data = NULL;
  ierr = PetscFree(*ssa);CHKERRQ(ierr);


  PetscFunctionReturn(0);
}

PetscErrorCode SSASetType(SpatialSSA ssa, spatial_ssa_type type) {
  
  PetscErrorCode ierr; 

  PetscFunctionBegin; 

  if(type==SSA_TYPE_HM) {
    {
      SpatialSSA_HM *hm;
      ierr = PetscMalloc1(1,&hm);CHKERRQ(ierr);
      hm->nLevels=0;
      hm->hmVoxelRatesum=PETSC_NULL;
      hm->totVoxels=0;
      ssa->data = hm;
    }
    ssa->ssa_type             = type;
    ssa->ops.ssa_setgeometry      = SSASetGeometry_HM;
    ssa->ops.ssa_speciessetup     = SSASpeciesSetUp_HM;
    ssa->ops.ssa_setup            = SSASetUp_HM;
    ssa->ops.ssa_getnextsubvolume = SSAGetNextSubvolume_HM;
    ssa->ops.ssa_stateinitialize  = SSAStateInitialize_HM;
    ssa->ops.ssa_addtolocation = SSASpeciesAddToVoxel_HM;
    ssa->ops.ssa_removefromlocation = SSASpeciesRemoveFromVoxel_HM;
    ssa->ops.ssa_getnexteventtime = SSAGetNextEventTime_ratesum;
    ssa->ops.ssa_updatehoppingratescallback = SSAUpdateHoppingRatesCallback_HM;
    ssa->ops.ssa_updatepairratescallback = SSAUpdatePairRatesCallback_HM;
    ssa->ops.ssa_destroy = SSADestroy_HM;
  } else if(type==SSA_TYPE_NSM) {
    {
      SpatialSSA_NSM *nsm;
      ierr = PetscMalloc1(1,&nsm);CHKERRQ(ierr);
      nsm->queue=PETSC_NULL;
      nsm->queuedForUpdate=PETSC_NULL;
      nsm->updateQueue=PETSC_NULL;
      nsm->nQueuedForUpdate=0;
      ssa->data = nsm;
    }
    ssa->ssa_type             = type;
    ssa->ops.ssa_setgeometry      = PETSC_NULL;
    ssa->ops.ssa_speciessetup     = PETSC_NULL;
    ssa->ops.ssa_setup            = SSASetUp_NSM;
    ssa->ops.ssa_getnextsubvolume = SSAGetNextSubvolume_NSM;
    ssa->ops.ssa_stateinitialize  = SSAStateInitialize_NSM;
    ssa->ops.ssa_addtolocation = SSASpeciesAddToVoxel_NSM;
    ssa->ops.ssa_removefromlocation = SSASpeciesRemoveFromVoxel_NSM;
    ssa->ops.ssa_getnexteventtime = SSAGetNextEventTime_NSM;
    ssa->ops.ssa_updatehoppingratescallback = SSAUpdateHoppingRatesCallback_NSM;
    ssa->ops.ssa_updatepairratescallback = SSAUpdatePairRatesCallback_NSM;
    ssa->ops.ssa_destroy = SSADestroy_NSM;
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"SSA type %d not implemented\n",type);CHKERRQ(ierr); /* TODO: should be a string */
    return 1;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode SSASetGeometry(SpatialSSA ssa, CRDMEGeometry geom) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(ssa) {
    ssa->geom=geom;
    ierr = PetscMalloc1(geom->N,&ssa->updatedArr);CHKERRQ(ierr);
    ierr = PetscArrayzero(ssa->updatedArr,geom->N);CHKERRQ(ierr);
    if(ssa->ops.ssa_setgeometry) {
      ierr = ssa->ops.ssa_setgeometry(ssa,geom);CHKERRQ(ierr);
    }
    ierr = SSASetUp(ssa);CHKERRQ(ierr);
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Error: null SSA\n");CHKERRQ(ierr);
    return 1;
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode SSASetReactionSystem(SpatialSSA ssa, ReactionSystem rs) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(ssa) {
    ssa->rs=rs;
  } else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Error: null SSA\n");CHKERRQ(ierr);
    return 1;
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode SSASetSeed(SpatialSSA ssa, unsigned short seed[3]) {
  
  PetscFunctionBegin;

  ssa->seed[0] = seed[0];
  ssa->seed[1] = seed[1];
  ssa->seed[2] = seed[2];

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetMonitor(SpatialSSA ssa,SSAMonitor monitor) {
  
  PetscFunctionBegin;

  ssa->monitor = monitor;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetMonitorContext(SpatialSSA ssa,void *ctx) {
  
  PetscFunctionBegin;

  ssa->monitor_ctx = ctx;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetContext(SpatialSSA ssa, void *ctx) {
  
  PetscFunctionBegin;

  ssa->ctx = ctx;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetNumTrials(SpatialSSA ssa, PetscInt nTrials) {
  
  PetscFunctionBegin;

  ssa->nTrials=nTrials;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetFinalTime(SpatialSSA ssa, PetscReal Tf) {
  
  PetscFunctionBegin;

  ssa->Tf = Tf;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetCheckpoints(SpatialSSA ssa,PetscInt nCheckpoints,PetscReal *checkpoints) {

	PetscFunctionBegin;

	ssa->checkpoints  = checkpoints;
	ssa->nCheckpoints = nCheckpoints;

	PetscFunctionReturn(0);
}

PetscErrorCode SSASetInitialPopulation(SpatialSSA ssa, PetscInt *pop) {

  PetscFunctionBegin;

  for(PetscInt sp=0;sp<ssa->state->nSpecies;sp++) {
    ssa->state->spec[sp]->initCondition->pop=pop[sp];
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesSetInitialPopulation(SpatialSSA ssa, PetscInt sp, PetscInt pop) {

  PetscFunctionBegin;

  ssa->state->spec[sp]->initCondition->pop=pop;

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetTrackEmptyVoxelRates(SpatialSSA ssa, PetscBool track) {

  PetscFunctionBegin;

  ssa->trackEmptyVoxelRates=track;

  PetscFunctionReturn(0);
}


PetscErrorCode SSASetFromOptions(SpatialSSA ssa) {
  
  PetscErrorCode ierr;
  PetscInt       nTrials;
  PetscInt       M=ssa->state->nSpecies;
  PetscInt       pop[M],nfnd=M;
  PetscReal      Tf;
  char           optionName[PETSC_MAX_OPTION_NAME];
  SSASpecies     s;
  PetscBool      track,flg;

  PetscFunctionBegin;

  ierr = PetscOptionsGetInt(NULL,NULL,"-ssa_num_trials",&nTrials,&flg);CHKERRQ(ierr);
  if(flg) {
    ierr = SSASetNumTrials(ssa,nTrials);CHKERRQ(ierr);
  }

  ierr = PetscOptionsGetReal(NULL,NULL,"-ssa_set_final_time",&Tf,&flg);CHKERRQ(ierr);
  if(flg) {
    ierr = SSASetFinalTime(ssa,Tf);CHKERRQ(ierr);
  }

  ierr = PetscOptionsGetIntArray(NULL,NULL,"-ssa_init_population",pop,&nfnd,&flg);CHKERRQ(ierr);
  for(PetscInt sp=0;sp<nfnd;sp++) {
    ssa->state->spec[sp]->initCondition->pop=pop[sp];
  }

  for(PetscInt sp=0;sp<M;sp++) {
    s=ssa->state->spec[sp];
    ierr = PetscSNPrintf(optionName,sizeof(optionName),"-ssa_init_population_species_%s",s->rsSpec->name);CHKERRQ(ierr);
    ierr = PetscOptionsGetInt(NULL,NULL,optionName,&pop[0],&flg);CHKERRQ(ierr);
    if(flg) {
      s->initCondition->pop=pop[0];
    }
  }

  ierr = PetscOptionsGetBool(NULL,NULL,"-ssa_track_empty_voxel_rates",&track,&flg);CHKERRQ(ierr);
  if(flg) {
    ssa->trackEmptyVoxelRates=track;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASetInitialCondition(SpatialSSA ssa,const char *specName, SpatDistributionFn density,const char* name) {
  PetscErrorCode ierr;
  PetscInt       sp;
  SSASpecies     s,s2;
  PetscBool      match=PETSC_FALSE;
  CRDMEGeometry  geom=ssa->geom;
  CRDMEGeomVoxel *voxel=&geom->work_voxel[0];
  PetscReal      mass,dens;
  PetscInt       N=geom->N;

  PetscFunctionBegin;

  ierr = ReactionSystemSpeciesGetIndex(ssa->rs,specName,&sp);CHKERRQ(ierr);
  s=ssa->state->spec[sp];
  s->initCondition->density=density; 

  for(PetscInt sp2=0;sp2<ssa->state->nSpecies;sp2++) {
    s2   = ssa->state->spec[sp2];
    ierr = PetscStrcmp(s->initCondition->name,s2->initCondition->name,&match);CHKERRQ(ierr);
    if(match) {
      break;
    }
  } 
  
  if(!match) {
    ierr = PetscFree(s->initCondition->cumDensity);CHKERRQ(ierr);
  }

  for(PetscInt sp2=0;sp2<ssa->state->nSpecies;sp2++) {
    s2   = ssa->state->spec[sp2];
    ierr = PetscStrcmp(name,s2->initCondition->name,&match);CHKERRQ(ierr);
    if(match) {
      s->initCondition->cumDensity=s2->initCondition->cumDensity;
      ierr = PetscSNPrintf(s->initCondition->name,sizeof(s->initCondition->name),name);CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }
  }
  ierr = PetscSNPrintf(s->initCondition->name,sizeof(s->initCondition->name),name);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&s->initCondition->cumDensity);CHKERRQ(ierr);
  for(CRDMEGeomGloIdx i=0;i<N;i++) {
    ierr=CRDMEGeometryNodeGetVoxel(geom,i,voxel);CHKERRQ(ierr);
    ierr=CRDMEGeometryVoxelGetArea(geom,*voxel,&mass);CHKERRQ(ierr);
    ierr = density(geom,i,&dens,ssa->ctx);CHKERRQ(ierr);
    if(i==0) {
      s->initCondition->cumDensity[i]=mass*dens;
    } else {
      s->initCondition->cumDensity[i]=s->initCondition->cumDensity[i-1]+mass*dens;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SpatialSSASpeciesComputeUnbindRates(SpatialSSA ssa, PetscInt sp, SSASpecies s) {
  PetscErrorCode          ierr;
  SSAComputeNbhdRatesCtx  ctx;
  PetscInt                N=ssa->geom->N;
  PetscReal               max=0.0;

  PetscFunctionBegin;

  if(s->reactions.nUnbind) { ierr=PetscArrayzero(s->reactions.unbindRates[0],s->reactions.nUnbind*N);CHKERRQ(ierr); }
  ctx.ssa=ssa;
  ctx.voxelPop =PETSC_NULL;
  for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
    SpatIaStencil stencil=s->reactions.unbindKernels[rn].stencil;
    for(CRDMEGeomGloIdx i=0;i<N;i++) {
      ctx.locRate  =&s->reactions.unbindRates[rn][i];
      ctx.reaction =&s->reactions.unbindKernels[rn];
      if(stencil) {
        if(stencil->vals[1]) {
          if(stencil->boxPeriodic) {
            s->reactions.unbindRates[rn][i] = stencil->vals[1][stencil->stencilLen-1];
          } else {
            s->reactions.unbindRates[rn][i] = stencil->vals[1][stencil->rowPtr[i+1]-1];
          }
        } else {
           ierr = CRDMEGeometryIterateVoxelStencil(ssa->geom,i,stencil,SSAComputeUnbindRatesStencilCallback,1,&ctx);CHKERRQ(ierr);
        }
      } else {
        ierr = CRDMEGeometryIterateVoxelNbhd(ssa->geom,i,s->reactions.Unbind[rn]->radius,SSAComputeUnbindRatesCallback,&ctx);CHKERRQ(ierr);
      }
      max = PetscMax(max,s->reactions.unbindRates[rn][i]);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAReactionSetRejectionFn(SpatialSSA ssa,const char *name,RxRejectionFn reject,void *ctx) {
  PetscErrorCode  ierr;
  PetscInt        rn;
  RSReaction      *rsReac;
  SSAPairReaction *ssaReac;

  PetscFunctionBegin;

  ierr    = ReactionSystemReactionGetIndex(ssa->rs,name,&rn);CHKERRQ(ierr);
  rsReac  = &ssa->rs->reac[rn];
  ssaReac = (SSAPairReaction *)rsReac->data;

  ssaReac->reject=reject;
  ssaReac->rejectCtx=ctx;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAReactionSetDoiType(SpatialSSA ssa,const char *name,SSADoiType type) {
  PetscErrorCode  ierr;
  PetscInt        rn,sp1,sp2;
  RSReaction      *rsReac;
  SSAPairReaction *ssaReac;
  DoiIaFnCtx      *doiCtx;

  PetscFunctionBegin;

  ierr    = ReactionSystemReactionGetIndex(ssa->rs,name,&rn);CHKERRQ(ierr);
  if(rn < 0) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Error setting reaction doi type; no reaction named %s found\n",name);
  }

  rsReac  = &ssa->rs->reac[rn];
  if(!rsReac->isDoi) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to set Doi type for non-Doi reaction %s\n",name);
  }
  if(type==DOI_NONE) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to set Doi reaction %s with type DOI_NONE\n",name);
  }

  ssaReac = (SSAPairReaction *)rsReac->data;
  ssaReac->doiType=type;
  ierr = PetscMalloc1(1,&doiCtx);CHKERRQ(ierr);
  doiCtx->kap=rsReac->rate;
  doiCtx->r  =rsReac->radius;
  doiCtx->w  =NULL;
  if(rsReac->isReversible) {
    if(rsReac->nReactants==2) {
      sp1  = rsReac->reactants[0];
      sp2  = rsReac->reactants[1];
    } else {
      sp1  = rsReac->products[0];
      sp2  = rsReac->products[1];
    }
    if(rsReac->nReactants==1 && ssa->state->spec[sp1]->potentials.pp[sp2]) { 
      doiCtx->w=ssa->state->spec[sp1]->potentials.pp[sp2]->potFn;
    } else {
      doiCtx->w=PETSC_NULL;
    }
  }
  if(ssaReac->kernCtx) {
    ierr = PetscFree(ssaReac->kernCtx);CHKERRQ(ierr);
  }
  ssaReac->kernCtx=doiCtx;
  switch(type) {
    case(DOI_LUMP):
      ssaReac->kern = DoiLumpIaFn;
      ssaReac->isInStencil=VoxelIsInStencil_DoiLump;
      doiCtx->isInStencil=VoxelIsInStencil_DoiLump;
      break;
    case(SYM_DOI_LUMP):
      ssaReac->kern = SymmetricDoiLumpIaFn;
      ssaReac->isInStencil=VoxelIsInStencil_SymmetricDoiLump;
      doiCtx->isInStencil=VoxelIsInStencil_SymmetricDoiLump;
      break;
    case(DOI_STANDARD):
      ssaReac->kern = DoiIaFn;
      ssaReac->isInStencil=VoxelIsInStencil_Doi;
      doiCtx->isInStencil=VoxelIsInStencil_Doi;
      break;
    case(DOI_LUMP_CONSTANT):
      if(rsReac->nReactants==2) {
        ssaReac->kern=SymmetricDoiLumpRejectIaFn;
        ssaReac->isInStencil=VoxelIsInStencil_SymmetricDoiLump;
        doiCtx->isInStencil=VoxelIsInStencil_SymmetricDoiLump;
      } else {
        ssaReac->kern=DoiLumpRejectIaFn;
        ssaReac->isInStencil=VoxelIsInStencil_DoiLump;
        doiCtx->isInStencil=VoxelIsInStencil_DoiLump;
      }
      break;
    case(DOI_STANDARD_CONSTANT):  
      ssaReac->kern=DoiRejectIaFn;
      ssaReac->isInStencil=VoxelIsInStencil_Doi;
      doiCtx->isInStencil=VoxelIsInStencil_Doi;
      break;
    default:
      SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"Tried to create Doi reaction %s with wrong type %d\n",name,type);
  }
  
  PetscFunctionReturn(0);
}



PetscErrorCode SSASetUp(SpatialSSA ssa) {
  PetscErrorCode     ierr;
  CRDMEGeometry      geom=ssa->geom;
  ReactionSystem     rs=ssa->rs;
  PetscInt           N,NEdges,M=rs->nSpecies,sp2,nn;
  PetscReal          mass;
  CRDMEGeomVoxel     *voxel=&geom->work_voxel[0];
  SSAState           state=ssa->state;
  RSSpecies          *rsSpec;
  RSReaction         *reac;
  SSASpecies         ssaSpec;
  SmoothIaFnCtx      *smthCtx;
  PetscReal          *unifDensity;

  PetscFunctionBegin;
  ssa->setupCalled=PETSC_TRUE;

  if(geom) {
    if(!geom->setupCalled) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"SSA set up called before geometry set up.\n");
    }
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"SSA set up called before geometry object created.\n");
  }
  if(!rs) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"SSA set up called before reaction system added.\n");
  }

  N     =geom->N;
  NEdges=geom->NEdges;

  ierr = PetscMalloc1(1,&ssa->state);CHKERRQ(ierr);
  ierr = PetscMalloc1(N,&ssa->state->voxelRatesum);CHKERRQ(ierr);
  state=ssa->state;
  ierr = PetscMalloc1(N,&unifDensity);CHKERRQ(ierr);
  unifDensity[0]=0.0;
  for(CRDMEGeomGloIdx i=0;i<N;i++) {
    ierr=CRDMEGeometryNodeGetVoxel(geom,i,voxel);CHKERRQ(ierr);
    ierr=CRDMEGeometryVoxelGetArea(geom,*voxel,&mass);CHKERRQ(ierr);
    if(i==0) {
      unifDensity[i]=mass;
    } else {
      unifDensity[i]=unifDensity[i-1]+mass;
    }
  }

  state->nSpecies=M;
  ierr=PetscMalloc1(M,&state->spec);CHKERRQ(ierr);
  for(PetscInt sp=0;sp<M;sp++) {
    ierr = PetscMalloc1(1,&ssaSpec);CHKERRQ(ierr);
    ierr = PetscMalloc1(N,&ssaSpec->voxelPop);CHKERRQ(ierr);
    ssaSpec->pop=0;
  
    ssaSpec->reactions.nOrd1  =0;
    ssaSpec->reactions.nUnbind=0;
    ssaSpec->reactions.nOrd2  =0;
    ssaSpec->reactions.nPairEffects=0;
    ssaSpec->reactions.Ord0  =NULL;

    rsSpec=&rs->spec[sp];
    ssaSpec->rsSpec=rsSpec;

    for(PetscInt rn=0;rn<rs->nReactions;rn++) {
      reac=&rs->reac[rn];
      if(!reac->nReactants && reac->products[0]==sp) {
        ssaSpec->reactions.Ord0=reac;
      } else if(reac->nReactants==1 && reac->reactants[0]==sp) {
        if(!reac->rateFn && !reac->isDoi) {
          ssaSpec->reactions.nOrd1++;
        } else {
          ssaSpec->reactions.nUnbind++;
        }
      } else if(reac->nReactants==2 && reac->reactants[0]==sp) {
        ssaSpec->reactions.nOrd2++;
      } else if(reac->nReactants==2 && reac->reactants[1]==sp) {
        ssaSpec->reactions.nPairEffects++;
      }
    }

    ierr = PetscMalloc1(ssaSpec->reactions.nOrd1,&ssaSpec->reactions.Ord1);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nUnbind,&ssaSpec->reactions.Unbind);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nOrd2,&ssaSpec->reactions.Ord2);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nUnbind,&ssaSpec->reactions.unbindRates);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nOrd2,&ssaSpec->reactions.pairRates);CHKERRQ(ierr);
    if(ssaSpec->reactions.nUnbind) {
      ierr = PetscMalloc1(N*ssaSpec->reactions.nUnbind,&ssaSpec->reactions.unbindRates[0]);CHKERRQ(ierr);
      for(PetscInt rn=1;rn<ssaSpec->reactions.nUnbind;rn++) {
        ssaSpec->reactions.unbindRates[rn]=ssaSpec->reactions.unbindRates[0] + N*rn;
      }
    }

    if(ssaSpec->reactions.nOrd2) {
      ierr = PetscMalloc1(N*ssaSpec->reactions.nOrd2,&ssaSpec->reactions.pairRates[0]);CHKERRQ(ierr);
      for(PetscInt rn=1;rn<ssaSpec->reactions.nOrd2;rn++) {
        ssaSpec->reactions.pairRates[rn]  =ssaSpec->reactions.pairRates[0] + N*rn;
      }
    }
    ierr = PetscMalloc1(ssaSpec->reactions.nUnbind,&ssaSpec->reactions.unbindKernels);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nOrd2,&ssaSpec->reactions.pairKernels);CHKERRQ(ierr);
    ierr = PetscMalloc1(ssaSpec->reactions.nPairEffects,&ssaSpec->reactions.pairEffects);CHKERRQ(ierr);

    ssaSpec->reactions.nOrd1=0;    
    ssaSpec->reactions.nUnbind=0;
    ssaSpec->reactions.nOrd2=0;
    ssaSpec->reactions.nPairEffects=0;

    for(PetscInt rn=0;rn<rs->nReactions;rn++) {

      reac=&rs->reac[rn];
      if(reac->nReactants==1 && reac->reactants[0]==sp) {
        if(!reac->rateFn && !reac->isDoi) {
          ssaSpec->reactions.Ord1[ssaSpec->reactions.nOrd1]=reac;
          ssaSpec->reactions.nOrd1++;
        } else {
          ssaSpec->reactions.Unbind[ssaSpec->reactions.nUnbind]=reac;
          ssaSpec->reactions.nUnbind++;
        }
      } else if(reac->nReactants==2 && reac->reactants[0]==sp) {
        ssaSpec->reactions.Ord2[ssaSpec->reactions.nOrd2]=reac;
        ssaSpec->reactions.nOrd2++;
      } else if(reac->nReactants==2 && reac->reactants[1]==sp) {
        ssaSpec->reactions.pairEffects[ssaSpec->reactions.nPairEffects][0]=reac->reactants[0];
        ssaSpec->reactions.nPairEffects++;
      }
    }

    ierr = PetscMalloc1(M,&ssaSpec->potentials.pp);CHKERRQ(ierr);
    for(PetscInt pn=0;pn<M;pn++) {
      ssaSpec->potentials.pp[pn]=PETSC_NULL;
    }
    if(rsSpec->backgroundPotential) {
      ssaSpec->hasPot=PETSC_TRUE;
      ssaSpec->potentials.bg=rsSpec->backgroundPotential;
    } else {
      ssaSpec->hasPot=PETSC_FALSE;
      ssaSpec->potentials.bg=PETSC_NULL;
    }
    for(PetscInt pn=0;pn<rs->nPairPotentials;pn++) {
      if(rs->pairPotentials[pn].species[0]==sp) {
        ssaSpec->potentials.pp[rs->pairPotentials[pn].species[1]]=&rs->pairPotentials[pn];
        ssaSpec->hasPot=PETSC_TRUE;
      } else if(rs->pairPotentials[pn].species[1]==sp) {
        ssaSpec->potentials.pp[rs->pairPotentials[pn].species[0]]=&rs->pairPotentials[pn];
        ssaSpec->hasPot=PETSC_TRUE;
      }
    }
    if(ssaSpec->hasPot) { ierr=PetscMalloc1(N,&ssaSpec->potentials.potEnergy);CHKERRQ(ierr); }

    ssaSpec->hoppingRates.D = rsSpec->D;
    ierr = PetscMalloc1(N,&ssaSpec->hoppingRates.totDiffusionRate);CHKERRQ(ierr);
    ierr = PetscMalloc1(N,&ssaSpec->hoppingRates.edgeDiffusionRates);CHKERRQ(ierr);
    ierr = PetscMalloc1(NEdges,&ssaSpec->hoppingRates.edgeDiffusionRates[0]);CHKERRQ(ierr);
    for(CRDMEGeomGloIdx i=1;i<N;i++) {
      ierr = CRDMEGeometryNodeGetNumNeighbors(geom,i-1,&nn);CHKERRQ(ierr);
      ssaSpec->hoppingRates.edgeDiffusionRates[i] = ssaSpec->hoppingRates.edgeDiffusionRates[i-1] + nn;
    }
    
    ierr=PetscMalloc1(1,&ssaSpec->initCondition);CHKERRQ(ierr);
    ssaSpec->initCondition->pop=10;
    ssaSpec->initCondition->density=PETSC_NULL;
    ssaSpec->initCondition->cumDensity=PETSC_NULL;
    state->spec[sp]=ssaSpec;
  }
  for(PetscInt sp=0;sp<M;sp++) {
    ssaSpec=state->spec[sp];
    for(PetscInt rn=0;rn<state->spec[sp]->reactions.nOrd2;rn++) {
      reac=state->spec[sp]->reactions.Ord2[rn];
      sp2=reac->reactants[1];
      for(PetscInt i=0;i<state->spec[sp2]->reactions.nPairEffects;i++) {
        if(state->spec[sp2]->reactions.pairEffects[i][0]==sp) {
          state->spec[sp2]->reactions.pairEffects[i][1]=rn;
          break;
        }
      }
    }

    for(PetscInt rn=0;rn<ssaSpec->reactions.nOrd2;rn++) {
      reac=ssaSpec->reactions.Ord2[rn];
      reac->data=&ssaSpec->reactions.pairKernels[rn];
      ssaSpec->reactions.pairKernels[rn].kern   = PETSC_NULL;
      ssaSpec->reactions.pairKernels[rn].kernCtx=PETSC_NULL;
      ssaSpec->reactions.pairKernels[rn].reject=PETSC_NULL;
      ssaSpec->reactions.pairKernels[rn].rejectCtx=PETSC_NULL;
      ssaSpec->reactions.pairKernels[rn].doiType=DOI_NONE;
      ssaSpec->reactions.pairKernels[rn].isInStencil=PETSC_NULL;
      ssaSpec->reactions.pairKernels[rn].stencil=PETSC_NULL;
      if(reac->isDoi) {
        if(reac->isReversible && reac->nProducts < 2) {
          ierr = SSAReactionSetDoiType(ssa,reac->name,SYM_DOI_LUMP);CHKERRQ(ierr);
        } else {
          ierr = SSAReactionSetDoiType(ssa,reac->name,SYM_DOI_LUMP);CHKERRQ(ierr);
        }
      } else {
        ierr = PetscMalloc1(1,&smthCtx);CHKERRQ(ierr);
        smthCtx->ctx=ssa->ctx;
        smthCtx->k=reac->rateFn;
        smthCtx->r=reac->radius;
        smthCtx->kap=reac->rate;
        ssaSpec->reactions.pairKernels[rn].kernCtx=smthCtx;
        ssaSpec->reactions.pairKernels[rn].kern=SmoothIaFn;
        smthCtx=PETSC_NULL;
      }
    }

    for(PetscInt rn=0;rn<ssaSpec->reactions.nUnbind;rn++) {
      PetscReal normalization;
      reac=ssaSpec->reactions.Unbind[rn];
      reac->data=&ssaSpec->reactions.unbindKernels[rn];
      ssaSpec->reactions.unbindKernels[rn].kern   = PETSC_NULL;
      ssaSpec->reactions.unbindKernels[rn].kernCtx=PETSC_NULL;
      ssaSpec->reactions.unbindKernels[rn].reject=PETSC_NULL;
      ssaSpec->reactions.unbindKernels[rn].rejectCtx=PETSC_NULL;
      ssaSpec->reactions.unbindKernels[rn].doiType=DOI_NONE;
      ssaSpec->reactions.unbindKernels[rn].isInStencil=PETSC_NULL;
      ssaSpec->reactions.unbindKernels[rn].stencil=PETSC_NULL;
      if(reac->isDoi) {
          ierr = SSAReactionSetDoiType(ssa,reac->name,DOI_LUMP);CHKERRQ(ierr);
      } else {
        ierr = PetscMalloc1(1,&smthCtx);CHKERRQ(ierr);
        smthCtx->ctx=ssa->ctx;
        smthCtx->k=reac->rateFn;
        smthCtx->r=reac->radius;
        smthCtx->kap=reac->rate;
        ssaSpec->reactions.unbindKernels[rn].kernCtx=smthCtx;
        ssaSpec->reactions.unbindKernels[rn].kern=SmoothIaFn;
        smthCtx=PETSC_NULL;
      }      
      ierr = SSAComputeUnbindingNormalization(ssa,&ssaSpec->reactions.unbindKernels[rn],&normalization);CHKERRQ(ierr);

      reac->rate /= normalization;
      if(reac->isDoi) {
        ((DoiIaFnCtx *)ssaSpec->reactions.unbindKernels[rn].kernCtx)->kap = reac->rate;
      }
      /*
      if(reac->isReversible) {
        PetscReal factor;
        ierr = SSAComputeBindingFactor(ssa,reac->data,&factor);CHKERRQ(ierr);
        reac->reverseRx->rate /= factor;
      }
      */
    }
  }

  for(PetscInt sp=0;sp<M;sp++) {
    ierr = SSASetInitialCondition(ssa,state->spec[sp]->rsSpec->name,uniformDistribution,"uniform");CHKERRQ(ierr);
  }

  if(ssa->ops.ssa_setup) {
    ierr=ssa->ops.ssa_setup(ssa);CHKERRQ(ierr);
  }

  ssa->itContexts.hoppingRatesUpdateCtx.ssa=ssa;
  ssa->itContexts.hoppingRatesUpdateCtx.ratesum=&ssa->state->ratesum;
  ssa->itContexts.hoppingRatesUpdateCtx.voxelRatesum=ssa->state->voxelRatesum;

  ssa->itContexts.nbhdRatesUpdateCtx.ssa=ssa;
  ssa->itContexts.nbhdRatesUpdateCtx.ratesum=&ssa->state->ratesum;
  ssa->itContexts.nbhdRatesUpdateCtx.voxelRatesum=ssa->state->voxelRatesum;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAPopulationInitFromCumDistribution(SpatialSSA ssa, PetscInt pop, PetscReal *cum_mass, PetscInt *voxelPop) {
  PetscInt       N   = ssa->geom->N,loc;
  PetscReal      vol = 0.0,r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

	vol = cum_mass[N-1];
	for(PetscInt j=0;j<pop;j++) {
    r    = vol*erand48(ssa->seed);
    ierr = BinarySearchReal(cum_mass,0,N,r,&loc);CHKERRQ(ierr);
    if(loc < 0) {
      PetscPrintf(PETSC_COMM_SELF,"Error initializing from distribution.\n");
      PetscFunctionReturn(1);
    } else {
      voxelPop[loc]++;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAVoxelComputePairRates(SpatialSSA ssa, SSASpecies s, CRDMEGeomGloIdx i) {
  PetscErrorCode          ierr;
  PetscInt                reactant;
  SSASpecies              s2;
  PetscReal               r;
  SSAComputeNbhdRatesCtx  ctx;
  SpatIaStencil           stencil;

  PetscFunctionBegin;

  ctx.ssa=ssa;
  for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
    s->reactions.pairRates[rn][i] = 0.0;
    stencil     =s->reactions.pairKernels[rn].stencil;
    reactant    =s->reactions.Ord2[rn]->reactants[1];
    r           =s->reactions.Ord2[rn]->radius;
    s2          =ssa->state->spec[reactant];
    ctx.voxelPop=s2->voxelPop;
    ctx.locRate  =&s->reactions.pairRates[rn][i];
    ctx.reaction =&s->reactions.pairKernels[rn];
    if(stencil) {
      ierr = CRDMEGeometryIterateVoxelStencil(ssa->geom,i,stencil,SSAComputePairRatesStencilCallback,0,&ctx);CHKERRQ(ierr);
    } else {
      ierr = CRDMEGeometryIterateVoxelNbhd(ssa->geom,i,r,SSAComputePairRatesCallback,&ctx);CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesComputePairRates(SpatialSSA ssa, SSASpecies s) {
  PetscErrorCode          ierr;

  PetscFunctionBegin;
  
  for(CRDMEGeomGloIdx i=0;i<ssa->geom->N;i++) {
    ierr = SSAVoxelComputePairRates(ssa,s,i);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAReactionPrecomputeRates(SpatialSSA ssa, const char *rx_name) {
  PetscErrorCode              ierr;
  CRDMEGeometry               geom=ssa->geom;
  ReactionSystem              rs=ssa->rs;
  PetscInt                    rn;
  SpatIaStencil               stenc;
  RSReaction                  *rsReac;
  SSAPairReaction             *ssaReac;
  PetscBool                   computeRatesFwd,computeRatesBwd,constant=PETSC_FALSE;
  PetscReal                   rsqr;
  MeshIaFnBool                isInStencil;

  PetscFunctionBegin;
  
  if(!ssa->setupCalled) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute rates for reaction %s before setup called.\n",rx_name);
  }

  if(!ssa->setupCalled) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute rates for reaction %s, but a reaction by that name does not exist.\n", rx_name);
  }

  ierr    = ReactionSystemReactionGetIndex(rs,rx_name,&rn);CHKERRQ(ierr);
  rsReac  = &rs->reac[rn];
  ssaReac = (SSAPairReaction *)rsReac->data;

  ierr = PetscMalloc1(1,&stenc);CHKERRQ(ierr);
  ierr = PetscMalloc1(rsReac->isReversible ? 2 : 1,&stenc->constantVals);CHKERRQ(ierr);
  ierr = PetscMalloc1(rsReac->isReversible ? 2 : 1,&stenc->vals);CHKERRQ(ierr);
  ierr = PetscMalloc1(rsReac->isReversible ? 2 : 1,&stenc->valsWin);CHKERRQ(ierr);

  stenc->constantVals[0]=rsReac->rate;
  if(rsReac->isDoi) {
    if(ssaReac->doiType==DOI_LUMP || ssaReac->doiType==SYM_DOI_LUMP || ssaReac->doiType==DOI_LUMP_CONSTANT) {
      isInStencil=VoxelIsInStencil_SymmetricDoiLump;
    } else {
      isInStencil=ssaReac->isInStencil;
    }
    if(ssaReac->doiType == DOI_LUMP_CONSTANT || ssaReac->doiType==DOI_CONSTANT || ssaReac->doiType == DOI_STANDARD_CONSTANT) {
      constant = PETSC_TRUE;
    }
  } else {
    isInStencil=VoxelIsInStencil_Smooth;
  }

  rsqr = PetscSqr(rsReac->radius);
  ierr = CRDMEGeometryComputeStencilIndices(ssa->geom,stenc,rsReac->radius,isInStencil,&rsqr);CHKERRQ(ierr);
  stenc->constantVals[0] = rsReac->rate;
  stenc->nVals = 1;
  computeRatesFwd = (rsReac->isReversible && ssa->rs->spec[rsReac->products[0]].backgroundPotential) || !constant;
  if(computeRatesFwd) {
    ierr = CRDMEGeometryComputeStencilValues(geom,stenc,ssaReac->kern,ssaReac->kernCtx,PETSC_NULL,PETSC_NULL,0,PETSC_TRUE,PETSC_FALSE);CHKERRQ(ierr);
  } else {
  	stenc->vals[0] = PETSC_NULL;
    ssaReac->stencil=stenc;
  }
  ssaReac->stencil=stenc;

  if(rsReac->isReversible) {
    ssaReac = (SSAPairReaction *)rsReac->reverseRx->data;
    stenc->constantVals[1] = rsReac->reverseRx->rate;
    computeRatesBwd = (rsReac->isReversible && ssa->state->spec[rsReac->reactants[0]]->potentials.pp[rsReac->reactants[1]]) || !constant;
    if(computeRatesBwd) {
      ierr = CRDMEGeometryComputeStencilValues(geom,stenc,ssaReac->kern,ssaReac->kernCtx,PETSC_NULL,PETSC_NULL,1,PETSC_FALSE,PETSC_TRUE);CHKERRQ(ierr);
    } else {
      stenc->vals[0] = PETSC_NULL;
    }
    ssaReac->stencil=stenc;
    stenc->nVals=2;
  }




  PetscFunctionReturn(0);
}

PetscErrorCode SSAPairPotentialPrecompute(SpatialSSA ssa, const char *spec1, const char *spec2) {
  PetscErrorCode              ierr;
  CRDMEGeometry               geom=ssa->geom;
  ReactionSystem              rs=ssa->rs;
  PetscInt                    sp1,sp2;
  pairPotential               *pp;
  SpatIaStencil               stenc;
  PetscReal                   rsqr;

  PetscFunctionBegin;
  
  if(!ssa->setupCalled) {
    SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute pair potential between %s and %s before setup called.\n",spec1,spec2);
  }

  ierr    = ReactionSystemSpeciesGetIndex(rs,spec1,&sp1);CHKERRQ(ierr);

  if(sp1 < 0) {
    SETERRQ3(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute rates pair potential between %s and %s, but a species named %s does not exist.\n",spec1,spec2,spec1);
  }

  ierr    = ReactionSystemSpeciesGetIndex(rs,spec2,&sp2);CHKERRQ(ierr);

  if(sp2 < 0) {
    SETERRQ3(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute rates pair potential between %s and %s, but a species named %s does not exist.\n",spec1,spec2,spec2);
  }

  pp = ssa->state->spec[sp1]->potentials.pp[sp2];

  if(!pp) {
    SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONGSTATE,"Tried to precompute rates pair potential between %s and %s, but the potential is null.\n",spec1,spec2);
  }

  ierr = PetscMalloc1(1,&stenc);CHKERRQ(ierr);
  ierr = PetscMalloc1(1,&stenc->vals);CHKERRQ(ierr);
  ierr = PetscMalloc1(1,&stenc->valsWin);CHKERRQ(ierr);

  rsqr = PetscSqr(pp->radius);
  ierr = CRDMEGeometryComputeStencilIndices(geom,stenc,pp->radius,VoxelIsInStencil_Edge,&rsqr);CHKERRQ(ierr);
  ierr = CRDMEGeometryComputeStencilValues(geom,stenc,PETSC_NULL,PETSC_NULL,pp->potFn,ssa->ctx,0,PETSC_FALSE,PETSC_FALSE);CHKERRQ(ierr);

  pp->data = stenc;
  PetscFunctionReturn(0);
}


PETSC_STATIC_INLINE PetscReal bernoulli(PetscReal z) {
  return (PetscAbsReal(z) < 1e-4 ? ( PetscAbsReal(z) < 1e-8 ? 1.0 : 1.0 / (1.0 + .5 * z) ) : z / expm1(z));
}

PetscErrorCode SSAVoxelComputeEnergy(SpatialSSA ssa, SSASpecies s, CRDMEGeomGloIdx i) {
  PetscErrorCode          ierr;
  CRDMEGeometry           geom=ssa->geom;
  SSAComputeEnergyCtx     ctx;
  RSSpecies               *rsSpec=s->rsSpec;
  PetscInt                M=ssa->rs->nSpecies;
  pairPotential           *pp;

  PetscFunctionBegin; 

  s->potentials.potEnergy[i] = 0.0;
  if(rsSpec->backgroundPotential) {
    CRDMEGeomPoint x;
    ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
    s->potentials.potEnergy[i] += rsSpec->backgroundPotential(x,ssa->rs->ctx);;        
  }
  ctx.ssa=ssa;
  for(PetscInt sp2=0;sp2<M;sp2++) {
    pp=s->potentials.pp[sp2];
    if(pp) {
      ctx.pp=pp;
      ctx.voxelPop =ssa->state->spec[sp2]->voxelPop;
      ctx.locEnergy=&s->potentials.potEnergy[i];
      if(pp->data) {
        SpatIaStencil   stencil=pp->data;
        ierr = CRDMEGeometryIterateVoxelStencil(geom,i,stencil,SSAComputeEnergyStencilCallback,0,&ctx);CHKERRQ(ierr);
      } else {
        ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,ctx.pp->radius,SSAComputeEnergyCallback,&ctx);CHKERRQ(ierr);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode SSAVoxelComputeDiffusionRates(SpatialSSA ssa, SSASpecies s, CRDMEGeomGloIdx i) {
  PetscErrorCode          ierr;
  CRDMEGeometry           geom=ssa->geom;
  CRDMEGeomGloIdx         Aj[geom->maxNodeDegree],j;
  PetscReal               Ax[geom->maxNodeDegree],rsqr,D=s->hoppingRates.D,selfEdgeDiff;
  PetscInt                nn,self=s->rsSpec->idx;

  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetEdgeWeights(geom,i,&nn,Aj,Ax);CHKERRQ(ierr);        
  s->hoppingRates.totDiffusionRate[i] = 0.0;
  for(PetscInt jj=0;jj<nn;jj++) {
    j = Aj[jj];
    if(!s->hasPot) {
      s->hoppingRates.edgeDiffusionRates[i][jj] = D*Ax[jj];
    } else {
      if(!s->potentials.pp[self]) {
          s->hoppingRates.edgeDiffusionRates[i][jj] =D*Ax[jj]*bernoulli(s->potentials.potEnergy[j] - s->potentials.potEnergy[i]);
      } else {
        if(s->potentials.pp[self]->data) {
          SpatIaStencil stencil = s->potentials.pp[self]->data;
          if(stencil->boxPeriodic) { /* neighbors of a node are always included in the stencil and follow the same order */
            selfEdgeDiff = stencil->vals[0][0] - stencil->vals[0][jj+1];
          } else {
            selfEdgeDiff = stencil->vals[0][stencil->rowPtr[i]] -  stencil->vals[0][stencil->rowPtr[i]+jj+1];
          }
        } else {
          ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
          selfEdgeDiff = s->potentials.pp[self]->potFn(0,ssa->ctx) - s->potentials.pp[self]->potFn(rsqr,ssa->ctx);
        }
        s->hoppingRates.edgeDiffusionRates[i][jj] = D*Ax[jj]*bernoulli(s->potentials.potEnergy[j] - s->potentials.potEnergy[i] + selfEdgeDiff);
      } 
    }
    s->hoppingRates.totDiffusionRate[i]      += s->hoppingRates.edgeDiffusionRates[i][jj];
  } 

  PetscFunctionReturn(0);
}


PetscErrorCode SSASpeciesComputeDiffusionRates(SpatialSSA ssa, SSASpecies s) {
  PetscErrorCode          ierr;
  PetscInt                N=ssa->geom->N;

  PetscFunctionBegin;

  if(s->hasPot) {
    for(CRDMEGeomGloIdx i=0;i<N;i++) {
      ierr = SSAVoxelComputeEnergy(ssa,s,i);CHKERRQ(ierr);
    }
  }

  for(CRDMEGeomGloIdx i=0;i<N;i++) {
    if(s->voxelPop[i] || !s->hasPot) {
      ierr = SSAVoxelComputeDiffusionRates(ssa,s,i);CHKERRQ(ierr);
    } else {
      s->hoppingRates.totDiffusionRate[i]=0.0;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesPopulationInitialize(SpatialSSA ssa,PetscInt sp) {
  PetscInt         N     =ssa->geom->N;
  SSAState         state =ssa->state;
  SSASpecies       s=state->spec[sp];
  SSAInitCondition *ic=s->initCondition;
  PetscErrorCode   ierr;

  PetscFunctionBegin;

  s->pop=ic->pop;
  ierr=PetscArrayzero(s->voxelPop,N);CHKERRQ(ierr);
  ierr=SSAPopulationInitFromCumDistribution(ssa,s->pop,ic->cumDensity,s->voxelPop);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode SSAStateInitialize(SpatialSSA ssa) {
  ReactionSystem  rs    =ssa->rs;
  PetscInt        M     =rs->nSpecies;
  PetscInt        N     =ssa->geom->N;
  SSAState        state  =ssa->state;
  SSASpecies      s;
  PetscErrorCode  ierr;

  PetscFunctionBegin;

  state->prevTime=0.0;
  state->curTime=0.0;
  state->ratesum=0.0;
  state->oldRatesum=0.0;
  state->nextCheckpoint=0;
  state->nEvents=0;
  ierr=PetscArrayzero(state->voxelRatesum,ssa->geom->N);CHKERRQ(ierr);
  for(PetscInt sp=0;sp<M;sp++) {
    s=state->spec[sp];
    ierr=SSASpeciesPopulationInitialize(ssa,sp);CHKERRQ(ierr);
  }

  for(PetscInt sp=0;sp<M;sp++) {
    s=state->spec[sp];
    if(s->hasPot) { ierr=PetscArrayzero(s->potentials.potEnergy,N);CHKERRQ(ierr); }
    if(s->reactions.nOrd2) { ierr=PetscArrayzero(s->reactions.pairRates[0],s->reactions.nOrd2*N);CHKERRQ(ierr); }
    if(!ssa->curSimulationNumber && s->reactions.nUnbind) { ierr = SpatialSSASpeciesComputeUnbindRates(ssa,sp,s);CHKERRQ(ierr); }
    ierr = SSASpeciesComputeDiffusionRates(ssa,s);CHKERRQ(ierr);
    ierr = SSASpeciesComputePairRates(ssa,s);CHKERRQ(ierr);
    for(CRDMEGeomGloIdx i=0;i<N;i++) {
      if(s->voxelPop[i]) {
        ssa->state->voxelRatesum[i] += s->voxelPop[i]*s->hoppingRates.totDiffusionRate[i];
        ssa->state->ratesum         += s->voxelPop[i]*s->hoppingRates.totDiffusionRate[i];
    
        if(s->reactions.Ord0) {
          state->voxelRatesum[i] += s->voxelPop[i]*s->reactions.Ord0->rate;
          state->ratesum         += s->voxelPop[i]*s->reactions.Ord0->rate;
        }
        for(PetscInt rn=0;rn<s->reactions.nOrd1;rn++) {
          state->voxelRatesum[i] += s->voxelPop[i]*s->reactions.Ord1[rn]->rate;
          state->ratesum         += s->voxelPop[i]*s->reactions.Ord1[rn]->rate;
        }
        for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
          state->voxelRatesum[i] += s->voxelPop[i]*s->reactions.unbindRates[rn][i];
          state->ratesum         += s->voxelPop[i]*s->reactions.unbindRates[rn][i];
        }
        for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
          ssa->state->voxelRatesum[i] += s->voxelPop[i]*s->reactions.pairRates[rn][i];
          ssa->state->ratesum         += s->voxelPop[i]*s->reactions.pairRates[rn][i];        
        }
      }
    }
  }

  if(ssa->ops.ssa_stateinitialize) {
    ierr = ssa->ops.ssa_stateinitialize(ssa);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SpatialSSASpeciesUpdatePairRates(SpatialSSA ssa, CRDMEGeomGloIdx i, SSASpecies s, PetscBool add) {
  PetscErrorCode          ierr;
  PetscInt                reactant,rn;
  SSASpecies              s2;
  PetscScalar             r;
  SpatIaStencil           stencil;
  SSAUpdateNbhdRatesCtx   *ctx=&ssa->itContexts.nbhdRatesUpdateCtx;

  PetscFunctionBegin;
  
  for(PetscInt en=0;en<s->reactions.nPairEffects;en++) {
    reactant    =s->reactions.pairEffects[en][0];
    rn          =s->reactions.pairEffects[en][1];
    s2          =ssa->state->spec[reactant];
    stencil     =s2->reactions.pairKernels[rn].stencil;
    r=s2->reactions.Ord2[rn]->radius;

    ctx->locRate =s2->reactions.pairRates[rn];
    ctx->reaction=&s2->reactions.pairKernels[rn];
    ctx->add     =add;
    ctx->voxelPop=s2->voxelPop;
    if(stencil) {
      ierr = CRDMEGeometryIterateVoxelStencil(ssa->geom,i,stencil,SSAUpdatePairRatesStencilCallback,0,ctx);CHKERRQ(ierr);
    } else {
      r=s2->reactions.Ord2[rn]->radius;
      ierr = CRDMEGeometryIterateVoxelNbhd(ssa->geom,i,r,SSAUpdatePairRatesCallback,ctx);CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SpatialSSASpeciesUpdateDiffusionRates(SpatialSSA ssa, CRDMEGeomGloIdx i, SSASpecies s, PetscBool add) {
  PetscErrorCode           ierr;
  CRDMEGeometry            geom=ssa->geom;
  SSAUpdateEnergyCtx       energyCtx;
  SSAUpdateHoppingRatesCtx *ratesCtx;
  SSASpecies               sb;
  PetscReal                r;
  PetscInt                 M=ssa->rs->nSpecies;
  pairPotential            *pp;

  PetscFunctionBegin;

  ratesCtx=&ssa->itContexts.hoppingRatesUpdateCtx;
  for(PetscInt spb=0;spb<M;spb++) {
    pp=s->potentials.pp[spb];
    if(pp) {
      sb=ssa->state->spec[spb];
      energyCtx.add=add;
      energyCtx.locEnergy=sb->potentials.potEnergy;
      energyCtx.pp=s->potentials.pp[spb];
      energyCtx.ssa=ssa;
      energyCtx.voxelPop=sb->voxelPop;
      ratesCtx->D=sb->hoppingRates.D;
      ratesCtx->spec=sb;
      if(pp->data) {
        SpatIaStencil   stencil = pp->data;
        ierr = CRDMEGeometryIterateVoxelStencil(geom,i,stencil,SSAUpdateEnergyStencilCallback,0,&energyCtx);CHKERRQ(ierr);
        ratesCtx->D=sb->hoppingRates.D;
        ratesCtx->spec=sb;
        ierr = CRDMEGeometryIterateVoxelStencil(geom,i,stencil,SSAUpdateHoppingRatesStencilCallback,0,ratesCtx);CHKERRQ(ierr);
      } else {
        r=s->potentials.pp[spb]->radius;
        ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,r,SSAUpdateEnergyCallback,&energyCtx);CHKERRQ(ierr);
        ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,r,SSAUpdateHoppingRatesCallback,ratesCtx);CHKERRQ(ierr);
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SpatialSSASpeciesUpdateRates(SpatialSSA ssa, CRDMEGeomGloIdx i, SSASpecies s, PetscBool add) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(s->hasPot) {
    ierr = SpatialSSASpeciesUpdateDiffusionRates(ssa,i,s,add);CHKERRQ(ierr);
  }
  if(s->reactions.nPairEffects) {
    ierr = SpatialSSASpeciesUpdatePairRates(ssa,i,s,add);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


PetscErrorCode SSAGetNextEventTime(SpatialSSA ssa) {

  PetscErrorCode ierr;
  PetscFunctionBegin;

  if(ssa->ops.ssa_getnexteventtime) {
    ierr=ssa->ops.ssa_getnexteventtime(ssa);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SSAGetNextEventTime_ratesum(SpatialSSA ssa) {

  PetscFunctionBegin;

  ssa->state->prevTime = ssa->state->curTime;
  ssa->state->curTime =  ssa->state->prevTime-PetscLogReal(erand48(ssa->seed))/ssa->state->ratesum;

  PetscFunctionReturn(0);
}


PetscErrorCode SSAGetNextSubvolume(SpatialSSA ssa) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  if(ssa->ops.ssa_getnextsubvolume) {
    ierr = ssa->ops.ssa_getnextsubvolume(ssa);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"SSA type %d has no support for operation get next subvolume\n",ssa->ssa_type);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAAddToLocation(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {
  SSAState        state   = ssa->state;
  SSASpecies      s       = state->spec[sp];
  PetscErrorCode  ierr;

  PetscFunctionBegin;

  ssa->state->oldRatesum  = ssa->state->ratesum;

  if(!s->voxelPop[i]) { 
    ierr = SSAVoxelComputeDiffusionRates(ssa,s,i);CHKERRQ(ierr); 
  }

  s->pop++;
  s->voxelPop[i]++;

  state->voxelRatesum[i] += s->hoppingRates.totDiffusionRate[i];
  state->ratesum         += s->hoppingRates.totDiffusionRate[i];

  if(s->reactions.Ord0) {
    state->voxelRatesum[i] += s->reactions.Ord0->rate;
    state->ratesum         += s->reactions.Ord0->rate;
  }
  for(PetscInt rn=0;rn<s->reactions.nOrd1;rn++) {
    state->voxelRatesum[i] += s->reactions.Ord1[rn]->rate;
    state->ratesum         += s->reactions.Ord1[rn]->rate;

  }
  for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
    state->voxelRatesum[i] += s->reactions.unbindRates[rn][i];
    state->ratesum         += s->reactions.unbindRates[rn][i];
  }
  for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
    state->voxelRatesum[i] += s->reactions.pairRates[rn][i];
    state->ratesum         += s->reactions.pairRates[rn][i];
  }

  if(ssa->ops.ssa_addtolocation) {
    ierr = ssa->ops.ssa_addtolocation(ssa,sp,i);CHKERRQ(ierr);
  }

  ierr = SpatialSSASpeciesUpdateRates(ssa,i,s,PETSC_TRUE);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

PetscErrorCode SSARemoveFromLocation(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i) {
  SSAState        state   = ssa->state;
  SSASpecies      s       = state->spec[sp];
  PetscErrorCode  ierr;

  PetscFunctionBegin;

  ssa->state->oldRatesum  = ssa->state->ratesum;

  state->voxelRatesum[i] -= s->hoppingRates.totDiffusionRate[i];
  state->ratesum         -= s->hoppingRates.totDiffusionRate[i];

  s->pop--;
  s->voxelPop[i]--;

  if(s->reactions.Ord0) {
    state->voxelRatesum[i] -= s->reactions.Ord0->rate;
    state->ratesum         -= s->reactions.Ord0->rate;
  }
  for(PetscInt rn=0;rn<s->reactions.nOrd1;rn++) {
    state->voxelRatesum[i] -= s->reactions.Ord1[rn]->rate;
    state->ratesum         -= s->reactions.Ord1[rn]->rate;
  }
  for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
    state->voxelRatesum[i] -= s->reactions.unbindRates[rn][i];
    state->ratesum         -= s->reactions.unbindRates[rn][i];
  }
  for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
    state->voxelRatesum[i] -= s->reactions.pairRates[rn][i];
    state->ratesum         -= s->reactions.pairRates[rn][i];
  }

  if(ssa->ops.ssa_removefromlocation) {
    ierr = ssa->ops.ssa_removefromlocation(ssa,sp,i);CHKERRQ(ierr);
  }

  ierr = SpatialSSASpeciesUpdateRates(ssa,i,s,PETSC_FALSE);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}


PetscErrorCode SSASwitchLocation(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j) {
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  ierr = SSAAddToLocation(ssa,sp,i);CHKERRQ(ierr);
  ierr = SSARemoveFromLocation(ssa,sp,j);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode SSASampleDiffusionEdge(SpatialSSA ssa, PetscInt sp, CRDMEGeomGloIdx i, PetscBool *eventFnd) {
  CRDMEGeometry   geom=ssa->geom;
  PetscInt        nn,jj;
  CRDMEGeomGloIdx Aj[geom->maxNodeDegree];
  SSAState        state=ssa->state;
  SSASpecies      s=state->spec[sp];

  CRDMEGeometryNodeGetNeighbors(geom,i,&nn,Aj);
  state->rand = (state->rand - state->sum) / s->voxelPop[i];
  state->sum = 0.0;
  for(jj=0;jj<nn;jj++) {
    state->sum += s->hoppingRates.edgeDiffusionRates[i][jj];
    if(state->sum > state->rand) {
      *eventFnd=PETSC_TRUE;
      state->lastEvent.addLoc[0] = Aj[jj];
      break;
    }
  }

  return 0;
}

PetscErrorCode SSASamplePairReactantLocation(SpatialSSA ssa, PetscInt sp, PetscInt rn, CRDMEGeomGloIdx i, PetscBool *eventFnd) {
  PetscErrorCode           ierr;
  SSAState                 state=ssa->state;
  SSASpecies               s=state->spec[sp];
  SSASamplePairReactantCtx sampleCtx;
  SpatIaStencil            stencil=s->reactions.pairKernels[rn].stencil;
  PetscReal                r=s->reactions.Ord2[rn]->radius;
  PetscInt                 sp2=s->reactions.Ord2[rn]->reactants[1];

  PetscFunctionBegin;

  state->rand = (state->rand - state->sum) / s->voxelPop[i];
  state->sum = 0.0;
  state->lastEvent.remLoc[1]=-1;

  sampleCtx.reaction =&s->reactions.pairKernels[rn];
  sampleCtx.ssa      = ssa;
  sampleCtx.vpB      = state->spec[sp2]->voxelPop;

  if(stencil) {
    sampleCtx.useInt=(stencil->vals[0] ? PETSC_FALSE : PETSC_TRUE);
    if(sampleCtx.useInt) {
      sampleCtx.num   =(PetscInt) (state->rand / stencil->constantVals[0]);
      sampleCtx.intSum=0;
    }
    ierr = CRDMEGeometryIterateVoxelStencil(ssa->geom,i,stencil,SSASamplePairReactantStencilCallback,0,&sampleCtx);CHKERRQ(ierr);
  } else {
    ierr = CRDMEGeometryIterateVoxelNbhd(ssa->geom,i,r,SSASamplePairReactantCallback,&sampleCtx);CHKERRQ(ierr);
  }

  if(state->lastEvent.remLoc[1] < 0) {
    *eventFnd=PETSC_FALSE;
  } else {
    *eventFnd=PETSC_TRUE;
  }

  if(sampleCtx.useInt) {
    ssa->state->sum  = sampleCtx.intSum;
    ssa->state->rand = sampleCtx.num;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAPlacePairReactionProduct(SpatialSSA ssa) {
  CRDMEGeomGloIdx i=ssa->state->lastEvent.remLoc[0];
  CRDMEGeomGloIdx j=ssa->state->lastEvent.remLoc[1];

  PetscFunctionBegin;

  if(KISS % 2) {
    ssa->state->lastEvent.addLoc[0] = i;
  } else {
    ssa->state->lastEvent.addLoc[0] = j;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode SSASampleUnbindProductLocations(SpatialSSA ssa, PetscInt sp, PetscInt rn, CRDMEGeomGloIdx i, PetscBool *eventFnd) {
  PetscErrorCode             ierr;
  SSAState                   state=ssa->state;
  SSASpecies                 s=ssa->state->spec[sp];
  SSASampleUnbindLocationCtx sampleCtx;
  CRDMEGeometry              geom=ssa->geom;
  PetscReal                  r=s->reactions.Unbind[rn]->radius;
  SpatIaStencil              stencil=s->reactions.unbindKernels[rn].stencil;
  PetscReal                  val;
  PetscBool                  constStenc=(stencil && !stencil->vals[1]);

  PetscFunctionBegin;
  
  if(KISS % 2) {
    state->lastEvent.addSpec[0]=s->reactions.Unbind[rn]->products[0];
    state->lastEvent.addSpec[1]=s->reactions.Unbind[rn]->products[1];
  } else {
    state->lastEvent.addSpec[1]=s->reactions.Unbind[rn]->products[0];
    state->lastEvent.addSpec[0]=s->reactions.Unbind[rn]->products[1];
  }

  state->lastEvent.addLoc[0] = i;
  state->lastEvent.addLoc[1] =-1;

  if(constStenc) {
    val         = stencil->constantVals[1];
    state->rand = (state->rand - state->sum) / (s->voxelPop[i]*val);
  } else {
    state->rand = (state->rand - state->sum) / s->voxelPop[i];
  } 
  
  if(stencil && !constStenc) { 
    PetscInt jj;
    if(stencil->boxPeriodic) {
      ierr = CRDMEGeomBoxSearchPeriodicStencil(ssa->geom,stencil,i,state->rand,&state->lastEvent.addLoc[1]);CHKERRQ(ierr);
    } else {
      ierr = BinarySearchReal(stencil->vals[1],stencil->rowPtr[i],stencil->rowPtr[i+1],state->rand,&jj);CHKERRQ(ierr);
      if(jj < 0) { 
        state->lastEvent.addLoc[1] = -1;
      } else {
        state->lastEvent.addLoc[1] = stencil->indices[jj];
      }
    }
  } else if(stencil) {
    state->sum = 0.0;
    sampleCtx.ssa     =ssa;
    ierr = CRDMEGeometryIterateVoxelStencil(geom,i,stencil,SSASampleUnbindLocationStencilCallback,1,&sampleCtx);CHKERRQ(ierr);
  } else {
    sampleCtx.reaction=&s->reactions.unbindKernels[rn];
    sampleCtx.ssa     =ssa;
    ierr = CRDMEGeometryIterateVoxelNbhd(geom,i,r,SSASampleUnbindLocationCallback,&sampleCtx);CHKERRQ(ierr);
  }

  if(state->lastEvent.addLoc[1] < 0) {
    *eventFnd=PETSC_FALSE;
  } else {
    *eventFnd=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PETSC_STATIC_INLINE void setLastEventDiffusion(PetscInt sp, CRDMEGeomGloIdx i, ssaEvent_t *lastEvent) {
  lastEvent->type=DIFFUSION;
  lastEvent->nAdd=1;
  lastEvent->nRem=1;
  lastEvent->remLoc[0]=i;
  lastEvent->addSpec[0]=sp;
  lastEvent->remSpec[0]=sp;
}

PETSC_STATIC_INLINE void setLastEventReactionOrd0(PetscInt sp, CRDMEGeomGloIdx i, ssaEvent_t *lastEvent) {
  lastEvent->type=REACTION_ORD_0;
  lastEvent->nAdd=1;
  lastEvent->addLoc[0]=i;
}

PETSC_STATIC_INLINE void setLastEventReactionOrd1(RSReaction *reac, PetscInt rn, PetscInt sp, CRDMEGeomGloIdx i, ssaEvent_t *lastEvent) {
  lastEvent->type=REACTION_ORD_1;
  lastEvent->idx=rn;
  lastEvent->nAdd=reac->nProducts;
  for(PetscInt k=0;k<reac->nProducts;k++) {
    lastEvent->addLoc[k]=i;
  }
  lastEvent->nRem=1;
  lastEvent->remLoc[0]=i;
}

PETSC_STATIC_INLINE void setLastEventReactionOrd2(RSReaction *reac, PetscInt rn, PetscInt sp, CRDMEGeomGloIdx i, ssaEvent_t *lastEvent) {
  lastEvent->type=REACTION_ORD_2;
  lastEvent->idx =rn;
  lastEvent->remSpec[1]=reac->reactants[1];
  if(reac->nProducts<2) {
    lastEvent->nRem=2;
  } else {
    lastEvent->nRem=1;
  }
  lastEvent->nAdd=1;
  lastEvent->addSpec[0]=reac->products[0];
  lastEvent->remLoc[0]=i;
  lastEvent->remSpec[0]=sp;
}

PETSC_STATIC_INLINE void setLastEventReactionUnbind(RSReaction *reac, PetscInt rn, PetscInt sp, CRDMEGeomGloIdx i, ssaEvent_t *lastEvent) {
  lastEvent->type=REACTION_UNBIND;
  lastEvent->idx =rn;
  lastEvent->nRem=1;
  lastEvent->remLoc[0]=i;
  lastEvent->remSpec[0]=sp;
  lastEvent->nAdd=2;  
}

PetscErrorCode SSASolve(SpatialSSA ssa) {

  SSAState          state = ssa->state;
  SSASpecies        s;
  PetscInt          M=ssa->rs->nSpecies,c=0;
  CRDMEGeomGloIdx   nsv;
  RSReaction        *reac;
  PetscBool         halt=PETSC_FALSE;
  PetscBool         eventFnd;
  PetscErrorCode    ierr;
  
  PetscFunctionBegin;

  ssa->curSimulationNumber=0;
  while(ssa->curSimulationNumber < ssa->nTrials) {
    ierr = SSAStateInitialize(ssa);CHKERRQ(ierr);
    ierr = ssa->monitor(ssa,&halt,PETSC_FALSE,ssa->monitor_ctx);CHKERRQ(ierr);
    halt = PETSC_FALSE;
    while(!halt) {
      ierr = SSAGetNextEventTime(ssa);CHKERRQ(ierr);
      if(state->curTime >= ssa->Tf || state->ratesum <= 1e-12) {
        halt = PETSC_TRUE;
        state->curTime = ssa->Tf;
        if(ssa->checkpoints) {
          while(ssa->Tf >= ssa->checkpoints[state->nextCheckpoint] && state->nextCheckpoint < ssa->nCheckpoints) {
	          ierr = ssa->monitor(ssa,&halt,PETSC_FALSE,ssa->monitor_ctx);CHKERRQ(ierr);
	          state->nextCheckpoint++;
          }
        } else {
          ierr = ssa->monitor(ssa,&halt,PETSC_FALSE,ssa->monitor_ctx);CHKERRQ(ierr);
        }
        break;
      }

      if(ssa->checkpoints) {
        while(state->curTime >= ssa->checkpoints[state->nextCheckpoint]) {
        	ierr = ssa->monitor(ssa,&halt,PETSC_TRUE,ssa->monitor_ctx);CHKERRQ(ierr);
        	state->nextCheckpoint++;
        }
      }

      ierr = SSAGetNextSubvolume(ssa);CHKERRQ(ierr);
      if(state->rand < 1e-13) {
        return 1;
      }
      state->sum=0.0;
      eventFnd=PETSC_FALSE;
      state->lastEvent.reject   =PETSC_FALSE;
      state->lastEvent.revReject=PETSC_FALSE;
      nsv=state->lastEvent.loc;
      for(PetscInt sp=0;sp<M && !eventFnd;sp++) {
        s=state->spec[sp];
        state->sum+=s->voxelPop[nsv]*s->hoppingRates.totDiffusionRate[nsv];
        if(state->sum > state->rand) {
          setLastEventDiffusion(sp,nsv,&state->lastEvent);
          state->sum-=s->voxelPop[nsv]*s->hoppingRates.totDiffusionRate[nsv];
          ierr = SSASampleDiffusionEdge(ssa,sp,nsv,&eventFnd);CHKERRQ(ierr);
          if(!eventFnd) {
            ierr = PetscPrintf(PETSC_COMM_SELF,"Warning: Diffusion event not found. Simulation %d; event %d; time %1.15f; prev time %1.15f; voxel %d\n",ssa->curSimulationNumber,ssa->state->nEvents,ssa->state->curTime,ssa->state->prevTime,nsv);
            ierr = PetscPrintf(PETSC_COMM_SELF,"  Random number: %1.15f, rate sum: %1.15f, sum: %1.15f\n",state->rand,state->ratesum,state->sum);CHKERRQ(ierr);
            eventFnd=PETSC_TRUE;
          }
        }

        if(s->reactions.Ord0 && !eventFnd) {
          state->sum += s->voxelPop[nsv]*s->reactions.Ord0->rate;
          if(state->sum > state->rand) {
            setLastEventReactionOrd0(sp,nsv,&state->lastEvent);
            reac=s->reactions.Ord0;
            eventFnd=PETSC_TRUE;
            if(reac->isReversible) {
              ierr = ReversibleReactionRejectionFn(ssa);CHKERRQ(ierr);
            } 
          }
        }

        for(PetscInt rn=0;rn<s->reactions.nOrd1 && !eventFnd;rn++) {
          state->sum +=  s->voxelPop[nsv]*s->reactions.Ord1[rn]->rate;
          if(state->sum > state->rand) {
            eventFnd=PETSC_TRUE;
            reac=s->reactions.Ord1[rn];
            setLastEventReactionOrd1(reac,rn,sp,nsv,&state->lastEvent);
            if(reac->isReversible) {
              ierr = ReversibleReactionRejectionFn(ssa);CHKERRQ(ierr);
            }
          }
        }


        for(PetscInt rn=0;rn<s->reactions.nOrd2 && !eventFnd;rn++) {
          state->sum += s->voxelPop[nsv]*s->reactions.pairRates[rn][nsv];
          if(state->sum > state->rand) {
            state->sum -= s->voxelPop[nsv]*s->reactions.pairRates[rn][nsv];
            reac=s->reactions.Ord2[rn];
            setLastEventReactionOrd2(reac,rn,sp,nsv,&state->lastEvent);
            ierr = SSASamplePairReactantLocation(ssa,sp,rn,nsv,&eventFnd);CHKERRQ(ierr);
            if(eventFnd) {
              if(reac->nProducts<2) {
                state->lastEvent.remSpec[1]=reac->reactants[1];
                ierr = SSAPlacePairReactionProduct(ssa);CHKERRQ(ierr);            
              } else {
                state->lastEvent.addLoc[0]=state->lastEvent.remLoc[0];
              }
              if(s->reactions.pairKernels[rn].reject) {
                ierr = s->reactions.pairKernels[rn].reject(ssa,s->reactions.pairKernels[rn].rejectCtx);CHKERRQ(ierr);
              }
              if(reac->isReversible && !state->lastEvent.reject) {
                ierr = ReversibleReactionRejectionFn(ssa);CHKERRQ(ierr);
              }
            } else if(!eventFnd) {
              ierr = PetscPrintf(PETSC_COMM_SELF,"Warning: Pair reaction event not found. Simulation %d; event %d; time %1.15f; prev time %1.15f; voxel %d\n",ssa->curSimulationNumber,ssa->state->nEvents,ssa->state->curTime,ssa->state->prevTime,nsv);
              ierr = PetscPrintf(PETSC_COMM_SELF,"  Random number: %1.15f, rate sum: %1.15f, sum: %1.15f\n",state->rand,state->ratesum,state->sum);CHKERRQ(ierr);
              eventFnd=PETSC_TRUE;
            }
          }
        }
        
        for(PetscInt rn=0;rn<s->reactions.nUnbind && !eventFnd;rn++) {
          state->sum +=  s->voxelPop[nsv]*s->reactions.unbindRates[rn][nsv];
          if(state->sum > state->rand) {
            state->sum -= s->voxelPop[nsv]*s->reactions.unbindRates[rn][nsv];
            reac=s->reactions.Unbind[rn];
            setLastEventReactionUnbind(reac,rn,sp,nsv,&state->lastEvent); 
            ierr = SSASampleUnbindProductLocations(ssa,sp,rn,nsv,&eventFnd);CHKERRQ(ierr);
            if(!eventFnd) {
              ierr = PetscPrintf(PETSC_COMM_SELF,"Warning: Unbind reaction event not found. Simulation %d; event %d; time %1.15f; prev time %1.15f; voxel %d\n",ssa->curSimulationNumber,ssa->state->nEvents,ssa->state->curTime,ssa->state->prevTime,nsv);
              ierr = PetscPrintf(PETSC_COMM_SELF,"  Random number: %1.15f, rate sum: %1.15f, sum: %1.15f; total unbind rate: %1.14f\n",state->rand,state->ratesum,state->sum,s->reactions.unbindRates[rn][nsv]);CHKERRQ(ierr);
              eventFnd=PETSC_TRUE;
            }
            if(s->reactions.unbindKernels[rn].reject) {
              ierr = s->reactions.unbindKernels[rn].reject(ssa,s->reactions.unbindKernels[rn].rejectCtx);CHKERRQ(ierr);
            }
            if(reac->isReversible && !state->lastEvent.reject) {
              ierr = ReversibleReactionRejectionFn(ssa);CHKERRQ(ierr);
            }
          }
        }
      }
      
      if(eventFnd && !state->lastEvent.revReject && !state->lastEvent.reject) {
        for(PetscInt k=0;k<state->lastEvent.nAdd;k++) {
          ierr = SSAAddToLocation(ssa,state->lastEvent.addSpec[k],state->lastEvent.addLoc[k]);CHKERRQ(ierr);
        }
        for(PetscInt k=0;k<state->lastEvent.nRem;k++) {
          ierr = SSARemoveFromLocation(ssa,state->lastEvent.remSpec[k],state->lastEvent.remLoc[k]);CHKERRQ(ierr);
        }
      }
      if(eventFnd) {
        state->nEvents++;
      } else {
        ierr = PetscPrintf(PETSC_COMM_SELF,"Warning: no event found. Simulation %d; event %d; time %1.15f; prev time %1.15f; voxel %d\n",ssa->curSimulationNumber,ssa->state->nEvents,ssa->state->curTime,ssa->state->prevTime,nsv);
        ierr = PetscPrintf(PETSC_COMM_SELF,"  Random number: %1.15f, rate sum: %1.15f, sum: %1.15f\n",state->rand,state->ratesum,state->sum);CHKERRQ(ierr);
      }
      ierr = ssa->monitor(ssa,&halt,PETSC_FALSE,ssa->monitor_ctx);CHKERRQ(ierr);
    }
    ssa->curSimulationNumber++;
    if(ssa->curSimulationNumber / (1.0*ssa->nTrials) > c*.05) {
      PetscPrintf(PETSC_COMM_WORLD,"[%d]",c);
      c++;
    } 
  }

  PetscPrintf(PETSC_COMM_WORLD,"\n");

  PetscFunctionReturn(0); 
}

PetscErrorCode SSASpeciesPrintState(SpatialSSA ssa, PetscInt sp);

PetscErrorCode SSAPrintState(SpatialSSA ssa) {
  PetscErrorCode ierr;
  PetscReal      sum=0.0;

  PetscFunctionBegin;

  ierr = PetscPrintf(PETSC_COMM_SELF,"Current simulation number: %d\n",ssa->curSimulationNumber);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Current time: %1.7f\n",ssa->state->curTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Previous time step: %1.7f\n",ssa->state->curTime - ssa->state->prevTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Rate sum: %1.7f\n",ssa->state->ratesum);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing voxel ratesums:\n");CHKERRQ(ierr);
  for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
    for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %1.7f; ",10*c+l,ssa->state->voxelRatesum[10*c+l]);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
  }
  for(PetscInt c=0;c<ssa->geom->N;c++) {
    sum+=ssa->state->voxelRatesum[c];
  }
  if(PetscAbs(sum-ssa->state->ratesum) < PETSC_SMALL) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"Ratesum and voxel ratesums are consistent (difference = %1.15f)\n",PetscAbs(sum-ssa->state->ratesum));CHKERRQ(ierr);
  }
  switch(ssa->state->lastEvent.type) {
    case DIFFUSION:
      ierr = PetscPrintf(PETSC_COMM_SELF,"    Last event diffusion species %d from node %d to node %d\n",
            ssa->state->lastEvent.addSpec[0],ssa->state->lastEvent.remLoc[0],ssa->state->lastEvent.addLoc[0]);CHKERRQ(ierr);
      break;
    case REACTION_ORD_0:
      ierr = PetscPrintf(PETSC_COMM_SELF,"    Last event creation reaction species %d, node %d\n",
                                            ssa->state->lastEvent.addSpec[0],ssa->state->lastEvent.addLoc[0]);CHKERRQ(ierr);
      break;
    case REACTION_ORD_1:
      ierr = PetscPrintf(PETSC_COMM_SELF,"    Last event order 1 reaction species %d, node %d\n",
                            ssa->state->lastEvent.remSpec[0],ssa->state->lastEvent.remLoc[0]);CHKERRQ(ierr);
      break;
    case REACTION_ORD_2:
      ierr = PetscPrintf(PETSC_COMM_SELF,"    Last event order 2 reaction species %d and %d, nodes %d and %d\n",
                      ssa->state->lastEvent.remSpec[0],ssa->state->lastEvent.remSpec[1],
                      ssa->state->lastEvent.remLoc[0],ssa->state->lastEvent.remLoc[1]);CHKERRQ(ierr);
      break;
    case REACTION_UNBIND:
      ierr = PetscPrintf(PETSC_COMM_SELF,"    Last event unbind reaction species %d, node %d to species %d and %d, nodes %d and %d\n",
              ssa->state->lastEvent.remSpec[0],ssa->state->lastEvent.remLoc[0],
              ssa->state->lastEvent.addSpec[0],ssa->state->lastEvent.addSpec[1],
              ssa->state->lastEvent.addLoc[0],ssa->state->lastEvent.addLoc[1]);CHKERRQ(ierr);
      break;
  }
  if(ssa->state->lastEvent.reject) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"      Last reaction rejected.\n");CHKERRQ(ierr);
  } else if(ssa->state->lastEvent.revReject) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"      Last reaction rejected reversibly.\n");CHKERRQ(ierr);
  }

  for(PetscInt sp=0;sp<ssa->state->nSpecies;sp++) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
    ierr = SSASpeciesPrintState(ssa,sp);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASpeciesPrintState(SpatialSSA ssa, PetscInt sp) {
  SSAState       state=ssa->state;
  SSASpecies     s=state->spec[sp];
  PetscInt       nn;
  PetscReal      sum;
  PetscBool      consistent;

  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = PetscPrintf(PETSC_COMM_SELF,"Printing state for species %s index %d\n",s->rsSpec->name,sp);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Current population: %d\n",s->pop);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing current voxel populations:\n");CHKERRQ(ierr);
  for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
    for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %d; ",10*c+l,s->voxelPop[10*c+l]);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
  }
  ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing current total diffusion rates:\n");CHKERRQ(ierr);
  for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
    for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %1.6f; ",10*c+l,s->hoppingRates.totDiffusionRate[10*c+l]);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
  }
  consistent=PETSC_TRUE;
  for(PetscInt i=0; i<ssa->geom->N;i++) {
    sum=0.0;
    ierr = CRDMEGeometryNodeGetNumNeighbors(ssa->geom,i,&nn);CHKERRQ(ierr);
    for(PetscInt jj=0;jj<nn;jj++) {
      sum+=s->hoppingRates.edgeDiffusionRates[i][jj];
    }
    if(PetscAbs(sum-s->hoppingRates.totDiffusionRate[i]) > PETSC_SMALL) {
      consistent=PETSC_FALSE;
      ierr = PetscPrintf(PETSC_COMM_SELF,"Total diffusion rate %1.15f and edge rates not consistent, voxel %d\n",s->hoppingRates.totDiffusionRate[i],i);CHKERRQ(ierr);
      for(PetscInt jj=0;jj<nn;jj++) {
        ierr = PetscPrintf(PETSC_COMM_SELF,"  %1.15f\n",s->hoppingRates.edgeDiffusionRates[i][jj]);CHKERRQ(ierr);
      }
    }
  }
  if(consistent) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"  All total diffusion and edge rates are consistent\n");CHKERRQ(ierr);
  }

  for(PetscInt rn=0;rn<s->reactions.nOrd2;rn++) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing pair reaction rates, reaction %s index %d:\n",s->reactions.Ord2[rn]->name,rn);CHKERRQ(ierr);
    for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
      for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
        ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %1.6f; ",10*c+l,s->reactions.pairRates[rn][10*c+l]);CHKERRQ(ierr);
      }
      ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
    }
  }

    for(PetscInt rn=0;rn<s->reactions.nUnbind;rn++) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing unbind reaction rates, reaction %s index %d:\n",s->reactions.Unbind[rn]->name,rn);CHKERRQ(ierr);
    for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
      for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
        ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %1.6f; ",10*c+l,s->reactions.unbindRates[rn][10*c+l]);CHKERRQ(ierr);
      }
      ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
    }
  }


  

  if(s->hasPot) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"  Printing potential energy:\n");CHKERRQ(ierr);
    for(PetscInt c=0;c<20 && 10*c < ssa->geom->N;c++) {
      for(PetscInt l=0;l<10 && 10*c+l<ssa->geom->N;l++) {
        ierr = PetscPrintf(PETSC_COMM_SELF,"%d: %1.6f; ",10*c+l,s->potentials.potEnergy[10*c+l]);CHKERRQ(ierr);
      }
      ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
    }
  }
  ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode SSACheckRatesum(SpatialSSA ssa) {

  PetscFunctionBegin;

  PetscFunctionReturn(0);
}

PetscErrorCode SSAView(SpatialSSA ssa) {
  PetscErrorCode ierr;
  ReactionSystem rs=ssa->rs;

  PetscFunctionBegin;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Spatial SSA simulated using %s.\n",ssa->ssa_type==SSA_TYPE_NSM ? "next subvolume method" : "hierarchical method");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Final time: %1.14f.\n",ssa->Tf);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"  Number of trials (per cpu): %d.\n",ssa->nTrials);CHKERRQ(ierr);

  ierr = CRDMEGeometryView_ASCII(ssa->geom);CHKERRQ(ierr);

  if(rs->nSpecies) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Reaction system with %d species and %d reactions\n",rs->nSpecies,rs->nReactions);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  I. Species\n");CHKERRQ(ierr);
  }
  for(PetscInt i=0; i < rs->nSpecies; i++) {
    ierr = ReactionSystemSpeciesView(rs,i);CHKERRQ(ierr);
  }
  if(rs->nReactions) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  II. Reactions\n");CHKERRQ(ierr);
  }
  for(PetscInt i=0; i < rs->nReactions; i++) {
    if(rs->reac[i].isReversible) {
      if(i < rs->reac[i].reverseRx->idx) {
        if(rs->reac[i].nProducts == 2 && rs->reac[i].nReactants==1) {
          RSReaction *reac=&rs->reac[i];
          PetscReal  norm;
          ierr = SSAComputeUnbindingNormalization(ssa,reac->data,&norm);CHKERRQ(ierr);
          reac->rate*=norm;     
          ierr = ReactionSystemReactionView(rs,i);CHKERRQ(ierr);
          reac->rate/=norm;    
        } else if(rs->reac[i].nProducts==1 && rs->reac[i].nReactants==2) {
          RSReaction *reac=rs->reac[i].reverseRx;
          PetscReal  norm;
          ierr = SSAComputeUnbindingNormalization(ssa,reac->data,&norm);CHKERRQ(ierr);
          reac->rate*=norm;     
          ierr = ReactionSystemReactionView(rs,i);CHKERRQ(ierr);
          reac->rate/=norm;    
        } else {
          ierr = ReactionSystemReactionView(rs,i);CHKERRQ(ierr);
        }
      }
    } else {
      ierr = ReactionSystemReactionView(rs,i);CHKERRQ(ierr);
    }

    if(rs->reac[i].data && (!rs->reac[i].isReversible || i < rs->reac[i].reverseRx->idx)) {
      SSAPairReaction *reac=rs->reac[i].data;
      SpatIaStencil   stencil=reac->stencil;
      PetscPrintf(PETSC_COMM_WORLD,"      Ratio of reaction radius to mesh size is %1.14f (should be greater than 1)\n",rs->reac[i].radius / ssa->geom->hmax);CHKERRQ(ierr);
      if(rs->reac[i].isDoi) {
        ierr = PetscPrintf(PETSC_COMM_WORLD,"      Using doi type ");CHKERRQ(ierr);
        switch(reac->doiType) {
          case(DOI_NONE):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"none.\n");CHKERRQ(ierr);
            break;
          case(DOI_LUMP):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"doi lump.\n");CHKERRQ(ierr);
            break;
          case(SYM_DOI_LUMP):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"symmetric doi lump.\n");CHKERRQ(ierr);
            break;
          case(DOI_STANDARD):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"standard doi.\n");CHKERRQ(ierr);
            break;
          case(DOI_LUMP_CONSTANT):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"doi lump with constant rates (rejection).\n");CHKERRQ(ierr);
            break;
          case(DOI_STANDARD_CONSTANT):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"standard doi with constant rates (rejection).\n");CHKERRQ(ierr);
            break;
          case(DOI_CONSTANT):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"pointwise doi.\n");CHKERRQ(ierr);
            break;
          case(DOI_CUSTOM):
            ierr = PetscPrintf(PETSC_COMM_WORLD,"custom evaluation.\n");CHKERRQ(ierr);
            break;
        }
      }
      if(stencil) {
        ierr = PetscPrintf(PETSC_COMM_WORLD,"      Rates precomputed in reaction stencil with length %d.\n",stencil->stencilLen);CHKERRQ(ierr);
      }
    }
  }
  
  if(rs->nPairPotentials) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  III. Pair potentials\n");CHKERRQ(ierr);
    for(PetscInt i = 0; i < rs->nPairPotentials; i++) { 
      ierr = ReactionSystemPairPotentialView(rs,i);CHKERRQ(ierr);
      if(rs->pairPotentials[i].data) {
        SpatIaStencil stencil=rs->pairPotentials[i].data;
              PetscPrintf(PETSC_COMM_WORLD,"      Ratio of interaction radius to mesh size is %1.14f (should be greater than 1)\n",rs->pairPotentials[i].radius / ssa->geom->hmax);CHKERRQ(ierr);
        ierr = PetscPrintf(PETSC_COMM_WORLD,"      Potential values precomputed in stencil with length %d.\n",stencil->stencilLen);CHKERRQ(ierr);
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAWritePopulations_HDF5(SpatialSSA ssa) {
  return(0);
}

PETSC_STATIC_INLINE PetscReal RadialBoltzmannGaussQuad(PetscInt dim,PetscReal r,SpatialIaFn f,void *ctx) { 
  PetscInt  npts=64;
  PetscReal points[64] = {0.0243502926634244325089558,0.0729931217877990394495429,0.1214628192961205544703765,0.1696444204239928180373136,0.2174236437400070841496487,0.2646871622087674163739642,0.3113228719902109561575127,0.3572201583376681159504426,0.4022701579639916036957668,0.4463660172534640879849477,0.4894031457070529574785263,0.5312794640198945456580139,0.5718956462026340342838781,0.6111553551723932502488530,0.6489654712546573398577612,0.6852363130542332425635584,0.7198818501716108268489402,0.7528199072605318966118638,0.7839723589433414076102205,0.8132653151227975597419233,0.8406292962525803627516915,0.8659993981540928197607834,0.8893154459951141058534040,0.9105221370785028057563807,0.9295691721319395758214902,0.9464113748584028160624815,0.9610087996520537189186141,0.9733268277899109637418535,0.9833362538846259569312993,0.9910133714767443207393824,0.9963401167719552793469245,0.9993050417357721394569056};
  PetscReal weights[64] = {0.0486909570091397203833654,0.0485754674415034269347991,0.0483447622348029571697695,0.0479993885964583077281262,0.0475401657148303086622822,0.0469681828162100173253263,0.0462847965813144172959532,0.0454916279274181444797710,0.0445905581637565630601347,0.0435837245293234533768279,0.0424735151236535890073398,0.0412625632426235286101563,0.0399537411327203413866569,0.0385501531786156291289625,0.0370551285402400460404151,0.0354722132568823838106931,0.0338051618371416093915655,0.0320579283548515535854675,0.0302346570724024788679741,0.0283396726142594832275113,0.0263774697150546586716918,0.0243527025687108733381776,0.0222701738083832541592983,0.0201348231535302093723403,0.0179517157756973430850453,0.0157260304760247193219660,0.0134630478967186425980608,0.0111681394601311288185905,0.0088467598263639477230309,0.0065044579689783628561174,0.0041470332605624676352875,0.0017832807216964329472961};
  for(PetscInt i=npts/2;i<npts;i++) {
    points[i] = -points[i-npts/2];
    weights[i] = weights[i-npts/2];
  } 

  PetscReal sum=0.0,rsqr;
  for(PetscInt i=0;i<npts;i++) points[i] = .5*(points[i]+1.0)*r;

  switch(dim) {
    case 1:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*PetscExpReal(-f(rsqr,ctx));
    } 
    break;
    case 2:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*2*PETSC_PI*points[i]*PetscExpReal(-f(rsqr,ctx));
    } 
    break;
    case 3:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*4*PETSC_PI*rsqr*PetscExpReal(-f(rsqr,ctx));
    }
    break; 
  }
  return sum;
}

PETSC_STATIC_INLINE PetscReal RadialGaussQuad(PetscInt dim,PetscReal r,SpatialIaFn f,void *ctx) { 
  PetscInt  npts=64;
  PetscReal points[64] = {0.0243502926634244325089558,0.0729931217877990394495429,0.1214628192961205544703765,0.1696444204239928180373136,0.2174236437400070841496487,0.2646871622087674163739642,0.3113228719902109561575127,0.3572201583376681159504426,0.4022701579639916036957668,0.4463660172534640879849477,0.4894031457070529574785263,0.5312794640198945456580139,0.5718956462026340342838781,0.6111553551723932502488530,0.6489654712546573398577612,0.6852363130542332425635584,0.7198818501716108268489402,0.7528199072605318966118638,0.7839723589433414076102205,0.8132653151227975597419233,0.8406292962525803627516915,0.8659993981540928197607834,0.8893154459951141058534040,0.9105221370785028057563807,0.9295691721319395758214902,0.9464113748584028160624815,0.9610087996520537189186141,0.9733268277899109637418535,0.9833362538846259569312993,0.9910133714767443207393824,0.9963401167719552793469245,0.9993050417357721394569056};
  PetscReal weights[64] = {0.0486909570091397203833654,0.0485754674415034269347991,0.0483447622348029571697695,0.0479993885964583077281262,0.0475401657148303086622822,0.0469681828162100173253263,0.0462847965813144172959532,0.0454916279274181444797710,0.0445905581637565630601347,0.0435837245293234533768279,0.0424735151236535890073398,0.0412625632426235286101563,0.0399537411327203413866569,0.0385501531786156291289625,0.0370551285402400460404151,0.0354722132568823838106931,0.0338051618371416093915655,0.0320579283548515535854675,0.0302346570724024788679741,0.0283396726142594832275113,0.0263774697150546586716918,0.0243527025687108733381776,0.0222701738083832541592983,0.0201348231535302093723403,0.0179517157756973430850453,0.0157260304760247193219660,0.0134630478967186425980608,0.0111681394601311288185905,0.0088467598263639477230309,0.0065044579689783628561174,0.0041470332605624676352875,0.0017832807216964329472961};
  for(PetscInt i=npts/2;i<npts;i++) {
    points[i] = -points[i-npts/2];
    weights[i] = weights[i-npts/2];
  } 

  PetscReal sum=0.0,rsqr;
  for(PetscInt i=0;i<npts;i++) points[i] = .5*(points[i]+1.0)*r;
  switch(dim) {
    case 1:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*f(rsqr,ctx);
    } 
    break;
    case 2:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*2*PETSC_PI*points[i]*f(rsqr,ctx);
    } 
    break;
    case 3:
    for(PetscInt i =0;i<npts;i++) {
      rsqr = PetscSqr(points[i]);
      sum += weights[i]*4*PETSC_PI*rsqr*f(rsqr,ctx);
    }
    break; 
  }
  return sum;
}


PETSC_STATIC_INLINE PetscReal SphereVolume(PetscReal radius, PetscInt dim) {
  switch(dim) {
    case 1:
      return 2*radius;
    case 2:
      return PetscSqr(radius)*PETSC_PI;
    case 3:
      return (4./3.)*PetscSqr(radius)*radius*PETSC_PI;
  }
  return 0.0;
}

PetscErrorCode SSAComputeUnbindingNormalization(SpatialSSA ssa,SSAPairReaction *reac,PetscReal *norm) {

  PetscFunctionBegin;

  if(reac->doiType != DOI_NONE) {
    DoiIaFnCtx *doiCtx = reac->kernCtx;
    if(doiCtx->w) {
      *norm = RadialBoltzmannGaussQuad(ssa->geom->dim,doiCtx->r,doiCtx->w,ssa->ctx);
    } else {
      *norm = SphereVolume(doiCtx->r,ssa->geom->dim);
    }
  } else {
    SmoothIaFnCtx *smthCtx = reac->kernCtx;
    *norm = RadialGaussQuad(ssa->geom->dim,smthCtx->r,smthCtx->k,ssa->ctx);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAComputeBindingFactor(SpatialSSA ssa,SSAPairReaction *reac,PetscReal *fac) {

  PetscErrorCode ierr;
  PetscFunctionBegin;

  *fac = 1.0;
  if(reac->doiType != DOI_NONE) {
    DoiIaFnCtx *doiCtx = reac->kernCtx;
    if(doiCtx->w) {
      PetscReal Vint = SphereVolume(doiCtx->r,ssa->geom->dim);
      PetscReal Veff = RadialBoltzmannGaussQuad(ssa->geom->dim,doiCtx->r,doiCtx->w,ssa->ctx);
      PetscReal V;
      ierr = CRDMEGeometryGetDomainVolume(ssa->geom,&V);CHKERRQ(ierr);
      *fac = (V - (Vint - Veff)) / (V*Veff);
    } 
  } 

  PetscFunctionReturn(0);
}
