#include <ssa.h>
#include <ssanbhditerators.h>

PETSC_STATIC_INLINE PetscReal bernoulli(PetscReal z) {
  return (PetscAbs(z) < 1e-4 ? ( PetscAbs(z) < 1e-8 ? 1.0 : 1.0 / (1.0 + .5*z)) : z / expm1(z));
}

/* Compute rates */

PetscErrorCode SSAComputePairRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeNbhdRatesCtx *c=ctx;
  PetscBool              self=PETSC_FALSE;
  PetscReal              rate;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  ierr = c->reaction->kern(geom,i,j,&rate,c->reaction->kernCtx);CHKERRQ(ierr);
  if(rate < PETSC_SMALL) {
    *cap=PETSC_TRUE;
  } else {
    *cap=PETSC_FALSE;
    if(c->voxelPop[j]) {
      *c->locRate+=c->voxelPop[j]*rate;
      if(self) *c->locRate-=rate;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAComputePairRatesStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeNbhdRatesCtx *c=ctx;
  PetscBool              self=PETSC_FALSE;

  PetscFunctionBegin;

  if(c->voxelPop[j]) {
    *c->locRate+=c->voxelPop[j]*val;
    if(self) *c->locRate-=val;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAComputeUnbindRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeNbhdRatesCtx *c=ctx;
  PetscReal              rate,mass;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  ierr = c->reaction->kern(geom,i,j,&rate,c->reaction->kernCtx);CHKERRQ(ierr);
  if(rate < PETSC_SMALL) {
    *cap=PETSC_TRUE;
  } else {
    *cap=PETSC_FALSE;
    ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
    *c->locRate+=mass*rate;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAComputeUnbindRatesStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeNbhdRatesCtx *c=ctx;
  PetscReal              mass;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
  *c->locRate+=mass*val;

  PetscFunctionReturn(0);
}

/* Compute energy */

PetscErrorCode SSAComputeEnergyCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeEnergyCtx    *c=(SSAComputeEnergyCtx *)ctx;

  PetscFunctionBegin;

  if(c->voxelPop[j]) {
    PetscReal              rsqr;
    PetscReal              pointPot;
    PetscErrorCode         ierr;

    *cap=PETSC_FALSE;
    ierr=CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
    pointPot=c->pp->potFn(rsqr,c->ssa->ctx);
    if(pointPot < PETSC_SMALL) {
      *cap=PETSC_TRUE;
    }
    *c->locEnergy+=c->voxelPop[j]*pointPot;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAComputeEnergyStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAComputeEnergyCtx    *c=(SSAComputeEnergyCtx *)ctx;

  if(c->voxelPop[j]) {
    *c->locEnergy+=c->voxelPop[j]*val;
  }

  return(0);
}


PetscErrorCode SSAPairRxPrecomputeRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAPrecomputeRatesCtx        *c=ctx;  
  PetscBool                    in=PETSC_FALSE;
  PetscReal                    rate;
  PetscInt                     idx;
  SSAPairReaction              *reac=c->data;
  PetscErrorCode               ierr;

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = c->VoxelIsInStencil(geom,i,j,&in,c->ctx);CHKERRQ(ierr);
  if(in) {
    idx = c->stencil->rowPtr[i+1];
    c->stencil->indices[idx] = j;
    if(c->computeRates) {
      ierr = reac->kern(geom,i,j,&rate,reac->kernCtx);CHKERRQ(ierr);
      c->stencil->vals[0][idx] = rate;
    }
    c->stencil->rowPtr[i+1]++;
  } else {
    *cap=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAPairPotPrecomputeRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAPrecomputeRatesCtx        *c=ctx;  
  PetscBool                    in=PETSC_FALSE;
  PetscInt                     idx;
  pairPotential                *pp=c->data;
  PetscReal                    rsqr;
  PetscErrorCode               ierr;

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = c->VoxelIsInStencil(geom,i,j,&in,c->ctx);CHKERRQ(ierr);
  if(in) {
    idx = c->stencil->rowPtr[i+1];
    c->stencil->indices[idx] = j;
    ierr = CRDMEGeometryNodesGetDistanceSqr(geom,i,j,&rsqr);CHKERRQ(ierr);
    c->stencil->vals[0][idx] = pp->potFn(rsqr,c->ssa->ctx);
    c->stencil->rowPtr[i+1]++;
  } else {
    *cap=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

/* Update rates */ 

PetscErrorCode SSAUpdatePairRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateNbhdRatesCtx *c=ctx;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  if(c->ssa->trackEmptyVoxelRates || c->voxelPop[j]) {
    ierr = c->reaction->kern(geom,j,i,&c->rate,c->reaction->kernCtx);CHKERRQ(ierr);
    if(c->rate < PETSC_SMALL) {
      *cap=PETSC_TRUE;
    } else {
      *cap=PETSC_FALSE;
      c->rate=c->add ? c->rate : -c->rate;
      c->locRate[j] += c->rate;
      if(c->voxelPop[j]) {    
        c->rate            *=c->voxelPop[j];
        c->voxelRatesum[j] +=c->rate;
        *c->ratesum        +=c->rate;

        if(c->ssa->ops.ssa_updatepairratescallback) {
          ierr = c->ssa->ops.ssa_updatepairratescallback(geom,i,j,radius,ctx);CHKERRQ(ierr);
        } 
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdatePairRatesStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateNbhdRatesCtx *c=ctx;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  if(c->ssa->trackEmptyVoxelRates || c->voxelPop[j]) {
    c->rate=c->add ? val : -val;
    c->locRate[j] += c->rate;
    if(c->voxelPop[j]) {    
      c->rate            *=c->voxelPop[j];
      c->voxelRatesum[j] +=c->rate;
      *c->ratesum        +=c->rate;
      if(c->ssa->ops.ssa_updatepairratescallback) {
        ierr = c->ssa->ops.ssa_updatepairratescallback(geom,i,j,val,ctx);CHKERRQ(ierr);
      } 
    }
  }

  PetscFunctionReturn(0);
}

/* update energy */

PetscErrorCode SSAUpdateEnergyCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateEnergyCtx     *c=ctx;
  PetscReal              rsqr;
  PetscReal              pointPot;
  PetscErrorCode         ierr;

  PetscFunctionBegin;

  *cap=PETSC_FALSE;
  ierr = CRDMEGeometryNodesGetDistanceSqr(geom,j,i,&rsqr);CHKERRQ(ierr);
  if(c->ssa->trackEmptyVoxelRates || c->voxelPop[j]) {
    ierr = CRDMEGeometryNodesGetDistanceSqr(geom,j,i,&rsqr);CHKERRQ(ierr);
    pointPot=c->pp->potFn(rsqr,c->ssa->ctx);
    if(pointPot < PETSC_SMALL) {
      *cap=PETSC_TRUE;
    }
    c->locEnergy[j] += (c->add ? pointPot : -pointPot);
  } else if(rsqr >= PetscSqr(radius)) {
    *cap=PETSC_TRUE;
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdateEnergyStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateEnergyCtx     *c=ctx;

  c->locEnergy[j] += (c->add ? val : -val);

  return(0);
}


PetscErrorCode SSAUpdateHoppingRatesCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateHoppingRatesCtx *c=ctx;
  SSASpecies               spec=c->spec;
  PetscInt                 len,Aj[geom->maxNodeDegree],k,self=spec->rsSpec->idx;
  PetscReal                Ax[geom->maxNodeDegree];
  PetscReal                rateDiff=0.0,rsqr;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  if(c->ssa->trackEmptyVoxelRates || spec->voxelPop[j]) {
    ierr = CRDMEGeometryNodeGetEdgeWeights(geom,j,&len,Aj,Ax);CHKERRQ(ierr);
    rateDiff-=spec->hoppingRates.totDiffusionRate[j];
    spec->hoppingRates.totDiffusionRate[j] = 0.0;          
    for(PetscInt kk=0;kk<len;kk++) {
      k=Aj[kk];
      if(!c->ssa->trackEmptyVoxelRates && !spec->voxelPop[k]) {
        ierr = SSAVoxelComputeEnergy(c->ssa,spec,k);CHKERRQ(ierr);
      }
      if(!spec->potentials.pp[self]) {
        spec->hoppingRates.edgeDiffusionRates[j][kk] = c->D*Ax[kk]*bernoulli(spec->potentials.potEnergy[k] - spec->potentials.potEnergy[j]);
      } else {
        ierr = CRDMEGeometryNodesGetDistanceSqr(geom,j,k,&rsqr);CHKERRQ(ierr);
        spec->hoppingRates.edgeDiffusionRates[j][kk] = c->D*Ax[kk]*bernoulli(spec->potentials.potEnergy[k] - spec->potentials.potEnergy[j]
                                                            - (spec->potentials.pp[self]->potFn(rsqr,c->ssa->ctx) - spec->potentials.pp[self]->potFn(0,c->ssa->ctx)) );
      }
      spec->hoppingRates.totDiffusionRate[j] += spec->hoppingRates.edgeDiffusionRates[j][kk];
    }
    rateDiff+=spec->hoppingRates.totDiffusionRate[j];
  }

  if(spec->voxelPop[j]) {
    rateDiff*=spec->voxelPop[j];
    *c->ratesum+=rateDiff;
    c->voxelRatesum[j]+=rateDiff;
    c->rateDiff=rateDiff;
    if(c->ssa->ops.ssa_updatehoppingratescallback) {
      ierr = c->ssa->ops.ssa_updatehoppingratescallback(geom,i,j,val,ctx);CHKERRQ(ierr);
    } 
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSAUpdateHoppingRatesStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSAUpdateHoppingRatesCtx *c=ctx;
  SSASpecies               sb=c->spec;

  if(sb->voxelPop[j]) {
    c->rateDiff = -sb->hoppingRates.totDiffusionRate[j]; 
    SSAVoxelComputeDiffusionRates(c->ssa,sb,j);
    c->rateDiff+=sb->hoppingRates.totDiffusionRate[j];
    c->rateDiff*=sb->voxelPop[j];
    *c->ratesum+=c->rateDiff;
    c->voxelRatesum[j]+=c->rateDiff;
    if(c->ssa->ops.ssa_updatehoppingratescallback) {
      c->ssa->ops.ssa_updatehoppingratescallback(geom,i,j,val,ctx);
    }
  }

  return(0);
}

PetscErrorCode SSASamplePairReactantCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSASamplePairReactantCtx *c=ctx;
  SSAState                 state=c->ssa->state;
  PetscReal                rate;
  PetscErrorCode           ierr;

  PetscFunctionBegin;

  ierr = c->reaction->kern(geom,i,j,&rate,c->reaction->kernCtx);CHKERRQ(ierr);
  if(rate < PETSC_SMALL) {
    *cap=PETSC_TRUE;
  } else {
    *cap=PETSC_FALSE;
    if(c->vpB[j]) {
      state->sum+=c->vpB[j]*rate;
      if(state->sum > state->rand) {
        *exit=PETSC_TRUE;
        state->lastEvent.remLoc[1]=j;
      }    
    }
  }


  PetscFunctionReturn(0);
}

PetscErrorCode SSASamplePairReactantStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSASamplePairReactantCtx *c=ctx;
  SSAState                 state=c->ssa->state;

  PetscFunctionBegin;

  if(c->vpB[j]) {
    if(c->useInt) { 
      c->intSum+=c->vpB[j];
      if(c->intSum > c->num) { 
        state->lastEvent.remLoc[1]=j;
        *exit = PETSC_TRUE;
      } 
    } else {
      state->sum += c->vpB[j]*val;
      if(state->sum > state->rand) {
        state->lastEvent.remLoc[1]=j;
        *exit = PETSC_TRUE;
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASampleUnbindLocationCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar radius, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSASampleUnbindLocationCtx *c=ctx;
  SSAState                   state=c->ssa->state;
  PetscReal                  rate,mass;
  PetscErrorCode             ierr;

  PetscFunctionBegin;

  ierr = c->reaction->kern(geom,i,j,&rate,c->reaction->kernCtx);CHKERRQ(ierr);
  if(rate < PETSC_SMALL) {
    *cap=PETSC_TRUE;
  } else {
    *cap=PETSC_FALSE;
    ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
    state->sum+=mass*rate;
    if(state->sum > state->rand) {
      *exit=PETSC_TRUE;
      state->lastEvent.addLoc[1]=j;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SSASampleUnbindLocationStencilCallback(CRDMEGeometry geom, CRDMEGeomGloIdx i, CRDMEGeomGloIdx j, PetscScalar val, void *ctx, PetscBool *cap, PetscBool *exit) {
  SSASampleUnbindLocationCtx *c=ctx;
  SSAState                   state=c->ssa->state;
  PetscReal                  mass;
  PetscErrorCode             ierr;

  PetscFunctionBegin;

  ierr = CRDMEGeometryNodeGetVoxelArea(geom,j,&mass);CHKERRQ(ierr);
  state->sum+=mass;
  if(state->sum > state->rand) {
    *exit=PETSC_TRUE;
    state->lastEvent.addLoc[1]=j;
  }

  PetscFunctionReturn(0);
}