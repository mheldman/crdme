/*
 * voxel_quad.c
 *
 *  Created on: Nov 12, 2020
 *      Author: maxheldman
 */

#include <stdlib.h>
#include <math.h>

double w  =   .333333333333333;
double wsqr = .111111111111111;
double w1 = .666666666666667;
double w2 = .166666666666667;


double voxel_quad(double (*f)(double,double), double xc1, double xc2, double *coords, int num_vertices, int is_bndry) {
  
  double integral = 0.0;
  double a1, a2, b1, b2;
  double vol;
  
  for(int i = 0; i < num_vertices - is_bndry; i++){
    
    if(i == num_vertices-1) { 
      a1 = coords[2*i]; a2 = coords[2*i+1]; 
      b1 = coords[0]; b2 = coords[1]; 
    } else { 
      a1 = coords[2*i]; a2 = coords[2*i+1]; 
      b1 = coords[2*i+2]; b2 = coords[2*i+3]; 
    }
    
    vol = .5*fabs(a1*(b2 - xc2) + b1*(xc2 - a2) + xc1*(a2 - b2));
    //vol = 1;
    
    integral += vol*w*f(w1*xc1 + w2*a1 + w2*b1, w1*xc2 + w2*a2 + w2*b2);
    integral += vol*w*f(w2*xc1 + w1*a1 + w2*b1, w2*xc2 + w1*a2 + w2*b2);
    integral += vol*w*f(w2*xc1 + w2*a1 + w1*b1, w2*xc2 + w2*a2 + w1*b2);
  }
  return integral;
}



double dbl_voxel_quad(double (*f)(double,double,double,double), double xc1, double xc2, double yc1, double yc2, double *coords1, double *coords2, int num_vertices1, int num_vertices2, int is_bndry1, int is_bndry2) {
  double integral = 0.0;
  double a11, a21, b11, b21;
  double a12, a22, b12, b22;
  double p1, p2;
  //double vol1, vol2;
  
  for(int i = 0; i < num_vertices1 - is_bndry1; i++){
    if(i == num_vertices1-1) {      /* if it's not on the boundary, then the cell wraps around. */
      a11 = coords1[2*i]; a21 = coords1[2*i+1]; 
      b11 = coords1[0]; b21 = coords1[1]; 
    } else { 
      a11 = coords1[2*i]; a21 = coords1[2*i+1]; 
      b11 = coords1[2*i+2]; b21 = coords1[2*i+3]; 
    }
       
    //vol1 = .5*fabs(a11*(b21 - xc2) + b11*(xc2 - a21) + xc1*(a21 - b21));
    //vol1 = 1;
       
    for(int j = 0; j < num_vertices2 - is_bndry2; j++) {
      if(j == num_vertices2-1) { 
        a12 = coords2[2*j]; a22 = coords2[2*j+1]; 
        b12 = coords2[0]; b22 = coords2[1]; 
      } else { 
        a12 = coords2[2*j]; a22 = coords2[2*j+1]; 
        b12 = coords2[2*j+2]; b22 = coords2[2*j+3]; 
      }
      
      //vol2 = .5*fabs(a12*(b22 - yc2) + b12*(yc2 - a22) + yc1*(a22 - b22));
      //vol2 = 1;
        
      integral += wsqr*f(w1*xc1 + w2*a11 + w2*b11, w1*xc2 + w2*a21 + w2*b21, w1*yc1 + w2*a12 + w2*b12, w1*yc2 + w2*a22 + w2*b22);
      integral += wsqr*f(w2*xc1 + w1*a11 + w2*b11, w2*xc2 + w1*a21 + w2*b21, w1*yc1 + w2*a12 + w2*b12, w1*yc2 + w2*a22 + w2*b22);
      integral += wsqr*f(w2*xc1 + w2*a11 + w1*b11, w2*xc2 + w2*a21 + w1*b21, w1*yc1 + w2*a12 + w2*b12, w1*yc2 + w2*a22 + w2*b22);
      integral += wsqr*f(w1*xc1 + w2*a11 + w2*b11, w1*xc2 + w2*a21 + w2*b21, w2*yc1 + w1*a12 + w2*b12, w2*yc2 + w1*a22 + w2*b22);
      integral += wsqr*f(w2*xc1 + w1*a11 + w2*b11, w2*xc2 + w1*a21 + w2*b21, w2*yc1 + w1*a12 + w2*b12, w2*yc2 + w1*a22 + w2*b22);
      integral += wsqr*f(w2*xc1 + w2*a11 + w1*b11, w2*xc2 + w2*a21 + w1*b21, w2*yc1 + w1*a12 + w2*b12, w2*yc2 + w1*a22 + w2*b22);
      integral += wsqr*f(w1*xc1 + w2*a11 + w2*b11, w1*xc2 + w2*a21 + w2*b21, w2*yc1 + w2*a12 + w1*b12, w2*yc2 + w2*a22 + w1*b22);
      integral += wsqr*f(w2*xc1 + w1*a11 + w2*b11, w2*xc2 + w1*a21 + w2*b21, w2*yc1 + w2*a12 + w1*b12, w2*yc2 + w2*a22 + w1*b22);
      integral += wsqr*f(w2*xc1 + w2*a11 + w1*b11, w2*xc2 + w2*a21 + w1*b21, w2*yc1 + w2*a12 + w1*b12, w2*yc2 + w2*a22 + w1*b22);
    }
  }
  return integral;
}
  

