/*
 * dual_mesh.c
 *
 *  Created on: Oct 30, 2020
 *      Author: maxheldman
 */

#include "crdme.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <petsc.h>

PetscErrorCode orient(double *v, int ilo, int ihi, int idx) {
  int a,c;  
  int det;
  double tmp[2*(ihi - ilo)];
  
  if(idx == ilo) {
    a = ihi - 1;
    c = ilo + 1;
  } else if(idx == ihi - 1) {
    a = ihi - 2;
    c = ilo;
  } else {
    a = idx - 1;
    c = idx + 1;
  }
  
  det = (v[2*idx] - v[2*a])*(v[2*c+1] - v[2*idx+1]) < (v[2*c] - v[2*idx])*(v[2*idx+1]-v[2*a+1]);
  if(det) {
    for(int i = 0; i < ihi - ilo; i++) {
      tmp[2*i]   =  v[2*(ihi-i-1)];
      tmp[2*i+1] =  v[2*(ihi-i-1)+1];
    }
    
    for(int i = 0; i < 2*(ihi - ilo); i++) {
       v[2*ilo + i] = tmp[i];
    }
  }
  return 0;
}



void compute_dual_mesh_vtk(CRDMEGeometry geom, int *cells, double *dual_nodes, int *offsets, int *types) {
  
  /* first, compute all nodes in the dual mesh (number of edges + number of cells in original mesh) */
  int startj, prevj;
  int edge_is_bndry[geom->num_edges];
  int ilo, ihi, idx_min;
  double scale = 1e-14;
  /* length of cells should be sum_{i < #voxels} 2*(geom->Ap[i + 1] - geom->Ap[i]) */
  /* one dual node for every edge, and one for every face between edges */
  /* (also one for each boundary vertex) */
  
  /* length of dual_nodes should be 3*(#cells)
sdfg
  /* compute an ordering for the dual nodes */
  int i, j, k, l = 0;
  int cell_count = 0, cell_found = 0;
  int offset = 0;
  for(int i = 0; i < geom->num_voxels; i++) {
    for(int jj = geom->Ap[i]; jj < geom->Ap[i + 1]; jj++) { /* we're going to check and see which edges belong to multiple cells */
      j = geom->Aj[jj];
      cell_count = 0;
      for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1]; kk++) { /* look at all sequences of distinct edges i -- j -- k. see if k -- i */
        k = geom->Aj[kk];
        if(k != i) {
          for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1]; ll++) { /* see if k -- i */
            if(geom->Aj[ll] == i) { 
              cell_count++;
              break; 
            }
          }
        }
      }
      edge_is_bndry[jj] = cell_count - 1;
    }
  }
  
  for(i = 0; i < geom->num_voxels; i++) {
    types[i] = 7;
    ilo = l;
    for(int jj = geom->Ap[i]; jj < geom->Ap[i + 1]; jj++) { /* if j is a boundary edge, start with i -- j */
      j = geom->Aj[jj];                                     /* otherwise, it doesn't matter. start with j = Ap[i + 1] - 1*/
      if(!edge_is_bndry[jj]) {
        dual_nodes[3*l] = geom->nodes[2*i];
        dual_nodes[3*l + 1] = geom->nodes[2*i+1];
        dual_nodes[3*l + 2] = 0.0;
        cells[l] = l;
        l++;
        break;
      }
    }
    idx_min = ilo;
    startj = j;
    prevj  = -1;
    cell_found = 1;
    while(j != startj || cell_found) {                                /* loop until we come back to the starting edge */
      cell_found = 0;                                                                /* or we can't find another cell */  
      dual_nodes[3*l]     = .5*(geom->nodes[2*i] + geom->nodes[2*j]);  /* add edge i -- j */ 
      dual_nodes[3*l + 1] = .5*(geom->nodes[2*i + 1] + geom->nodes[2*j + 1]);
      dual_nodes[3*l + 2] = 0.0;
      dual_nodes[3*l]     = (1-scale)*dual_nodes[3*l]+ scale*geom->nodes[2*i];
      dual_nodes[3*l + 1] = (1-scale)*dual_nodes[3*l + 1]+ scale*geom->nodes[2*i + 1];
      cells[l] = l;
      if(dual_nodes[3*l+1] < dual_nodes[3*idx_min+1]) {
        idx_min = l;
      } else if(dual_nodes[3*l+1] == dual_nodes[3*idx_min+1]) {
        if(dual_nodes[3*l] > dual_nodes[3*idx_min]) {
          idx_min = l;
        }
      }
      l++;      
      for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1] && !cell_found; kk++) {    /* loop over edges i -- j -- k */
        k = geom->Aj[kk];
        if(k != prevj && k != i) {                                              /* make sure we don't find the previous cell */
          for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1] && !cell_found; ll++) { /* loop over edges i -- j -- k -- Aj[ll] */
            if(geom->Aj[ll] == i) {                                             /* if Aj[ll] == i, we've found a cell */
              cell_found = 1;
              dual_nodes[3*l]     = (geom->nodes[2*i] + geom->nodes[2*j] + geom->nodes[2*k])/3.;
              dual_nodes[3*l + 1] = (geom->nodes[2*i+1] + geom->nodes[2*j+1] + geom->nodes[2*k+1])/3.;
              dual_nodes[3*l + 2] = 0.0;
              dual_nodes[3*l]     = (1-scale)*dual_nodes[3*l]+ scale*geom->nodes[2*i];
              dual_nodes[3*l + 1] = (1-scale)*dual_nodes[3*l+1] + scale*geom->nodes[2*i + 1];
              cells[l] = l;
              if(dual_nodes[3*l+1] < dual_nodes[3*idx_min+1]) {
                idx_min = l;
              } else if(dual_nodes[3*l + 1] == dual_nodes[3*idx_min + 1]) {
                if(dual_nodes[3*l] > dual_nodes[3*idx_min]) {
                  idx_min = l;
                }
              }
              l++;
              prevj = j;
              j = k;                                                            /* start again with edge i -- k */
            }
          }
        }
      }
      
      if(!cell_found) {
        ihi = l;
        break;
      }
      
      if(j == startj) {
        ihi = l;
        break;
      }
      
    }   
    //orient(dual_nodes,ilo,ihi,idx_min);
    offsets[i] = l;
  }
}

PetscErrorCode get_voxel(CRDMEGeometry geom, int i, double *voxel, int *hi) {
  int startj, prevj;
  int edge_is_bndry[320];
  int ilo, ihi, idx_min, e;
  double scale = 1e-14;
  PetscErrorCode ierr; 
  
  /* length of nodes should be 2*sum_{i < #voxels} 2*(geom->Ap[i + 1] - geom->Ap[i]) */
  /* one dual node for every edge, and one for every face between edges */
  /* (also one for each boundary vertex) */
  
  /* length of offsets should be #nodes */
  /* length of is_bndry should be #nodes */

  /* compute an ordering for the dual nodes */
  int j, k, l = 0;
  int cell_count = 0, cell_found = 0;
  int offset = 0;
  for(int jj = geom->Ap[i],e = 0; jj < geom->Ap[i + 1]; jj++, e++) { /* we're going to check and see which edges belong to multiple cells */
    j = geom->Aj[jj];
    cell_count = 0;
    for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1]; kk++) { /* look at all sequences of distinct edges i -- j -- k. see if k -- i */
      k = geom->Aj[kk];
      if(k != i) {
        for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1]; ll++) { /* see if k -- i */
          if(geom->Aj[ll] == i) { 
            cell_count++; 
            break; 
          }
        }
      }
    }
    edge_is_bndry[e] = cell_count - 1;
  }

  ilo = l;
  for(int jj = geom->Ap[i], e=0; jj < geom->Ap[i + 1]; jj++,e++) { /* if j is a boundary edge, start with i -- j */
    j = geom->Aj[jj];                                     /* otherwise, it doesn't matter. start with j = Ap[i + 1] - 1*/
    if(!edge_is_bndry[e]) {
      voxel[2*l]   = geom->nodes[2*i];
      voxel[2*l+1] = geom->nodes[2*i+1];
      l++;
      break;
    }
  }
  idx_min = ilo;
  startj = j;
  prevj  = -1;
  cell_found = 1;
  while(j != startj || cell_found) {                                /* loop until we come back to the starting edge */
    cell_found = 0;                                                                /* or we can't find another cell */  
    voxel[2*l]   = .5*(geom->nodes[2*i] + geom->nodes[2*j]);  /* add edge i -- j */ 
    voxel[2*l+1] = .5*(geom->nodes[2*i + 1] + geom->nodes[2*j + 1]);
    if(voxel[2*l+1] < voxel[2*idx_min+1]) {
      idx_min = l;
    } else if(voxel[2*l+1] == voxel[2*idx_min+1]) {
      if(voxel[2*l] > voxel[2*idx_min]) {
        idx_min = l;
      }
    }
    l++;      
    for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1] && !cell_found; kk++) {    /* loop over edges i -- j -- k */
      k = geom->Aj[kk];
      if(k != prevj && k != i) {                                              /* make sure we don't find the previous cell */
        for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1] && !cell_found; ll++) { /* loop over edges i -- j -- k -- Aj[ll] */
          if(geom->Aj[ll] == i) {                                             /* if Aj[ll] == i, we've found a cell */
            cell_found = 1;
            voxel[2*l]   = (geom->nodes[2*i] + geom->nodes[2*j] + geom->nodes[2*k])/3.;
            voxel[2*l+1] = (geom->nodes[2*i+1] + geom->nodes[2*j+1] + geom->nodes[2*k+1])/3.;
            if(voxel[2*l+1] < voxel[2*idx_min+1]) {
              idx_min = l;
            } else if(voxel[2*l+1] == voxel[2*idx_min+1]) {
              if(voxel[2*l] > voxel[2*idx_min]) {
                idx_min = l;
              }
            }
            l++;
            prevj = j;
            j = k;                                                            /* start again with edge i -- k */
          }
        }
      }
    }
    
    if(!cell_found) {
      ihi = l;
      break;
    }
    
    if(j == startj) {
      ihi = l;
      break;
    }
    
  }   
  ierr = orient(voxel,0,ihi,idx_min);CHKERRQ(ierr);
  *hi = ihi;
  return 0;
}

PetscErrorCode compute_dual_mesh(CRDMEGeometry geom, int *offsets, double *dual_nodes) {
  
  /* first, compute all nodes in the dual mesh (number of edges + number of cells in original mesh) */
  int startj, prevj;
  int edge_is_bndry[geom->num_edges];
  int ilo, ihi, idx_min;
  double scale = 1e-14;
  PetscErrorCode ierr; 
  
  /* length of nodes should be 2*sum_{i < #voxels} 2*(geom->Ap[i + 1] - geom->Ap[i]) */
  /* one dual node for every edge, and one for every face between edges */
  /* (also one for each boundary vertex) */
  
  /* length of offsets should be #nodes */
  /* length of is_bndry should be #nodes */

  /* compute an ordering for the dual nodes */
  int i, j, k, l = 0;
  int cell_count = 0, cell_found = 0;
  int offset = 0;
  
  PetscFunctionBegin;
  offsets[0] = 0;
  for(int i = 0; i < geom->num_voxels; i++) {
    for(int jj = geom->Ap[i]; jj < geom->Ap[i + 1]; jj++) { /* we're going to check and see which edges belong to multiple cells */
      j = geom->Aj[jj];
      cell_count = 0;
      for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1]; kk++) { /* look at all sequences of distinct edges i -- j -- k. see if k -- i */
        k = geom->Aj[kk];
        if(k != i) {
          for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1]; ll++) { /* see if k -- i */
            if(geom->Aj[ll] == i) { 
              cell_count++;
              break; 
            }
          }
        }
      }
      edge_is_bndry[jj] = cell_count - 1;
    }
  }
  for(i = 0; i < geom->num_voxels; i++) {
    ilo = l;
    for(int jj = geom->Ap[i]; jj < geom->Ap[i + 1]; jj++) { /* if j is a boundary edge, start with i -- j */
      j = geom->Aj[jj];                                     /* otherwise, it doesn't matter. start with j = Ap[i + 1] - 1*/
      if(!edge_is_bndry[jj]) {
        dual_nodes[2*l]   = geom->nodes[2*i];
        dual_nodes[2*l+1] = geom->nodes[2*i+1];
        l++;
        break;
      }
    }
    idx_min = ilo;
    startj = j;
    prevj  = -1;
    cell_found = 1;
    while(j != startj || cell_found) {                                /* loop until we come back to the starting edge */
      cell_found = 0;                                                                /* or we can't find another cell */  
      dual_nodes[2*l]   = .5*(geom->nodes[2*i] + geom->nodes[2*j]);  /* add edge i -- j */ 
      dual_nodes[2*l+1] = .5*(geom->nodes[2*i + 1] + geom->nodes[2*j + 1]);
      if(dual_nodes[2*l+1] < dual_nodes[2*idx_min+1]) {
        idx_min = l;
      } else if(dual_nodes[2*l+1] == dual_nodes[2*idx_min+1]) {
        if(dual_nodes[2*l] > dual_nodes[2*idx_min]) {
          idx_min = l;
        }
      }
      l++;      
      for(int kk = geom->Ap[j]; kk < geom->Ap[j + 1] && !cell_found; kk++) {    /* loop over edges i -- j -- k */
        k = geom->Aj[kk];
        if(k != prevj && k != i) {                                              /* make sure we don't find the previous cell */
          for(int ll = geom->Ap[k]; ll < geom->Ap[k + 1] && !cell_found; ll++) { /* loop over edges i -- j -- k -- Aj[ll] */
            if(geom->Aj[ll] == i) {                                             /* if Aj[ll] == i, we've found a cell */
              cell_found = 1;
              dual_nodes[2*l]   = (geom->nodes[2*i] + geom->nodes[2*j] + geom->nodes[2*k])/3.;
              dual_nodes[2*l+1] = (geom->nodes[2*i+1] + geom->nodes[2*j+1] + geom->nodes[2*k+1])/3.;
              if(dual_nodes[2*l+1] < dual_nodes[2*idx_min+1]) {
                idx_min = l;
              } else if(dual_nodes[2*l+1] == dual_nodes[2*idx_min+1]) {
                if(dual_nodes[2*l] > dual_nodes[2*idx_min]) {
                  idx_min = l;
                }
              }
              l++;
              prevj = j;
              j = k;                                                            /* start again with edge i -- k */
            }
          }
        }
      }
      
      if(!cell_found) {
        ihi = l;
        break;
      }
      
      if(j == startj) {
        ihi = l;
        break;
      }
      
    }   
    ierr = orient(dual_nodes,ilo,ihi,idx_min);CHKERRQ(ierr);
    offsets[i+1] = l;
  }
  return 0;
}
