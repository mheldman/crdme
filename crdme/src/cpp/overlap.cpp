#include <overlap.hpp>
#include <petsc.h> 
extern "C" {
#include <overlap.h>
}

PetscReal sphereTetIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint tet[4]) {
  vector_t v0{tet[0][0], tet[0][1], tet[0][2]};
  vector_t v1{tet[1][0], tet[1][1], tet[1][2]};
  vector_t v2{tet[2][0], tet[2][1], tet[2][2]};
  vector_t v3{tet[3][0], tet[3][1], tet[3][2]};
  vector_t c{center[0], center[1], center[2]};

  if((v1- v0).cross(v2 - v0).dot(v3 - v0) < scalar_t(0)) {
    vector_t tmp{v2};
    v2 = v3;
    v3 = tmp;
  }

  if((v1 - v0).cross(v2 - v0).dot(v3 - v0) < 1e-14) {
    for(auto v : {v0,v1,v2,v3}) {
      printf("(%1.7f, %1.7f, %1.7f)\n",v[0],v[1],v[2]);
    }
  }

  Tetrahedron tetra{v0, v1, v2, v3};
  Sphere s{c, radius};

  return overlap(s, tetra);
};

PetscReal sphereBoxIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint limits[2]) {
  CRDMEGeomPoint lower = {limits[0][0], limits[0][1], limits[0][2]};
  CRDMEGeomPoint upper = {limits[1][0], limits[1][1], limits[1][2]};

  vector_t v0{upper[0], lower[1], lower[2]};
  vector_t v1{upper[0], upper[1], lower[2]};
  vector_t v2{lower[0], upper[1], lower[2]};
  vector_t v3{lower[0], lower[1], lower[2]};
  vector_t v4{upper[0], lower[1], upper[2]};
  vector_t v5{upper[0], upper[1], upper[2]};
  vector_t v6{lower[0], upper[1], upper[2]};
  vector_t v7{lower[0], lower[1], upper[2]};
  vector_t c{center[0], center[1], center[2]};

  Hexahedron hex{v0, v1, v2, v3, v4, v5, v6, v7};
  Sphere s{c, radius};
  
  auto result = overlap(s, hex);

  return result;
};

PetscReal spherePentIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint Pent[5]) {

  vector_t v0{Pent[0][0], Pent[0][1], Pent[0][2]};
  vector_t v1{Pent[1][0], Pent[1][1], Pent[1][2]};
  vector_t v2{Pent[2][0], Pent[2][1], Pent[2][2]};
  vector_t v3{Pent[3][0], Pent[3][1], Pent[3][2]};
  vector_t v4{Pent[4][0], Pent[4][1], Pent[4][2]};
  vector_t c{center[0], center[1], center[2]};

  Wedge pent{v0, v1, v2, v3, v4};
  Sphere s{c, radius};
  
  auto result = overlap(s, pent);

  return result;
};
