#include <petsc.h>
#include <statistics.h>

PetscErrorCode ComputeArrayMeanReal(PetscReal *trials, PetscInt ntrials, PetscReal *mean) { /* TODO: add these to new module "statistics" */
  PetscReal      localmean;
  PetscMPIInt    sze;
  
  PetscFunctionBegin;

  localmean = 0.0;
  for(PetscInt i = 0; i < ntrials; i++) {
    localmean+=trials[i];
  }
  MPI_Allreduce(&localmean,mean,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *mean/=(sze*ntrials);
  
  PetscFunctionReturn(0);
}

PetscErrorCode ComputeArrayStdDeviationReal(PetscReal *trials, PetscInt ntrials, PetscReal mean, PetscReal *std) {
  PetscReal localstd;
  PetscInt  sze;
  
  PetscFunctionBegin;

  localstd = 0.0;
  for(PetscInt i = 0; i < ntrials; i++) {
    localstd += (mean - trials[i])*(mean - trials[i]);
  }
  MPI_Allreduce(&localstd,std,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *std/=( sze*ntrials - 1);
  *std = PetscSqrtReal(*std/(sze*ntrials));

  PetscFunctionReturn(0);
}

PetscErrorCode ComputeArrayMeanInt(PetscInt *trials, PetscInt ntrials, PetscReal *mean) { /* TODO: add these to new module "statistics" */
  PetscMPIInt    sze,glosum,locsum;
  
  PetscFunctionBegin;

  locsum = 0;
  for(PetscInt i = 0; i < ntrials; i++) {
    locsum+=trials[i];
  }

  MPI_Allreduce(&locsum,&glosum,1,MPI_INT,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *mean = glosum/(PetscReal)(sze*ntrials);

  PetscFunctionReturn(0);
}

PetscErrorCode ComputeArrayStdDeviationInt(PetscInt *trials, PetscInt ntrials, PetscReal mean, PetscReal *std) {
  PetscReal   locsumsqr;
  PetscMPIInt sze;
  
  PetscFunctionBegin;

  locsumsqr = 0.0;
  for(PetscInt i = 0; i < ntrials; i++) {
    locsumsqr += PetscSqr(mean - trials[i]);
  }
  MPI_Allreduce(&locsumsqr,std,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD,&sze);
  *std/=( sze*ntrials - 1 );
  *std = PetscSqrtReal(*std/(sze*ntrials));

  PetscFunctionReturn(0);
}