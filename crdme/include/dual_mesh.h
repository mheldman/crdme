/*
 * dual_mesh.h
 *
 *  Created on: Oct 31, 2020
 *      Author: maxheldman
 */

#include <stdlib.h>
#include <stdio.h>
#include "crdme.h"


#ifndef CRDME_TWOPARTICLE_C_DUAL_MESH_H_
#define CRDME_TWOPARTICLE_C_DUAL_MESH_H_

void compute_dual_mesh_vtk(CRDMEGeometry geom, int *cells, double *dual_nodes, int *offsets, int *types);
PetscErrorCode compute_dual_mesh(CRDMEGeometry geom, int *offsets, double *dual_nodes);
PetscErrorCode get_voxel(CRDMEGeometry,int,double*,int*);


#endif /* CRDME_TWOPARTICLE_C_DUAL_MESH_H_ */
