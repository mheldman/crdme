/*
 * rng.h
 *
 *  Created on: Apr 12, 2021
 *      Author: maxheldman
 */

#ifndef CRDME_INCLUDE_RNG_H_
#define CRDME_INCLUDE_RNG_H_

#include <stdio.h>


#define znew (z_rng=36969*(z_rng&65535)+(z_rng>>16))
#define wnew (w_rng=18000*(w_rng&65535)+(w_rng>>16))
#define MWC ((znew<<16)+wnew )
#define SHR3 (jsr_rng^=(jsr_rng<<17), jsr_rng^=(jsr_rng>>13), jsr_rng^=(jsr_rng<<5))
#define CONG (jcong_rng=69069*jcong_rng+1234567)
#define KISS ((MWC^CONG)+SHR3)
#define UNI (KISS*2.328306e-10)
#define VNI ((long) KISS)*4.656613e-10
#define UC (unsigned char) /*a cast operation*/
/* Global static variables: */
static unsigned int z_rng=12, w_rng=2, jsr_rng=231, jcong_rng=1;//z_rng=362436069, w_rng=521288629, jsr_rng=123456789, jcong_rng=380116160;
#endif /* CRDME_INCLUDE_RNG_H_ */
