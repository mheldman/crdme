#include <petsc.h>

#ifndef _STATISTICS_H_
#define _STATISTICS_H_

PetscErrorCode ComputeArrayMeanReal(PetscReal*,PetscInt, PetscReal*);
PetscErrorCode ComputeArrayStdDeviationReal(PetscReal*,PetscInt,PetscReal,PetscReal*);
PetscErrorCode ComputeArrayMeanInt(PetscInt*,PetscInt, PetscReal*);
PetscErrorCode ComputeArrayStdDeviationInt(PetscInt*,PetscInt,PetscReal,PetscReal*);
#endif