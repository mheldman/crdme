/*
 * crdmegeometry.h
 *
 *  Created on: Jul 13, 2021
 *      Author: maxheldman
 */

#ifndef CRDME_INCLUDE_CRDMEGEOMETRY_H_
#define CRDME_INCLUDE_CRDMEGEOMETRY_H_

#include <petsc.h>

typedef enum geom_type { GEOMETRY_TYPE_GCON, GEOMETRY_TYPE_BOX } CRDMEGeometryType;

typedef struct _crdme_geometry *CRDMEGeometry;
typedef struct _SpatIaStencil  *SpatIaStencil;
typedef PetscReal (*SpatialIaFn)(PetscScalar,void*); 

typedef PetscReal  CRDMEGeomPoint[3];
typedef PetscInt   CRDMEGeomCoordinate[3];
typedef PetscInt   CRDMEGeomGloIdx;

typedef struct {
  CRDMEGeomGloIdx i;
  PetscInt        length; 
  CRDMEGeomPoint* nodes;
  PetscReal       vol;
} CRDMEGeomVoxel;

struct _SpatIaStencil {
  PetscBool        boxPeriodic;
  PetscInt         *rowPtr;
  MPI_Win          rowPtrWin;
  PetscInt         stencilLen;
  CRDMEGeomGloIdx  *indices;
  MPI_Win          indicesWin;
  PetscInt         nVals;
  MPI_Win          *valsWin;
  PetscReal        **vals;
  PetscReal        *constantVals;
  MPI_Info         win_info;
};

typedef PetscErrorCode (*MeshIaFn)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);
typedef PetscErrorCode (*SpatDistributionFn)(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*,void*);
typedef PetscErrorCode (*MeshIaFnBool)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*,void*);
typedef PetscReal (*SpatialIaFn)(PetscScalar,void*); /* assumed to depend only on interaction radius */
typedef PetscReal (*SpatialFn)(CRDMEGeomPoint,void*);

typedef struct {
  SpatIaStencil stencil;
  MeshIaFnBool  VoxelIsInStencil;
  void          *ctx;
  PetscInt      count;
  PetscInt      offset;
} computeStencilIndicesCtx;

typedef struct {
  MeshIaFn    meshFn;
  void        *meshFnCtx;
  SpatialIaFn spatIaFn;
  void        *spatIaFnCtx;
  PetscInt    count;
  PetscInt    offset;
  PetscReal   *vals;
  PetscBool   boxPeriodic;
} computeStencilValuesCtx;

typedef PetscErrorCode (*VoxelNbhdCallback)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

#define PeriodicDist(a,b,p)  PetscMin(PetscAbs((a)-(b)),p-PetscAbs((a)-(b)))
#define PerDistSqr(a,b,p)    PetscSqr(PeriodicDist(a,b,p))
#define PerIndex(i,p)        (i) < 0 ? (p) + (i) : ( (i) >= (p) ? (i) - (p) : (i))

typedef struct {
  PetscErrorCode (*crdmegeom_setup)(CRDMEGeometry);
  PetscErrorCode (*crdmegeom_destroy)(CRDMEGeometry*);    
  PetscErrorCode (*crdmegeom_nodegetcoords)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
  PetscErrorCode (*crdmegeom_nodegetvoxel)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomVoxel*);
  PetscErrorCode (*crdmegeom_nodegetedgeweights)(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*,PetscReal*);
  PetscErrorCode (*crdmegeom_nodegetnumneighbors)(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*);
  PetscErrorCode (*crdmegeom_nodegetneighbors)(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*);
  PetscErrorCode (*crdmegeom_voxelgetarea)(CRDMEGeometry,CRDMEGeomVoxel,PetscReal*);
  PetscErrorCode (*crdmegeom_nodesgetdistancesqr)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*);
  PetscErrorCode (*crdmegeom_pointsgetdistancesqr)(CRDMEGeometry,CRDMEGeomPoint,CRDMEGeomPoint,PetscReal*);
  PetscErrorCode (*crdmegeom_samplefromvoxel)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
  PetscErrorCode (*crdmegeom_iteratevoxelnbhd)(CRDMEGeometry,CRDMEGeomGloIdx,PetscScalar,VoxelNbhdCallback,void*);
  PetscErrorCode (*crdmegeom_nodegetvoxelarea)(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*);
} crdmegeometry_ops;

struct _crdme_geometry {
  void                *data;
  CRDMEGeometryType   type;
  crdmegeometry_ops   ops;
  PetscInt            dim;
  PetscScalar         hmax;
  PetscScalar         hmin;
  PetscInt            N;
  PetscInt            NEdges; /* computed as [ len(Ap) - num_voxels ] / 2 */
  PetscInt            NPrimalCells;
  CRDMEGeometry       coarse;    /* a coarse mesh, possibly overlaying the geomtry */
  PetscReal           coord_min[3];
  PetscReal           coord_max[3];
  CRDMEGeomVoxel      work_voxel[2];
  PetscInt            maxNodeDegree;
  PetscInt            voxelNodesSize;
  PetscInt            qArrLen;
  CRDMEGeomGloIdx     *qArr;
  PetscBool           *visitedArr;
  PetscBool           setupCalled;
};

PetscErrorCode CRDMEGeometryCreate(CRDMEGeometry*);
PetscErrorCode CRDMEGeometryDestroy(CRDMEGeometry*);
PetscErrorCode CRDMEGeometrySetUp(CRDMEGeometry);
PetscErrorCode CRDMEGeometrySetType(CRDMEGeometry,CRDMEGeometryType);
PetscErrorCode CRDMEGeometryNodeGetCoordinates(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryNodeGetVoxel(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomVoxel*);
PetscErrorCode CRDMEGeometryNodeGetVoxelArea(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*);
PetscErrorCode CRDMEGeometryNodeGetNumNeighbors(CRDMEGeometry,PetscInt,PetscInt*); 
PetscErrorCode CRDMEGeometryNodeGetNeighbors(CRDMEGeometry,PetscInt,PetscInt*,CRDMEGeomGloIdx*); 
PetscErrorCode CRDMEGeometryNodeGetEdgeWeights(CRDMEGeometry,PetscInt,PetscInt*,CRDMEGeomGloIdx*,PetscReal*);
PetscErrorCode CRDMEGeometryVoxelGetArea(CRDMEGeometry,CRDMEGeomVoxel,PetscReal*); 
PetscErrorCode CRDMEGeometryGetDomainVolume(CRDMEGeometry,PetscReal*);
PetscErrorCode CRDMEGeometrySetDimension(CRDMEGeometry,PetscInt);
PetscErrorCode CRDMEGeometryNodesGetDistanceSqr(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*);
PetscErrorCode CRDMEGeometrySampleFromVoxel(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryIterateVoxelNbhd(CRDMEGeometry,CRDMEGeomGloIdx,PetscScalar,VoxelNbhdCallback,void*);
PetscErrorCode CRDMEGeometryVoxelSphereIntersectionArea(CRDMEGeometry,CRDMEGeomPoint,PetscScalar,CRDMEGeomVoxel,PetscReal*);
PetscErrorCode CRDMEGeometryPointsGetDistanceSqr(CRDMEGeometry,CRDMEGeomPoint,CRDMEGeomPoint,PetscReal*);
PetscErrorCode CRDMEGeometryMinDistanceSqr(CRDMEGeometry,CRDMEGeomPoint,CRDMEGeomVoxel,PetscReal*);
PetscErrorCode CRDMEGeometryComputeStencilValues(CRDMEGeometry,SpatIaStencil,MeshIaFn,void*,SpatialIaFn,void*,PetscInt,PetscBool,PetscBool);
PetscErrorCode CRDMEGeometryComputeStencilIndices(CRDMEGeometry,SpatIaStencil,PetscScalar,MeshIaFnBool,void*);
PetscErrorCode CRDMEGeometryIterateVoxelStencil(CRDMEGeometry,CRDMEGeomGloIdx,SpatIaStencil,VoxelNbhdCallback,PetscInt,void*);
PetscErrorCode CRDMEGeometryView_ASCII(CRDMEGeometry);
#endif /* CRDME_INCLUDE_CRDMEGEOMETRY_H_ */
