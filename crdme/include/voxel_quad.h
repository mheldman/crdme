/*
 * voxel_quad.h
 *
 *  Created on: Nov 13, 2020
 *      Author: maxheldman
 */

#ifndef CRDME_TWOPARTICLE_C_VOXEL_QUAD_H_
#define CRDME_TWOPARTICLE_C_VOXEL_QUAD_H_
double voxel_quad(double (*f)(double,double), double xc1, double xc2, double *coords, int num_vertices, int is_bndry);
double dbl_voxel_quad(double (*f)(double,double,double,double), double xc1, double xc2, double yc1, double yc2, double *coords1, 
                      double *coords2, int num_vertices1, int num_vertices2, int is_bndry1, int is_bndry2);
#endif /* CRDME_TWOPARTICLE_C_VOXEL_QUAD_H_ */
