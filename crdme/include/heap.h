#ifndef _HEAP_
#define _HEAP_

#define HEAP_ARITY 1

typedef struct _priority_queue *PriorityQueue;

struct _priority_queue {
  PetscInt  *index;
  PetscInt  *nextSubvolume;
  PetscReal *voxelNRT;
  PetscInt  N;
  void      *data;
};

PetscErrorCode PriorityQueueInitialize(PriorityQueue);
PetscErrorCode PriorityQueueUpdate(PriorityQueue,PetscInt,PetscReal);
PetscErrorCode PriorityQueueTestParity(PriorityQueue);
PetscErrorCode PriorityQueueView(PriorityQueue);
#endif