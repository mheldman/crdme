#include <crdmegeometry.h>
#include <reactionsystem.h>
#include <ssareactions.h>

#ifndef _SSA_NBHD_ITERATORS_H_
#define _SSA_NBHD_ITERATORS_H_

/* forward declarations */

typedef struct _spatial_ssa   *SpatialSSA;
typedef struct _ssa_species   *SSASpecies;

typedef struct {
  SpatialSSA      ssa;
  PetscInt        *voxelPop;
  SSAPairReaction *reaction;
  PetscReal       *locRate;
} SSAComputeNbhdRatesCtx;

PetscErrorCode SSAComputePairRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAComputePairRatesStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAComputeUnbindRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAComputeUnbindRatesStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  SpatIaStencil   stencil;
  MeshIaFnBool    VoxelIsInStencil;
  PetscReal       radius;
  PetscBool       computeRates;
  void            *data;
  void            *ctx;
} SSAPrecomputeRatesCtx;

PetscErrorCode SSAComputeStencilSizeCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAPairRxPrecomputeRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAPairPotPrecomputeRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  PetscInt        *voxelPop;
  pairPotential   *pp;
  PetscReal       *locEnergy;
} SSAComputeEnergyCtx;

PetscErrorCode SSAComputeEnergyCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAComputeEnergyStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  PetscBool       add;
  SSAPairReaction *reaction;
  PetscReal       *locRate;
  PetscInt        *voxelPop;
  PetscReal       *voxelRatesum;
  PetscReal       *ratesum;
  PetscReal       rate;
  void            *data;
} SSAUpdateNbhdRatesCtx;

PetscErrorCode SSAUpdatePairRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAUpdatePairRatesStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  PetscInt        *voxelPop;
  pairPotential   *pp;
  PetscInt        add;
  PetscReal       *locEnergy;
} SSAUpdateEnergyCtx;

PetscErrorCode SSAUpdateEnergyCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAUpdateEnergyStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  PetscReal       D;
  SSASpecies      spec;
  PetscReal       *voxelRatesum;
  PetscReal       *ratesum;
  PetscReal       rateDiff;
  void            *data;
} SSAUpdateHoppingRatesCtx;

PetscErrorCode SSAUpdateHoppingRatesCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAUpdateHoppingRatesStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSAUpdateHoppingRatesNeighborsStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  PetscInt        vpA;
  PetscInt        *vpB;
  PetscBool       useInt;
  PetscInt        intSum;
  PetscInt        num;
  SSAPairReaction *reaction;
} SSASamplePairReactantCtx;

PetscErrorCode SSASamplePairReactantCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSASamplePairReactantStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

typedef struct {
  SpatialSSA      ssa;
  SSAPairReaction *reaction;
  PetscInt        voxelPop;
} SSASampleUnbindLocationCtx;

PetscErrorCode SSASampleUnbindLocationCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode SSASampleUnbindLocationStencilCallback(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);

#endif