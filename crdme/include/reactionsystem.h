/*
 * reactionsystem.h
 *
 *  Created on: Sep 3, 2020
 *      Author: maxheldman
 */

#ifndef REACTION_SYSTEM_H_
#define REACTION_SYSTEM_H_

#include <petsc.h>
#include <crdmegeometry.h>

typedef struct _rx_system *ReactionSystem;

typedef PetscReal (*SpatialIaFn)(PetscScalar,void*); /* assumed to depend only on interaction radius */
typedef PetscReal (*SpatialFn)(CRDMEGeomPoint,void*);

typedef struct _reaction       RSReaction;
typedef struct _pair_potential pairPotential;
typedef struct _species        RSSpecies;

typedef enum {ORDER_ZERO,ORDER_ONE,ORDER_TWO} reactionOrder;
typedef enum {DECAY,CONVERSION,FISSION}       orderOneReactionType ;
typedef enum {ANNIHILATION,FUSION,ENZYMATIC}  orderTwoReactionType ;

struct _reaction {
  char                 name[128];
  PetscInt             idx;
	PetscInt             nReactants;
  PetscInt             reactants[2];
  PetscInt             nProducts;
  PetscInt             products[2];
  PetscScalar          rate;
  SpatialIaFn          rateFn;
  PetscBool            isDoi;
  PetscBool            isReversible;
  RSReaction           *reverseRx;
  PetscScalar          radius;
  void*                data;
};

struct _pair_potential {
  PetscInt       species[2];
  SpatialIaFn    potFn;
  PetscScalar    radius;
  void*          data;
};

struct _species {
  char       name[128];
  PetscInt   idx;
  PetscReal  D;  
  SpatialFn  backgroundPotential;
};

struct  _rx_system {
  PetscInt        nSpecies;
  RSSpecies       *spec;
  PetscInt        nReactions;
  RSReaction      *reac;
  PetscInt        nPairPotentials;
  pairPotential   *pairPotentials;
  void            *data;
  void            *ctx;
};

PetscErrorCode ReactionSystemCreate(ReactionSystem*);
PetscErrorCode ReactionSystemDestroy(ReactionSystem*);
PetscErrorCode ReactionSystemSetContext(ReactionSystem,void*);

PetscErrorCode ReactionSystemAddSpecies(ReactionSystem,const char*);
PetscErrorCode ReactionSystemSpeciesGetIndex(ReactionSystem,const char*,PetscInt*);
PetscErrorCode ReactionSystemReactionGetIndex(ReactionSystem,const char*,PetscInt*);

PetscErrorCode ReactionSystemAddReaction(ReactionSystem,PetscInt,PetscInt*,PetscInt,PetscInt*,SpatialIaFn,PetscScalar,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddCreation(ReactionSystem,const char*,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddDecay(ReactionSystem,const char*,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddAnnihilation(ReactionSystem,const char*,const char*,SpatialIaFn,PetscScalar,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddConversion(ReactionSystem,const char*,const char*,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddFusion(ReactionSystem,const char*,const char*,const char*,SpatialIaFn,PetscScalar,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddFission(ReactionSystem,const char*,const char*,const char*,SpatialIaFn,PetscScalar,PetscScalar,const char*);
PetscErrorCode ReactionSystemAddEnzymatic(ReactionSystem,const char*,const char *,const char*,SpatialIaFn,PetscScalar,PetscScalar,const char*);
PetscErrorCode ReactionSystemReactionMakeReversible(ReactionSystem,const char*,PetscScalar);

PetscErrorCode ReactionSystemAddReverseReaction(ReactionSystem,const char*,PetscScalar,const char*);

PetscErrorCode ReactionSystemAddPairPotential(ReactionSystem,const char*,const char*,PetscScalar,SpatialIaFn);
PetscErrorCode ReactionSystemSpeciesSetDiffusionConstant(ReactionSystem,const char*,PetscReal);
PetscErrorCode ReactionSystemSpeciesAddBackgroundPotential(ReactionSystem,const char*,SpatialFn);

PetscErrorCode ReactionSystemSpeciesView(ReactionSystem,PetscInt);
PetscErrorCode ReactionSystemReactionView(ReactionSystem,PetscInt);
PetscErrorCode ReactionSystemPairPotentialView(ReactionSystem,PetscInt);
PetscErrorCode ReactionSystemView(ReactionSystem); 

/*
PetscErrorCode ReactionSystemDoiComputeRate(CRDME,int,int,double,double*,doi_type);
PetscErrorCode CRDMEDoiIntegrate(CRDME,double*,int,double*,double,PetscBool,double*); 
PetscErrorCode PolygonCircleIntersectionArea(double*,int,double,double,double,double*);
PetscErrorCode CRDMEView_ascii(CRDME);
PetscErrorCode CRDMEGeomView_vtk(CRDME,const char*);
*/

#endif /* REACTION_SYSTEM_H_ */

