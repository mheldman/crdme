/*
 * crdmeunstructuredgrid.h
 *
 *  Created on: Jul 13, 2021
 *      Author: maxheldman
 */

#ifndef CRDME_INCLUDE_CRDMEGEOMGCON_H_
#define CRDME_INCLUDE_CRDMEGEOMGCON_H_

#include <petsc.h>
#include <crdmegeometry.h>
/* CRDME unstructured mesh - GCON = general connectivity. 
 * distinguished from a structured mesh because the nodes, mass matrix, and Laplacian
 * must be stored explicitly.  
 */

typedef struct _unstructured_mesh *CRDMEGeometry_GCON;

typedef struct {
  CRDMEGeomGloIdx idx[4];
  PetscScalar     vol;
} tetCell_t;

struct _unstructured_mesh {
  PetscInt  *Ap; /* discrete Laplacian matrix in CSR format */
  PetscInt  *Aj; /* Ap and Aj can also be used for adjacency info */
  PetscReal *Ax; /* stored in CSR format */
  PetscReal *nodes; /* mesh nodes (dual mesh voxels). length 2 * num_voxels */
  PetscReal *mass;  /* diagonal of mass matrix */
  PetscBool *bndry;
  tetCell_t *tets; 
  PetscInt  *cellPtr;
  PetscInt  *cellIdx;
  PetscBool edgesSorted;
  PetscBool boundarySetup;
};

PetscErrorCode CRDMEGeomGCONSetLaplacian(CRDMEGeometry,const PetscInt*,const PetscInt*,const PetscReal*);
PetscErrorCode CRDMEGeomGCONSetMass(CRDMEGeometry,const PetscReal*);
PetscErrorCode CRDMEGeomGCONSetNodes(CRDMEGeometry,const PetscReal*);
PetscErrorCode CRDMEGeomGCONCreateFromFolder(CRDMEGeometry,const char*);

PetscErrorCode CRDMEGeomGCONShiftScaleCoordinates(CRDMEGeometry,PetscReal*,PetscReal);

PetscErrorCode CRDMEGeometryNodeGetNumNeighbors_GCON(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*);
PetscErrorCode CRDMEGeometryNodeGetNeighbors_GCON(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*);
PetscErrorCode CRDMEGeometryNodeGetEdgeWeights_GCON(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*,PetscReal*);
PetscErrorCode CRDMEGeometryNodeGetCoordinates_GCON(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryNodeGetVoxel_GCON(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomVoxel*);
PetscErrorCode CRDMEGeometryNodeGetVoxelArea_GCON(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*);
PetscErrorCode CRDMEGeometryNodesGetDistanceSqr_GCON(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*);
PetscErrorCode CRDMEGeometryPointsGetDistanceSqr_GCON(CRDMEGeometry,CRDMEGeomPoint,CRDMEGeomPoint,PetscReal*);
PetscErrorCode CRDMEGeometryVoxelGetArea_GCON(CRDMEGeometry,CRDMEGeomVoxel,PetscReal*);
PetscErrorCode CRDMEGeometrySampleFromVoxel_GCON(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryDestroy_GCON(CRDMEGeometry *geom);
PetscErrorCode CRDMEGeometrySetUp_GCON(CRDMEGeometry);
PetscErrorCode CRDMEGeomGCONGmshCreateFromFile(CRDMEGeometry,const char*); 
PetscErrorCode CRDMEGeomGCONCreateFromDMPlex(CRDMEGeometry,DM);


#endif /* CRDME_INCLUDE_CRDMEGEOMGCON_H_ */
