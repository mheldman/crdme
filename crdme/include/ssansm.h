
#include <ssa.h>
#include <heap.h>

typedef struct _ssa_nsm  SpatialSSA_NSM;

struct _ssa_nsm {
  PriorityQueue   queue;
  PetscInt        nQueuedForUpdate;
  PetscBool       *queuedForUpdate;
  PetscReal       *voxelRateDiff;
  CRDMEGeomGloIdx *updateQueue;
};

PetscErrorCode SSACreate_NSM(SpatialSSA*);
PetscErrorCode SSASetUp_NSM(SpatialSSA);
PetscErrorCode SSAGetNextSubvolume_NSM(SpatialSSA);
PetscErrorCode SSAGetNextEventTime_NSM(SpatialSSA);
PetscErrorCode SSAStateInitialize_NSM(SpatialSSA);
PetscErrorCode SSASpeciesAddToVoxel_NSM(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSASpeciesRemoveFromVoxel_NSM(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSAUpdateHoppingRatesCallback_NSM(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*);
PetscErrorCode SSAUpdatePairRatesCallback_NSM(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*);
PetscErrorCode SSADestroy_NSM(SpatialSSA*);
