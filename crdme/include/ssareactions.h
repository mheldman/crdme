#include <petsc.h>
#include <crdmegeometry.h>
#include <reactionsystem.h>

#ifndef _SSA_REACTIONS_H_
#define _SSA_REACTIONS_H_

typedef struct _spatial_ssa *SpatialSSA;

typedef enum _doi_type {DOI_NONE, DOI_LUMP, SYM_DOI_LUMP, DOI_STANDARD, DOI_LUMP_CONSTANT, DOI_STANDARD_CONSTANT, DOI_CONSTANT, DOI_CUSTOM} SSADoiType;

PetscErrorCode DoiIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);
PetscErrorCode DoiLumpIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);
PetscErrorCode SymmetricDoiLumpIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);

PetscErrorCode DoiRejectIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);
PetscErrorCode DoiLumpRejectIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);
PetscErrorCode SymmetricDoiLumpRejectIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);

PetscErrorCode VoxelIsInStencil_Doi(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*,void*);
PetscErrorCode VoxelIsInStencil_DoiLump(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*,void*);
PetscErrorCode VoxelIsInStencil_SymmetricDoiLump(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*in,void*);

typedef struct { 
  PetscScalar  r;
  PetscScalar  kap;
  MeshIaFnBool isInStencil;
  SpatialIaFn  w;
  void         *ctx;
} DoiIaFnCtx;

PetscErrorCode SmoothIaFn(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*,void*);

PetscErrorCode VoxelIsInStencil_Smooth(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*in,void*);
PetscErrorCode VoxelIsInStencil_Edge(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscBool*in,void*);

typedef struct {
  SpatialIaFn k;
  PetscScalar r;
  PetscScalar kap;
  void  *ctx;
} SmoothIaFnCtx;

typedef PetscErrorCode (*RxRejectionFn)(SpatialSSA,void*);

typedef struct {
  RxRejectionFn reject;
  void          *ctx;
} SSARxRejectionFn;

PetscErrorCode DoiLumpPairRejectionFn(SpatialSSA,void*);
PetscErrorCode SymmetricDoiLumpPairRejectionFn(SpatialSSA,void*);
PetscErrorCode DoiLumpUnbindRejectionFn(SpatialSSA,void*);

typedef struct {
  MeshIaFn        kern;
  void            *kernCtx;
  RxRejectionFn   reject;
  void            *rejectCtx;
  SSADoiType      doiType;
  MeshIaFnBool    isInStencil;
  SpatIaStencil   stencil;
} SSAPairReaction;

typedef struct {
  PetscInt           pop;
  SpatDistributionFn density;
  PetscReal          *cumDensity;
  char               name[128];
} SSAInitCondition;

PetscErrorCode uniformDistribution(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*,void*);
PetscErrorCode ReversibleReactionRejectionFn(SpatialSSA);

#endif