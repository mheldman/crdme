#ifndef OVERLAP_H
#define OVERLAP_H
#include <petsc.h>

typedef PetscReal CRDMEGeomPoint[3];

#ifdef __cplusplus
extern "C" {
#endif

PetscReal sphereTetIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint tet[4]);
PetscReal sphereBoxIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint box[2]);
PetscReal spherePentIntersection(CRDMEGeomPoint center, PetscReal radius, CRDMEGeomPoint Pent[2]);

#ifdef __cplusplus
}
#endif

#endif