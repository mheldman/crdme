#include <petsc.h>

PetscErrorCode BinarySearchReal(PetscReal*,PetscInt,PetscInt,PetscReal,PetscInt*);
PetscErrorCode WriteBinaryDataToFile(const char *,PetscInt,void *,PetscInt,PetscDataType);
PetscErrorCode PlotDataArray(const char*,const char*,const char*,const char*,PetscInt,PetscReal*,PetscReal[2],PetscReal*,PetscReal[2]);
