/*
 * ssa_dm.h
 *
 *  Created on: Jan 15, 2021
 *      Author: maxheldman
 */

#ifndef SSA_DM_H_
#define SSA_DM_H_
#include <petsc.h>

#include "crdme.h"
#include "ssa.h"

typedef enum ssa_dm_event_type { SSA_DIFFUSION_A, SSA_DIFFUSION_B, SSA_DIFFUSION_C, SSA_RX_ABC, SSA_RX_CAB } event_type;
typedef enum ssa_potential_type { SSA_POT_TYPE_NONE, SSA_POT_TYPE_BACKGROUND, SSA_POT_TYPE_PAIR } ssa_potential_type;
typedef enum ssa_dm_initial_condition { SSA_IC_UNIFORM, SSA_IC_POINT, SSA_IC_CUSTOM } ic_type;

typedef struct { 
  double *edge_pot;   /* array containing potential over each edge for this particle (null for background potential) */
  double *edge_rate;  /* array containing rate over each edge for this particle */
  double voxel_rate;  /* array containing rate in each voxel for this particle */
} SSADMHoppingRates;

typedef struct _ssa_dm_species_state *SSADMSpeciesState;
typedef struct _ssa_dm_state *SSADMState;
typedef PetscErrorCode (*StateInit)(SpatialSSA,SSADMSpeciesState*);
typedef PetscErrorCode (*StateReset)(SpatialSSA,SSADMSpeciesState*);

struct _ssa_dm_species_state {
    
  int  max;              /* maximum number of each particle */
  int  num;
  int  num_v;

  int  *loc;    /* location of each particle */
  int  *pop;    /* in each voxel, number of each type of particle */
  int  *first;    /* in each voxel, the particle number of each particle in that voxel */
  
  ssa_potential_type     pot_type;  
  double                 *background_rate; /* background potential over each edge for each type (ignore for now?) */
                                            /* points to the Laplacian if there is no background potential */
  SSADMHoppingRates      *hr; 
  
  double  *dec_rates;  /* for each A0 reaction, the rate in each voxel */
  double  *cre_rates;  /* for each 0A reaction, the rate in each voxel */
  int     n_pair;
  double  *pair_ratesum;
  int     *rx_num;
  int     n_unbnd;
  double  *unbnd_ratesum; /* total reaction rate sum for each CAB reaction, per C */
  double  *coarse_rate;
};

struct _ssa_dm_state {
  SSADMSpeciesState *species; /* each species has its own state */
  double ratesum;
  double time;
  int    last_event; /* previous event number, so that the ssa_dm_monitor can e.g. keep track of
                         the number of the various event types that occurred */
};

typedef struct {
 SSADMState state;
 SSADMState start;
 StateInit  init;
 StateReset reset;
 int        max_edge;
} SSA_DM;

PetscErrorCode SSASolve_DM(SpatialSSA);
PetscErrorCode SSADestroy_DM(SpatialSSA*); /* TODO */

PetscErrorCode SSASetUp_DM(SpatialSSA);
PetscErrorCode SSADMStateDuplicate(SpatialSSA,SSADMState*);
PetscErrorCode SSADMSetState(SpatialSSA,SSADMState);
PetscErrorCode SSADMSetMaxParticles(SpatialSSA,int*);
PetscErrorCode SSADMICFunctionSet(SpatialSSA,StateInit);
PetscErrorCode SSADMResetFunctionSet(SpatialSSA,StateReset);
PetscErrorCode SSADMGetState(SpatialSSA,SSADMState*); 
PetscErrorCode SSADMStateInitialize(SpatialSSA,ic_type,int *num);
PetscErrorCode SSADMStateViewVTK(SpatialSSA,const char*);

#endif /* SSA_DM_H_ */
