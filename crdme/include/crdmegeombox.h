/*
 * crdmestructuredgrid.h
 *
 *  Created on: Jul 13, 2021
 *      Author: maxheldman
 */

#ifndef CRDME_INCLUDE_CRDMEGEOMBOX_H_
#define CRDME_INCLUDE_CRDMEGEOMBOX_H_

#include <petsc.h>
#include <crdmegeometry.h>

typedef struct _box_mesh *CRDMEGeometry_Box;

struct _box_mesh {
  PetscInt        m[3];            /* number of points in three coordinate directions */
  PetscScalar     h[3];            /* spacing in each coordinate direction */
  PetscScalar     hsqr[3];
  PetscReal       L[3];
  PetscBool       periodicity[3];  /* periodicity in each direction */
  PetscScalar     edge_weights[3]; /* 1.0 / (h[i]*h[i]) */
  PetscInt        *Ap;
  CRDMEGeomGloIdx *Aj;
  PetscReal       *Ax;
};

PetscErrorCode CRDMEGeomBoxSet(CRDMEGeometry,PetscReal*,PetscInt*,PetscBool*);

PetscErrorCode CRDMEGeometryNodeGetNumNeighbors_Box(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*);
PetscErrorCode CRDMEGeometryNodeGetNeighbors_Box(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*);
PetscErrorCode CRDMEGeometryNodeGetEdgeWeights_Box(CRDMEGeometry,CRDMEGeomGloIdx,PetscInt*,CRDMEGeomGloIdx*,PetscReal*);
PetscErrorCode CRDMEGeometryNodeGetCoordinates_Box(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryNodeGetVoxel_Box(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomVoxel*);
PetscErrorCode CRDMEGeometryNodeGetVoxelArea_Box(CRDMEGeometry,CRDMEGeomGloIdx,PetscReal*);

PetscErrorCode CRDMEGeometryNodesGetDistanceSqr_Box(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal*);
PetscErrorCode CRDMEGeometryPointsGetDistanceSqr_Box(CRDMEGeometry,CRDMEGeomPoint,CRDMEGeomPoint,PetscReal*);
PetscErrorCode CRDMEGeometryVoxelGetArea_Box(CRDMEGeometry,CRDMEGeomVoxel,PetscReal*);
PetscErrorCode CRDMEGeometrySampleFromVoxel_Box(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomPoint);
PetscErrorCode CRDMEGeometryIterateVoxelNbhd_Box(CRDMEGeometry,CRDMEGeomGloIdx,PetscScalar,VoxelNbhdCallback,void*);
PetscErrorCode CRDMEGeometryDestroy_Box(CRDMEGeometry*);
PetscErrorCode CRDMEGeometrySetUp_Box(CRDMEGeometry);
PetscErrorCode computeStencilIndicesCallback_PeriodicBox(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*,PetscBool*,PetscBool*);
PetscErrorCode CRDMEGeomBoxIteratePeriodicStencil(CRDMEGeometry,CRDMEGeomGloIdx,SpatIaStencil,VoxelNbhdCallback,PetscInt,void*);
PetscErrorCode CRDMEGeomBoxSearchPeriodicStencil(CRDMEGeometry,SpatIaStencil,CRDMEGeomGloIdx,PetscReal,CRDMEGeomGloIdx*);

#endif /* CRDME_INCLUDE_CRDMEGEOMBOX_H_ */
