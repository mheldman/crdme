/*
 * ssa.h
 *
 *  Created on: Sep 11, 2020
 *      Author: maxheldman
 */

#ifndef _SSA_H_
#define _SSA_H_

#include <petsc.h>
#include <crdmegeometry.h>
#include <reactionsystem.h>
#include <ssareactions.h>
#include <ssanbhditerators.h>


typedef enum spatial_ssa_type   { SSA_TYPE_HM, SSA_TYPE_NSM } spatial_ssa_type;

typedef struct _spatial_ssa       *SpatialSSA;
typedef struct _ssa_state         *SSAState;
typedef struct _ssa_species       *SSASpecies;

typedef PetscErrorCode (*SSAMonitor)(SpatialSSA,PetscBool*,PetscBool,void*);

typedef struct {
  PetscErrorCode (*ssa_solve)(SpatialSSA);
  PetscErrorCode (*ssa_setgeometry)(SpatialSSA,CRDMEGeometry);
  PetscErrorCode (*ssa_setup)(SpatialSSA);
  PetscErrorCode (*ssa_speciessetup)(SpatialSSA,PetscInt);
  PetscErrorCode (*ssa_getnextsubvolume)(SpatialSSA);
  PetscErrorCode (*ssa_getnexteventtime)(SpatialSSA);
  PetscErrorCode (*ssa_stateinitialize)(SpatialSSA);
  PetscErrorCode (*ssa_addtolocation)(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
  PetscErrorCode (*ssa_removefromlocation)(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
  PetscErrorCode (*ssa_updatehoppingratescallback)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal,void*);
  PetscErrorCode (*ssa_updatepairratescallback)(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscReal,void*);
  PetscErrorCode (*ssa_destroy)(SpatialSSA*);
} ssa_ops;

typedef struct {
  SSAUpdateHoppingRatesCtx hoppingRatesUpdateCtx;
  SSAUpdateNbhdRatesCtx    nbhdRatesUpdateCtx;
} ssa_iteratorCtx;

struct _ssa_species {
  PetscInt        pop;
  PetscInt        *voxelPop;

  struct {
    RSReaction      *Ord0;
    PetscInt        nOrd1;
    RSReaction      **Ord1;
    PetscInt        nUnbind;
    RSReaction      **Unbind;
    PetscInt        nOrd2;
    RSReaction      **Ord2;
    PetscReal       **unbindRates;       /* per site per reaction */
    SSAPairReaction *unbindKernels;
    PetscReal       **pairRates;         /* per site per reaction */  
    SSAPairReaction *pairKernels;
    PetscInt        nPairEffects;
    PetscInt        (*pairEffects)[2];        /* pairEffects[0]=species,[1]=pair reaction number */
  } reactions;

  PetscBool hasPot;
  struct {
    SpatialFn     bg;
    pairPotential **pp;
    PetscReal     *potEnergy;          /* per voxel */
  } potentials;

  struct {
    PetscReal D;
    PetscReal *totDiffusionRate;    /* per voxel (not used if no potentials) */
    PetscReal **edgeDiffusionRates; /* per voxel per edge (not used if no potentials): D*Ax[e]*(potEnergy[e] - potEnergy[i]) */
  } hoppingRates;
  
  SSAInitCondition *initCondition;
  RSSpecies       *rsSpec;
  void            *data;
};

typedef enum _eventType {DIFFUSION,REACTION_ORD_0,REACTION_ORD_1,REACTION_ORD_2,REACTION_UNBIND} eventType; 

typedef struct {
  eventType        type;  
  PetscInt         idx; 
  CRDMEGeomGloIdx  loc;
  PetscBool        reject;
  PetscBool        revReject;
  PetscInt         nAdd;
  CRDMEGeomGloIdx  addLoc[2];
  PetscInt         addSpec[2];
  PetscInt         nRem;
  CRDMEGeomGloIdx  remLoc[2];
  PetscInt         remSpec[2];
} ssaEvent_t;

struct _ssa_state {
  PetscReal       prevTime;
  PetscReal       curTime;
  PetscReal       *voxelRatesum;
  PetscReal       oldRatesum;
  PetscReal       ratesum;
  ssaEvent_t      lastEvent; /* event type (reaction/diffusion), which reaction (or species), and the locations */ 
  PetscInt        nEvents;
  PetscInt        nextCheckpoint;
  PetscReal       rand;
  PetscReal       sum;
  PetscInt        nSpecies;
  SSASpecies      *spec;
};

struct _spatial_ssa {
  ReactionSystem   rs;
  CRDMEGeometry    geom;
  ssa_ops          ops;
  ssa_iteratorCtx  itContexts;
  spatial_ssa_type ssa_type;
  unsigned short   seed[3];
  PetscInt         nTrials;
  PetscReal        Tf;
  PetscInt         curSimulationNumber;
  PetscInt         nCheckpoints;
  PetscReal        *checkpoints;
  SSAMonitor       monitor;
  void             *monitor_ctx;
  SSAState         state;
  PetscBool        trackEmptyVoxelRates;
  PetscBool        setupCalled;
  PetscMPIInt      rank;
  PetscMPIInt      np;
  PetscBool        *updatedArr;

  void             *ctx;
  void             *data;
};


PetscErrorCode SSACreate(SpatialSSA*); 
PetscErrorCode SSADestroy(SpatialSSA*); 

PetscErrorCode SSASetReactionSystem(SpatialSSA,ReactionSystem);            /*TODO: these should have corresponding "get" functions */
PetscErrorCode SSASetGeometry(SpatialSSA,CRDMEGeometry);            /*TODO: these should have corresponding "get" functions */
PetscErrorCode SSASetSeed(SpatialSSA,unsigned short[3]);
PetscErrorCode SSASetMonitor(SpatialSSA,SSAMonitor);
PetscErrorCode SSASetMonitorContext(SpatialSSA,void*);
PetscErrorCode SSASetContext(SpatialSSA,void*);
PetscErrorCode SSASetNumTrials(SpatialSSA,PetscInt);
PetscErrorCode SSASetFinalTime(SpatialSSA,PetscReal);
PetscErrorCode SSASetFromOptions(SpatialSSA);
PetscErrorCode SSAGetNextEventTime(SpatialSSA);
PetscErrorCode SSASetInitialCondition(SpatialSSA,const char*,SpatDistributionFn,const char*);
PetscErrorCode SSASetInitialPopulation(SpatialSSA,PetscInt*);
PetscErrorCode SSASpeciesSetInitialPopulation(SpatialSSA,PetscInt,PetscInt);
PetscErrorCode SSAReactionSetDoiType(SpatialSSA,const char*,SSADoiType);
PetscErrorCode SSAReactionSetRejectionFn(SpatialSSA,const char*,RxRejectionFn,void*);

PetscErrorCode SSAReactionPrecomputeRates(SpatialSSA,const char*);
PetscErrorCode SSAPairPotentialPrecompute(SpatialSSA,const char*,const char*);

PetscErrorCode SSAPrintState(SpatialSSA);
PetscErrorCode SSAGetNextEventTime_ratesum(SpatialSSA);
PetscErrorCode SSAStateInitialize(SpatialSSA);
PetscErrorCode SSAAddToLocation(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSARemoveFromLocation(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSAVoxelComputeEnergy(SpatialSSA,SSASpecies,CRDMEGeomGloIdx);
PetscErrorCode SSASetTrackEmptyVoxelRates(SpatialSSA,PetscBool);
PetscErrorCode SSAVoxelComputeDiffusionRates(SpatialSSA,SSASpecies,CRDMEGeomGloIdx);

PetscErrorCode SSASetType(SpatialSSA,spatial_ssa_type); 
PetscErrorCode SSASetUp(SpatialSSA); 
PetscErrorCode SSASolve(SpatialSSA); 
PetscErrorCode SSASetCheckpoints(SpatialSSA,int,double*);
PetscErrorCode SSAView(SpatialSSA);
PetscErrorCode SSAComputeUnbindingNormalization(SpatialSSA,SSAPairReaction*,PetscReal*);
PetscErrorCode SSAComputeBindingFactor(SpatialSSA,SSAPairReaction*,PetscReal*);

#endif /* TWOPARTICLE_SSA_H_ */
