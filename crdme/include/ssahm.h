
#include <petsc.h>
#include <ssa.h>

#ifndef _SSA_HM_H_
#define _SSA_HM_H_

//typedef struct _ssa_hm *SpatialSSA_HM;

typedef struct _ssa_hm {
  PetscInt  nLevels;
  PetscInt  totVoxels;
  PetscReal **hmVoxelRatesum;
} SpatialSSA_HM; 

PetscErrorCode SSACreate_HM(SpatialSSA*);
PetscErrorCode SSASetGeometry_HM(SpatialSSA,CRDMEGeometry);
PetscErrorCode SSASpeciesSetUp_HM(SpatialSSA,PetscInt); 
PetscErrorCode SSASetUp_HM(SpatialSSA);
PetscErrorCode SSAGetNextSubvolume_HM(SpatialSSA);
PetscErrorCode SSAStateInitialize_HM(SpatialSSA);
PetscErrorCode SSASpeciesAddToVoxel_HM(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSASpeciesRemoveFromVoxel_HM(SpatialSSA,PetscInt,CRDMEGeomGloIdx);
PetscErrorCode SSAUpdateHoppingRatesCallback_HM(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*);
PetscErrorCode SSAUpdatePairRatesCallback_HM(CRDMEGeometry,CRDMEGeomGloIdx,CRDMEGeomGloIdx,PetscScalar,void*);
PetscErrorCode SSADestroy_HM(SpatialSSA*);

#endif