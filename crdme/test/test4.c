/*
 * test0.c
 *
 *  Created on: Jan 25, 2021
 *      Author: maxheldman
 *      
 *      1. Read binary mesh data generated in Python for triangulation of a uniform mesh from file.
 *      2. View mesh ascii.
 *      3. View mesh VTK.
 *    
 */
#define _XOPEN_SOURCE 
#include "ssa.h"
#include "crdme.h"
#include "ssa_dm.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <petscsys.h>
#include <rng.h>

static char help[] = "";

typedef struct {
  double *cum_mass;
  int    num[3];
} AppCtx;

int    count = 0;
double inc   = .1;

static inline double psiAB(double x0, double x1, double y0, double y1, void *p) {
  double r = PetscSqrtReal( (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) );
  return r < .002025 ? 12500.*(r - .045)*(r - .045) : 0.0;
}

static inline double kAB(double x0, double x1, double y0, double y1, void *p) {
  double r = (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) ;
  return r < .002025  ? .246*PetscExpReal(-247.*r) : 0.0; /* .00561 */
}

static inline double kC(double x0, double x1, double y0, double y1, void *p) {
  double r = (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) ;
  return r < .002025 ? .004*PetscExpReal(-247.*r) : 0.0; /* .00561 */
}
//*PetscExpReal(-500000.*r) 
PetscBool Monitor(SpatialSSA ssa, int sims, void *ctx) {
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  CRDME                crdme = ssa->crdme;
  PetscBool            cond;

  /*
  if(state->time > count*inc) {
    SSADMStateViewVTK(ssa,"test4vtk_lvl6_gaussian/test2_view");
    count++;
  }
  */
  
  cond = (state->time > ssa->tf);
  
/*
  if(state->species[0]->num > 0) {
    PetscPrintf(PETSC_COMM_WORLD,"  %d %d %d\n",state->species[0]->num,state->species[1]->num,state->species[2]->num);
    for(int i = 0; i < state->species[0]->num_v; i++) {
      printf("  rx rate: %1.14f\n",state->species[0]->pair_ratesum[i]);
    }
    for(int i = 0; i < state->species[0]->num_v; i++) {
      printf("  diffusion rate A: %1.14f\n",state->species[0]->hr[i].voxel_rate);
    }
    for(int i = 0; i < state->species[1]->num_v; i++) {
      printf("  diffusion rate B: %1.14f\n",state->species[1]->hr[i].voxel_rate);
    }
    for(int i = 0; i < state->species[0]->num_v; i++) {
      PetscPrintf(PETSC_COMM_WORLD,"  loc A: %d (%f,%f)\n",state->species[0]->loc[i],
                                crdme->geom->nodes[2*state->species[0]->loc[i]],
                                crdme->geom->nodes[2*state->species[0]->loc[i]+1]);
    }
    for(int i = 0; i < state->species[1]->num_v; i++) {
      PetscPrintf(PETSC_COMM_WORLD,"  loc B: %d (%f,%f)\n",state->species[1]->loc[i],
          crdme->geom->nodes[2*state->species[1]->loc[i]],
               crdme->geom->nodes[2*state->species[1]->loc[i]+1]);
    }

 } else if(state->species[2]->num > 0) {
   PetscPrintf(PETSC_COMM_WORLD,"  %d %d %d\n",state->species[0]->num,state->species[1]->num,state->species[2]->num);
   for(int i = 0; i < state->species[2]->num_v; i++) {
     printf("  diffusion rate C: %1.14f\n",state->species[2]->hr[i].voxel_rate);
   }
   for(int i = 0; i < state->species[2]->num_v; i++) {
     PetscPrintf(PETSC_COMM_WORLD,"  loc C: %d (%f,%f)\n",state->species[2]->loc[i],
                               crdme->geom->nodes[2*state->species[2]->loc[i]],
                               crdme->geom->nodes[2*state->species[2]->loc[i]+1]);
   }
 
   printf("  unbind ratesum %1.14f\n",state->species[2]->unbnd_ratesum[state->species[2]->loc[0]]);
 }
  */
  if(cond) {
    double *trials = (double *) ssa->monitor_ctx;
    trials[sims] = 1.0*state->species[2]->num /  ( 1.0*state->species[2]->max );
  }
  return cond;
}

/*
static inline double kAB(double x0, double x1, double y0, double y1, void *p) {
  return (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) < .0001 ? 3989422804.0143275 : 0.0;
}

static inline double kC(double x0, double x1, double y0, double y1, void *p) {
  return (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) < .0001 ? 3989422804.0143275 : 0.0;
}
*/
PetscErrorCode set_geom(CRDME crdme);

int bin_search(double *arr, int l, int h, double r) {
  
  if(r < arr[l]) {
    return l;
  }
  if(h - l >= 1) {
    int mid = l + (h - l) / 2;
    if(arr[mid] < r && arr[mid+1] > r) {
      return mid+1;
    }
    
    if(r > arr[mid]) {
      return bin_search(arr,mid+1,h,r);
    }
    
    return bin_search(arr,l,mid,r);
  }
  return -1;
}

PetscErrorCode InitDirac(SpatialSSA ssa, SSADMSpeciesState *species) {
  CRDME             crdme  = ssa->crdme;
  double            *nodes = crdme->geom->nodes;
  int               N = crdme->geom->num_voxels;
  int               M = crdme->Nspecies;
  int               idxC;
  SSADMSpeciesState s;
  
  
  for (int i = 0; i < N; i++) {
    if(nodes[2*i] == 0.0 && nodes[2*i + 1] == 0.0) { 
      idxC = i;
      break;
    }
  }
  
  s = species[2];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxC;
  }
  
  s = species[1];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxC;
  }
  
  s = species[0];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxC;
  }
  
  return 0;
}

PetscErrorCode InitFromDistribution(SpatialSSA ssa, SSADMSpeciesState *species) { //not correct as stated, should pick an element

  CRDME             crdme  = ssa->crdme;
  AppCtx            *user = (AppCtx*) ssa->ctx;
  double            *cum_mass  = user->cum_mass;
  int               N      = crdme->geom->num_voxels;
  int               M = crdme->Nspecies;
  double            vol = 0.0,r,sum;
  SSADMSpeciesState s;
  
  PetscFunctionBegin;
  vol = cum_mass[N-1];

  for(int i = 0; i < M; i++) {
    s = species[i];
    for(int j = 0; j < s->num; j++) {
      r = vol*UNI;
      s->loc[j] = bin_search(cum_mass,0,N,r);
      if(s->loc[j] < 0) {
        PetscPrintf(PETSC_COMM_WORLD,"Error in binary search.\n");
        return 1;
      }
    }
  }
  return 0;
}

PetscErrorCode ResetDirac(SpatialSSA ssa, SSADMSpeciesState *species) { 
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  PetscErrorCode       ierr;
  
  ierr = SSADMSetState(ssa,ssa_dm->start);CHKERRQ(ierr);
  
  return 0;
}

PetscErrorCode ResetFromDistribution(SpatialSSA ssa, SSADMSpeciesState *species) {
  PetscErrorCode       ierr;
  AppCtx               *ctx = ssa->ctx;
  PetscFunctionBegin;
  
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,ctx->num);CHKERRQ(ierr);
  
  return 0;
}

PetscErrorCode ComputeMean(double *trials, int ntrials, double *mean) { /* TODO: add these to new module "statistics" */
  double         localmean;
  int            sze;
  
  localmean = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localmean+=trials[i];
  }
  MPI_Allreduce(&localmean,mean,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *mean/=(sze*ntrials);
  return 0;
}

PetscErrorCode ComputeStdDeviation(double *trials, int ntrials, double mean, double *std) {
  double         localstd;
  int            sze;
  
  localstd = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localstd += (mean - trials[i])*(mean - trials[i]);
  }
  MPI_Allreduce(&localstd,std,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *std/=( sze*ntrials - 1);
  *std = PetscSqrtReal(*std/(sze*ntrials));
  return 0;
}

int main(int argc, char **argv) {
  CRDME                crdme; 
  SpatInteractionFn_2D k=kAB,psi=psiAB;
  double               *trials,mean,std;
  int                  ntrials,rank,sze;
  PetscBool            flg,init_unif,init_gaussian;
  SpatialSSA           ssa;
  AppCtx               user;
  double               t1,t2,tf;
  PetscErrorCode       ierr;
  
  PetscFunctionBegin;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);
  
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  unsigned short seed[3] = {rank,0,0};
  
  ierr = CRDMECreate(&crdme);CHKERRQ(ierr);

  if(!crdme) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME object is null\n");
  }  
  if(!crdme->geom) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME geometry object is null\n");
  }
  
  ierr = set_geom(crdme);CHKERRQ(ierr);
  
  //ierr = CRDMEGeomView_vtk(crdme,"test0_geom");CHKERRQ(ierr);
  
  /* set CRDME info */
  
  ierr = CRDMESetNspecies(crdme,3);CHKERRQ(ierr);
  
  ierr = CRDMESetSpeciesName(crdme,0,"A");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,0,.05724);CHKERRQ(ierr);

  ierr = CRDMESetSpeciesName(crdme,1,"B");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,1,0.02864);CHKERRQ(ierr);
  
  ierr = CRDMESetSpeciesName(crdme,2,"C");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,2,0.027528);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"Adding pair potential A and B..\n"); 
  ierr = CRDMEAddPairPotential(crdme,0,1,psi,.045);CHKERRQ(ierr);
  //ierr = CRDMEAddPairPotential(crdme,1,2,psi,.045);CHKERRQ(ierr);
  //ierr = CRDMEAddPairPotential(crdme,1,2,psi,.045);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Adding binding reaction..\n");
  ierr = CRDMEAddPairBinding(crdme,0,1,2,kAB,.045);CHKERRQ(ierr); 
  PetscPrintf(PETSC_COMM_WORLD,"Adding unbinding reaction..\n");
  ierr = CRDMEAddPairUnbinding(crdme,2,0,1,kC,.045);CHKERRQ(ierr); 

  ierr = CRDMEView_ascii(crdme);CHKERRQ(ierr);

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_DM);CHKERRQ(ierr);
  ierr = SSASetCRDME(ssa,crdme);CHKERRQ(ierr);
  
  int num[3]={0,0,1},max[3]={1,1,1};
  int nfound = 3;
  ierr = PetscOptionsGetIntArray(NULL,NULL,"-init_population",num,&nfound,NULL);CHKERRQ(ierr);
  max[0] = PetscMax(num[0],num[1]) + num[2];
  max[1] = PetscMax(num[0],num[1]) + num[2];
  max[2] = PetscMax(num[0],num[1]) + num[2];
  user.num[0] = num[0];
  user.num[1] = num[1];
  user.num[2] = num[2];
  ierr = SSADMSetMaxParticles(ssa,max);CHKERRQ(ierr);     /* TODO: rethink? */
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-init_uniform",&init_unif,&flg);CHKERRQ(ierr); /* set bounds */
  ierr = PetscOptionsGetBool(NULL,NULL,"-init_gaussian",&init_gaussian,&flg);CHKERRQ(ierr); /* set bounds */

  if(init_unif) {
    ierr = PetscMalloc1(crdme->geom->num_voxels,&user.cum_mass);CHKERRQ(ierr);
    user.cum_mass[0] = crdme->geom->mass[0];
    for(int i = 1; i < crdme->geom->num_voxels; i++) { user.cum_mass[i] = user.cum_mass[i-1]+crdme->geom->mass[i]; }
    ierr = SSADMICFunctionSet(ssa,InitFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
    ierr = SSADMResetFunctionSet(ssa,ResetFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
  } else if(init_gaussian) {
    double w,x,y,c;
    double *nodes=crdme->geom->nodes;
    ierr = PetscMalloc1(crdme->geom->num_voxels,&user.cum_mass);CHKERRQ(ierr);
    ierr = PetscOptionsGetReal(NULL,NULL,"-ic_gaussian_width",&w,&flg);CHKERRQ(ierr); /* set bounds */
    if(!flg) w=1.0;
    x=nodes[0];
    y=nodes[1];
    c = 1.0/(2.0*PETSC_PI*w*w);
    user.cum_mass[0] = c*PetscExpReal(-(x*x + y*y)/(2.*w*w))*crdme->geom->mass[0];
    for(int i = 1; i < crdme->geom->num_voxels; i++) { 
      x=nodes[2*i];
      y=nodes[2*i+1];
      user.cum_mass[i] = user.cum_mass[i-1]+c*PetscExpReal(-(x*x + y*y)/(2.*w*w))*crdme->geom->mass[i]; 
    }
    ierr = SSADMICFunctionSet(ssa,InitFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
    ierr = SSADMResetFunctionSet(ssa,ResetFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
  } else {
    ierr = SSADMICFunctionSet(ssa,InitDirac);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
    ierr = SSADMResetFunctionSet(ssa,ResetDirac);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
  }

  ierr = SSASetContext(ssa,&user);CHKERRQ(ierr);
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,num);CHKERRQ(ierr);

  ierr = SSASetSeed(ssa,seed);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  
  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ssa_num_trials",&ntrials,&flg);CHKERRQ(ierr); /* set bounds */
  if(!flg) {
    ntrials = 10000;
    ierr = SSASetNumTrials(ssa,ntrials);CHKERRQ(ierr);
  }
  ierr = PetscMalloc1(ntrials,&trials);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-ssa_final_time",&tf,&flg);CHKERRQ(ierr); /* set bounds */
  if(flg) {
    ierr = SSASetFinalTime(ssa,tf);CHKERRQ(ierr);
  } else {
    ierr = SSASetFinalTime(ssa,500.0);CHKERRQ(ierr);
  }

  ierr = SSASetMonitorContext(ssa,trials);CHKERRQ(ierr);
  z_rng*=rank; /* TODO: function to set seed */
  /* ierr = SSADMStateViewVTK(ssa,"test4_ic_view");CHKERRQ(ierr); */
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,num);CHKERRQ(ierr);
  if(!init_unif && !init_gaussian) {
    SSA_DM *ssa_dm = ssa->data;
    ierr = SSADMStateDuplicate(ssa,&ssa_dm->start);CHKERRQ(ierr);
  }

  /* ierr = SSADMStateViewVTK(ssa,"test4_ic_view");CHKERRQ(ierr); */
  t1   = MPI_Wtime();
  ierr = SSASolve(ssa);CHKERRQ(ierr);
  t2 = MPI_Wtime();
  PetscPrintf(PETSC_COMM_WORLD,"Elapsed time is %f\n", t2 - t1 );
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  if(ntrials*sze > 1) {
    ierr = ComputeMean(trials,ntrials,&mean);
    ierr = ComputeStdDeviation(trials,ntrials,mean,&std);
  } else {
    mean = trials[0];
    std  = 0.0;
  }

  PetscPrintf(PETSC_COMM_WORLD,"P(bound): %1.14f \pm %1.14f\n",mean,2.0*std);

  ierr = CRDMEDestroy(&crdme);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);  
  PetscFunctionReturn(0);
}
PetscErrorCode set_geom(CRDME crdme) {
  FILE           *f;
  int            lvl,N,Nnodes,Nedges;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscOptionsGetInt(NULL,NULL,"-refine_lvl",&lvl,NULL);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Uniform mesh, refinement level %d\n",lvl);
  Nnodes = (PetscPowInt(2,lvl+1)+1)*(PetscPowInt(2,lvl+1)+1);
  
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i",lvl);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Reading mesh from directory %s\n",fname);
  PetscPrintf(PETSC_COMM_WORLD,"Reading row pointer..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Ap",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nnodes+1,&crdme->geom->Ap);CHKERRQ(ierr);
  fread(&N,sizeof(int),1,f);
  fread(crdme->geom->Ap,sizeof(int),Nnodes+1,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading column indices..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Aj",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  fread(&Nedges,sizeof(int),1,f);
  ierr = PetscMalloc1(Nedges,&crdme->geom->Aj);CHKERRQ(ierr);
  fread(crdme->geom->Aj,sizeof(int),Nedges,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading matrix data..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Ax",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nedges,&crdme->geom->Ax);CHKERRQ(ierr);
  fread(crdme->geom->Ax,sizeof(double),Nedges,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading mesh nodes..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/nodes",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(2*Nnodes,&crdme->geom->nodes);CHKERRQ(ierr);
  fread(crdme->geom->nodes,sizeof(double),2*Nnodes,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading lumped mass matrix..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/mass",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nnodes,&crdme->geom->mass);CHKERRQ(ierr);
  fread(crdme->geom->mass,sizeof(double),Nnodes,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"finished reading mesh\n");
  
  crdme->geom->num_voxels = Nnodes;
  crdme->geom->num_edges = Nedges;
  return 0;
}

/*

mpirun -n 1 ./test4  -ssa_num_trials 100 -crdme_compute_pot_interaction_stencil 0 -crdme_compute_binding_stencil 0 -refine_lvl 6 -init_population 1,1,0 -ssa_final_time 10.0 
 mpirun -n 1 ./test4 -crdme_compute_pot_interaction_stencil 0 -crdme_compute_binding_stencil 0 -refine_lvl 5 -init_population 500,500,0 -ssa_num_trials 2 -ssa_final_time .2
 * 
mpirun -n 4 ./test3  -ssa_num_trials 10 -crdme_compute_pot_interaction_stencil 0 -crdme_compute_binding_stencil 0 -refine_lvl 4 -init_population 0,0,10
*/ 
 
