/*
 * test0.c
 *
 *  Created on: Jan 25, 2021
 *      Author: maxheldman
 *      
 *      1. Read binary mesh data generated in Python for triangulation of a uniform mesh from file.
 *      2. View mesh ascii.
 *      3. View mesh VTK.
 *    
 */

#include "ssa.h"
#include "crdme.h"
#include "ssa_dm.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <petscsys.h>

static char help[] = "";

typedef struct {
  double *cum_mass;
} AppCtx;

static inline double psiAB(double x0, double x1, double y0, double y1, void *p) {
  return 100.*PetscExpReal(-( (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1)  ));
}

static inline double kAB(double x0, double x1, double y0, double y1, void *p) {
  double r = (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) ;
  //return r < .87 ? PetscExpReal(24.710396314861363 + -60.*r) : 0.0;
  return 100.*exp(-r);
}

PetscErrorCode set_geom(CRDME crdme);

PetscErrorCode InitDirac(SpatialSSA ssa, SSADMSpeciesState *species) {
  CRDME             crdme  = ssa->crdme;
  double            *nodes = crdme->geom->nodes;
  int               N = crdme->geom->num_voxels;
  int               M = crdme->Nspecies;
  int               idxA,idxB;
  SSADMSpeciesState s;
  
  PetscFunctionBegin;
  
  for (int i = 0; i < N; i++) {
    if(nodes[2*i] == 0.0 && nodes[2*i + 1] == 0.5) { 
      idxA = i;
      break;
    }
  }
  s = species[0];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxA;
  }
  
  for (int i = 0; i < N; i++) {
    if(nodes[2*i] == 0.0 && nodes[2*i + 1] == -0.5) { 
      idxB = i;
      break;
    }
  }
  
  s = species[1];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxB;
  }
  
  return 0;
}

int bin_search(double *arr, int l, int h, double r) {
  
  if(h >= 1) {
    int mid = l + (h - l) / 2;
    if(arr[mid] < r && arr[mid+1] > r) {
      return mid;
    }
    
    if(r > arr[mid+1]) {
      return bin_search(arr,mid+1,h,r);
    }
    
    return bin_search(arr,l,mid,r);

  }
  return -1;
}

PetscErrorCode InitUniform(SpatialSSA ssa, SSADMSpeciesState *species) { //not correct as stated, should pick an element

  CRDME             crdme  = ssa->crdme;
  AppCtx            *user = (AppCtx*) ssa->ctx;
  double            *cum_mass  = user->cum_mass;
  int               N      = crdme->geom->num_voxels;
  int               M = crdme->Nspecies;
  double            vol = 0.0,r,sum;
  SSADMSpeciesState s;
  
  PetscFunctionBegin;
  vol = cum_mass[N-1];
  
  for(int i = 0; i < M; i++) {
    s = species[i];
    for(int j = 0; j < s->max; j++) {
      r = vol*erand48(ssa->seed);
      s->loc[j] = bin_search(cum_mass,0,N,r);
    }
  }
  
  return 0;
}

PetscBool Monitor(SpatialSSA ssa, int sims, void *ctx) {
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  PetscBool            cond;
     

  
  cond = (state->species[1]->num == 0 || state->time > 10.0);
  if(cond) {
    double *trials = (double *) ssa->monitor_ctx;
    trials[sims] = state->time;
  }
  return cond;
}

PetscErrorCode Reset(SpatialSSA ssa, SSADMSpeciesState *species) { 
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  PetscErrorCode       ierr;
  
  ierr = SSADMSetState(ssa,ssa_dm->start);CHKERRQ(ierr);
  
  return 0;
}

PetscErrorCode ComputeMean(double *trials, int ntrials, double *mean) { /* TODO: add these to new module "statistics" */
  double         localmean;
  int            sze;
  
  localmean = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localmean+=trials[i];
  }
  MPI_Allreduce(&localmean,mean,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *mean/=(sze*ntrials);
  return 0;
}

PetscErrorCode ComputeStdDeviation(double *trials, int ntrials, double mean, double *std) {
  double         localstd;
  int            sze;
  
  localstd = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localstd += (mean - trials[i])*(mean - trials[i]);
  }
  MPI_Allreduce(&localstd,std,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *std/=( sze*ntrials - 1);
  *std = PetscSqrtReal(*std/(sze*ntrials));
  return 0;
}

int main(int argc, char **argv) {
  CRDME                crdme; 
  SpatInteractionFn_2D k=kAB,psi=psiAB;
  double               *trials,mean,std;
  int                  ntrials,rank;
  PetscBool            flg;
  SpatialSSA           ssa;
  AppCtx               user;
  PetscErrorCode       ierr;
  
  PetscFunctionBegin;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);
  
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  unsigned short seed[3] = {rank,0,0};
  
  ierr = CRDMECreate(&crdme);CHKERRQ(ierr);
  
  if(!crdme) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME object is null\n");
  }  
  if(!crdme->geom) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME geometry object is null\n");
  }
  
  ierr = set_geom(crdme);CHKERRQ(ierr);
  
  //ierr = CRDMEGeomView_vtk(crdme,"test0_geom");CHKERRQ(ierr);
  
  /* set CRDME info */
  
  ierr = CRDMESetNspecies(crdme,2);CHKERRQ(ierr);
  
  ierr = CRDMESetSpeciesName(crdme,0,"A");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,0,1.0);CHKERRQ(ierr);

  ierr = CRDMESetSpeciesName(crdme,1,"B");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,1,1.0);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"Adding pair potential..\n"); 
  ierr = CRDMEAddPairPotential(crdme,0,1,psi,0.5);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Adding annihilation reaction..\n");
  ierr = CRDMEAddPairAnnihilation(crdme,0,1,k,1.0);CHKERRQ(ierr); 
  
  ierr = CRDMEView_ascii(crdme);CHKERRQ(ierr);

  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_DM);CHKERRQ(ierr);
  ierr = SSASetCRDME(ssa,crdme);CHKERRQ(ierr);

  ierr = PetscMalloc1(crdme->geom->num_voxels,&user.cum_mass);CHKERRQ(ierr);
  user.cum_mass[0] = crdme->geom->mass[0];
  for(int i = 1; i < crdme->geom->num_voxels; i++) { user.cum_mass[i] = user.cum_mass[i-1]+crdme->geom->mass[i]; }

  ierr = SSASetContext(ssa,&user);CHKERRQ(ierr);
 
  int num[2]={1,1};
  int nfound = 2;
  ierr = PetscOptionsGetIntArray(NULL,NULL,"-init_population",num,&nfound,NULL);CHKERRQ(ierr);

  ierr = SSADMSetMaxParticles(ssa,num);CHKERRQ(ierr);     /* TODO: rethink? */
  ierr = SSADMICFunctionSet(ssa,InitDirac);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,num);CHKERRQ(ierr);

  ierr = SSASetSeed(ssa,seed);CHKERRQ(ierr);
  ierr = SSASetFinalTime(ssa,10.0);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  ierr = SSADMResetFunctionSet(ssa,Reset);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
  
  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ssa_num_trials",&ntrials,&flg);CHKERRQ(ierr); /* set bounds */
  if(!flg) {
    ntrials = 10000;
    ierr = SSASetNumTrials(ssa,ntrials);CHKERRQ(ierr);
  }
  ierr = PetscMalloc1(ntrials,&trials);CHKERRQ(ierr);
  ierr = SSASetMonitorContext(ssa,trials);CHKERRQ(ierr);

  /* ierr = SSADMStateViewVTK(ssa,"test0_ic_view");CHKERRQ(ierr); */
  ierr = SSASolve(ssa);CHKERRQ(ierr);
  ierr = ComputeMean(trials,ntrials,&mean);
  ierr = ComputeStdDeviation(trials,ntrials,mean,&std);

  PetscPrintf(PETSC_COMM_WORLD,"mean reaction time %1.14f \pm %1.14f\n",mean,2.0*std);

  ierr = CRDMEDestroy(&crdme);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode set_geom(CRDME crdme) {
  FILE           *f;
  int            lvl,N,Nnodes,Nedges;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscOptionsGetInt(NULL,NULL,"-refine_lvl",&lvl,NULL);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Uniform mesh, refinement level %d\n",lvl);
  Nnodes = (PetscPowInt(2,lvl+1)+1)*(PetscPowInt(2,lvl+1)+1);
  
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i",lvl);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Reading mesh from directory %s\n",fname);
  PetscPrintf(PETSC_COMM_WORLD,"Reading row pointer..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Ap",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nnodes+1,&crdme->geom->Ap);CHKERRQ(ierr);
  fread(&N,sizeof(int),1,f);
  fread(crdme->geom->Ap,sizeof(int),Nnodes+1,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading column indices..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Aj",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  fread(&Nedges,sizeof(int),1,f);
  ierr = PetscMalloc1(Nedges,&crdme->geom->Aj);CHKERRQ(ierr);
  fread(crdme->geom->Aj,sizeof(int),Nedges,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading matrix data..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/Ax",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nedges,&crdme->geom->Ax);CHKERRQ(ierr);
  fread(crdme->geom->Ax,sizeof(double),Nedges,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading mesh nodes..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/nodes",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(2*Nnodes,&crdme->geom->nodes);CHKERRQ(ierr);
  fread(crdme->geom->nodes,sizeof(double),2*Nnodes,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"Reading lumped mass matrix..\n");
  ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i/mass",lvl);CHKERRQ(ierr);
  f = fopen(fname, "rb");
  ierr = PetscMalloc1(Nnodes,&crdme->geom->mass);CHKERRQ(ierr);
  fread(crdme->geom->mass,sizeof(double),Nnodes,f);
  fclose(f);
  PetscPrintf(PETSC_COMM_WORLD,"finished reading mesh\n");
  
  crdme->geom->num_voxels = Nnodes;
  crdme->geom->num_edges = Nedges;
  return 0;
}

/*
  mpirun -n 4 ./test1  -ssa_num_trials 100 -crdme_compute_pot_interaction_stencil 1 -crdme_compute_annihilation_stencil 0 -refine_lvl 5
*/ 
 
