/*
 * test0.c
 *
 *  Created on: Jan 25, 2021
 *      Author: maxheldman
 *      
 *      1. Read binary mesh data generated in Python for triangulation of a uniform mesh from file.
 *      2. View mesh ascii.
 *      3. View mesh VTK.
 *    
 */

#include "ssa.h"
#include "crdme.h"
#include "ssa_dm.h"
#include <crdmegeometry.h>
#include <crdmegeomgcon.h>
#include <crdmegeombox.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <petscsys.h>
#include <rng.h>


static char help[] = "";

typedef struct {
	double *trials;
	int    *chkpts;
	int    n_chk_pts;
} monitor_ctx;

typedef struct {
  double *cum_mass_A;
  double *cum_mass_B;
  int    num[2];
  double h;
  double rmin;
  double rsqr;
  double alpha;
  double cutoff;
  double frce;
  double shift;
  PetscBool LJ;
} AppCtx;

int    count = 0;
double inc   = .0001;

static inline double sqrf(double x) {
  return x*x;
}

static inline double psiAB(double r, void *p) {
  AppCtx *ctx = (AppCtx *)p;
  //return r < .00001 ? 10.*(PetscSqrtReal(r) - .01)*(PetscSqrtReal(r)  - .01) : 0.0;
  if(r < ctx->cutoff) {
    double a;
    if(r > 1e-13) {
      a = ctx->rsqr/r;
      //a = PetscPowRealInt(.0001/r,3);
    } else {
      //a = PetscPowRealInt(.0001/ctx->h,3);
      a = ctx->rsqr/ctx->h;
    }
    a = a*a*a;
    return ctx->frce*( a*(a - 1.0) + ctx->shift);
  }
  return 0.0;
}

static inline double kABdoi(double r, void *p) {
  return r < .0001 ? 1e9 : 0.0;
}

static inline double kAB(double r, void *p) {
  return r < .01 ? 23239.748675269988*PetscExpReal(-2400.0*r) : 0.0;
}

//*PetscExpReal(-500000.*r) 
PetscBool Monitor(SpatialSSA ssa, int sims, void *ctx) {
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  SSADMState           state = ssa_dm->state;
  PetscBool            cond;
  monitor_ctx          *c = (monitor_ctx *) ssa->monitor_ctx;
//
//  if(state->time > count*inc) {
//   // SSADMStateViewVTK(ssa,"test2_view");
//    count++;
//  }
  	cond = (state->time > .1 || state->species[0]->num == 0);
  	if(state->time >= ssa->checkpoints[ssa->next_checkpoint]) {
  		c->chkpts[c->n_chk_pts*sims + ssa->next_checkpoint] = state->species[0]->num;
  	}

//    cond = (state->time > .01 || state->species[0]->num==0);
//    printf("%d %f\n",sims,state->time);
//    printf("  %d %d\n",state->species[0]->num,state->species[1]->num);
//    printf("  loc a: %d %d\n",state->species[0]->loc[0],state->species[0]->pop[state->species[0]->loc[0]]);
//    printf("  loc b: %d %d\n",state->species[1]->loc[0],state->species[1]->pop[state->species[1]->loc[0]]);
//    printf("  rx rate %1.14f\n",state->species[0]->pair_ratesum[0]);
//    printf("  diff a %1.14f\n",state->species[0]->hr[0].voxel_rate);
//    printf("  diff b %1.14f\n",state->species[1]->hr[0].voxel_rate);
//

  if(cond) {
    c->trials[sims] = state->time;
  }
  return cond;
}

int bin_search(double *arr, int l, int h, double r) {
  
  if(r < arr[l]) {
    return l;
  }
  if(h - l >= 1) {
    int mid = l + (h - l) / 2;
    if(arr[mid] < r && arr[mid+1] > r) {
      return mid+1;
    }
    
    if(r > arr[mid]) {
      return bin_search(arr,mid+1,h,r);
    }
    
    return bin_search(arr,l,mid,r);
  }
  return -1;
}

PetscErrorCode InitDirac(SpatialSSA ssa, SSADMSpeciesState *species) {
  CRDME             crdme  = ssa->crdme;
  CRDMEGeometry     geom   = crdme->geom;
  int               N = crdme->geom->N;
  int               M = crdme->Nspecies;
  int               idxC;
  double            x[2];
  PetscErrorCode    ierr;
  SSADMSpeciesState s;
  
  
  for (int i = 0; i < N; i++) {
    ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
    if(x[0] == 0.0 && x[1] == 0.0) { 
      idxC = i;
      break;
    }
  }
  
  s = species[1];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxC;
  }
  
  s = species[0];
  for(int j = 0; j < s->max; j++) {
     s->loc[j] = idxC;
  }
  
  return 0;
}

PetscErrorCode InitFromDistribution(SpatialSSA ssa, SSADMSpeciesState *species) { //not correct as stated, should pick an element

  CRDME             crdme  = ssa->crdme;
  AppCtx            *user = (AppCtx*) ssa->ctx;
  double            *cum_mass;
  int               N      = crdme->geom->N;
  int               M = crdme->Nspecies;
  double            vol = 0.0,r,sum;
  double            x[2],xb[2];
  SSADMSpeciesState s;
  
  PetscFunctionBegin;

	for(int i = 0; i < M; i++) {
		s = species[i];
		if(i == 0) {
			cum_mass=user->cum_mass_A;
		} else {
			cum_mass=user->cum_mass_B;
		}
	  vol = cum_mass[N-1];
		for(int j = 0; j < s->num; j++) {
			r = vol*UNI;
			s->loc[j] = bin_search(cum_mass,0,N,r);
			if(s->loc[j] < 0) {
				PetscPrintf(PETSC_COMM_SELF,"Error in binary search.\n");
				return 1;
			}
			if(i == 1) {

			}
		}
  }

  return 0;
}

PetscErrorCode ResetDirac(SpatialSSA ssa, SSADMSpeciesState *species) { 
  SSA_DM               *ssa_dm = (SSA_DM*) ssa->data;
  PetscErrorCode       ierr;
  
  ierr = SSADMSetState(ssa,ssa_dm->start);CHKERRQ(ierr);
  
  return 0;
}

PetscErrorCode ResetFromDistribution(SpatialSSA ssa, SSADMSpeciesState *species) {
  PetscErrorCode       ierr;
  AppCtx               *ctx = ssa->ctx;
  PetscFunctionBegin;
  
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,ctx->num);CHKERRQ(ierr);
  
  return 0;
}

PetscErrorCode ComputeMean(double *trials, int ntrials, double *mean) { /* TODO: add these to new module "statistics" */
  double         localmean;
  int            sze;
  
  localmean = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localmean+=trials[i];
  }
  MPI_Allreduce(&localmean,mean,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *mean/=(sze*ntrials);
  return 0;
}

PetscErrorCode ComputeStdDeviation(double *trials, int ntrials, double mean, double *std) {
  double         localstd;
  int            sze;
  
  localstd = 0.0;
  for(int i = 0; i < ntrials; i++) {
    localstd += (mean - trials[i])*(mean - trials[i]);
  }
  MPI_Allreduce(&localstd,std,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  *std/=( sze*ntrials - 1);
  *std = PetscSqrtReal(*std/(sze*ntrials));
  return 0;
}


int main(int argc, char **argv) {
  CRDME                crdme;
  CRDMEGeometry        geom;
  SpatInteractionFn_2D psi=psiAB;
  double               *trials,mean,std;
  int                  ntrials,rank,sze,lvl;
  PetscBool            flg=0,init_unif=0,init_gaussian=0,square=0,gk=0,dk=0;
  SpatialSSA           ssa;
  AppCtx               user;
  double               t1,t2,tf,r=.001,D=10.0,kap=1.e9;
  char                 fname[PETSC_MAX_PATH_LEN];
  monitor_ctx          m_ctx;
  PetscErrorCode       ierr;
  
  PetscFunctionBegin;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsView(NULL,PETSC_VIEWER_STDOUT_(PETSC_COMM_WORLD));CHKERRQ(ierr);
  
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  unsigned short seed[3] = {rank,0,0};
  
  ierr = CRDMECreate(&crdme);CHKERRQ(ierr);

  ierr = CRDMESetContext(crdme,&user);CHKERRQ(ierr);
  ierr = CRDMEGeometrySetDimension(crdme->geom,2);CHKERRQ(ierr);
  if(!crdme) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME object is null\n");
  }  
  if(!crdme->geom) {
    PetscPrintf(PETSC_COMM_WORLD,"Error: CRDME geometry object is null\n");
  }
  ierr = PetscOptionsGetBool(NULL,NULL,"-square",&square,&flg);CHKERRQ(ierr); /* set bounds */
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-refine_lvl",&lvl,NULL);CHKERRQ(ierr);  
  if(square) {
    PetscBool square_use_box=PETSC_FALSE;
    ierr = PetscOptionsGetBool(NULL,NULL,"-square_use_box",&square_use_box,&flg);CHKERRQ(ierr); /* set bounds */
    if(square_use_box) {
      PetscBool p[2] = {PETSC_FALSE, PETSC_FALSE};
      ierr = CRDMEGeometrySetType(crdme->geom,GEOMETRY_TYPE_BOX);CHKERRQ(ierr);
      ierr = CRDMEGeomBoxSetDomain(crdme->geom,-1.0,1.0,-1.0,1.0);CHKERRQ(ierr);
      ierr = CRDMEGeomBoxSetPeriodicity(crdme->geom,p);CHKERRQ(ierr);
      int m = PetscPowInt(2,lvl+1)+1;
      ierr = CRDMEGeomBoxSetGrid(crdme->geom,m,m);CHKERRQ(ierr);
    } else {
      ierr = CRDMEGeometrySetType(crdme->geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
      ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/uniform_mesh/mesh%i",lvl);CHKERRQ(ierr);
      ierr = CRDMEGeomGCONSetFromFolder(crdme->geom,fname);CHKERRQ(ierr);
    }
    //D = 40./(.01*PETSC_PI);
    D   = 1000.0;
    r = .01;
  } else {
    ierr = CRDMEGeometrySetType(crdme->geom,GEOMETRY_TYPE_GCON);CHKERRQ(ierr);
    ierr = PetscSNPrintf(fname,sizeof(fname),"./mesh_data_binary/circle/mesh%i",lvl);CHKERRQ(ierr);
    ierr = CRDMEGeomGCONSetFromFolder(crdme->geom,fname);CHKERRQ(ierr);
  }
  geom = crdme->geom;
  //ierr = CRDMEGeomView_vtk(crdme,"circle_geom");CHKERRQ(ierr);
  /* set CRDME info */
  ierr = PetscOptionsGetReal(NULL,NULL,"-reaction_rate",&kap,&flg);CHKERRQ(ierr); /* set bounds */
  ierr = PetscOptionsGetReal(NULL,NULL,"-diffusion_rate",&D,&flg);CHKERRQ(ierr); /* set bounds */
  ierr = PetscOptionsGetReal(NULL,NULL,"-reaction_radius",&r,&flg);CHKERRQ(ierr); /* set bounds */

  ierr = CRDMESetNspecies(crdme,2);CHKERRQ(ierr);

  ierr = CRDMESetSpeciesName(crdme,0,"A");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,0,D);CHKERRQ(ierr);

  ierr = CRDMESetSpeciesName(crdme,1,"B");CHKERRQ(ierr);
  ierr = CRDMESpeciesSetDiffusionConstant(crdme,1,D);CHKERRQ(ierr);
  int num[2]={1,1},max[2]={1,1};
  int nfound = 2;
  ierr = PetscOptionsGetIntArray(NULL,NULL,"-init_population",num,&nfound,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-gaussian_kernel",&gk,&flg);CHKERRQ(ierr); /* set bounds */
  ierr = PetscOptionsGetBool(NULL,NULL,"-doi_kernel",&dk,&flg);CHKERRQ(ierr); /* set bounds */

  PetscPrintf(PETSC_COMM_WORLD,"Adding binding reaction..\n");
  if(gk) {
    ierr = CRDMEAddPairAnnihilation(crdme,0,1,kAB,r);CHKERRQ(ierr); 
  } else if(dk) {
    ierr = CRDMEAddPairAnnihilation(crdme,0,1,kABdoi,r);CHKERRQ(ierr); 
  } else {
  	PetscBool stencil = PETSC_TRUE;
  	PetscBool standard     = PETSC_TRUE;
  	PetscBool no_stencil = PETSC_FALSE;
    ierr = PetscOptionsGetBool(NULL,NULL,"-standard",&standard,&flg);CHKERRQ(ierr); /* set bounds */
    ierr = PetscOptionsGetBool(NULL,NULL,"-stencil",&stencil,&flg);CHKERRQ(ierr); /* set bounds */
    ierr = PetscOptionsGetBool(NULL,NULL,"-no_stencil",&no_stencil,&flg);CHKERRQ(ierr); /* set bounds */
    if(no_stencil) {
    	if(standard) {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_STANDARD_SQUARE,DOI,0);CHKERRQ(ierr);
    	} else {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_LUMP,DOI,0);CHKERRQ(ierr);
    	}
    } else {
      if(stencil && standard) {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_STANDARD_SQUARE,STENCIL,0);CHKERRQ(ierr);
      } else if(stencil) {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_LUMP,STENCIL,0);CHKERRQ(ierr);
      } else if(standard) {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_STANDARD_SQUARE,DOI_REJECT,0);CHKERRQ(ierr);
      } else {
      	ierr = CRDMEAddPairAnnihilationDoi(crdme,0,1,kap,r,DOI_LUMP,DOI_REJECT,0);CHKERRQ(ierr);
      }
    }
  }

  user.h = geom->hmin*geom->hmin/64.0;
  user.frce = 10.0;
  ierr = PetscOptionsGetReal(NULL,NULL,"-pair_potential_force_constant",&user.frce,NULL);CHKERRQ(ierr); /* set bounds */
  PetscBool use_potential = PETSC_FALSE;
  ierr = PetscOptionsGetBool(NULL,NULL,"-use_potential",&use_potential,&flg);CHKERRQ(ierr); /* set bounds */
  if(use_potential) {
    PetscPrintf(PETSC_COMM_WORLD,"Adding pair potential..\n");
    user.LJ = PETSC_FALSE;
    ierr = CRDMEAddPairPotential(crdme,0,1,psi,2.5*r);CHKERRQ(ierr);
    user.cutoff=(2.5*r)*(2.5*r);
    user.LJ = PETSC_TRUE;
    user.rsqr = r*r;
    user.shift = PetscPowRealInt(user.rsqr/(user.cutoff),12) - PetscPowRealInt(user.rsqr/(user.cutoff),6);
  }

  //PetscPrintf(PETSC_COMM_WORLD,"Adding binding reaction..\n");
  //PetscPrintf(PETSC_COMM_WORLD,"Adding unbinding reaction..\n");
  ierr = CRDMEView_ascii(crdme);CHKERRQ(ierr);
  
  ierr = SSACreate(&ssa);CHKERRQ(ierr);
  ierr = SSASetType(ssa,SSA_TYPE_DM);CHKERRQ(ierr);
  ierr = SSASetCRDME(ssa,crdme);CHKERRQ(ierr);

  max[0] = num[0];
  max[1] = num[1];
  user.num[0] = num[0];
  user.num[1] = num[1];
  ierr = SSADMSetMaxParticles(ssa,max);CHKERRQ(ierr);     /* TODO: rethink? */
//  ierr = PetscOptionsGetBool(NULL,NULL,"-init_uniform",&init_unif,&flg);CHKERRQ(ierr); /* set bounds */
//  ierr = PetscOptionsGetBool(NULL,NULL,"-init_gaussian",&init_gaussian,&flg);CHKERRQ(ierr); /* set bounds */
//
//  if(init_unif) { /* TODO: init from distribution should be built in */
//    double area;
//    ierr = PetscMalloc1(geom->N,&user.cum_mass);CHKERRQ(ierr);
//    ierr = CRDMEGeometryNodeGetVoxelArea(geom,0,&area);CHKERRQ(ierr);
//    user.cum_mass[0] = area;
//    for(int i = 1; i < geom->N; i++) {
//      ierr = CRDMEGeometryNodeGetVoxelArea(geom,i,&area);CHKERRQ(ierr);
//      user.cum_mass[i] = user.cum_mass[i-1]+area;
//    }
//    ierr = SSADMICFunctionSet(ssa,InitFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
//    ierr = SSADMResetFunctionSet(ssa,ResetFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
//  } else if(init_gaussian) {
//    double w,c,area,x[2];
//    ierr = PetscMalloc1(geom->N,&user.cum_mass);CHKERRQ(ierr);
//    ierr = PetscOptionsGetReal(NULL,NULL,"-ic_gaussian_width",&w,&flg);CHKERRQ(ierr); /* set bounds */
//    if(!flg) w=1.0;
//    c = 1.0/(2.0*PETSC_PI*w*w);
//    ierr = CRDMEGeometryNodeGetCoordinates(geom,0,x);CHKERRQ(ierr);
//    ierr = CRDMEGeometryNodeGetVoxelArea(geom,0,&area);CHKERRQ(ierr);
//    user.cum_mass[0] = PetscExpReal(-10.*(sqrf(PetscSinReal(.5*PETSC_PI*x[0])) + sqrf(PetscSinReal(.5*PETSC_PI*x[1]))))*area;//c*PetscExpReal(-(x[0]*x[0] + x[1]*x[1])/(2.*w*w))*area;
//    for(int i = 1; i < geom->N; i++) {
//      ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
//      ierr = CRDMEGeometryNodeGetVoxelArea(geom,i,&area);CHKERRQ(ierr);
//      user.cum_mass[i] = user.cum_mass[i-1]+ PetscExpReal(-(sqrf(PetscSinReal(.5*PETSC_PI*x[0])) + sqrf(PetscSinReal(.5*PETSC_PI*x[1]))))*area;//c*PetscExpReal(-(x[0]*x[0] + x[1]*x[1])/(2.*w*w))*area;
//    }
//    ierr = SSADMICFunctionSet(ssa,InitFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
//    ierr = SSADMResetFunctionSet(ssa,ResetFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
//  } else {
//    ierr = SSADMICFunctionSet(ssa,InitDirac);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
//    ierr = SSADMResetFunctionSet(ssa,ResetDirac);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */
//  }

  double w,c=.2,area,x[2];
  ierr = PetscMalloc1(geom->N,&user.cum_mass_A);CHKERRQ(ierr);
  ierr = PetscMalloc1(geom->N,&user.cum_mass_B);CHKERRQ(ierr);
	ierr = CRDMEGeometryNodeGetCoordinates(geom,0,x);CHKERRQ(ierr);
	ierr = CRDMEGeometryNodeGetVoxelArea(geom,0,&area);CHKERRQ(ierr);
	user.cum_mass_A[0] = PetscExpReal(-(sqrf(x[0]-.05) + sqrf(x[1]-.05))/(2.*sqrf(.005)))*area;//c*PetscExpReal(-(x[0]*x[0] + x[1]*x[1])/(2.*w*w))*area;
	user.cum_mass_B[0] = PetscExpReal(-(sqrf(x[0]+.05) + sqrf(x[1]+.05))/(2.*sqrf(.005)))*area;//c*PetscExpReal(-(x[0]*x[0] + x[1]*x[1])/(2.*w*w))*area;
	for(int i = 1; i < geom->N; i++) {
		ierr = CRDMEGeometryNodeGetCoordinates(geom,i,x);CHKERRQ(ierr);
		ierr = CRDMEGeometryNodeGetVoxelArea(geom,i,&area);CHKERRQ(ierr);
		user.cum_mass_A[i] = user.cum_mass_A[i-1]+  PetscExpReal(-(sqrf(x[0]-.5) + sqrf(x[1]-.5))/(2.*sqrf(.1)))*area;//c*PetscExpReal(-(x[0]*x[0] + x[1]*x[1])/(2.*w*w))*area;
		user.cum_mass_B[i] = user.cum_mass_B[i-1]+  PetscExpReal(-(sqrf(x[0]+.5) + sqrf(x[1]+.5))/(2.*sqrf(.1)))*area;
	}
	ierr = SSADMICFunctionSet(ssa,InitFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAICFunctionSet */
	ierr = SSADMResetFunctionSet(ssa,ResetFromDistribution);CHKERRQ(ierr); /* TODO: this should be SSAResetFunctionSet */

  int n_chk_pts;
  ierr = PetscOptionsGetInt(NULL,NULL,"-ssa_num_checkpoints",&n_chk_pts,&flg);CHKERRQ(ierr); /* set bounds */
  if(!flg) {
    n_chk_pts = 100;
  }

  double chkpt[n_chk_pts];
  double h = 1.0/(n_chk_pts - 1);
  for(int i = 0; i < n_chk_pts; i++) {
  	chkpt[i] = h*i;
  	chkpt[i] = PetscPowRealInt(chkpt[i],5);
  	chkpt[i] /= 10;
  }
//  for(int i = 0; i < n_chk_pts; i++) {
//  	PetscPrintf(PETSC_COMM_WORLD,"%1.15f ",chkpt[i]);
//  }
  PetscPrintf(PETSC_COMM_WORLD,"\n ");
  ierr = SSASetCheckpoints(ssa,chkpt);CHKERRQ(ierr);

  ierr = SSASetContext(ssa,&user);CHKERRQ(ierr);
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,num);CHKERRQ(ierr);

  ierr = SSASetSeed(ssa,seed);CHKERRQ(ierr);
  ierr = SSASetMonitor(ssa,Monitor);CHKERRQ(ierr);
  
  ierr = SSASetFromOptions(ssa);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ssa_num_trials",&ntrials,&flg);CHKERRQ(ierr); /* set bounds */
  if(!flg) {
    ntrials = 10000;
    ierr = SSASetNumTrials(ssa,ntrials);CHKERRQ(ierr);
  }
  ierr = PetscMalloc1(n_chk_pts*ntrials,&m_ctx.chkpts);CHKERRQ(ierr);
  ierr = PetscMalloc1(ntrials,&m_ctx.trials);CHKERRQ(ierr);
  m_ctx.n_chk_pts = n_chk_pts;
  ierr = PetscArrayzero(m_ctx.chkpts,n_chk_pts*ntrials);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-ssa_final_time",&tf,&flg);CHKERRQ(ierr); /* set bounds */
  if(flg) {
    ierr = SSASetFinalTime(ssa,tf);CHKERRQ(ierr);
  } else {
    ierr = SSASetFinalTime(ssa,500.0);CHKERRQ(ierr);
  }

  ierr = SSASetMonitorContext(ssa,&m_ctx);CHKERRQ(ierr);
  z_rng*=rank; /* TODO: function to set seed */
  ierr = SSADMStateInitialize(ssa,SSA_IC_CUSTOM,num);CHKERRQ(ierr);
  if(!init_unif && !init_gaussian) {
    SSA_DM *ssa_dm = ssa->data;
    ierr = SSADMStateDuplicate(ssa,&ssa_dm->start);CHKERRQ(ierr);
  }

  /* ierr = SSADMStateViewVTK(ssa,"test4_ic_view");CHKERRQ(ierr); */
  t1   = MPI_Wtime();
  ierr = SSASolve(ssa);CHKERRQ(ierr);
  t2 = MPI_Wtime();
  PetscPrintf(PETSC_COMM_SELF,"process %d simulations finished\n",rank);
  PetscPrintf(PETSC_COMM_SELF,"Elapsed time is %f\n", t2 - t1 );
  MPI_Comm_size(PETSC_COMM_WORLD, &sze);
  trials = m_ctx.trials;
  if(ntrials*sze > 1) {
    ierr = ComputeMean(trials,ntrials,&mean);
    ierr = ComputeStdDeviation(trials,ntrials,mean,&std);
  } else {
    mean = trials[0];
    std  = 0.0;
  }
//	for(int i = 0; i < ntrials; i++) {
//		for(int j = 0; j < n_chk_pts; j++) {
//			PetscPrintf(PETSC_COMM_WORLD,"%i ",m_ctx.chkpts[i*n_chk_pts + j]);
//		}
//		PetscPrintf(PETSC_COMM_WORLD,"\n");
//	}

//  {
//  	FILE *f;
//        ierr = PetscSNPrintf(fname,sizeof(fname),"./test5_output/circle/mesh%i/init_pop_%i/rx_times/rank%i",lvl,num[0],rank);CHKERRQ(ierr);
//  	f = fopen(fname, "wb");
//  	fwrite(trials,sizeof(double),ntrials,f);
//  	fclose(f);
//
//    ierr = PetscSNPrintf(fname,sizeof(fname),"./test5_output/circle/mesh%i/init_pop_%i/checkpoints/rank%i",lvl,num[0],rank);CHKERRQ(ierr);
//  	f = fopen(fname, "wb");
//  	fwrite(m_ctx.chkpts,sizeof(int),n_chk_pts*ntrials,f);
//  	fclose(f);
//  }



  PetscPrintf(PETSC_COMM_WORLD,"reaction time: %1.14f \pm %1.14f\n",mean,2.0*std);

  ierr = CRDMEDestroy(&crdme);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);  
  PetscFunctionReturn(0);
}

/*


mpirun -n 1 ./test5  -init_population 1,1  -ssa_num_trials 1000     \
                     -square -reaction_radius .04 -diffusion_rate 1263.0 \
                     -reaction_rate 1e9 -pair_potential_force_constant 1.0  \
                     -crdme_doi_quad_rtol 1e-5 -crdme_doi_quad_atol 1e-5 -stencil 0 -standard 0 \
                     -refine_lvl 6 -no_stencil -use_potential

mpirun -n 16 ./test5 -crdme_compute_pot_interaction_stencil 0 -init_population 1,1 -ssa_num_trials 5000 -init_uniform -refine_lvl 2
 
mpirun -n 4 ./test5 -init_population 1,1  -init_uniform  -ssa_num_trials 1000 -reaction_radius .01 -diffusion_rate 1000.0 -square  \
                      -crdme_doi_type 2 -crdme_compute_annihilation_stencil 0 -crdme_compute_pot_interaction_stencil 0  \
                      -crdme_doi_quad_scheme1 0 -crdme_doi_quad_scheme2 0 -crdme_doi_min_area 1e-6 -crdme_doi_max_area 1e-3  \
                      -crdme_doi_quad_rtol 1e-5 -crdme_doi_quad_atol 1e-10 \
                      -refine_lvl 5
                      
qsub -pe mpi_28_tasks_per_node 28 -V -b y -o test5-doi-large-r.qlog /usr3/graduate/heldmanm/petsc/arch-linux-c-debug/bin/mpirun -n 28 ./test5 -init_population 1,1  -init_uniform  -ssa_num_trials 100000 -reaction_radius .01 -diffusion_rate 1.0   \
                      -crdme_doi_type 1 -crdme_compute_annihilation_stencil 0 -crdme_compute_pot_interaction_stencil 1    -square \
                      -reaction_radius .01 -diffusion_rate 1000.0 -reaction_rate 1e9 -pair_potential_force_constant 10.0\
                      -refine_lvl 2

mpirun -n 4 ./test5  -init_population 1000,1000  -ssa_num_trials 100  -init_uniform \
                       -square \
                      -reaction_radius .1 -diffusion_rate 1263.0 -reaction_rate 1e9 -pair_potential_force_constant 10.0\
                      -crdme_doi_quad_rtol 1e-8 -crdme_doi_quad_atol 1e-10 \
                      -refine_lvl 3
                      
mpirun -n 1 ./test5  -init_population 1,1  -ssa_num_trials 100000  -init_gaussian -ic_gaussian_width .35  \
                     -crdme_doi_type 2 -crdme_compute_pot_interaction_stencil 0   -square \
                      -reaction_radius .35 -diffusion_rate 1263.0 -reaction_rate 1e9 -pair_potential_force_constant 10.0  \
                      -crdme_doi_quad_rtol 1e-8 -crdme_doi_quad_atol 1e-12 \
                      -square -refine_lvl 2

qsub -pe mpi_28_tasks_per_node 28 -V -b y -o LJ-square-unif.qlog  -l h_rt=24:00:00 /usr3/graduate/heldmanm/petsc/arch-linux-c-debug/bin/mpirun -n 2 ./test5   -init_population 100,100 -ssa_num_trials 10000  -init_uniform  \
                     -crdme_doi_type 2  -square -square_use_box \
                      -reaction_radius .04 -diffusion_rate 1000.0 -reaction_rate 1e9 -pair_potential_force_constant 1.0  \

                      -crdme_doi_quad_rtol 1e-8 -crdme_doi_quad_atol 1e-10 \
                      -refine_lvl 2
 
  -init_gaussian -ic_gaussian_width .01 -gaussian_kernel -doi_kernel 

mpirun -n 4 ./test5 -crdme_compute_pot_interaction_stencil 0 -init_population 1000,1 -ssa_num_trials 1000 -init_uniform -crdme_compute_annihilation_stencil 0 -crdme_doi_type 0 -crdme_doi_min_area 1e-6 -crdme_doi_max_area 1e-3 -crdme_doi_quad_scheme1 0 -crdme_doi_quad_scheme2 0 -crdme_doi_type 2 -crdme_doi_quad_rtol 1e-5 -refine_lvl 0
*/ 
